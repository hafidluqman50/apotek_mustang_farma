-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Feb 22, 2023 at 05:49 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotek_mustang_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile_instansi`
--

CREATE TABLE `profile_instansi` (
  `id_profile_instansi` int NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `nomor_telepon_instansi` varchar(20) NOT NULL,
  `logo_instansi` text NOT NULL,
  `background_instansi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_instansi`
--

INSERT INTO `profile_instansi` (`id_profile_instansi`, `nama_instansi`, `alamat_instansi`, `nomor_telepon_instansi`, `logo_instansi`, `background_instansi`) VALUES
(1, 'Apotek Klinik88', 'Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang', '-', 'logo-klinik88.png', 'bg-klinik88.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  ADD PRIMARY KEY (`id_profile_instansi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  MODIFY `id_profile_instansi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
