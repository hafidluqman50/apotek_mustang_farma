<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/apotek/get-token',['uses' => 'ApiController@getToken']);

Route::group(['prefix'=>'apotek','middleware' => 'check.token'],function(){
    Route::get('/test',function() {
        $data = ['status' => true, 'message' => 'Berhasil Akses API'];

        return response()->json($data,200);
    });

    Route::post('/daftar-pasien',['uses' => 'ApiController@daftarPasien']);
    Route::post('/ambil-antrean',['uses' => 'ApiController@ambilAntrean']);
    Route::post('/status-antrean',['uses' => 'ApiController@statusAntrean']);
    Route::post('/batal-antre',['uses' => 'ApiController@batalAntre']);
});
