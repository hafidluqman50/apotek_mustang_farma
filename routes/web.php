<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/tes', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api/bpjs'],function() {
    Route::get('/spesialis',['as' => 'api-test','uses' => 'ApiController@index']);
    Route::get('/list-pendaftaran',['as' => 'api-test','uses' => 'ApiController@listPendaftaran']);
    Route::get('/get-peserta',['uses' => 'ApiController@getPeserta']);
    Route::get('/get-peserta-kd-provider/{no_kartu}',['uses' => 'ApiController@getPesertaKdProvider']);
    Route::get('/get-peserta-jenis-kartu/{jenis}/{no}',['uses' => 'ApiController@getPesertaByJenisKartu']);
});

Route::get('/coba/{id}',function($id){
	$db = App\Models\ResepModel::pasienRekamMedis($id);

	foreach ($db as $key => $value) {
		echo 'Nama Pasien '.$value->nama_pasien.'</br>';
	}
});

Route::post('/reset-token',function(){
	return csrf_token();
});

// Route::get('/',['as'=>'dashboard-user','uses'=>'DashboardController@dashboard']);

Route::get('/insert','AuthController@user');

Route::group(['middleware'=>'isLogin'],function(){
	Route::get('/',['as'=>'login','uses'=>'AuthController@index']);
	Route::get('/login',['as'=>'login','uses'=>'AuthController@index']);
	Route::post('/login/auth',['as'=>'login-auth','uses'=>'AuthController@login']);
});
Route::get('/logout',['as'=>'logout','uses'=>'AuthController@logout']);

Route::group(['prefix'=>'ajax'],function(){
	Route::get('/get-pasien/{id}',['as'=>'get-pasien-ajax','uses'=>'AjaxController@getPasien']);
	Route::get('/get-kategori-pasien/{id}',['as'=>'get-kategori-pasien-ajax','uses'=>'AjaxController@getKategoriPasien']);
	Route::get('/get-harga/{id}',['as'=>'get-harga-ajax','uses'=>'AjaxController@getHarga']);
	Route::get('/get-obat/{id}',['as'=>'get-obat-ajax','uses'=>'AjaxController@getObat']);
	Route::get('/get-obat-name/{id}',['as'=>'get-obat-name-ajax','uses'=>'AjaxController@getObatName']);
	Route::get('/get-biaya-lab/{id}',['as'=>'get-biaya-lab-ajax','uses'=>'AjaxController@getBiayaLab']);
	Route::get('/get-data-resep/{id}',['as'=>'get-data-resep-ajax','uses'=>'AjaxController@getDataResep']);
	Route::get('/get-info-pasien/{id}',['as'=>'get-info-pasien-ajax','uses'=>'AjaxController@getInfoPasien']);
    Route::get('/get-harga-obat/{id}',['as'=>'get-info-pasien-ajax','uses'=>'AjaxController@getHargaObat']);
    Route::get('/find-diagnosa-bpjs',['as' => 'find-diagnosa-bpjs', 'uses' => 'AjaxController@findDiagnosaBpjs']);
});

Route::group(['prefix'=>'datatables'],function(){
	Route::get('/data-pasien',['as'=>'data-pasien-datatables','uses'=>'AjaxController@dataPasien']);
	Route::get('/data-kategori-pasien',['as'=>'data-kategori-pasien-datatables','uses'=>'AjaxController@dataKategoriPasien']);
	Route::get('/data-dokter',['as'=>'data-dokter-datatables','uses'=>'AjaxController@dataDokter']);
    Route::get('/data-spesialis-dokter',['as'=>'data-dokter-datatables','uses'=>'AjaxController@dataSpesialisDokter']);
	Route::get('/data-golongan-obat',['as'=>'data-golongan-obat-datatables','uses'=>'AjaxController@dataGolonganObat']);
	Route::get('/data-obat/{ket_data}',['as'=>'data-obat-datatables','uses'=>'AjaxController@dataObat']);
	Route::get('/data-order-obat',['as'=>'data-obat-datatables','uses'=>'AjaxController@dataOrderObat']);
	Route::get('/data-jenis-obat',['as'=>'data-jenis-obat-datatables','uses'=>'AjaxController@dataJenisObat']);
	Route::get('/data-stok-opnem',['as'=>'data-obat-datatables','uses'=>'AjaxController@dataStokOpnem']);
	Route::get('/data-stok-opnem/detail/{id}',['as'=>'data-obat-datatables','uses'=>'AjaxController@dataStokOpnemDetail']);
	Route::get('/data-supplier-obat',['as'=>'data-supplier-datatables','uses'=>'AjaxController@dataSupplier']);
	Route::get('/data-masuk-obat',['as'=>'data-masuk-obat-datatables','uses'=>'AjaxController@dataPemasukan']);
	Route::get('/data-titip-obat',['as'=>'data-titip-obat-datatables','uses'=>'AjaxController@dataTitipObat']);
	Route::get('/data-supplier-beli',['as'=>'data-supplier-beli-datatables','uses'=>'AjaxController@dataSupplierBeli']);
	Route::get('/data-pembelian/{id}/{jenis}',['as'=>'data-pembelian-datatables','uses'=>'AjaxController@dataPembelian']);
	Route::get('/data-keluar-obat',['as'=>'data-keluar-obat-datatables','uses'=>'AjaxController@dataPengeluaran']);
	Route::get('/data-resep',['as'=>'data-resep-datatables','uses'=>'AjaxController@dataResep']);
    Route::get('/data-rujukan',['as'=>'data-resep-datatables','uses'=>'AjaxController@dataRujukan']);
	Route::get('/data-detail-resep/{id}',['as'=>'data-resep-datatables','uses'=>'AjaxController@dataDetailResep']);
	Route::get('/data-transaksi',['as'=>'data-transaksi-datatables','uses'=>'AjaxController@dataTransaksi']);
	Route::get('/data-total-penjualan',['as'=>'data-total-penjualan','uses'=>'AjaxController@dataTotalPenjualan']);
	Route::get('/data-tindakan-lab',['as'=>'data-tindakan-lab-datatables','uses'=>'AjaxController@dataTindakanLab']);
	Route::get('/data-pendaftaran/{id_dokter}',['as'=>'data-pendaftaran-datatables','uses'=>'AjaxController@dataPendaftaran']);
    Route::get('/data-pendaftaran-bpjs',['as'=>'data-pendaftaran-datatables','uses'=>'AjaxController@dataPendaftaranBpjs']);
    Route::get('/pasien-rekam-medis-admin',['as'=>'pasien-rekam-medis-datatables','uses'=>'AjaxController@dataRekamMedisAdmin']);
    Route::get('/pasien-rekam-medis-admin-all',['as'=>'pasien-rekam-medis-datatables-all','uses'=>'AjaxController@dataRekamMedisAdminAll']);
	Route::get('/pasien-rekam-medis/{id_dokter}',['as'=>'pasien-rekam-medis-datatables','uses'=>'AjaxController@dataPasienRekamMedis']);
	Route::get('/pasien-rekam-medis/{id_dokter}/rekam/{id_pasien}',['as'=>'daftar-rekam-medis-datatables','uses'=>'AjaxController@dataRekamMedis']);
	Route::get('/pasien-rekam-medis/{id_dokter}/rekam/{id_pasien}/detail/{id_resep}',['as'=>'rekam-medis-detail-datatables','uses'=>'AjaxController@dataDetailRekamMedis']);
	// Route::get('/data-rekam-medis/{id_dokter}',['as'=>'data-rekam-medis-datatables','uses'=>'AjaxController@dataRekamMedis']);
	// Route::get('/data-rekam-medis/{id_dokter}/{id_resep}',['as'=>'data-rekam-medis-detail','uses'=>'AjaxController@dataRekamMedisDetail']);
	Route::get('/diskon',['as'=>'atur-diskon-datatables','uses'=>'AjaxController@dataDiskon']);
	Route::get('/data-users',['as'=>'data-users-datatables','uses'=>'AjaxController@dataUsers']);
    Route::get('/data-penyakit',['as'=>'data-penyakit-datatables','uses'=>'AjaxController@dataPenyakit']);
    Route::get('/data-penyakit-bpjs',['as'=>'data-penyakit-datatables','uses'=>'AjaxController@dataPenyakitBpjs']);
});

Route::group(['middleware'=>'isAdmin','prefix'=>'admin'],function(){
	Route::get('/dashboard',['as'=>'dashboard-user','uses'=>'Admin\DashboardController@dashboard']);
    Route::get('/dashboard/rekam-medis-detail/{id}',['as'=>'dashboard-user','uses'=>'Admin\DashboardController@rekamMedisDetail']);
	
	// CRUD CUSTOMER //
	Route::get('/data-pasien',['as'=>'data-pasien','uses'=>'Admin\PasienController@tambah']);
	Route::get('/data-pasien/edit/{id}',['as'=>'data-pasien','uses'=>'Admin\PasienController@edit']);
	Route::get('/data-pasien/delete/{id}',['as'=>'data-pasien','uses'=>'Admin\PasienController@delete']);
	Route::post('/data-pasien/save',['as'=>'data-pasien','uses'=>'Admin\PasienController@save']);
    Route::get('/data-pasien/cetak-rekam-medis/{id}',['as' => 'laporan-pasien-cetak-rekam-medis','uses' => 'Admin\PasienController@cetakRekamMedis']);
	// END CRUD CUSTOMER //

	// CRUD KATEGORI PASIEN //
	Route::get('/data-kategori-pasien',['as'=>'data-kategori-pasien','uses'=>'Admin\KategoriPasienController@tambah']);
	Route::get('/data-kategori-pasien/edit/{id}',['as'=>'data-kategori-pasien','uses'=>'Admin\KategoriPasienController@edit']);
	Route::get('/data-kategori-pasien/delete/{id}',['as'=>'data-kategori-pasien','uses'=>'Admin\KategoriPasienController@delete']);
	Route::post('/data-kategori-pasien/save',['as'=>'data-kategori-pasien','uses'=>'Admin\KategoriPasienController@save']);
	// END CRUD KATEGORI PASIEN //

	// CRUD PENDAFTARAN //
	Route::get('/pendaftaran',['as'=>'pendaftaran-pasien','uses'=>'Admin\PendaftaranController@form']);
    Route::get('/pendaftaran/edit/{id}',['as'=>'pendaftaran-pasien','uses'=>'Admin\PendaftaranController@edit']);
	Route::get('/pendaftaran/delete/{id}',['as'=>'pendaftaran-delete-pasien','uses'=>'Admin\PendaftaranController@delete']);
	Route::post('/pendaftaran/save',['as'=>'pendaftaran-pasien-save','uses'=>'Admin\PendaftaranController@save']);
    Route::post('/pendaftaran/update/{id}',['as'=>'pendaftaran-pasien-save','uses'=>'Admin\PendaftaranController@update']);
	Route::get('/pendaftaran/check/{id}',['as'=>'pendaftaran-check-pasien','uses'=>'Admin\PendaftaranController@check']);
	Route::post('/pendaftaran/check/{id}',['as'=>'pendaftaran-save-check-pasien','uses'=>'Admin\PendaftaranController@saveCheck']);

    Route::get('/pendaftaran-bpjs',['as'=>'pendaftaran-bpjs-pasien','uses'=>'Admin\PendaftaranController@formBpjs']);
    // Route::get('/pendaftaran-bpjs/edit/{id}',['as'=>'pendaftaran-bpjs-pasien','uses'=>'Admin\PendaftaranController@edit']);
    Route::get('/pendaftaran-bpjs/delete/{no_kartu}/{tanggal_daftar}/{nomor_urut}/{kode_poli}',['as'=>'pendaftaran-bpjs-delete-pasien','uses'=>'Admin\PendaftaranController@deleteBpjs']);

    Route::post('/pendaftaran-bpjs/save',['as'=>'pendaftaran-bpjs-pasien-save','uses'=>'Admin\PendaftaranController@saveBpjs']);
    // Route::post('/pendaftaran-bpjs/update/{id}',['as'=>'pendaftaran-bpjs-pasien-save','uses'=>'Admin\PendaftaranController@update']);
	// END CRUD PENDAFTARAN //

	// CRUD GOLONGAN OBAT //
	Route::get('/data-golongan-obat',['as'=>'data-golongan-obat','uses'=>'Admin\GolonganObatController@tambah']);
	Route::get('/data-golongan-obat/edit/{id}',['as'=>'data-golongan-obat','uses'=>'Admin\GolonganObatController@edit']);
	Route::get('/data-golongan-obat/delete/{id}',['as'=>'data-golongan-obat','uses'=>'Admin\GolonganObatController@delete']);
	Route::post('/data-golongan-obat/save',['as'=>'data-golongan-obat','uses'=>'Admin\GolonganObatController@save']);
	// END CRUD GOLONGAN OBAT //

	// CRUD OBAT //
	Route::get('/data-obat',['as'=>'data-obat','uses'=>'Admin\ObatController@tambah']);
	Route::get('/data-obat/edit/{id}',['as'=>'data-obat','uses'=>'Admin\ObatController@edit']);
	Route::get('/data-obat/delete/{id}',['as'=>'data-obat','uses'=>'Admin\ObatController@delete']);
	Route::post('/data-obat/save',['as'=>'data-obat','uses'=>'Admin\ObatController@save']);
	// END CRUD OBAT //

	// CRUD JENIS OBAT //
	Route::get('/data-jenis-obat',['as'=>'data-jenis-obat','uses'=>'Admin\JenisObatController@tambah']);
	Route::get('/data-jenis-obat/edit/{id}',['as'=>'data-jenis-obat','uses'=>'Admin\JenisObatController@edit']);
	Route::get('/data-jenis-obat/delete/{id}',['as'=>'data-jenis-obat','uses'=>'Admin\JenisObatController@delete']);
	Route::post('/data-jenis-obat/save',['as'=>'data-jenis-obat','uses'=>'Admin\JenisObatController@save']);
	// END CRUD JENIS OBAT //

	// CRUD SUPPLIER OBAT //
	Route::get('/data-supplier-obat',['as'=>'data-supplier-obat','uses'=>'Admin\SupplierController@tambah']);
	Route::get('/data-supplier-obat/edit/{id}',['as'=>'data-supplier-obat','uses'=>'Admin\SupplierController@edit']);
	Route::get('/data-supplier-obat/delete/{id}',['as'=>'data-supplier-obat','uses'=>'Admin\SupplierController@delete']);
	Route::post('/data-supplier-obat/save',['as'=>'data-supplier-obat','uses'=>'Admin\SupplierController@save']);
	// END CRUD SUPPLIER OBAT //

	// CRUD PEMASUKKAN OBAT //
	Route::get('/data-masuk-obat',['as'=>'data-masuk-obat','uses'=>'Admin\PemasukanController@tambah']);
	Route::get('/data-masuk-obat/edit/{id}',['as'=>'data-masuk-obat','uses'=>'Admin\PemasukanController@edit']);
	Route::get('/data-masuk-obat/delete/{id}',['as'=>'data-masuk-obat','uses'=>'Admin\PemasukanController@delete']);
	Route::post('/data-masuk-obat/save',['as'=>'data-masuk-obat','uses'=>'Admin\PemasukanController@save']);
	Route::get('/data-masuk-obat/rekap',['as'=>'data-masuk-obat','uses'=>'Admin\PemasukanController@export']);
	// END CRUD PEMASUKKAN OBAT //

	// ROUTE BELI OBAT //
	// Route::get('/data-pembelian/bayar-semua/{id_supplier}',['as'=>'bayar-semua-obat','uses'=>'Admin\PemasukanController@bayarSemua']);
	Route::get('/data-pembelian/bayar-beli-semua/{id_supplier}',['as'=>'bayar-beli-semua','uses'=>'Admin\PemasukanController@bayarBeliSemua']);
	Route::get('/data-pembelian/bayar-titip-semua/{id_supplier}',['as'=>'bayar-titip-semua','uses'=>'Admin\PemasukanController@bayarTitipSemua']);
	Route::get('/data-pembelian/supplier/{id}/bayar/{id_bayar}',['as'=>'status-beli-obat','uses'=>'Admin\PemasukanController@bayar']);
	// END ROUTE BELI OBAT //

	// CRUD PENGELUARAN OBAT //
	Route::get('/data-keluar-obat',['as'=>'data-keluar-obat','uses'=>'Admin\PengeluaranController@tambah']);
	Route::get('/data-keluar-obat/edit/{id}',['as'=>'data-keluar-obat','uses'=>'Admin\PengeluaranController@edit']);
	Route::get('/data-keluar-obat/delete/{id}',['as'=>'data-keluar-obat','uses'=>'Admin\PengeluaranController@delete']);
	Route::get('/data-keluar-obat/rekap',['as'=>'data-keluar-obat','uses'=>'Admin\PengeluaranController@export']);
	Route::post('/data-keluar-obat/save',['as'=>'data-keluar-obat','uses'=>'Admin\PengeluaranController@save']);
	// END CRUD PENGELUARAN OBAT //

	// CRUD DOKTER //
	Route::get('/data-dokter',['as'=>'data-dokter','uses'=>'Admin\DokterController@tambah']);
	Route::get('/data-dokter/edit/{id}',['as'=>'data-dokter-edit','uses'=>'Admin\DokterController@edit']);
	Route::get('/data-dokter/status-akun/{id}',['as'=>'akun-status-dokter','uses'=>'Admin\DokterController@statusAkun']);
	Route::get('/data-dokter/delete/{id}',['as'=>'data-dokter-delete','uses'=>'Admin\DokterController@delete']);
	Route::post('/data-dokter/save',['as'=>'data-dokter-save','uses'=>'Admin\DokterController@save']);
	// END CRUD DOKTER //

    // CRUD DOKTER //
    Route::get('/data-spesialis-dokter',['as'=>'data-spesialis-dokter','uses'=>'Admin\SpesialisDokterController@tambah']);
    Route::get('/data-spesialis-dokter/edit/{id}',['as'=>'data-spesialis-dokter-edit','uses'=>'Admin\SpesialisDokterController@edit']);
    Route::get('/data-spesialis-dokter/delete/{id}',['as'=>'data-spesialis-dokter-delete','uses'=>'Admin\SpesialisDokterController@delete']);
    Route::post('/data-spesialis-dokter/save',['as'=>'data-spesialis-dokter-save','uses'=>'Admin\SpesialisDokterController@save']);
    // END CRUD DOKTER //

	// CRUD RESEP //
	Route::get('/data-resep',['as'=>'data-resep','uses'=>'Admin\ResepController@tambahResep']);
    Route::get('/data-resep/edit/{id}',['as'=>'data-resep','uses'=>'Admin\ResepController@editResep']);
    Route::post('/data-resep/update/{id}',['as'=>'data-resep','uses'=>'Admin\ResepController@update']);
	Route::get('/data-resep/delete/{id}',['as'=>'data-resep','uses'=>'Admin\ResepController@deleteResep']);
	Route::post('/data-resep/save',['as'=>'data-resep-save','uses'=>'Admin\ResepController@save']);

    Route::get('/data-resep/input-obat/{id}',['as'=>'data-detail-resep','uses'=>'Admin\ResepController@inputObat']);

    Route::post('/data-resep/input-obat/save/{id}',['as'=>'data-detail-resep','uses'=>'Admin\ResepController@inputObatSave']);

	Route::get('/data-resep/detail-resep/{id}',['as'=>'data-detail-resep','uses'=>'Admin\ResepController@detailResep']);
	Route::get('/data-resep/detail-resep/{id}/delete/{id_detail}',['as'=>'data-delete-detail','uses'=>'Admin\ResepController@deleteDetailResep']);
	// END CRUD RESEP //

	// CRUD TINDAKAN LAB //
	Route::get('/tindakan-lab',['as'=>'tindakan-lab-admin','uses'=>'Admin\TindakanLabController@tambah']);
	Route::get('/tindakan-lab/edit/{id}',['as'=>'tindakan-lab-admin','uses'=>'Admin\TindakanLabController@edit']);
	Route::get('/tindakan-lab/delete/{id}',['as'=>'tindakan-lab-admin','uses'=>'Admin\TindakanLabController@delete']);
	Route::post('/tindakan-lab/save',['as'=>'tindakan-lab-admin','uses'=>'Admin\TindakanLabController@save']);
	// END CRUD TINDAKAN LAB //

	// CRUD TRANSAKSI //
	Route::get('/data-transaksi',['as'=>'data-transaksi','uses'=>'Admin\TransaksiController@tambah']);
	Route::get('/data-transaksi/delete/{id}',['as'=>'data-resep-cetak','uses'=>'Admin\TransaksiController@delete']);
	Route::post('/data-transaksi/save',['as'=>'data-transaksi','uses'=>'Admin\TransaksiController@save']);
	Route::get('/data-transaksi/cetak/{id}',['as'=>'data-transaksi-cetak','uses'=>'Admin\TransaksiController@cetak']);
	Route::get('/data-transaksi/export',['as'=>'data-transaksi-export','uses'=>'Admin\TransaksiController@export']);
	// END CRUD TRANSAKSI //

	// CRUD USERS //
	Route::get('/data-users',['as'=>'data-users','uses'=>'Admin\UsersController@tambah']);
	Route::get('/data-users/edit/{id}',['as'=>'data-users','uses'=>'Admin\UsersController@edit']);
	Route::get('/data-users/delete/{id}',['as'=>'data-users','uses'=>'Admin\UsersController@delete']);
	Route::get('/data-users/status-user/{id}',['as'=>'data-users','uses'=>'Admin\UsersController@statusUser']);
	Route::post('/data-users/save',['as'=>'data-users','uses'=>'Admin\UsersController@save']);
	// END CRUD USERS //

	// CRUD STOK OPNEM //
	Route::get('/stok-opnem',['as' => 'stok-opnem-form', 'uses' => 'Admin\StokOpnemController@tambah']);
	Route::post('/stok-opnem/input-sebagian',['uses' => 'Admin\StokOpnemController@inputSebagian']);
	Route::post('/stok-opnem/save',['as' => 'stok-opnem-form', 'uses' => 'Admin\StokOpnemController@save']);
	Route::get('/stok-opnem/delete/{id}',['as' => 'stok-opnem-delete', 'uses' => 'Admin\StokOpnemController@delete']);
	// END CRUD STOK OPNEM //

	// ROUTE LAPORAN //
	Route::get('/laporan-pasien',['as'=>'laporan-pasien-admin','uses'=>'Admin\PasienController@index']);
    Route::get('/laporan-pasien-bpjs',['as'=>'laporan-pasien-admin','uses'=>'Admin\PasienController@pasienBpjs']);
	Route::get('/laporan-kategori-pasien',['as'=>'laporan-kategori-pasien-admin','uses'=>'Admin\KategoriPasienController@index']);
	Route::get('/laporan-pendaftaran',['as'=>'laporan-pendaftaran-pasien-admin','uses'=>'Admin\PendaftaranController@index']);
    Route::get('/laporan-pendaftaran-bpjs',['as'=>'laporan-pendaftaran-pasien-admin','uses'=>'Admin\PendaftaranController@indexBpjs']);
	Route::get('/laporan-dokter',['as'=>'laporan-dokter','uses'=>'Admin\DokterController@index']);
    Route::get('/laporan-spesialis-dokter',['as'=>'laporan-dokter','uses'=>'Admin\SpesialisDokterController@index']);
	Route::get('/laporan-golongan-obat',['as'=>'laporan-golongan-obat','uses'=>'Admin\GolonganObatController@index']);
	Route::get('/laporan-obat',['as'=>'laporan-obat','uses'=>'Admin\ObatController@index']);
    Route::get('/laporan-obat/cetak',['as'=>'laporan-obat','uses'=>'Admin\ObatController@cetak']);
	Route::get('/laporan-jenis-obat',['as'=>'laporan-jenis-obat','uses'=>'Admin\JenisObatController@index']);
	Route::get('/laporan-order-obat',['as'=>'laporan-order-obat','uses'=>'Admin\ObatController@orderObat']);
	Route::get('/laporan-supplier',['as'=>'laporan-supplier','uses'=>'Admin\SupplierController@index']);
	Route::get('/laporan-pemasukan',['as'=>'laporan-pemasukan-obat','uses'=>'Admin\PemasukanController@index']);
	Route::get('/laporan-pengeluaran',['as'=>'laporan-pengeluaran-obat','uses'=>'Admin\PengeluaranController@index']);
	Route::get('/laporan-pembelian',['as'=>'laporan-pembelian-obat','uses'=>'Admin\PemasukanController@pembelian']);
	Route::get('/laporan-pembelian/detail/{id}',['as'=>'laporan-detail-beli-obat','uses'=>'Admin\PemasukanController@beliDetail']);
	Route::get('/laporan-resep',['as'=>'laporan-resep','uses'=>'Admin\ResepController@resep']);
	Route::get('/laporan-resep/detail/{id}',['as'=>'laporan','uses'=>'Admin\ResepController@detailResep']);
    Route::get('/laporan-rujukan',['as'=>'laporan-rujukan','uses'=>'Admin\RujukanController@index']);
	Route::get('/laporan-transaksi',['as'=>'laporan-transaksi','uses'=>'Admin\TransaksiController@index']);
	Route::get('/laporan-total-penjualan',['as'=>'laporan-total-penjualan','uses'=>'Admin\TransaksiController@totalPenjualan']);
    Route::get('/laporan-total-penjualan/cetak',['as'=>'laporan-total-penjualan','uses'=>'Admin\TransaksiController@exportTotalPenjualan']);
	Route::get('/laporan-tindakan-lab',['as'=>'laporan-tindakan-lab','uses'=>'Admin\TindakanLabController@index']);
	Route::get('/laporan-users',['as'=>'laporan-users','uses'=>'Admin\UsersController@index']);
	Route::get('/laporan-users/status-user/{id}',['as'=>'laporan-users','uses'=>'Admin\UsersController@statusUser']);
	Route::get('/laporan-stok-opnem',['as' => 'laporan-stok-opnem','uses' => 'Admin\StokOpnemController@index']);
    Route::get('/laporan-rekap',['as' => 'laporan-stok-opnem','uses' => 'Admin\LaporanController@index']);
    Route::get('/laporan-rekap/cetak',['as' => 'laporan-stok-opnem','uses' => 'Admin\LaporanController@laporan']);
	// END ROUTE LAPORAN //

	Route::get('/ubah-profile',['as'=>'ubah-profile-user','uses'=>'Admin\DashboardController@ubahProfile']);
	Route::post('/ubah-profile/save',['as'=>'ubah-profile-save','uses'=>'Admin\DashboardController@save']);

    Route::get('/data-penyakit',['as'=>'data-penyakit','uses'=>'Admin\DataPenyakitController@index']);
    Route::get('/data-penyakit-bpjs',['as'=>'data-penyakit-bpjs','uses'=>'Admin\DataPenyakitController@penyakitBpjs']);
});

Route::group(['middleware'=>'isInventory','prefix'=>'inventory'],function(){
	Route::get('/dashboard',['as'=>'dashboard-user','uses'=>'Inventory\DashboardController@dashboard']);

	// CRUD OBAT //
	Route::get('/data-obat',['as'=>'data-obat','uses'=>'Inventory\ObatController@tambah']);
	Route::get('/data-obat/edit/{id}',['as'=>'data-obat','uses'=>'Inventory\ObatController@edit']);
	Route::get('/data-obat/delete/{id}',['as'=>'data-obat','uses'=>'Inventory\ObatController@delete']);
	Route::post('/data-obat/save',['as'=>'data-obat','uses'=>'Inventory\ObatController@save']);
	// END CRUD OBAT //

	// CRUD JENIS OBAT //
	// Route::get('/data-jenis-obat',['as'=>'data-jenis-obat','uses'=>'Inventory\JenisObatController@tambah']);
	// Route::get('/data-jenis-obat/edit/{id}',['as'=>'data-jenis-obat','uses'=>'Inventory\JenisObatController@edit']);
	// Route::get('/data-jenis-obat/delete/{id}',['as'=>'data-jenis-obat','uses'=>'Inventory\JenisObatController@delete']);
	// Route::post('/data-jenis-obat/save',['as'=>'data-jenis-obat','uses'=>'Inventory\JenisObatController@save']);
	// END CRUD JENIS OBAT //

	// CRUD SUPPLIER OBAT //
	Route::get('/data-supplier-obat',['as'=>'data-supplier-obat','uses'=>'Inventory\SupplierController@tambah']);
	Route::get('/data-supplier-obat/edit/{id}',['as'=>'data-supplier-obat','uses'=>'Inventory\SupplierController@edit']);
	Route::get('/data-supplier-obat/delete/{id}',['as'=>'data-supplier-obat','uses'=>'Inventory\SupplierController@delete']);
	Route::post('/data-supplier-obat/save',['as'=>'data-supplier-obat','uses'=>'Inventory\SupplierController@save']);
	// END CRUD SUPPLIER OBAT //

	// CRUD PEMASUKKAN OBAT //
	Route::get('/data-masuk-obat',['as'=>'data-masuk-obat','uses'=>'Inventory\PemasukanController@tambah']);
	Route::get('/data-masuk-obat/delete/{id}',['as'=>'data-masuk-obat','uses'=>'Inventory\PemasukanController@delete']);
	Route::post('/data-masuk-obat/save',['as'=>'data-masuk-obat','uses'=>'Inventory\PemasukanController@save']);
	Route::get('/data-masuk-obat/rekap',['as'=>'data-masuk-obat','uses'=>'Inventory\PemasukanController@export']);
	// END CRUD PEMASUKKAN OBAT //

	// ROUTE BELI OBAT //
	Route::get('/data-pembelian/bayar-semua/{id_supplier}',['as'=>'bayar-semua-obat','uses'=>'Inventory\PemasukanController@bayarSemua']);
	Route::get('/data-pembelian/supplier/{id}/bayar/{id_bayar}',['as'=>'status-beli-obat','uses'=>'Inventory\PemasukanController@bayar']);
	// END ROUTE BELI OBAT //

	// CRUD PENGELUARAN OBAT //
	Route::get('/data-keluar-obat',['as'=>'data-keluar-obat','uses'=>'Inventory\PengeluaranController@tambah']);
	Route::get('/data-keluar-obat/delete/{id}',['as'=>'data-keluar-obat','uses'=>'Inventory\PengeluaranController@delete']);
	Route::post('/data-keluar-obat/save',['as'=>'data-keluar-obat','uses'=>'Inventory\PengeluaranController@save']);
	// END CRUD PENGELUARAN OBAT //

    // CRUD STOK OPNEM //
    Route::get('/stok-opnem',['as' => 'stok-opnem-form', 'uses' => 'Inventory\StokOpnemController@tambah']);
    Route::post('/stok-opnem/input-sebagian',['uses' => 'Inventory\StokOpnemController@inputSebagian']);
    Route::post('/stok-opnem/save',['as' => 'stok-opnem-form', 'uses' => 'Inventory\StokOpnemController@save']);
    Route::get('/stok-opnem/delete/{id}',['as' => 'stok-opnem-delete', 'uses' => 'Inventory\StokOpnemController@delete']);
    // END CRUD STOK OPNEM //

	// ROUTE LAPORAN //	
	Route::get('/laporan-obat',['as'=>'laporan-obat','uses'=>'Inventory\ObatController@index']);
	Route::get('/laporan-jenis-obat',['as'=>'laporan-jenis-obat','uses'=>'Inventory\JenisObatController@index']);
	Route::get('/laporan-order-obat',['as'=>'laporan-order-obat','uses'=>'Inventory\ObatController@orderObat']);
	Route::get('/laporan-supplier',['as'=>'laporan-supplier','uses'=>'Inventory\SupplierController@index']);
	Route::get('/laporan-pemasukan',['as'=>'laporan-pemasukan-obat','uses'=>'Inventory\PemasukanController@index']);
	Route::get('/laporan-pembelian',['as'=>'laporan-pembelian-obat','uses'=>'Inventory\PemasukanController@pembelian']);
	Route::get('/laporan-pembelian/detail/{id}',['as'=>'laporan-detail-beli-obat','uses'=>'Inventory\PemasukanController@beliDetail']);
	Route::get('/laporan-pengeluaran',['as'=>'laporan-pengeluaran-obat','uses'=>'Inventory\PengeluaranController@index']);
    Route::get('/laporan-stok-opnem',['as' => 'laporan-stok-opnem','uses' => 'Inventory\StokOpnemController@index']);
	// END ROUTE LAPORAN //

	Route::get('/ubah-profile',['as'=>'ubah-profile-user','uses'=>'Inventory\DashboardController@ubahProfile']);
	Route::post('/ubah-profile/save',['as'=>'ubah-profile-save','uses'=>'Inventory\DashboardController@save']);
});

Route::group(['middleware'=>'isDokter','prefix'=>'dokter'],function(){
	Route::get('/dashboard',['as'=>'dashboard-dokter','uses'=>'Dokter\DashboardController@dashboard']);
	Route::get('/data-resep',['as'=>'dashboard-dokter','uses'=>'Dokter\ResepController@tambahResep']);
	Route::get('/data-resep/delete/{id}',['as'=>'data-resep','uses'=>'Dokter\ResepController@deleteResep']);
	Route::post('/data-resep/save',['as'=>'data-resep-save','uses'=>'Dokter\ResepController@save']);

	Route::get('/data-resep/detail-resep/{id}',['as'=>'data-detail-resep','uses'=>'Dokter\ResepController@detailResep']);
	Route::get('/data-resep/detail-resep/{id}/delete/{id_detail}',['as'=>'data-delete-detail','uses'=>'Dokter\ResepController@deleteDetailResep']);

	Route::get('/laporan-rekam-medis',['as'=>'rekam-medis','uses'=>'Dokter\RekamMedisController@index']);
	Route::get('/laporan-rekam-medis/{id_dokter}/rekam/{id}',['as'=>'rekam-medis-pasien','uses'=>'Dokter\RekamMedisController@rekamMedis']);
    Route::get('/laporan-rekam-medis/{id_dokter}/rekam/{id}/edit/{id_detail}',['as'=>'rekam-medis-pasien','uses'=>'Dokter\RekamMedisController@editRekamMedis']);
    Route::post('/laporan-rekam-medis/{id_dokter}/rekam/{id}/update/{id_detail}',['as'=>'rekam-medis-pasien','uses'=>'Dokter\RekamMedisController@updateRekamMedis']);
	Route::get('/laporan-rekam-medis/{id_dokter}/rekam/{id}/detail/{id_detail}',['as'=>'rekam-medis-detail','uses'=>'Dokter\RekamMedisController@detailRekamMedis']);

	Route::get('/ubah-profile',['as'=>'ubah-profile-user','uses'=>'Dokter\DashboardController@ubahProfile']);
	Route::post('/ubah-profile/save',['as'=>'ubah-profile-save','uses'=>'Dokter\DashboardController@save']);
});

Route::group(['middleware'=>'isOperator','prefix'=>'operator'],function(){
	Route::get('/dashboard',['as'=>'dashboard-operator','uses'=>'Operator\DashboardController@dashboard']);
	
	// CRUD CUSTOMER //
	Route::get('/data-pasien',['as'=>'data-pasien','uses'=>'Operator\PasienController@tambah']);
	Route::get('/data-pasien/edit/{id}',['as'=>'data-pasien','uses'=>'Operator\PasienController@edit']);
	Route::get('/data-pasien/delete/{id}',['as'=>'data-pasien','uses'=>'Operator\PasienController@delete']);
	Route::post('/data-pasien/save',['as'=>'data-pasien','uses'=>'Operator\PasienController@save']);
	// END CRUD CUSTOMER //

	// CRUD PENDAFTARAN //
	Route::get('/pendaftaran',['as'=>'pendaftaran-pasien','uses'=>'Operator\PendaftaranController@form']);
	Route::get('/pendaftaran/delete/{id}',['as'=>'pendaftaran-delete-pasien','uses'=>'Operator\PendaftaranController@delete']);
	Route::post('/pendaftaran/save',['as'=>'pendaftaran-pasien-save','uses'=>'Operator\PendaftaranController@save']);

    Route::get('/pendaftaran-bpjs',['as'=>'pendaftaran-bpjs-pasien','uses'=>'Operator\PendaftaranController@formBpjs']);
    // Route::get('/pendaftaran-bpjs/edit/{id}',['as'=>'pendaftaran-bpjs-pasien','uses'=>'Operator\PendaftaranController@edit']);
    Route::get('/pendaftaran-bpjs/delete/{no_kartu}/{tanggal_daftar}/{nomor_urut}/{kode_poli}',['as'=>'pendaftaran-bpjs-delete-pasien','uses'=>'Operator\PendaftaranController@deleteBpjs']);

    Route::post('/pendaftaran-bpjs/save',['as'=>'pendaftaran-bpjs-pasien-save','uses'=>'Operator\PendaftaranController@saveBpjs']);
	// END CRUD PENDAFTARAN //

	// CRUD TINDAKAN LAB //
	// Route::get('/tindakan-lab',['as'=>'tindakan-lab-Operator','uses'=>'Resep\TindakanLabController@tambah']);
	// Route::get('/tindakan-lab/edit/{id}',['as'=>'tindakan-lab-Operator','uses'=>'Resep\TindakanLabController@edit']);
	// Route::get('/tindakan-lab/delete/{id}',['as'=>'tindakan-lab-Operator','uses'=>'Resep\TindakanLabController@delete']);
	// Route::post('/tindakan-lab/save',['as'=>'tindakan-lab-Operator','uses'=>'Resep\TindakanLabController@save']);
	// END CRUD TINDAKAN LAB //

	// CRUD RESEP //
	Route::get('/data-resep',['as'=>'data-resep','uses'=>'Operator\ResepController@tambahResep']);
	Route::get('/data-resep/delete/{id}',['as'=>'data-resep','uses'=>'Operator\ResepController@deleteResep']);
	Route::post('/data-resep/save',['as'=>'data-resep-save','uses'=>'Operator\ResepController@save']);

    Route::get('/data-resep/input-obat/{id}',['as'=>'data-detail-resep','uses'=>'Operator\ResepController@inputObat']);

    Route::post('/data-resep/input-obat/save/{id}',['as'=>'data-detail-resep','uses'=>'Operator\ResepController@inputObatSave']);

	Route::get('/data-resep/detail-resep/{id}',['as'=>'data-detail-resep','uses'=>'Operator\ResepController@detailResep']);
	Route::get('/data-resep/detail-resep/{id}/delete/{id_detail}',['as'=>'data-delete-detail','uses'=>'Operator\ResepController@deleteDetailResep']);
	// END CRUD RESEP //
	

	Route::get('/data-transaksi',['as'=>'data-transaksi','uses'=>'Operator\TransaksiController@tambah']);
	Route::get('/data-transaksi/delete/{id}',['as'=>'data-resep-cetak','uses'=>'Operator\TransaksiController@delete']);
	Route::post('/data-transaksi/save',['as'=>'data-transaksi','uses'=>'Operator\TransaksiController@save']);
	Route::get('/data-transaksi/cetak/{id}',['as'=>'data-transaksi-cetak','uses'=>'Operator\TransaksiController@cetak']);
	Route::get('/data-transaksi/export',['as'=>'data-transaksi-export','uses'=>'Operator\TransaksiController@export']);

	// ROUTE LAPORAN //
	Route::get('/laporan-pasien',['as'=>'laporan-pasien-Operator','uses'=>'Operator\PasienController@index']);
	Route::get('/laporan-pendaftaran',['as'=>'laporan-pendaftaran-pasien-Operator','uses'=>'Operator\PendaftaranController@index']);
    Route::get('/laporan-pendaftaran-bpjs',['as'=>'laporan-pendaftaran-pasien-admin','uses'=>'Operator\PendaftaranController@indexBpjs']);
	Route::get('/laporan-transaksi',['as'=>'laporan-transaksi','uses'=>'Operator\TransaksiController@index']);
	// Route::get('/laporan-tindakan-lab',['as'=>'laporan-tindakan-lab','uses'=>'Resep\TindakanLabController@index']);
	Route::get('/laporan-resep',['as'=>'laporan-resep','uses'=>'Operator\ResepController@resep']);
	Route::get('/laporan-resep/detail/{id}',['as'=>'laporan','uses'=>'Operator\ResepController@detailResep']);
	// END ROUTE LAPORAN //

	Route::get('/ubah-profile',['as'=>'ubah-profile-user','uses'=>'Operator\DashboardController@ubahProfile']);
	Route::post('/ubah-profile/save',['as'=>'ubah-profile-save','uses'=>'Operator\DashboardController@save']);
});

Route::group(['middleware'=>'isResep','prefix'=>'resep'],function(){
	Route::get('/dashboard',['as'=>'dashboard-user','uses'=>'Resep\DashboardController@dashboard']);
	
	// CRUD CUSTOMER //
	// Route::get('/data-pasien',['as'=>'data-pasien','uses'=>'Resep\PasienController@tambah']);
	// Route::get('/data-pasien/edit/{id}',['as'=>'data-pasien','uses'=>'Resep\PasienController@edit']);
	// Route::get('/data-pasien/delete/{id}',['as'=>'data-pasien','uses'=>'Resep\PasienController@delete']);
	// Route::post('/data-pasien/save',['as'=>'data-pasien','uses'=>'Resep\PasienController@save']);
	// END CRUD CUSTOMER //

	// CRUD RESEP //
	// Route::get('/data-resep',['as'=>'data-resep','uses'=>'Resep\ResepController@tambahResep']);
	// Route::get('/data-resep/delete/{id}',['as'=>'data-resep','uses'=>'Resep\ResepController@deleteResep']);
	// Route::post('/data-resep/save',['as'=>'data-resep-save','uses'=>'Resep\ResepController@save']);

	// Route::get('/data-resep/detail-resep/{id}',['as'=>'data-detail-resep','uses'=>'Resep\ResepController@detailResep']);
	// Route::get('/data-resep/detail-resep/{id}/delete/{id_detail}',['as'=>'data-delete-detail','uses'=>'Resep\ResepController@deleteDetailResep']);
	// END CRUD RESEP //

	// CRUD TINDAKAN LAB //
	// Route::get('/tindakan-lab',['as'=>'tindakan-lab-admin','uses'=>'Resep\TindakanLabController@tambah']);
	// Route::get('/tindakan-lab/edit/{id}',['as'=>'tindakan-lab-admin','uses'=>'Resep\TindakanLabController@edit']);
	// Route::get('/tindakan-lab/delete/{id}',['as'=>'tindakan-lab-admin','uses'=>'Resep\TindakanLabController@delete']);
	// Route::post('/tindakan-lab/save',['as'=>'tindakan-lab-admin','uses'=>'Resep\TindakanLabController@save']);
	// END CRUD TINDAKAN LAB //

	// CRUD TRANSAKSI //
	Route::get('/data-transaksi',['as'=>'data-transaksi','uses'=>'Resep\TransaksiController@tambah']);
	Route::get('/data-transaksi/delete/{id}',['as'=>'data-resep-cetak','uses'=>'Resep\TransaksiController@delete']);
	Route::post('/data-transaksi/save',['as'=>'data-transaksi','uses'=>'Resep\TransaksiController@save']);
	Route::get('/data-transaksi/cetak/{id}',['as'=>'data-transaksi-cetak','uses'=>'Resep\TransaksiController@cetak']);
	Route::get('/data-transaksi/export',['as'=>'data-transaksi-export','uses'=>'Resep\TransaksiController@export']);
	// END CRUD TRANSAKSI //

	// ROUTE LAPORAN //
	// Route::get('/laporan-pasien',['as'=>'laporan-pasien-admin','uses'=>'Resep\PasienController@index']);
	// Route::get('/laporan-resep',['as'=>'laporan-resep','uses'=>'Resep\ResepController@resep']);
	// Route::get('/laporan-resep/detail/{id}',['as'=>'laporan','uses'=>'Resep\ResepController@detailResep']);
	Route::get('/laporan-transaksi',['as'=>'laporan-transaksi','uses'=>'Resep\TransaksiController@index']);
	// Route::get('/laporan-tindakan-lab',['as'=>'laporan-tindakan-lab','uses'=>'Resep\TindakanLabController@index']);
	// END ROUTE LAPORAN //

	Route::get('/ubah-profile',['as'=>'ubah-profile-user','uses'=>'Resep\DashboardController@ubahProfile']);
	Route::post('/ubah-profile/save',['as'=>'ubah-profile-save','uses'=>'Resep\DashboardController@save']);
});