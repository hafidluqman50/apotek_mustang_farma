-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Feb 04, 2023 at 04:05 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_apotek`
--

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem`
--

CREATE TABLE `stok_opnem` (
  `id_stok_opnem` int NOT NULL,
  `tanggal_stok_opnem` date NOT NULL,
  `keterangan` text,
  `status_input` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem_detail`
--

CREATE TABLE `stok_opnem_detail` (
  `id_stok_opnem_detail` int NOT NULL,
  `id_stok_opnem` int NOT NULL,
  `id_obat` int NOT NULL,
  `stok_komputer` int DEFAULT NULL,
  `stok_fisik` int DEFAULT NULL,
  `stok_selisih` int DEFAULT NULL,
  `sub_nilai` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `stok_opnem_detail`
--
DELIMITER $$
CREATE TRIGGER `stok_obat_update` AFTER INSERT ON `stok_opnem_detail` FOR EACH ROW UPDATE obat SET stok_obat = NEW.stok_fisik WHERE id_obat = NEW.id_obat
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  ADD PRIMARY KEY (`id_stok_opnem`);

--
-- Indexes for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD PRIMARY KEY (`id_stok_opnem_detail`),
  ADD KEY `id_stok_opnem` (`id_stok_opnem`),
  ADD KEY `id_obat` (`id_obat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  MODIFY `id_stok_opnem` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  MODIFY `id_stok_opnem_detail` int NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD CONSTRAINT `stok_opnem_detail_ibfk_1` FOREIGN KEY (`id_stok_opnem`) REFERENCES `stok_opnem` (`id_stok_opnem`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_opnem_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
