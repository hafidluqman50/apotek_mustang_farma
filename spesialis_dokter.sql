-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Mar 07, 2023 at 08:55 AM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotek_mustang_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `spesialis_dokter`
--

CREATE TABLE `spesialis_dokter` (
  `id_spesialis_dokter` int NOT NULL,
  `nama_spesialis_dokter` varchar(100) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spesialis_dokter`
--

INSERT INTO `spesialis_dokter` (`id_spesialis_dokter`, `nama_spesialis_dokter`, `status_delete`) VALUES
(1, 'Umum', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `spesialis_dokter`
--
ALTER TABLE `spesialis_dokter`
  ADD PRIMARY KEY (`id_spesialis_dokter`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `spesialis_dokter`
--
ALTER TABLE `spesialis_dokter`
  MODIFY `id_spesialis_dokter` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
