-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jul 28, 2023 at 06:45 PM
-- Server version: 8.0.19
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apotek_mustang_farma`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_resep`
--

CREATE TABLE `detail_resep` (
  `id_detail_resep` int NOT NULL,
  `id_resep` int NOT NULL,
  `id_obat` int NOT NULL,
  `banyak_obat` int NOT NULL,
  `dosis` varchar(10) NOT NULL,
  `sub_total` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_resep`
--

INSERT INTO `detail_resep` (`id_detail_resep`, `id_resep`, `id_obat`, `banyak_obat`, `dosis`, `sub_total`, `created_at`, `updated_at`) VALUES
(24, 27, 5, 1, '3 x 1 Hari', 14000, '2023-02-16 00:08:43', '2023-02-16 00:08:43'),
(25, 27, 18, 1, '3 x 1 Hari', 840, '2023-02-16 00:08:43', '2023-02-16 00:08:43'),
(28, 30, 4, 1, '3 x 1 Hari', 14000, '2023-03-01 14:37:12', '2023-03-01 14:37:12'),
(29, 31, 4, 1, '3 x 1 Hari', 14000, '2023-03-01 14:56:17', '2023-03-01 14:56:17'),
(36, 29, 5, 1, '3 x 1 Hari', 14000, '2023-05-09 15:25:46', '2023-05-09 15:25:46'),
(37, 29, 4, 1, '3 x 1 Hari', 14000, '2023-05-09 15:25:46', '2023-05-09 15:25:46'),
(38, 28, 7, 1, '3 x 1 Hari', 7000, '2023-05-09 15:26:20', '2023-05-09 15:26:20'),
(39, 28, 4, 1, '3 x 1 Hari', 14000, '2023-05-09 15:26:20', '2023-05-09 15:26:20');

--
-- Triggers `detail_resep`
--
DELIMITER $$
CREATE TRIGGER `kurang_stok_resep` AFTER INSERT ON `detail_resep` FOR EACH ROW BEGIN
	UPDATE obat SET stok_obat = stok_obat - NEW.banyak_obat WHERE id_obat = NEW.id_obat;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int NOT NULL,
  `id_users` int NOT NULL,
  `nama_dokter` varchar(100) NOT NULL,
  `id_spesialis_dokter` int NOT NULL,
  `nomor_telepon_dokter` varchar(20) NOT NULL,
  `biaya_dokter` int NOT NULL,
  `status_dokter` int NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `id_users`, `nama_dokter`, `id_spesialis_dokter`, `nomor_telepon_dokter`, `biaya_dokter`, `status_dokter`, `status_delete`) VALUES
(1, 8, 'dr. OZ', 1, '085238992182', 100000, 1, 0),
(2, 7, 'dr. James Andrews', 1, '081234567890', 150000, 1, 0),
(3, 9, 'dr. Aswan', 1, '0812312312312', 50000, 1, 1),
(4, 10, 'dr. Aswan', 1, '088888', 100000, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `golongan_obat`
--

CREATE TABLE `golongan_obat` (
  `id_golongan_obat` int NOT NULL,
  `nama_golongan` varchar(50) NOT NULL,
  `slug_golongan` text NOT NULL,
  `foto_golongan` text NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `golongan_obat`
--

INSERT INTO `golongan_obat` (`id_golongan_obat`, `nama_golongan`, `slug_golongan`, `foto_golongan`, `status_delete`) VALUES
(1, 'Obat Bebas', 'obat-bebas', '_icon_golongan_5c5ec6ca8952b_obat-bebas.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `harga_obat`
--

CREATE TABLE `harga_obat` (
  `id_harga_obat` int NOT NULL,
  `nama_harga` varchar(70) NOT NULL,
  `id_obat` int NOT NULL,
  `harga_total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `harga_obat`
--

INSERT INTO `harga_obat` (`id_harga_obat`, `nama_harga`, `id_obat`, `harga_total`) VALUES
(29, 'alpha', 24, 15217),
(30, 'beta', 24, 15556),
(31, 'gama', 24, 15909),
(32, 'partai', 24, 16471),
(33, 'eceran', 24, 17500),
(34, 'luar-kota', 24, 21538),
(35, 'lain-lain', 24, 28000),
(36, 'alpha', 30, 21739),
(37, 'beta', 30, 22222),
(38, 'gama', 30, 22727),
(39, 'partai', 30, 23529),
(40, 'eceran', 30, 25000),
(41, 'luar-kota', 30, 30769),
(42, 'lain-lain', 30, 40000),
(43, 'alpha', 32, 21739),
(44, 'beta', 32, 22222),
(45, 'gama', 32, 22727),
(46, 'partai', 32, 23529),
(47, 'eceran', 32, 25000),
(48, 'luar-kota', 32, 30769),
(49, 'lain-lain', 32, 40000),
(50, 'alpha', 34, 7609),
(51, 'alpha', 34, 7609),
(52, 'alpha', 34, 7609);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_obat`
--

CREATE TABLE `jenis_obat` (
  `id_jenis_obat` int NOT NULL,
  `nama_jenis_obat` varchar(100) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_obat`
--

INSERT INTO `jenis_obat` (`id_jenis_obat`, `nama_jenis_obat`, `status_delete`) VALUES
(1, 'Tablet', 0),
(2, 'Sirup', 0),
(3, 'Bubuks', 1),
(4, 'Bubuk', 1),
(5, 'Bubuk', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_pasien`
--

CREATE TABLE `kategori_pasien` (
  `id_kategori_pasien` int NOT NULL,
  `nama_kategori` varchar(70) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_pasien`
--

INSERT INTO `kategori_pasien` (`id_kategori_pasien`, `nama_kategori`, `status_delete`) VALUES
(1, 'Umum', 0),
(2, 'BPJS', 0),
(3, 'VIP', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kredit`
--

CREATE TABLE `kredit` (
  `id_kredit` int NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `nomor_telepon` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit`
--

INSERT INTO `kredit` (`id_kredit`, `nama_pelanggan`, `nomor_telepon`) VALUES
(1, 'dr. James Andrew', '0888888888'),
(4, 'Mahmud', '0'),
(5, 'Mahmud', '0'),
(6, 'Mahmud', '0'),
(7, 'Mahmud', '0');

-- --------------------------------------------------------

--
-- Table structure for table `kredit_det`
--

CREATE TABLE `kredit_det` (
  `id_kredit_det` int NOT NULL,
  `id_kredit` int NOT NULL,
  `id_obat` int NOT NULL,
  `banyak_obat` int NOT NULL,
  `bentuk_satuan` varchar(10) NOT NULL,
  `jenis_harga` varchar(50) NOT NULL,
  `sub_total` int NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `status_kredit` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kredit_det`
--

INSERT INTO `kredit_det` (`id_kredit_det`, `id_kredit`, `id_obat`, `banyak_obat`, `bentuk_satuan`, `jenis_harga`, `sub_total`, `tanggal_jatuh_tempo`, `status_kredit`, `created_at`, `updated_at`) VALUES
(19, 1, 24, 5, 'pcs', 'alpha', 15217, '2019-02-21', 1, '2019-02-14 12:24:50', '2021-06-07 11:32:39'),
(20, 1, 30, 5, 'pcs', 'alpha', 15217, '2021-06-10', 1, '2021-06-07 14:23:42', '2021-06-07 14:25:15'),
(21, 1, 30, 5, 'pcs', 'alpha', 15217, '2021-06-10', 1, '2021-06-07 14:27:57', '2021-06-07 14:36:05'),
(22, 1, 30, 0, 'pcs', 'alpha', 217390, '2021-06-10', 1, '2021-06-07 14:45:59', '2021-06-07 14:46:19');

--
-- Triggers `kredit_det`
--
DELIMITER $$
CREATE TRIGGER `kredit_event` AFTER INSERT ON `kredit_det` FOR EACH ROW BEGIN
	SET @bobot_satuan:=(SELECT bobot_satuan FROM obat WHERE id_obat = NEW.id_obat);
    IF NEW.bentuk_satuan = 'box' THEN
    	UPDATE obat SET stok_obat = stok_obat - (@bobot_satuan * NEW.banyak_obat) WHERE id_obat = NEW.id_obat;
    ELSE
    	UPDATE obat SET stok_obat = stok_obat - NEW.banyak_obat WHERE id_obat = NEW.id_obat;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id_obat` int NOT NULL,
  `kode_obat` varchar(20) NOT NULL,
  `nomor_batch` varchar(255) NOT NULL,
  `id_jenis_obat` int NOT NULL,
  `nama_obat` varchar(70) NOT NULL,
  `id_golongan_obat` int NOT NULL,
  `tanggal_expired` date NOT NULL,
  `harga_obat` int NOT NULL,
  `harga_jual` int NOT NULL,
  `stok_obat` int NOT NULL,
  `bobot_satuan` int NOT NULL,
  `ket_data` varchar(30) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obat`
--

INSERT INTO `obat` (`id_obat`, `kode_obat`, `nomor_batch`, `id_jenis_obat`, `nama_obat`, `id_golongan_obat`, `tanggal_expired`, `harga_obat`, `harga_jual`, `stok_obat`, `bobot_satuan`, `ket_data`, `status_delete`) VALUES
(4, 'OMF-TBT-00000000001', 'D 02302025', 1, 'Paramex', 1, '2020-09-10', 20000, 14000, 395, 20, 'inventory', 0),
(5, 'OMF-TBT-00000000002', 'D 12312312', 1, 'Bionix', 1, '2030-01-31', 20000, 14000, 995, 20, 'inventory', 0),
(7, 'OMF-TBT-00000000003', 'C 23232323', 1, 'Mixagrip', 1, '2025-05-25', 10000, 7000, 1999, 20, 'inventory', 0),
(8, 'OMF-TBT-00000000004', 'D 20202020', 2, 'OBH Combi', 1, '2018-09-19', 20000, 14000, 3000, 20, 'inventory', 0),
(9, 'OMF-TBT-00000000005', 'D 0000001', 1, 'Panadol Hijau', 1, '2018-09-27', 1000, 700, 4009, 20, 'inventory', 0),
(18, 'OMF-TBT-00000000010', 'E 100201002', 2, 'Bisoprolol', 1, '2019-01-17', 1200, 840, 4989, 20, 'inventory', 0),
(24, 'OMF-TBT-00000000011', 'D 130303013', 1, 'Mevinal', 1, '2019-02-01', 20000, 14000, 6000, 20, 'inventory', 0),
(29, 'OMF-SRP-00000000012', 'D 101010100110', 2, 'Puyer', 1, '2019-02-22', 100000, 70000, 7000, 20, 'inventory', 0),
(30, 'OMF-SRP-00000000012', 'D 20202020', 2, 'OBH Combi', 1, '2018-09-19', 20000, 14000, 8000, 20, 'inventory', 0),
(31, 'OMF-SRP-00000000012', 'D 01555555555', 2, 'Oskadin', 1, '2019-03-21', 15000, 10500, 9000, 10, 'inventory', 0),
(32, 'OMF-TBT-00000000012', 'D 02302025', 1, 'Paramex', 1, '2020-09-10', 20000, 14000, 10000, 20, 'inventory', 0),
(33, 'OMF-TBT-00000000013', 'D098301', 1, 'Paradox', 1, '2023-02-03', 10000, 7000, 11000, 10, 'inventory', 0),
(34, 'OMF-TBT-00000000014', '020202020', 1, 'Konidin', 1, '2023-02-05', 10000, 7000, 9990, 10, 'inventory', 1),
(35, 'OMF-TBT-00000000015', '020202020', 1, 'Konidin', 1, '2023-02-05', 10000, 7000, 10000, 10, 'inventory', 1),
(36, 'OMF-TBT-00000000016', '020202020', 1, 'Konidin', 1, '2023-02-05', 10000, 7000, 10000, 10, 'inventory', 1),
(37, 'OMF-TBT-00000000017', '020202020', 1, 'Konidin', 1, '2023-02-05', 10000, 7000, 9999, 10, 'inventory', 1),
(38, 'OMF-TBT-00000000018', '0102020', 1, 'Konidin', 1, '2000-09-23', 20000, 14000, 10, 10, 'kasir', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `id_pasien` int NOT NULL,
  `kode_pasien` varchar(20) NOT NULL,
  `no_bpjs` varchar(70) NOT NULL,
  `id_kategori_pasien` int NOT NULL,
  `nama_pasien` varchar(70) NOT NULL,
  `nomor_telepon_pasien` varchar(20) NOT NULL,
  `alamat_pasien` text NOT NULL,
  `jenis_kelamin` enum('Laki - Laki','Perempuan') NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`id_pasien`, `kode_pasien`, `no_bpjs`, `id_kategori_pasien`, `nama_pasien`, `nomor_telepon_pasien`, `alamat_pasien`, `jenis_kelamin`, `tanggal_lahir`, `status_delete`) VALUES
(18, 'PSN-AMF-00000000001', '', 1, 'Tony', '081238283182', 'Jln. Merdeka No.3', 'Laki - Laki', '2000-09-23', 0),
(19, 'PSN-AMF-00000000002', '', 1, 'Rudi', '081567284182', 'Jln. Muso Salim Gang 8', 'Laki - Laki', '2000-09-23', 0),
(20, 'PSN-AMF-00000000003', '', 1, 'Muhammad Andy', '085723890999', 'Jln. P.M. Noor', 'Laki - Laki', '2017-10-30', 0),
(21, 'PSN-AMF-00000000005', '', 1, 'Ilham', '0888888888', 'Jln. Mongonsidi', 'Laki - Laki', '2000-09-23', 0),
(22, 'PSN-AMF-00000000007', '', 1, 'Muhammad Ilham', '0841231232', 'Jln. Rajawali Dalam 3', 'Laki - Laki', '2015-10-29', 0),
(23, 'PSN-AMF-00000000010', '12012030303', 2, 'Fajar', '08538888888', '-', 'Laki - Laki', '1999-02-02', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pemasukan_obat`
--

CREATE TABLE `pemasukan_obat` (
  `id_masuk` int NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `id_supplier` int NOT NULL,
  `id_obat` int NOT NULL,
  `stok_masuk` int NOT NULL,
  `bentuk_stok` varchar(10) NOT NULL,
  `stok_pakai` int NOT NULL,
  `keterangan` text NOT NULL,
  `jenis_masuk` varchar(30) NOT NULL,
  `ppn` int NOT NULL,
  `diskon` double NOT NULL,
  `banyak_bayar` int NOT NULL,
  `status_beli` int NOT NULL,
  `tanggal_harus_bayar` date NOT NULL,
  `id_users` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pemasukan_obat`
--

INSERT INTO `pemasukan_obat` (`id_masuk`, `tanggal_masuk`, `id_supplier`, `id_obat`, `stok_masuk`, `bentuk_stok`, `stok_pakai`, `keterangan`, `jenis_masuk`, `ppn`, `diskon`, `banyak_bayar`, `status_beli`, `tanggal_harus_bayar`, `id_users`, `created_at`, `updated_at`) VALUES
(5, '2018-09-28', 4, 5, 200, 'pcs', 0, 'Pemasukan Dari Supplier', 'supplier', 0, 0, 200000, 1, '2019-02-04', 1, '2018-09-25 08:59:16', '2019-03-13 10:29:42'),
(6, '2018-09-28', 4, 8, 10, 'box', 0, 'Pemasukan Dari Supplier', 'supplier', 0, 0, 200000, 1, '2019-02-04', 1, '2018-09-25 08:53:48', '2019-03-13 10:29:42'),
(13, '2019-03-30', 2, 8, 100, 'pcs', 26, 'Pemasukan Dari Supplier', 'titip', 0, 0, 520000, 0, '2019-04-06', 1, '2019-03-20 20:48:37', '2023-02-03 14:45:39'),
(14, '2023-02-03', 2, 4, 10, 'pcs', 0, 'Pemasukan Dari Supplier', 'supplier', 0, 0, 200000, 0, '2023-02-03', 1, '2023-02-03 14:48:51', '2023-02-03 14:48:51'),
(15, '2023-02-10', 2, 4, 10, 'pcs', 0, 'Pemasukan Dari Supplier', 'supplier', 0, 0, 200000, 0, '2023-02-10', 1, '2023-02-10 01:59:58', '2023-02-10 01:59:58'),
(16, '2023-02-10', 2, 4, 10, 'pcs', 0, 'Pemasukan Dari Supplier', 'supplier', 10, 50, 77000, 0, '2023-02-10', 1, '2023-02-10 15:31:21', '2023-02-10 15:31:21'),
(17, '2023-02-13', 2, 9, 10, 'pcs', 0, 'Pemasukan Dari Supplier', 'titip', 10, 50, 3850, 0, '2023-02-14', 1, '2023-02-13 19:56:55', '2023-02-13 19:56:55');

--
-- Triggers `pemasukan_obat`
--
DELIMITER $$
CREATE TRIGGER `masuk_stok` AFTER INSERT ON `pemasukan_obat` FOR EACH ROW BEGIN
	SET @bobot_satuan := (SELECT bobot_satuan FROM obat WHERE id_obat=NEW.id_obat);
    
    IF NEW.bentuk_stok = 'box' THEN
    	UPDATE obat SET stok_obat = NEW.stok_masuk * @bobot_satuan + stok_obat WHERE id_obat = NEW.id_obat;
    ELSE
    	UPDATE obat SET stok_obat = NEW.stok_masuk + stok_obat WHERE id_obat = NEW.id_obat;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE `pendaftaran` (
  `id_daftar` int NOT NULL,
  `tgl_daftar` date NOT NULL,
  `id_pasien` int NOT NULL,
  `id_dokter` int NOT NULL,
  `kode_provider_peserta` varchar(10) DEFAULT NULL,
  `kunjungan_sakit` int DEFAULT NULL,
  `laju_respirasi` int DEFAULT NULL,
  `kode_poli_bpjs` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lingkar_perut` int DEFAULT NULL,
  `denyut_jantung` int DEFAULT NULL,
  `rujuk_balik` int DEFAULT NULL,
  `kode_tkp` int DEFAULT '10',
  `ket_daftar` varchar(50) NOT NULL,
  `tinggi_badan` double NOT NULL,
  `berat_badan` double NOT NULL,
  `tekanan_darah` varchar(10) NOT NULL,
  `suhu_badan` double NOT NULL,
  `keluhan` varchar(100) NOT NULL,
  `hasil_cek_darah` text NOT NULL,
  `pemeriksaan_penunjang` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status_delete` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pendaftaran`
--

INSERT INTO `pendaftaran` (`id_daftar`, `tgl_daftar`, `id_pasien`, `id_dokter`, `kode_provider_peserta`, `kunjungan_sakit`, `laju_respirasi`, `kode_poli_bpjs`, `lingkar_perut`, `denyut_jantung`, `rujuk_balik`, `kode_tkp`, `ket_daftar`, `tinggi_badan`, `berat_badan`, `tekanan_darah`, `suhu_badan`, `keluhan`, `hasil_cek_darah`, `pemeriksaan_penunjang`, `status_delete`, `created_at`, `updated_at`) VALUES
(2, '2019-02-07', 18, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 0, 0, '', 0, '', '', '', 0, '2019-02-07 10:47:05', '2019-02-07 10:47:05'),
(3, '2019-02-13', 20, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 0, 0, '', 0, '', '', '', 0, '2019-02-13 20:17:44', '2019-02-13 20:17:44'),
(4, '2023-02-03', 23, 1, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 0, 0, '', 0, '', '', '', 0, '2023-02-03 14:44:52', '2023-02-03 14:44:52'),
(5, '2023-02-05', 19, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 0, 0, '', 0, '', '', '', 0, '2023-02-05 22:31:47', '2023-02-05 22:31:47'),
(6, '2023-02-06', 20, 1, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 70.5, '120/90', 37.5, '', '', '', 0, '2023-02-06 20:54:22', '2023-02-06 20:54:22'),
(7, '2023-02-15', 18, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 80, '140/60', 37.5, '', '', '', 0, '2023-02-06 23:29:56', '2023-02-06 23:29:56'),
(9, '2023-02-17', 23, 1, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 70, '140/90', 35, '', '', '', 0, '2023-02-17 14:51:39', '2023-02-17 14:51:39'),
(10, '2023-02-17', 23, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 70, '140/90', 35, '', '{\"kolesterol\":\"200\",\"gula_darah\":\"100\",\"asam_urat\":\"6\"}', 'Cek Kaki', 0, '2023-02-17 14:55:50', '2023-02-17 14:55:50'),
(11, '2023-02-17', 18, 1, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 80, '140/90', 35, '', '', '', 0, '2023-02-17 09:11:09', '2023-02-17 09:12:08'),
(12, '2023-02-25', 19, 1, NULL, 0, 0, '', 0, 0, 0, 10, 'belum-masuk', 170, 60, '140/90', 35, 'Sakit Kepala', '{\"kolesterol\":\"200\",\"gula_darah\":\"100\",\"asam_urat\":\"6\"}', '', 0, '2023-02-25 16:32:55', '2023-02-25 16:32:55'),
(13, '2023-04-15', 18, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 160, 70, '140/90', 36.5, 'Sakit Kepala', '{\"kolesterol\":\"120\",\"gula_darah\":\"120\",\"asam_urat\":\"7\"}', '-', 0, '2023-04-15 16:48:13', '2023-04-15 16:48:13'),
(14, '2023-04-17', 18, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'masuk-resep', 170, 60, '140/90', 36.5, '-', '{\"kolesterol\":\"20\",\"gula_darah\":\"20\",\"asam_urat\":\"20\"}', '-', 0, '2023-04-17 15:39:38', '2023-04-17 15:39:38'),
(15, '2023-04-17', 23, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'belum-masuk', 170, 60, '140/90', 36.5, '-', '{\"kolesterol\":\"-\",\"gula_darah\":\"-\",\"asam_urat\":\"-\"}', '-', 0, '2023-04-17 18:44:35', '2023-04-17 18:44:35'),
(16, '2023-05-22', 19, 2, NULL, 0, 0, '', 0, 0, 0, 10, 'belum-masuk', 170, 66, '140/90', 35, '-', '{\"kolesterol\":\"-\",\"gula_darah\":\"-\",\"asam_urat\":\"-\"}', '-', 1, '2023-05-22 21:36:03', '2023-05-22 22:09:45');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluaran_obat`
--

CREATE TABLE `pengeluaran_obat` (
  `id_keluar` int NOT NULL,
  `tanggal_keluar` date NOT NULL,
  `id_obat` int NOT NULL,
  `stok_keluar` int NOT NULL,
  `bentuk_stok` varchar(10) NOT NULL,
  `keterangan` text NOT NULL,
  `id_users` int NOT NULL,
  `jenis_keluar` varchar(15) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengeluaran_obat`
--

INSERT INTO `pengeluaran_obat` (`id_keluar`, `tanggal_keluar`, `id_obat`, `stok_keluar`, `bentuk_stok`, `keterangan`, `id_users`, `jenis_keluar`, `created_at`, `updated_at`) VALUES
(7, '2018-10-04', 5, 10, 'pcs', 'Transaksi Klinik', 3, 'transaksi', NULL, NULL),
(8, '2018-10-04', 8, 10, 'pcs', 'Transaksi Klinik', 3, 'transaksi', NULL, NULL),
(9, '2018-10-31', 18, 10, 'pcs', 'Pengeluaran Klinik', 1, 'non-transaksi', '2018-10-03 23:32:30', '2018-10-03 23:32:30'),
(24, '2018-10-08', 5, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(25, '2018-10-08', 8, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(26, '2018-10-08', 7, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(27, '2018-10-09', 18, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(28, '2018-10-09', 8, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(33, '2018-11-06', 4, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(34, '2018-11-20', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(65, '2019-02-07', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(66, '2019-02-07', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(76, '2019-02-07', 4, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(77, '2019-02-09', 9, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(78, '2019-02-09', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(79, '2019-02-09', 8, 2, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(89, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(90, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(91, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(92, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(93, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(94, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(95, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(96, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(97, '2019-02-13', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(98, '2019-02-13', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(99, '2019-02-13', 7, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(100, '2019-02-13', 18, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(101, '2019-02-13', 7, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(102, '2019-02-14', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-14 12:51:56', '2019-02-14 12:51:56'),
(103, '2019-02-14', 24, 1, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2019-02-14 12:52:21', '2019-02-14 12:52:21'),
(104, '2019-03-30', 30, 10, 'pcs', 'Pengeluaran Kasir', 1, 'non-transaksi', '2019-03-17 22:48:15', '2019-03-17 22:48:15'),
(105, '2021-06-07', 32, 10, 'pcs', 'Pengeluaran Kasir', 1, 'non-transaksi', '2021-06-07 11:00:36', '2021-06-07 11:00:36'),
(106, '2021-06-07', 32, 10, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2021-06-07 11:29:32', '2021-06-07 11:29:32'),
(107, '2021-06-07', 30, 10, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2021-06-07 11:32:10', '2021-06-07 11:32:10'),
(108, '2021-06-07', 30, 10, 'pcs', 'Transaksi Kasir Depan', 4, 'transaksi', '2021-06-07 14:46:19', '2021-06-07 14:46:19'),
(109, '2023-02-03', 4, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(110, '2023-02-03', 8, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(111, '2023-02-03', 8, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(112, '2023-02-03', 4, 1, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(113, '2023-02-06', 34, 10, 'pcs', 'Transaksi Klinik', 1, 'transaksi', NULL, NULL),
(114, '2023-02-07', 4, 1, 'pcs', 'Transaksi Klinik', 6, 'transaksi', NULL, NULL),
(115, '2023-02-07', 9, 1, 'pcs', 'Transaksi Klinik', 6, 'transaksi', NULL, NULL),
(116, '2023-02-10', 4, 10, 'pcs', 'Transaksi Pasien BPJS', 1, 'transaksi', NULL, NULL),
(117, '2023-02-13', 18, 10, 'pcs', 'Transaksi Pasien Umum', 1, 'transaksi', NULL, NULL),
(118, '2023-02-16', 5, 1, 'pcs', 'Transaksi Pasien Umum', 1, 'transaksi', NULL, NULL),
(119, '2023-02-16', 18, 1, 'pcs', 'Transaksi Pasien Umum', 1, 'transaksi', NULL, NULL),
(120, '2023-03-16', 4, 1, 'pcs', 'Transaksi Klinik', 6, 'transaksi', NULL, NULL),
(121, '2023-03-16', 4, 1, 'pcs', 'Transaksi Pasien Umum', 1, 'transaksi', NULL, NULL),
(122, '2023-07-07', 5, 1, 'pcs', 'Transaksi Pasien BPJS', 1, 'transaksi', NULL, NULL),
(123, '2023-07-07', 4, 1, 'pcs', 'Transaksi Pasien BPJS', 1, 'transaksi', NULL, NULL);

--
-- Triggers `pengeluaran_obat`
--
DELIMITER $$
CREATE TRIGGER `keluar_stok` AFTER INSERT ON `pengeluaran_obat` FOR EACH ROW BEGIN
	SET @nomor_batch = (SELECT nomor_batch FROM obat WHERE id_obat = NEW.id_obat);
	SET @bobot_satuan := (SELECT bobot_satuan FROM obat WHERE id_obat=NEW.id_obat);
    
    IF NEW.keterangan = 'Pengeluaran Klinik' THEN
    	SET @ket_data = 'klinik';
    ELSE
    	SET @ket_data = 'kasir';
    END IF;
    
    IF NEW.jenis_Keluar = 'non-transaksi' THEN
        IF NEW.bentuk_stok = 'box' THEN
        	UPDATE obat SET stok_obat = stok_obat - (NEW.stok_keluar * @bobot_satuan) WHERE id_obat = NEW.id_obat;
            UPDATE obat SET stok_obat = stok_obat + (NEW.stok_keluar * @bobot_satuan) WHERE nomor_batch = @nomor_batch AND ket_data = @ket_data;
        ELSE
        	UPDATE obat SET stok_obat = stok_obat - NEW.stok_keluar WHERE id_obat = NEW.id_obat;
        	UPDATE obat SET stok_obat = stok_obat + NEW.stok_keluar WHERE nomor_batch = @nomor_batch AND ket_data = @ket_data;
         END IF;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_instansi`
--

CREATE TABLE `profile_instansi` (
  `id_profile_instansi` int NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `nomor_telepon_instansi` varchar(20) NOT NULL,
  `logo_instansi` text NOT NULL,
  `background_instansi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_instansi`
--

INSERT INTO `profile_instansi` (`id_profile_instansi`, `nama_instansi`, `alamat_instansi`, `nomor_telepon_instansi`, `logo_instansi`, `background_instansi`) VALUES
(1, 'Apotek Klinik88', 'Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang', '-', 'logo-klinik88.png', 'bg-klinik88.png');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat`
--

CREATE TABLE `racik_obat` (
  `id_racik_obat` int NOT NULL,
  `id_racik_obat_data` int NOT NULL,
  `nama_racik` varchar(70) NOT NULL,
  `jenis_racik` varchar(10) NOT NULL,
  `jumlah_racik` int NOT NULL,
  `ongkos_racik` int NOT NULL,
  `harga_total_racik` int NOT NULL,
  `keterangan_racik` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat`
--

INSERT INTO `racik_obat` (`id_racik_obat`, `id_racik_obat_data`, `nama_racik`, `jenis_racik`, `jumlah_racik`, `ongkos_racik`, `harga_total_racik`, `keterangan_racik`) VALUES
(1, 1, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(2, 2, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(3, 3, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(4, 4, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(5, 5, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(6, 6, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(7, 7, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(8, 13, 'Mantap 1', 'non-dtd', 1, 1000, 14000, 'PCS'),
(9, 20, 'Test', 'non-dtd', 1, 1000, 14000, 'PCS'),
(10, 21, 'Mantap 1', 'non-dtd', 1, 1000, 24000, 'PCS'),
(11, 22, 'Obat Tanpa Racik', '-', 0, 0, 28000, '-');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_data`
--

CREATE TABLE `racik_obat_data` (
  `id_racik_obat_data` int NOT NULL,
  `tanggal_racik` date NOT NULL,
  `id_pasien` int NOT NULL,
  `id_dokter` int NOT NULL,
  `total_semua` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_data`
--

INSERT INTO `racik_obat_data` (`id_racik_obat_data`, `tanggal_racik`, `id_pasien`, `id_dokter`, `total_semua`) VALUES
(1, '2023-02-16', 18, 1, 25000),
(2, '2023-02-16', 18, 1, 25000),
(3, '2023-02-16', 18, 1, 25000),
(4, '2023-02-16', 18, 1, 25000),
(5, '2023-02-16', 18, 1, 25000),
(6, '2023-02-16', 18, 1, 25000),
(7, '2023-02-16', 18, 1, 25000),
(8, '2023-02-16', 18, 1, 25000),
(9, '2023-02-16', 18, 1, 25000),
(10, '2023-02-16', 18, 1, 25000),
(11, '2023-02-16', 18, 1, 25000),
(12, '2023-02-16', 18, 1, 25000),
(13, '2023-02-16', 18, 1, 15000),
(14, '2023-02-16', 18, 1, 15000),
(15, '2023-02-16', 18, 1, 15000),
(16, '2023-02-16', 18, 1, 15000),
(17, '2023-02-16', 18, 1, 15000),
(18, '2023-02-16', 18, 1, 15000),
(19, '2023-02-16', 18, 1, 15000),
(20, '2023-02-16', 18, 1, 15000),
(21, '2023-02-16', 18, 1, 25000),
(22, '2023-02-17', 18, 1, 28000);

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_detail`
--

CREATE TABLE `racik_obat_detail` (
  `id_racik_obat_detail` int NOT NULL,
  `id_racik_obat` int NOT NULL,
  `id_obat` int NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `embalase` int NOT NULL DEFAULT '0',
  `sub_total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_detail`
--

INSERT INTO `racik_obat_detail` (`id_racik_obat_detail`, `id_racik_obat`, `id_obat`, `jumlah`, `embalase`, `sub_total`) VALUES
(1, 2, 4, '1', 10000, 14000),
(2, 3, 4, '1', 10000, 14000),
(3, 4, 4, '1', 10000, 14000),
(4, 5, 4, '1', 10000, 14000),
(5, 6, 4, '1', 10000, 14000),
(6, 7, 4, '1', 10000, 14000),
(7, 8, 4, '1', 0, 14000),
(8, 9, 4, '1', 0, 14000),
(9, 10, 4, '1', 10000, 14000),
(10, 11, 4, '1', 0, 14000),
(11, 11, 5, '1', 0, 14000);

--
-- Triggers `racik_obat_detail`
--
DELIMITER $$
CREATE TRIGGER `kurang_stok_by_racik` AFTER INSERT ON `racik_obat_detail` FOR EACH ROW UPDATE obat SET stok_obat = stok_obat - NEW.jumlah WHERE id_obat = NEW.id_obat
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_sementara`
--

CREATE TABLE `racik_obat_sementara` (
  `id_racik_obat_sementara` int NOT NULL,
  `kode_racik` varchar(100) NOT NULL,
  `nama_racik` varchar(70) NOT NULL,
  `jenis_racik` varchar(10) NOT NULL,
  `jumlah_racik` int NOT NULL,
  `ongkos_racik` int NOT NULL,
  `total_racik` int NOT NULL,
  `keterangan_racik` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_sementara`
--

INSERT INTO `racik_obat_sementara` (`id_racik_obat_sementara`, `kode_racik`, `nama_racik`, `jenis_racik`, `jumlah_racik`, `ongkos_racik`, `total_racik`, `keterangan_racik`) VALUES
(1, '0c0c9e9d-92b9-490e-a96f-ffc634008b10', 'Mantap', 'non-dtd', 1, 1000, 24000, 'PCS'),
(5, 'bd9220ba-e511-4e72-81cf-597a18d40a69', 'Mantap', 'non-dtd', 1, 1000, 14000, 'PCS'),
(7, '65e732d5-68c0-4a29-8e4d-743d9a8ec3b3', 'Obat Tanpa Racik', '-', 0, 0, 0, '-'),
(9, '1b3b65e5-3f61-4ef9-b372-a1c8b12613fd', 'Mantap', 'non-dtd', 1, 10000, 14000, 'PCS');

-- --------------------------------------------------------

--
-- Table structure for table `racik_obat_sementara_detail`
--

CREATE TABLE `racik_obat_sementara_detail` (
  `id_racik_obat_sementara_detail` int NOT NULL,
  `id_racik_obat_sementara` int NOT NULL,
  `id_obat` int NOT NULL,
  `jumlah` int NOT NULL,
  `embalase` int NOT NULL,
  `sub_total` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `racik_obat_sementara_detail`
--

INSERT INTO `racik_obat_sementara_detail` (`id_racik_obat_sementara_detail`, `id_racik_obat_sementara`, `id_obat`, `jumlah`, `embalase`, `sub_total`) VALUES
(1, 1, 4, 1, 10000, 14000),
(5, 5, 4, 1, 0, 14000),
(9, 9, 4, 1, 0, 14000);

-- --------------------------------------------------------

--
-- Table structure for table `rekam_medis`
--

CREATE TABLE `rekam_medis` (
  `id_rekam_medis` int NOT NULL,
  `tgl_periksa` date NOT NULL,
  `id_dokter` int NOT NULL,
  `id_pasien` int NOT NULL,
  `keluhan` text NOT NULL,
  `diagnosa` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resep`
--

CREATE TABLE `resep` (
  `id_resep` int NOT NULL,
  `kode_resep` varchar(20) NOT NULL,
  `tgl_resep` date NOT NULL,
  `id_daftar` int NOT NULL,
  `id_dokter` int NOT NULL,
  `id_pasien` int NOT NULL,
  `tinggi_badan_resep` double NOT NULL,
  `berat_badan_resep` double NOT NULL,
  `suhu_badan_resep` double NOT NULL,
  `tekanan_darah_resep` varchar(10) NOT NULL,
  `hasil_cek_darah_resep` text NOT NULL,
  `keluhan_utama_resep` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diagnosa` text NOT NULL,
  `anemnesis` varchar(100) NOT NULL,
  `pemeriksaan_fisik` text NOT NULL,
  `pemeriksaan_penunjang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `terapi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `rujukan` varchar(100) NOT NULL,
  `keterangan_resep` text NOT NULL,
  `id_tindakan_lab` int DEFAULT NULL,
  `id_users` int NOT NULL,
  `ket_resep` varchar(15) NOT NULL,
  `tgl_kontrol_selanjutnya` date NOT NULL,
  `status_resep` int NOT NULL,
  `status_delete` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resep`
--

INSERT INTO `resep` (`id_resep`, `kode_resep`, `tgl_resep`, `id_daftar`, `id_dokter`, `id_pasien`, `tinggi_badan_resep`, `berat_badan_resep`, `suhu_badan_resep`, `tekanan_darah_resep`, `hasil_cek_darah_resep`, `keluhan_utama_resep`, `diagnosa`, `anemnesis`, `pemeriksaan_fisik`, `pemeriksaan_penunjang`, `terapi`, `rujukan`, `keterangan_resep`, `id_tindakan_lab`, `id_users`, `ket_resep`, `tgl_kontrol_selanjutnya`, `status_resep`, `status_delete`, `created_at`, `updated_at`) VALUES
(20, 'RSP-00000000012', '2019-04-13', 3, 2, 20, 0, 0, 0, '', '', '', 'Mantul', '', '', '', '', '', '', 1, 1, 'sudah-bayar', '0000-00-00', 0, 0, '2019-02-13 22:17:49', '2019-02-13 22:17:49'),
(21, 'RSP-00000000002', '2023-02-03', 2, 2, 18, 0, 0, 0, '', '', '', 'Kelelahan', '', '', '', '', '', '', 1, 1, 'sudah-bayar', '0000-00-00', 0, 0, '2023-02-03 14:22:12', '2023-02-03 14:22:12'),
(22, 'RSP-00000000003', '2023-02-03', 4, 1, 23, 0, 0, 0, '', '', '', 'Kelelahan', '', '', '', '', '', '', 1, 1, 'sudah-bayar', '0000-00-00', 0, 0, '2023-02-03 14:45:21', '2023-02-03 14:45:21'),
(23, 'RSP-00000000004', '2023-02-05', 5, 2, 19, 0, 0, 0, '', '', '', 'ACL Kena Bro', '', '', '', '', '', '', 1, 1, 'sudah-bayar', '0000-00-00', 0, 0, '2023-02-05 23:40:26', '2023-02-05 23:40:26'),
(24, 'RSP-00000000005', '2023-02-06', 7, 2, 18, 0, 0, 0, '', '', '', 'Pilek', '', '', '', '', '', '', 1, 7, 'sudah-bayar', '0000-00-00', 0, 0, '2023-02-06 23:34:19', '2023-02-06 23:34:19'),
(27, 'RSP-00000000006', '2023-02-16', 6, 1, 20, 0, 0, 0, '', '', '', 'Kurang Liburan', '-', '', '-', '-', '-', '', 1, 1, 'sudah-bayar', '0000-00-00', 0, 0, '2023-02-16 00:08:43', '2023-02-16 00:08:43'),
(28, 'RSP-00000000017', '2023-05-09', 9, 1, 23, 170, 70, 35, '140/90', '{\"kolesterol\":\"-\",\"gula_darah\":\"-\",\"asam_urat\":\"-\"}', 'Sakit Kepala', '-', '-', '', '', '-', '-', 'belum-bayar', 1, 7, 'belum-bayar', '2023-05-26', 0, 0, '2023-02-17 14:52:03', '2023-05-09 15:26:20'),
(29, 'RSP-00000000016', '2023-05-09', 10, 2, 23, 170, 70, 35, '140/90', '{\"kolesterol\":\"-\",\"gula_darah\":\"-\",\"asam_urat\":\"-\"}', 'Sakit Kaki', '-', '-', '', 'Cek Kaki', '-', '-', 'belum-bayar', 1, 7, 'sudah-bayar', '2023-04-23', 1, 0, '2023-02-17 14:56:44', '2023-05-09 15:25:46'),
(30, 'RSP-00000000009', '2023-03-01', 12, 1, 19, 0, 0, 0, '', '', '', 'Tifus', '-', '', '-', '-', '-', '', NULL, 1, 'sudah-bayar', '2023-04-26', 1, 0, '2023-03-01 13:29:03', '2023-03-01 14:37:12'),
(31, 'RSP-00000000010', '2023-03-01', 11, 1, 18, 0, 0, 0, '', '', '', 'Gagal Ginjal', '-', '', '-', '-', '-', '', 1, 8, 'sudah-bayar', '0000-00-00', 1, 0, '2023-03-01 14:51:31', '2023-03-01 14:56:17'),
(32, 'RSP-00000000001', '2023-03-16', 2, 2, 18, 0, 0, 0, '', '', 'Sakit Kepala', '-', '-', '', '-', '-', '-', '', NULL, 7, 'sudah-bayar', '2023-04-25', 0, 0, '2023-03-16 15:44:13', '2023-03-16 15:44:13'),
(34, 'RSP-00000000011', '2023-04-15', 13, 2, 18, 160, 70, 36.5, '140/90', '{\"kolesterol\":\"120\",\"gula_darah\":\"120\",\"asam_urat\":\"7\"}', 'Sakit Kepala', '-', '-', '<p>-</p>', '-', '<p>-</p>', '-', '-', 3, 7, 'belum-bayar', '2023-04-26', 0, 0, '2023-04-15 16:52:21', '2023-04-15 16:52:21'),
(38, 'RSP-00000000013', '2023-04-17', 14, 2, 18, 170, 60, 36.5, '140/90', '{\"kolesterol\":\"20\",\"gula_darah\":\"20\",\"asam_urat\":\"20\"}', '-', '-', '-', '<p>-</p>', '-', '<p>-</p>', '-', '-', 3, 7, 'sudah-bayar', '2023-04-25', 0, 0, '2023-04-17 15:51:43', '2023-04-17 15:51:43');

--
-- Triggers `resep`
--
DELIMITER $$
CREATE TRIGGER `event_hapus_resep` AFTER DELETE ON `resep` FOR EACH ROW BEGIN
	UPDATE pendaftaran SET ket_daftar='belum-masuk' WHERE id_daftar = OLD.id_daftar;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `ubah_ket_daftar` AFTER INSERT ON `resep` FOR EACH ROW BEGIN
	UPDATE pendaftaran SET ket_daftar='masuk-resep' WHERE id_daftar = NEW.id_daftar;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `spesialis_dokter`
--

CREATE TABLE `spesialis_dokter` (
  `id_spesialis_dokter` int NOT NULL,
  `nama_spesialis_dokter` varchar(100) NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spesialis_dokter`
--

INSERT INTO `spesialis_dokter` (`id_spesialis_dokter`, `nama_spesialis_dokter`, `status_delete`) VALUES
(1, 'Umum', 0);

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem`
--

CREATE TABLE `stok_opnem` (
  `id_stok_opnem` int NOT NULL,
  `tanggal_stok_opnem` date NOT NULL,
  `keterangan` text,
  `status_input` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stok_opnem`
--

INSERT INTO `stok_opnem` (`id_stok_opnem`, `tanggal_stok_opnem`, `keterangan`, `status_input`, `created_at`, `updated_at`) VALUES
(1, '2023-02-03', 'Test Stok Opnem', 1, '2023-02-05 22:28:04', '2023-02-05 22:28:04'),
(2, '2023-02-03', 'Test Stok Opnem', 1, '2023-02-05 22:28:31', '2023-02-05 22:28:31'),
(3, '2023-02-03', 'Test Stok Opnem', 1, '2023-02-05 22:29:47', '2023-02-05 22:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `stok_opnem_detail`
--

CREATE TABLE `stok_opnem_detail` (
  `id_stok_opnem_detail` int NOT NULL,
  `id_stok_opnem` int NOT NULL,
  `id_obat` int NOT NULL,
  `stok_komputer` int DEFAULT NULL,
  `stok_fisik` int DEFAULT NULL,
  `stok_selisih` int DEFAULT NULL,
  `sub_nilai` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stok_opnem_detail`
--

INSERT INTO `stok_opnem_detail` (`id_stok_opnem_detail`, `id_stok_opnem`, `id_obat`, `stok_komputer`, `stok_fisik`, `stok_selisih`, `sub_nilai`) VALUES
(1, 3, 4, 407, 407, 0, 8140000),
(2, 3, 5, 198, 1000, -802, 20000000),
(3, 3, 7, 298, 2000, -1702, 20000000),
(4, 3, 8, 191, 3000, -2809, 60000000),
(5, 3, 9, 89, 4000, -3911, 4000000),
(6, 3, 18, 2, 5000, -4998, 6000000),
(7, 3, 24, 183, 6000, -5817, 120000000),
(8, 3, 29, 10, 7000, -6990, 700000000),
(9, 3, 30, 76, 8000, -7924, 160000000),
(10, 3, 31, 10, 9000, -8990, 135000000),
(11, 3, 32, 100, 10000, -9900, 200000000),
(12, 3, 33, 1000, 11000, -10000, 110000000);

--
-- Triggers `stok_opnem_detail`
--
DELIMITER $$
CREATE TRIGGER `stok_obat_update` AFTER INSERT ON `stok_opnem_detail` FOR EACH ROW UPDATE obat SET stok_obat = NEW.stok_fisik WHERE id_obat = NEW.id_obat
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_obat`
--

CREATE TABLE `supplier_obat` (
  `id_supplier` int NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `nomor_telepon` varchar(20) NOT NULL,
  `alamat_supplier` text NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_obat`
--

INSERT INTO `supplier_obat` (`id_supplier`, `nama_supplier`, `nomor_telepon`, `alamat_supplier`, `status_delete`) VALUES
(1, '-', '-', '-', 0),
(2, 'PT. Farmindo Mitra Mandiri', '05412323232', 'Jln. Kartini No. 18', 0),
(4, 'PT. Servier Indonesia', '02157903940', 'Menara Kadin Indonesia, \r\nKav . 2- 3,\r\n Jl. H. R. Rasuna Said, \r\nRT.1/RW.2,\r\n Kuningan, Kuningan Tim.,\r\n Kota Jakarta Selatan, \r\nDaerah Khusus Ibukota Jakarta 12950', 0),
(5, 'Kalbe Farma', '0', '-', 0),
(6, 'Ujangs', '0', '-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tindakan_lab`
--

CREATE TABLE `tindakan_lab` (
  `id_tindakan_lab` int NOT NULL,
  `id_spesialis_dokter` int NOT NULL,
  `nama_tindakan` varchar(100) NOT NULL,
  `biaya_lab` int NOT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tindakan_lab`
--

INSERT INTO `tindakan_lab` (`id_tindakan_lab`, `id_spesialis_dokter`, `nama_tindakan`, `biaya_lab`, `status_delete`) VALUES
(1, 1, 'Cek Kolestrol', 20000, 0),
(2, 1, 'Cek Gula Darah', 30000, 0),
(3, 1, 'Check up', 10000, 0),
(4, 1, '-', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int NOT NULL,
  `id_resep` int NOT NULL,
  `id_users` int NOT NULL,
  `tgl_byr` date NOT NULL,
  `biaya_klinik` int NOT NULL,
  `harga_dokter` int NOT NULL,
  `biaya_resep` int NOT NULL,
  `biaya_racik` int NOT NULL,
  `biaya_jasa_lab` int NOT NULL,
  `biaya_tambahan` int NOT NULL,
  `diskon` double NOT NULL,
  `ppn` int NOT NULL,
  `jumlah_byr` int NOT NULL,
  `bayar` int NOT NULL,
  `kembali` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_resep`, `id_users`, `tgl_byr`, `biaya_klinik`, `harga_dokter`, `biaya_resep`, `biaya_racik`, `biaya_jasa_lab`, `biaya_tambahan`, `diskon`, `ppn`, `jumlah_byr`, `bayar`, `kembali`, `created_at`, `updated_at`) VALUES
(18, 27, 1, '2023-02-16', 10000, 0, 14840, 0, 10000, 0, 10, 11, 13857, 100000, 86143, '2023-02-16 00:10:38', '2023-02-16 00:10:38'),
(19, 31, 6, '2023-03-16', 10000, 0, 10000, 15000, 20000, 0, 0, 0, 155000, 155000, 0, '2023-03-16 14:02:36', '2023-03-16 14:02:36'),
(20, 30, 1, '2023-03-16', 10000, 0, 10000, 15000, 20000, 15000, 0, 0, 170000, 170000, 0, '2023-03-16 15:19:33', '2023-03-16 15:19:33'),
(21, 38, 1, '2023-04-19', 15000, 150000, 10000, 15000, 10000, 0, 0, 0, 200000, 200000, 0, '2023-04-19 23:40:26', '2023-04-19 23:40:26'),
(22, 38, 1, '2023-04-19', 15000, 150000, 10000, 15000, 10000, 0, 0, 0, 200000, 200000, 0, '2023-04-19 23:41:04', '2023-04-19 23:41:04'),
(23, 32, 1, '2023-04-19', 15000, 150000, 10000, 15000, 0, 0, 0, 0, 190000, 200000, 10000, '2023-04-19 23:41:22', '2023-04-19 23:41:22'),
(24, 32, 1, '2023-04-19', 15000, 150000, 10000, 15000, 0, 0, 0, 0, 190000, 200000, 10000, '2023-04-19 23:41:38', '2023-04-19 23:41:38'),
(25, 29, 1, '2023-07-07', 15000, 150000, 10000, 15000, 20000, 0, 0, 0, 210000, 210000, 0, '2023-07-07 02:10:13', '2023-07-07 02:10:13');

--
-- Triggers `transaksi`
--
DELIMITER $$
CREATE TRIGGER `sudah_bayar_resep` AFTER INSERT ON `transaksi` FOR EACH ROW BEGIN
	UPDATE resep SET ket_resep='sudah-bayar' WHERE id_resep=NEW.id_resep;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_kasir`
--

CREATE TABLE `transaksi_kasir` (
  `id_transaksi` int NOT NULL,
  `id_users` int NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `kode_transaksi` int NOT NULL,
  `ppn` int NOT NULL,
  `biaya_racik` int NOT NULL,
  `biaya_resep` int NOT NULL,
  `biaya_tambahan` int NOT NULL,
  `total` int NOT NULL,
  `diskon` int NOT NULL DEFAULT '0',
  `bayar` int NOT NULL,
  `kembali` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi_kasir`
--

INSERT INTO `transaksi_kasir` (`id_transaksi`, `id_users`, `tanggal_transaksi`, `kode_transaksi`, `ppn`, `biaya_racik`, `biaya_resep`, `biaya_tambahan`, `total`, `diskon`, `bayar`, `kembali`, `created_at`, `updated_at`) VALUES
(1, 4, '2019-02-04', 1549217225, 0, 0, 0, 0, 2608, 0, 2608, 0, NULL, NULL),
(2, 4, '2019-02-04', 1549283209, 0, 0, 0, 0, 14344, 0, 20000, 5656, NULL, NULL),
(3, 4, '2019-02-04', 1549288800, 0, 0, 0, 0, 1304, 0, 20000, 18696, NULL, NULL),
(4, 4, '2019-02-04', 1549289525, 0, 0, 0, 0, 652, 0, 1000, 348, NULL, NULL),
(5, 4, '2019-02-05', 1549301995, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(6, 4, '2019-02-05', 1549301997, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(7, 4, '2019-02-05', 1549302010, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(8, 4, '2019-02-05', 1549302025, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(9, 4, '2019-02-05', 1549302093, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(10, 4, '2019-02-05', 1549302141, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(11, 4, '2019-02-05', 1549302142, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(12, 4, '2019-02-05', 1549302143, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(13, 4, '2019-02-05', 1549302223, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(14, 4, '2019-02-05', 1549302223, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(15, 4, '2019-02-05', 1549302263, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(16, 4, '2019-02-05', 1549302283, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(17, 4, '2019-02-05', 1549302284, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(18, 4, '2019-02-05', 1549302309, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(19, 4, '2019-02-05', 1549302361, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(20, 4, '2019-02-05', 1549302416, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(21, 4, '2019-02-05', 1549302417, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(22, 4, '2019-02-05', 1549302433, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(23, 4, '2019-02-05', 1549302438, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(24, 4, '2019-02-05', 1549302486, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(25, 4, '2019-02-07', 1549510226, 0, 0, 0, 0, 13040, 0, 14000, 960, NULL, NULL),
(26, 4, '2019-02-07', 1549510279, 0, 0, 0, 0, 127080, 14120, 130000, 2920, NULL, NULL),
(27, 4, '2019-02-07', 1549514136, 0, 0, 0, 0, 1304, 0, 2000, 696, NULL, NULL),
(28, 4, '2019-02-07', 1549514169, 0, 0, 0, 0, 1278, 26, 2000, 722, NULL, NULL),
(29, 4, '2019-02-07', 1549514371, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(30, 4, '2019-02-07', 1549514446, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(31, 4, '2019-02-07', 1549514448, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(32, 4, '2019-02-07', 1549514451, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(33, 4, '2019-02-07', 1549514457, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(34, 4, '2019-02-07', 1549514489, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(35, 4, '2019-02-07', 1549514665, 0, 0, 0, 0, 1278, 26, 10000, 8722, NULL, NULL),
(36, 4, '2019-02-11', 1549890741, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(37, 4, '2019-02-11', 1549890787, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(38, 4, '2019-02-11', 1549890836, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(39, 4, '2019-02-11', 1549890943, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(40, 4, '2019-02-11', 1549890994, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(41, 4, '2019-02-11', 1549890999, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(42, 4, '2019-02-11', 1549891013, 0, 0, 0, 0, 21739, 0, 100000, 78261, NULL, NULL),
(43, 4, '2019-02-13', 1550053321, 0, 0, 0, 0, 136953, 0, 200000, 63047, NULL, NULL),
(44, 4, '2019-02-14', 1550119916, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(45, 4, '2019-02-14', 1550119941, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(46, 4, '2021-06-07', 1623036402, 0, 0, 0, 0, 413041, 21739, 415000, 1959, NULL, NULL),
(47, 4, '2021-06-07', 1623036548, 0, 0, 0, 0, 413041, 21739, 415000, 1959, NULL, NULL),
(48, 4, '2021-06-07', 1623036572, 0, 0, 0, 0, 413041, 21739, 415000, 1959, NULL, NULL),
(49, 4, '2021-06-07', 1623036730, 0, 0, 0, 0, 195651, 21739, 200000, 4349, NULL, NULL),
(50, 4, '2021-06-07', 1623036759, 0, 0, 0, 0, 15217, 0, 200000, 184783, NULL, NULL),
(51, 4, '2021-06-07', 1623047115, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(52, 4, '2021-06-07', 1623047765, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(53, 4, '2021-06-07', 1623047783, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(54, 4, '2021-06-07', 1623047788, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(55, 4, '2021-06-07', 1623047800, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(56, 4, '2021-06-07', 1623047841, 0, 0, 0, 0, 15217, 0, 20000, 4783, NULL, NULL),
(57, 4, '2021-06-07', 1623048379, 0, 0, 0, 0, 217390, 0, 220000, 2610, NULL, NULL),
(58, 4, '2023-02-14', 1676381729, 11, 0, 0, 0, 5550, 5550, 60000, 54450, NULL, NULL),
(59, 4, '2023-02-17', 1676565963, 11, 0, 0, 0, 19980, 2220, 20000, 20, NULL, NULL),
(60, 4, '2023-02-17', 1676620177, 11, 0, 0, 0, 46842, 0, 50000, 3158, NULL, NULL),
(61, 4, '2023-02-17', 1676620319, 11, 0, 0, 0, 33531, 8383, 50000, 16470, NULL, NULL),
(62, 4, '2023-02-17', 1676621633, 11, 0, 0, 0, 37474, 9368, 40000, 2526, NULL, NULL),
(63, 4, '2023-02-17', 1676621661, 11, 0, 0, 0, 37474, 9368, 40000, 2526, NULL, NULL),
(64, 4, '2023-02-17', 1676621713, 11, 0, 0, 0, 37474, 9368, 40000, 2526, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_kasir_det`
--

CREATE TABLE `transaksi_kasir_det` (
  `id_transaksi_det` int NOT NULL,
  `id_transaksi` int NOT NULL,
  `id_obat` int NOT NULL,
  `jumlah` int NOT NULL,
  `bentuk_satuan` varchar(10) NOT NULL,
  `sub_total` int NOT NULL,
  `tipe_transaksi` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Item Kasir Detail';

--
-- Dumping data for table `transaksi_kasir_det`
--

INSERT INTO `transaksi_kasir_det` (`id_transaksi_det`, `id_transaksi`, `id_obat`, `jumlah`, `bentuk_satuan`, `sub_total`, `tipe_transaksi`, `created_at`, `updated_at`) VALUES
(42, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(43, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(44, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(45, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(46, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(47, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(48, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(49, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(50, 43, 24, 1, '', 15217, 'bayar-kredit', '2019-02-13 18:22:01', '2019-02-13 18:22:01'),
(51, 44, 24, 1, '', 15217, 'bayar-kredit', '2019-02-14 12:51:56', '2019-02-14 12:51:56'),
(52, 45, 24, 1, '', 15217, 'bayar-kredit', '2019-02-14 12:52:21', '2019-02-14 12:52:21'),
(53, 47, 32, 10, 'pcs', 217390, 'bayar-tunai', '2021-06-07 11:29:08', '2021-06-07 11:29:08'),
(54, 48, 32, 10, 'pcs', 217390, 'bayar-tunai', '2021-06-07 11:29:32', '2021-06-07 11:29:32'),
(55, 49, 30, 10, 'pcs', 217390, 'bayar-tunai', '2021-06-07 11:32:10', '2021-06-07 11:32:10'),
(56, 57, 30, 10, 'pcs', 217390, 'bayar-kredit', '2021-06-07 14:46:19', '2021-06-07 14:46:19'),
(57, 58, 37, 1, 'pcs', 10000, 'bayar-tunai', NULL, NULL),
(58, 59, 4, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(59, 60, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(60, 60, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(61, 61, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(62, 61, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(63, 62, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(64, 62, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(65, 63, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(66, 63, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(67, 64, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL),
(68, 64, 38, 1, 'pcs', 20000, 'bayar-tunai', NULL, NULL);

--
-- Triggers `transaksi_kasir_det`
--
DELIMITER $$
CREATE TRIGGER `kurang_stok_obat_kasir` AFTER INSERT ON `transaksi_kasir_det` FOR EACH ROW BEGIN
	SET @bobot_satuan := (SELECT bobot_satuan FROM obat WHERE id_obat = NEW.id_obat);
	IF NEW.tipe_transaksi = 'bayar-tunai' THEN
    	IF NEW.bentuk_satuan = 'box' THEN
            UPDATE obat SET stok_obat = stok_obat - (NEW.jumlah * @bobot_satuan) WHERE id_obat = NEW.id_obat;
        ELSE 
            UPDATE obat SET stok_obat = stok_obat - NEW.jumlah WHERE id_obat = NEW.id_obat;
        END IF;
    END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_racik_obat`
--

CREATE TABLE `transaksi_racik_obat` (
  `id_transaksi_racik_obat` int NOT NULL,
  `id_racik_obat_data` int NOT NULL,
  `kode_transaksi` int NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `jam_transaksi` time NOT NULL,
  `diskon` int DEFAULT NULL,
  `harga_total` int NOT NULL,
  `bayar` int NOT NULL,
  `kembalian` int NOT NULL,
  `id_users` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksi_racik_obat`
--

INSERT INTO `transaksi_racik_obat` (`id_transaksi_racik_obat`, `id_racik_obat_data`, `kode_transaksi`, `tanggal_transaksi`, `jam_transaksi`, `diskon`, `harga_total`, `bayar`, `kembalian`, `id_users`, `created_at`, `updated_at`) VALUES
(1, 7, 0, '2023-02-16', '14:33:16', 0, 25000, 50000, 25000, 1, '2023-02-16 14:33:16', '2023-02-16 14:33:16'),
(2, 8, 1676529318, '2023-02-16', '14:35:18', 0, 25000, 50000, 25000, 1, '2023-02-16 14:35:18', '2023-02-16 14:35:18'),
(3, 9, 1676529331, '2023-02-16', '14:35:31', 0, 25000, 50000, 25000, 1, '2023-02-16 14:35:31', '2023-02-16 14:35:31'),
(4, 10, 1676529332, '2023-02-16', '14:35:32', 0, 25000, 50000, 25000, 1, '2023-02-16 14:35:32', '2023-02-16 14:35:32'),
(5, 11, 1676529338, '2023-02-16', '14:35:38', 0, 25000, 50000, 25000, 1, '2023-02-16 14:35:38', '2023-02-16 14:35:38'),
(6, 12, 1676529351, '2023-02-16', '14:35:51', 0, 25000, 50000, 25000, 1, '2023-02-16 14:35:51', '2023-02-16 14:35:51'),
(7, 13, 1676531173, '2023-02-16', '15:06:13', 0, 15000, 100000, 85000, 1, '2023-02-16 15:06:13', '2023-02-16 15:06:13'),
(8, 14, 1676531200, '2023-02-16', '15:06:40', 0, 15000, 100000, 85000, 1, '2023-02-16 15:06:40', '2023-02-16 15:06:40'),
(9, 15, 1676531214, '2023-02-16', '15:06:54', 0, 15000, 100000, 85000, 1, '2023-02-16 15:06:54', '2023-02-16 15:06:54'),
(10, 16, 1676531225, '2023-02-16', '15:07:05', 0, 15000, 100000, 85000, 1, '2023-02-16 15:07:05', '2023-02-16 15:07:05'),
(11, 17, 1676531239, '2023-02-16', '15:07:20', 0, 15000, 100000, 85000, 1, '2023-02-16 15:07:20', '2023-02-16 15:07:20'),
(12, 18, 1676531253, '2023-02-16', '15:07:33', 0, 15000, 100000, 85000, 1, '2023-02-16 15:07:33', '2023-02-16 15:07:33'),
(13, 19, 1676531300, '2023-02-16', '15:08:20', 0, 15000, 100000, 85000, 1, '2023-02-16 15:08:20', '2023-02-16 15:08:20'),
(14, 20, 1676531325, '2023-02-16', '15:08:45', 0, 15000, 100000, 85000, 1, '2023-02-16 15:08:45', '2023-02-16 15:08:45'),
(15, 21, 1676531584, '2023-02-16', '15:13:04', 0, 25000, 25000, 0, 1, '2023-02-16 15:13:04', '2023-02-16 15:13:04'),
(16, 22, 1676621092, '2023-02-17', '16:04:52', 0, 28000, 30000, 2000, 4, '2023-02-17 16:04:52', '2023-02-17 16:04:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_users` int NOT NULL,
  `name` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_user` int NOT NULL COMMENT '5=admin;4=inventory;3=dokter;2=operator;1=resep;0=kasir',
  `active` int NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_delete` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_users`, `name`, `username`, `password`, `level_user`, `active`, `remember_token`, `status_delete`) VALUES
(1, 'Administrator', 'admin', '$2y$10$ljFZgN9zLglmkiEkYtMs3.EGYC1pDIIQoxoRf9h47xdl1ecc2fcim', 5, 1, 'gpuJN2KKiA8JNL1d6rHixwIBF7GmmjhJkczWziS3yf0Zd7HhLEphmmy0Z5mV', 0),
(2, 'Petugas Inventory', 'inventory', '$2y$10$r.CupfbK33nSYB7XJTg42.RpFxhvTcrjAolhj36g6pXW2SQJcyw3e', 4, 1, 'mcU2yZkqS9dY3VR8r3PHqFw54slQodqkvjAbMAaEOem1Zlhx16AD052faBbg', 0),
(3, 'Petugas Resep', 'resep', '$2y$10$j3ieOD3A1qHGaYuA1lAnq..ynX17bbtpt5dQZeYprxF.PNHD3ftNa', 1, 1, 'w2Hf8QabHvCWU9AgXatrGICc58DpSKrlfgXhS6Bssx6NINbOn7S3wxaSI97J', 0),
(4, 'kasir depan', 'kasir', '$2y$10$nQ4q5k8ehbfkMPfIY3uIUeiAG1SLW4cvUQWKlfAuK/.thS7FUfyj.', 0, 1, 'nPOmT9wdnja1u9msd9qZQV4BWYZPPOOoAJqamyzD91cKAKev9DPTOcbW7eFA', 0),
(6, 'Operator', 'operator', '$2y$10$OioNNqYgP0IzpPLacTHAPesQOOyoFGKpv.jl7Kg2.6.o1xOeflHFW', 2, 1, 'lrXIqn8JL9IAzRbIAWPnMZMZw13tyTbZnNOzjtOh8m3KCZtm1nCYWg3Z3o1a', 0),
(7, 'dr. James Andrews', 'james', '$2y$10$/fYACX0En7ep1ypJdDTJku.niXrWSmyZfaalS08eB1NGxGJAowgB2', 3, 1, '11Umc4r9XiCroiWXPReNhgmFTIXfmZlMeN7XMAIkgjsaaYzIlkjkQgr8fHym', 0),
(8, 'dr. OZ', 'oz', '$2y$10$oxS89ZYosxR33HpRz6KpPOEsf.qUIxXGIBlSfRbdsS4EucgSQTctC', 3, 1, 'svm4gc4oArpuEV4LZfv9FnOMP1FpjyCr1PqfDm5xTx9n6j4a4hCV2deiylTI', 0),
(9, 'dr. Aswan', 'aswan', '$2y$10$lqIracOViwsat9EtdYuqh.S0msP60oADxY6ANzThxnSnGgFywSKmO', 3, 1, NULL, 1),
(10, 'dr. Aswan', 'aswanganteng', '$2y$10$X9E4ArGn6xnNDRnhK2bQqeJbhxUVy9bqCXOJbJGr7RQfK.tIQvxGi', 3, 1, 'YnvQYTQCna9zI0R8KdmLAosQ4B6STPaB87AFTeKLuT5nW3hrWm7RxuUwwLTb', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_resep`
--
ALTER TABLE `detail_resep`
  ADD PRIMARY KEY (`id_detail_resep`),
  ADD KEY `id_resep` (`id_resep`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_spesialis_dokter` (`id_spesialis_dokter`);

--
-- Indexes for table `golongan_obat`
--
ALTER TABLE `golongan_obat`
  ADD PRIMARY KEY (`id_golongan_obat`);

--
-- Indexes for table `harga_obat`
--
ALTER TABLE `harga_obat`
  ADD PRIMARY KEY (`id_harga_obat`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `jenis_obat`
--
ALTER TABLE `jenis_obat`
  ADD PRIMARY KEY (`id_jenis_obat`);

--
-- Indexes for table `kategori_pasien`
--
ALTER TABLE `kategori_pasien`
  ADD PRIMARY KEY (`id_kategori_pasien`);

--
-- Indexes for table `kredit`
--
ALTER TABLE `kredit`
  ADD PRIMARY KEY (`id_kredit`);

--
-- Indexes for table `kredit_det`
--
ALTER TABLE `kredit_det`
  ADD PRIMARY KEY (`id_kredit_det`),
  ADD KEY `id_kredit` (`id_kredit`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id_obat`),
  ADD KEY `id_jenis_obat` (`id_jenis_obat`),
  ADD KEY `id_golongan_obat` (`id_golongan_obat`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id_pasien`),
  ADD KEY `id_kategori_pasien` (`id_kategori_pasien`);

--
-- Indexes for table `pemasukan_obat`
--
ALTER TABLE `pemasukan_obat`
  ADD PRIMARY KEY (`id_masuk`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_obat` (`id_obat`),
  ADD KEY `id_supplier` (`id_supplier`);

--
-- Indexes for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD PRIMARY KEY (`id_daftar`),
  ADD KEY `id_pasien` (`id_pasien`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- Indexes for table `pengeluaran_obat`
--
ALTER TABLE `pengeluaran_obat`
  ADD PRIMARY KEY (`id_keluar`),
  ADD KEY `id_users` (`id_users`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  ADD PRIMARY KEY (`id_profile_instansi`);

--
-- Indexes for table `racik_obat`
--
ALTER TABLE `racik_obat`
  ADD PRIMARY KEY (`id_racik_obat`),
  ADD KEY `id_racik_obat_data` (`id_racik_obat_data`);

--
-- Indexes for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  ADD PRIMARY KEY (`id_racik_obat_data`),
  ADD KEY `id_pasien` (`id_pasien`),
  ADD KEY `id_dokter` (`id_dokter`);

--
-- Indexes for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  ADD PRIMARY KEY (`id_racik_obat_detail`),
  ADD KEY `id_racik_obat` (`id_racik_obat`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `racik_obat_sementara`
--
ALTER TABLE `racik_obat_sementara`
  ADD PRIMARY KEY (`id_racik_obat_sementara`);

--
-- Indexes for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  ADD PRIMARY KEY (`id_racik_obat_sementara_detail`),
  ADD KEY `id_racik_obat_sementara` (`id_racik_obat_sementara`);

--
-- Indexes for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  ADD PRIMARY KEY (`id_rekam_medis`);

--
-- Indexes for table `resep`
--
ALTER TABLE `resep`
  ADD PRIMARY KEY (`id_resep`),
  ADD UNIQUE KEY `kode_resep` (`kode_resep`),
  ADD KEY `id_customer` (`id_pasien`),
  ADD KEY `resep_ibfk_2` (`id_users`),
  ADD KEY `id_dokter` (`id_dokter`),
  ADD KEY `id_daftar` (`id_daftar`),
  ADD KEY `resep_ibfk_6` (`id_tindakan_lab`);

--
-- Indexes for table `spesialis_dokter`
--
ALTER TABLE `spesialis_dokter`
  ADD PRIMARY KEY (`id_spesialis_dokter`);

--
-- Indexes for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  ADD PRIMARY KEY (`id_stok_opnem`);

--
-- Indexes for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD PRIMARY KEY (`id_stok_opnem_detail`),
  ADD KEY `id_stok_opnem` (`id_stok_opnem`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `supplier_obat`
--
ALTER TABLE `supplier_obat`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `tindakan_lab`
--
ALTER TABLE `tindakan_lab`
  ADD PRIMARY KEY (`id_tindakan_lab`),
  ADD KEY `id_spesialis_dokter` (`id_spesialis_dokter`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_resep` (`id_resep`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_user` (`id_users`);

--
-- Indexes for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  ADD PRIMARY KEY (`id_transaksi_det`),
  ADD KEY `id_kasir` (`id_transaksi`),
  ADD KEY `id_obat` (`id_obat`);

--
-- Indexes for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  ADD PRIMARY KEY (`id_transaksi_racik_obat`),
  ADD KEY `transaksi_racik_obat_ibfk_1` (`id_racik_obat_data`),
  ADD KEY `id_users` (`id_users`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_resep`
--
ALTER TABLE `detail_resep`
  MODIFY `id_detail_resep` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `dokter`
--
ALTER TABLE `dokter`
  MODIFY `id_dokter` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `golongan_obat`
--
ALTER TABLE `golongan_obat`
  MODIFY `id_golongan_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `harga_obat`
--
ALTER TABLE `harga_obat`
  MODIFY `id_harga_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `jenis_obat`
--
ALTER TABLE `jenis_obat`
  MODIFY `id_jenis_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `kategori_pasien`
--
ALTER TABLE `kategori_pasien`
  MODIFY `id_kategori_pasien` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kredit`
--
ALTER TABLE `kredit`
  MODIFY `id_kredit` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kredit_det`
--
ALTER TABLE `kredit_det`
  MODIFY `id_kredit_det` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id_pasien` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `pemasukan_obat`
--
ALTER TABLE `pemasukan_obat`
  MODIFY `id_masuk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  MODIFY `id_daftar` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `pengeluaran_obat`
--
ALTER TABLE `pengeluaran_obat`
  MODIFY `id_keluar` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `profile_instansi`
--
ALTER TABLE `profile_instansi`
  MODIFY `id_profile_instansi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `racik_obat`
--
ALTER TABLE `racik_obat`
  MODIFY `id_racik_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  MODIFY `id_racik_obat_data` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  MODIFY `id_racik_obat_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `racik_obat_sementara`
--
ALTER TABLE `racik_obat_sementara`
  MODIFY `id_racik_obat_sementara` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  MODIFY `id_racik_obat_sementara_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `rekam_medis`
--
ALTER TABLE `rekam_medis`
  MODIFY `id_rekam_medis` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resep`
--
ALTER TABLE `resep`
  MODIFY `id_resep` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `spesialis_dokter`
--
ALTER TABLE `spesialis_dokter`
  MODIFY `id_spesialis_dokter` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stok_opnem`
--
ALTER TABLE `stok_opnem`
  MODIFY `id_stok_opnem` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  MODIFY `id_stok_opnem_detail` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `supplier_obat`
--
ALTER TABLE `supplier_obat`
  MODIFY `id_supplier` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tindakan_lab`
--
ALTER TABLE `tindakan_lab`
  MODIFY `id_tindakan_lab` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  MODIFY `id_transaksi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  MODIFY `id_transaksi_det` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  MODIFY `id_transaksi_racik_obat` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_users` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_resep`
--
ALTER TABLE `detail_resep`
  ADD CONSTRAINT `detail_resep_ibfk_1` FOREIGN KEY (`id_resep`) REFERENCES `resep` (`id_resep`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_resep_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `dokter_ibfk_2` FOREIGN KEY (`id_spesialis_dokter`) REFERENCES `spesialis_dokter` (`id_spesialis_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `harga_obat`
--
ALTER TABLE `harga_obat`
  ADD CONSTRAINT `harga_obat_ibfk_1` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kredit_det`
--
ALTER TABLE `kredit_det`
  ADD CONSTRAINT `kredit_det_ibfk_1` FOREIGN KEY (`id_kredit`) REFERENCES `kredit` (`id_kredit`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kredit_det_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`id_jenis_obat`) REFERENCES `jenis_obat` (`id_jenis_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `obat_ibfk_2` FOREIGN KEY (`id_golongan_obat`) REFERENCES `golongan_obat` (`id_golongan_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `pasien`
--
ALTER TABLE `pasien`
  ADD CONSTRAINT `pasien_ibfk_1` FOREIGN KEY (`id_kategori_pasien`) REFERENCES `kategori_pasien` (`id_kategori_pasien`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `pemasukan_obat`
--
ALTER TABLE `pemasukan_obat`
  ADD CONSTRAINT `pemasukan_obat_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasukan_obat_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pemasukan_obat_ibfk_3` FOREIGN KEY (`id_supplier`) REFERENCES `supplier_obat` (`id_supplier`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `pendaftaran`
--
ALTER TABLE `pendaftaran`
  ADD CONSTRAINT `pendaftaran_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftaran_ibfk_2` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `pengeluaran_obat`
--
ALTER TABLE `pengeluaran_obat`
  ADD CONSTRAINT `pengeluaran_obat_ibfk_1` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pengeluaran_obat_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat`
--
ALTER TABLE `racik_obat`
  ADD CONSTRAINT `racik_obat_ibfk_1` FOREIGN KEY (`id_racik_obat_data`) REFERENCES `racik_obat_data` (`id_racik_obat_data`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_data`
--
ALTER TABLE `racik_obat_data`
  ADD CONSTRAINT `racik_obat_data_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON UPDATE CASCADE,
  ADD CONSTRAINT `racik_obat_data_ibfk_2` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_detail`
--
ALTER TABLE `racik_obat_detail`
  ADD CONSTRAINT `racik_obat_detail_ibfk_1` FOREIGN KEY (`id_racik_obat`) REFERENCES `racik_obat` (`id_racik_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `racik_obat_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON UPDATE CASCADE;

--
-- Constraints for table `racik_obat_sementara_detail`
--
ALTER TABLE `racik_obat_sementara_detail`
  ADD CONSTRAINT `racik_obat_sementara_detail_ibfk_1` FOREIGN KEY (`id_racik_obat_sementara`) REFERENCES `racik_obat_sementara` (`id_racik_obat_sementara`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `resep`
--
ALTER TABLE `resep`
  ADD CONSTRAINT `resep_ibfk_1` FOREIGN KEY (`id_pasien`) REFERENCES `pasien` (`id_pasien`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_ibfk_2` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_ibfk_4` FOREIGN KEY (`id_dokter`) REFERENCES `dokter` (`id_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_ibfk_5` FOREIGN KEY (`id_daftar`) REFERENCES `pendaftaran` (`id_daftar`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `resep_ibfk_6` FOREIGN KEY (`id_tindakan_lab`) REFERENCES `tindakan_lab` (`id_tindakan_lab`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `stok_opnem_detail`
--
ALTER TABLE `stok_opnem_detail`
  ADD CONSTRAINT `stok_opnem_detail_ibfk_1` FOREIGN KEY (`id_stok_opnem`) REFERENCES `stok_opnem` (`id_stok_opnem`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `stok_opnem_detail_ibfk_2` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON UPDATE CASCADE;

--
-- Constraints for table `tindakan_lab`
--
ALTER TABLE `tindakan_lab`
  ADD CONSTRAINT `tindakan_lab_ibfk_1` FOREIGN KEY (`id_spesialis_dokter`) REFERENCES `spesialis_dokter` (`id_spesialis_dokter`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_resep`) REFERENCES `resep` (`id_resep`) ON DELETE RESTRICT ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_kasir`
--
ALTER TABLE `transaksi_kasir`
  ADD CONSTRAINT `id_users` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_kasir_det`
--
ALTER TABLE `transaksi_kasir_det`
  ADD CONSTRAINT `id_obat` FOREIGN KEY (`id_obat`) REFERENCES `obat` (`id_obat`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_transaksi` FOREIGN KEY (`id_transaksi`) REFERENCES `transaksi_kasir` (`id_transaksi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_racik_obat`
--
ALTER TABLE `transaksi_racik_obat`
  ADD CONSTRAINT `transaksi_racik_obat_ibfk_1` FOREIGN KEY (`id_racik_obat_data`) REFERENCES `racik_obat_data` (`id_racik_obat_data`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_racik_obat_ibfk_3` FOREIGN KEY (`id_users`) REFERENCES `users` (`id_users`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
