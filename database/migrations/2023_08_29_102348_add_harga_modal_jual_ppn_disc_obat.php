<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHargaModalJualPpnDiscObat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obat', function(Blueprint $table) {
            $table->dropColumn('nomor_batch');
            $table->integer('ppn')->after('harga_obat');
            $table->double('disc')->after('harga_obat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obat', function(Blueprint $table) {
            $table->dropColumn(['ppn','disc']);
        });
    }
}
