@php
$level = Auth::user()->level_user == 5 ? 'admin' : (Auth::user()->level_user == 4 ? 'inventory' : (Auth::user()->level_user == 3 ? 'dokter' : (Auth::user()->level_user == 2 ? 'operator' : (Auth::user()->level_user == 1 ? 'resep' : ''))));
@endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('assets/dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">{{-- 
    <style>
    .content-header > h1 {
      color:white;
    }
    </style> --}}
    <style>
      select.select2 {
        position:static !important;
        outline:none !important;
      }
    </style>
</head>
<body class="hold-transition skin-green sidebar-mini fixed">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>MF</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Mustang</b>Farma</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
          <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                          <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                            <p>
                              {{Auth::user()->name}}
                            </p>
                          </li>
                          <li class="user-footer">
                            <div class="pull-left">
                              <a href="{{url('/'.$level.'/ubah-profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                              <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Logout</a>
                            </div>
                          </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li @if(isset($page)){!!$page=='dashboard'?'class="active"':''!!}@endif>
          <a href="{{url('/'.$level.'/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        @if($level == 'admin' || $level == 'operator')
        <li class="treeview @if(isset($link)){!!$link=='pasien'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-users"></i> <span>Pasien</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if (isset($page)){!!$page=='data-pasien'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-pasien')}}">
                <i class="fa fa-circle-o"></i> <span>Data Pasien</span>
              </a>
            </li>
            @if ($level == 'admin')
            <li @if (isset($page)){!!$page=='data-kategori-pasien'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-kategori-pasien')}}">
                <i class="fa fa-circle-o"></i> <span>Kategori Pasien</span>
              </a>
            </li>
            @endif
          </ul>
        </li>
          @if ($level == 'admin')
            <li @if (isset($page)){!!$page=='data-dokter'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-dokter')}}">
                <i class="fa fa-user-md"></i> <span>Dokter</span>
              </a>
            </li>
          @endif
        @endif
        @if ($level == 'admin' || $level == 'operator')
        <li @if (isset($page)){!!$page=='pendaftaran'?'class="active"':''!!}@endif>
          <a href="{{url('/'.$level.'/pendaftaran')}}">
            <i class="fa fa-id-badge"></i> <span>Pendaftaran</span>
          </a>
        </li>
        @endif
        @if ($level == 'admin' || $level == 'inventory')
        <li class="treeview @if(isset($link)){!!$link=='obat'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-medkit"></i> <span>Obat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if ($level=='admin')
            <li @if(isset($page)){!!$page=='data-golongan-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/data-golongan-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Golongan Obat</span>
              </a>
            </li>
            @endif
            <li @if(isset($page)){!!$page=='data-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Obat</span>
              </a>
            </li>
            @if($level=='admin')
            <li @if(isset($page)){!!$page=='data-jenis-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-jenis-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Jenis Obat</span>
              </a>
            </li>
            @endif
            <li @if(isset($page)){!!$page=='data-masuk'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-masuk-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Pemasukan Obat</span>
              </a>
            </li>
            <li @if(isset($page)){!!$page=='data-keluar'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-keluar-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Pengeluaran Obat</span>
              </a>
            </li>
            <li @if(isset($page)){!!$page=='data-supplier-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/data-supplier-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Supplier Obat</span>
              </a>
            </li>
          </ul>
        </li>
        @endif
        <li @if (isset($page)){!!$page=='stok-opnem'?'class="active"':''!!}@endif>
          <a href="{{ url('/'.$level.'/stok-opnem') }}">
            <i class="fa fa-database"></i> <span>Stok Opnem</span>
          </a>
        </li>
        @if($level == 'admin' || $level == 'operator')
        <li @if(isset($page)){!!$page=='resep'?'class="active"':''!!}@endif>
          <a href="{{url('/'.$level.'/data-resep')}}">
            <i class="fa fa-book"></i> <span>Resep</span>
          </a>
        </li>
        @endif
        @if ($level == 'admin')
        <li class="treeview @if (isset($link)){{$link=='transaksi'?'active menu-open':''}}@endif">
          <a href="#">
            <i class="fa fa-exchange"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if (isset($page)){!!$page=='transaksi'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/data-transaksi')}}">
                <i class="fa fa-circle-o"></i> <span>Data Transaksi</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='tindakan-lab'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/tindakan-lab')}}">
                <i class="fa fa-circle-o"></i> <span>Tindakan Lab</span>
              </a>
            </li>
          </ul>
        </li>
        @elseif($level == 'resep')
        <li @if(isset($page)){!!$page=='transaksi'?'class="active"':''!!}@endif>
          <a href="{{url('/resep/data-transaksi')}}">
            <i class="fa fa-exchange"></i> <span>Transaksi</span>
          </a>
        </li>
        @endif
        @if ($level == 'admin')
        <li @if(isset($page)){!!$page=='user'?'class="active"':''!!}@endif>
            <a href="{{url('/admin/data-users')}}">
                <i class="fa fa-users"></i> <span>Data Users</span>
            </a>
        </li>
        @endif
        <li class="treeview @if (isset($link)){{$link=='laporan'?'active menu-open':''}}@endif">
          <a href="#">
            <i class="fa fa-file"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if ($level == 'admin' || $level == 'operator')
            <li @if (isset($page)){!!$page=='laporan-pasien'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-pasien')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Pasien</span>
              </a>
            </li>
            @endif
            @if ($level == 'admin')
            <li @if (isset($page)){!!$page=='laporan-kategori-pasien'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/laporan-kategori-pasien')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Kategori Pasien</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-dokter'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/laporan-dokter')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Dokter</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-tindakan-lab'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/laporan-tindakan-lab')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Tindakan Lab</span>
              </a>
            </li>
            @endif
            @if ($level == 'admin' || $level == 'operator')
            <li @if (isset($page)){!!$page=='laporan-pendaftaran'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-pendaftaran')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Pendaftaran</span>
              </a>
            </li>
            @endif
            @if ($level == 'dokter')
            <li @if (isset($page)){!!$page=='laporan-rekam-medis'?'class="active"':''!!}@endif>
              <a href="{{url('/dokter/laporan-rekam-medis')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Rekam Medis</span>
              </a>
            </li>
            @endif
            @if ($level == 'admin')
            <li @if (isset($page)){!!$page=='laporan-golongan-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/admin/laporan-golongan-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Golongan Obat</span>
              </a>
            </li>
            @endif
            @if ($level == 'inventory' || $level == 'admin')
            <li @if (isset($page)){!!$page=='laporan-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Obat</span>
              </a>
            </li>
              @if($level == 'admin')
              <li @if (isset($page)){!!$page=='laporan-jenis-obat'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-jenis-obat')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Jenis Obat</span>
                </a>
              </li>
              @endif
              @if ($level == 'inventory' || $level == 'admin')
              <li @if (isset($page)){!!$page=='laporan-order-obat'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-order-obat')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Order Obat</span>
                </a>
              </li>
              <li @if (isset($page)){!!$page=='laporan-stok-opnem'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-stok-opnem')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Stok Opnem</span>
                </a>
              </li>
              <li @if (isset($page)){!!$page=='laporan-supplier'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-supplier')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Supplier</span>
                </a>
              </li>
              <li @if (isset($page)){!!$page=='laporan-pemasukan'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-pemasukan')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Pemasukan Obat</span>
                </a>
              </li>
              <li @if (isset($page)){!!$page=='laporan-pembelian'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-pembelian')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Pembelian Obat</span>
                </a>
              </li>
              <li @if (isset($page)){!!$page=='laporan-pengeluaran'?'class="active"':''!!}@endif>
                <a href="{{url('/'.$level.'/laporan-pengeluaran')}}">
                  <i class="fa fa-circle-o"></i> <span>Laporan Pengeluaran Obat</span>
                </a>
              </li>
              @endif
            @endif
            @if ($level == 'admin' || $level == 'operator')
            <li @if (isset($page)){!!$page=='laporan-resep'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-resep')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Resep</span>
              </a>
            </li>
            @endif
            @if ($level == 'admin' || $level == 'resep')
            <li @if (isset($page)){!!$page=='laporan-transaksi'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-transaksi')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Transaksi</span>
              </a>
            </li>
              @if ($level == 'admin')
                <li @if (isset($page)){!!$page=='laporan-total-penjualan'?'class="active"':''!!}@endif>
                  <a href="{{url('/admin/laporan-total-penjualan')}}">
                    <i class="fa fa-circle-o"></i> <span>Laporan Total Penjualan</span>
                  </a>
                </li>
              @endif
            @endif
            @if ($level == 'admin')
            <li @if (isset($page)){!!$page=='laporan-users'?'class="active"':''!!}@endif>
              <a href="{{url('/'.$level.'/laporan-users')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Users</span>
              </a>
            </li>
            @endif
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
