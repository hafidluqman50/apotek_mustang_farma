@extends('layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Transaksi</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/resep/data-transaksi/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Data Customer Resep</label>
								<select name="customer" id="customer" class="form-control select2" required="required" autofocus="autofocus">
									<option value="" selected="selected" disabled="disabled">=== Pilih Data Customer Resep ===</option>
									@foreach ($resep as $element)
									<option value="{{$element->id_resep}}">{{$element->kode_resep.' | '.human_date($element->tgl_resep).' | '.$element->nama_dokter.' | '.$element->nama_pasien.' | '.$element->jenis_kelamin}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Tindakan Lab</label>
								<select name="tindakan_lab" class="form-control select2">
									<option value="" selected="selected" disabled="disabled">=== Pilih Tindakan Lab ===</option>
									@foreach ($tindakan_lab as $element)
									<option value="{{$element->id_tindakan_lab}}">{{$element->nama_tindakan}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Biaya Resep</label>
								<input type="number" name="biaya_resep" class="form-control" placeholder="Biaya Resep" readonly="readonly" required="required">
								<label for="" id="biaya_resep">Rp. 0</label>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Biaya Dokter</label>
								<input type="number" name="biaya_dokter" class="form-control" placeholder="Isi Biaya Jasa Dokter" required="required" readonly="readonly">
								<label for="" id="biaya_dokter">Rp. 0</label>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Biaya Klinik</label>
								<input type="number" name="biaya_klinik" class="form-control" placeholder="Isi Biaya Klinik" required="required" readonly="readonly">
								<label for="" id="biaya_klinik">Rp. 0</label>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Biaya Jasa Lab</label>
								<input type="number" name="biaya_jasa_lab" class="form-control" placeholder="Isi Biaya Jasa Lab" required="required" readonly="readonly">
								<label for="" id="biaya_jasa_lab">Rp. 0</label>
							</div>
						</div>
						<div class="col-md-6">
							<label for="">Diskon</label>
							<div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
								<input type="number" class="form-control" id="diskon" placeholder="Isi Diskon; Ex:50;">
								<span class="input-group-addon">%</span>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Total Harga</label>
								<input type="number" name="total_harga" class="form-control" placeholder="Total Harga" readonly="readonly">
								<label for="" id="total_harga">Rp. 0</label>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Bayar</label>
								<input type="number" name="bayar" class="form-control" placeholder="Bayar" required="required">
								<label for="" id="bayar">Rp. 0</label>
							</div>
							<div class="form-group" style="border-bottom:1px solid lightgrey;">
								<label for="">Kembalian</label>
								<input type="number" name="kembali" class="form-control" placeholder="Kembalian" readonly="readonly">
								<label for="" id="kembalian">Rp. 0</label>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
		$('#customer').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-harga/'+val;
			$.ajax({
				url: getUrl,
			})
			.done(function(done) {
				var biaya_klinik     = Math.round(done.biaya_dokter * 10 / 100);
				var biaya_dokter     = done.biaya_dokter;
				var biaya_total_obat = done.total_obat;
				var total            = biaya_klinik + biaya_dokter + biaya_total_obat;
				if ($('#diskon').val() != '') {
					var diskon = parseInt($('#diskon').val());
					total = ((biaya_klinik + biaya_dokter + biaya_total_obat) * diskon) / 100;
				}
				$('input[name="biaya_resep"]').val(biaya_total_obat);
				$('#biaya_resep').html(rupiah_format(biaya_total_obat));
				$('input[name="biaya_dokter"]').val(biaya_dokter);
				$('#biaya_dokter').html(rupiah_format(biaya_dokter));
				$('input[name="biaya_klinik"]').val(biaya_klinik);
				$('#biaya_klinik').html(rupiah_format(biaya_klinik));
				$('input[name="total_harga"]').val(total);
				$('#total_harga').html(rupiah_format(total));
			})
			.fail(function(fail) {
				console.log(fail);
			});
		});
		
		$('select[name="tindakan_lab"]').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-biaya-lab/'+val;
			$.ajax({
				url: getUrl,
			})
			.done(function(done) {
				$('input[name="biaya_jasa_lab"]').val(done);
				$('#biaya_jasa_lab').html(rupiah_format(done));
				var get = $('input[name="total_harga"]').val();
				var total = parseInt(get) + parseInt(done);
				if ($('#diskon').val() != '') {
					var diskon = parseInt($('#diskon').val());
					total = ((parseInt(get) + parseInt(done)) * diskon) / 100;
				}
				$('input[name="total_harga"]').val(total);
				$('#total_harga').html(rupiah_format(total));
			})
			.fail(function(fail) {
				console.log(fail);
			});
		});

		$('#diskon').change(function(){
			var val_diskon = parseInt($(this).val());
			var val_total = parseInt($('input[name="total_harga"]').val());
			var total = (val_total * val_diskon) / 100;
			$('input[name="total_harga"]').val(total);
			$('#total_harga').html(rupiah_format(total));
		});

		$('input[name="bayar"]').keyup(function(){
			var val_total = parseInt($('input[name="total_harga"]').val());
			var val_bayar = parseInt($(this).val());
			$('input[name="kembali"]').val(val_bayar - val_total);
			$('#bayar').html(rupiah_format(val_bayar));
			$('#kembalian').html(rupiah_format(val_bayar - val_total));
		});
	});
</script>
@endsection