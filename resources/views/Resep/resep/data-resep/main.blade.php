@extends('layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Resep</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover data-resep force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Kode Resep</th>
							<th>Tanggal Resep</th>
							<th>Nama Dokter</th>
							<th>Nama Pasien</th>
							<th>Kategori Pasien</th>
							{{-- <th>Umur Pasien</th> --}}
							<th>Jenis Kelamin</th>
							<th>Input By</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var resep = $('.data-resep').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-resep') }}",
            columns:[
                {data:'id_resep',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'kode_resep',name:'kode_resep'},
                {data:'tgl_resep',name:'tgl_resep'},
                {data:'nama_dokter',name:'nama_dokter'},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'nama_kategori',name:'nama_kategori'},
                // {data:'umur_pasien',name:'umur_pasien'},
                {data:'jenis_kelamin',name:'jenis_kelamin'},
                {data:'name',name:'name'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        resep.on( 'order.dt search.dt', function () {
	        resep.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection