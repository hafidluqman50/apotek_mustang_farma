@extends('layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Resep</h1>
</section>

<section class="content">
	<form action="{{url('/resep/data-resep/save')}}" method="POST">
		{{csrf_field()}}
		<div class="row">
			@if (session()->has('berhasil'))
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					{{session('berhasil')}} <button class="close" data-dismiss="alert">X</button>
				</div>
			</div>
			@elseif(session()->has('habis'))
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible">
					<button class="close" data-dismiss="alert">X</button>
					<ul>
					@foreach (session('habis') as $key => $element)
						<li>{!!$element['text']!!}</li>
					@endforeach
					</ul>
				</div>
			</div>
			@endif
			<div class="col-md-4">
				<div class="box box-primary">
					<div class="box-header with-border">
						<button type="button" class="btn btn-info" id="input-customer">
							Input Pasien
						</button>
						<button type="button" class="btn btn-danger button-hide" id="batal-customer">
							Batal Input
						</button>
					</div>
					<div class="box-body">
						<div id="form-customer">
							<div class="form-group">
								<label for="">Dokter</label>
								<select name="dokter" class="form-control select2" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
									@foreach ($dokter as $element)
									<option value="{{$element->id_dokter}}" @if (old('dokter') !== null){!!$element->id_dokter == old('dokter')?'selected="selected"':''!!}@endif>{{$element->nama_dokter.' | '.$element->spesialis_poli}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Pasien</label>
								<select name="pasien" class="form-control select2" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Pasien ===</option>
									@foreach ($pasien->all() as $element)
									<option value="{{$element->id_pasien}}" @if(old('nama_pasien') !== null && old('tanggal_lahir') !== null){!!$pasien->idPasien(old('nama_pasien'),old('tanggal_lahir')) == $element->id_pasien ? 'selected="selected"' : ''!!} @elseif(old('pasien') !== null){!!$element->id_pasien == old('pasien') ? 'selected="selected"' : ''!!}@endif>{{$element->kode_pasien.' | '.$element->nama_pasien.' | '.hitung_umur($element->tanggal_lahir).' | '.$element->jenis_kelamin}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Kategori Pasien</label>
								<select name="kategori_pasien" class="form-control" disabled="disabled" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Kategori Pasien ===</option>
									@if (session()->has('habis'))
										@php
											if (old('pasien') !== null) {
												$id_pasien = old('pasien');
											}
											elseif (old('nama_pasien') !== null && old('tanggal_lahir') !== null) {
												$id_pasien = $pasien->idPasien(old('nama_pasien'),old('tanggal_lahir'));
											}
										@endphp
										<option value="{{$pasien->ktgPasien($id_pasien)->id_kategori_pasien}}" selected="selected">
											{{$pasien->ktgPasien($id_pasien)->nama_kategori}}
										</option>
									@endif
								</select>
							</div>
						</div>
						<div class="form-hide" id="form-hide-customer">
							<div class="form-group">
								<label for="">Dokter</label>
								<select name="dokter_input" class="form-control select2">
									<option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
									@foreach ($dokter as $element)
									<option value="{{$element->id_dokter}}">{{$element->nama_dokter}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Kategori Pasien</label>
								<select name="kategori_pasien_input" class="form-control select2">
									<option value="" selected="selected" disabled="disabled">=== Pilih Kategori ===</option>
									@foreach ($kategori_pasien->all() as $element)
									<option value="{{$element->id_kategori_pasien}}">{{$element->nama_kategori}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Nama Pasien</label>
								<input type="text" name="nama_pasien" class="form-control" placeholder="Isi Nama Pasien">
							</div>
							<div class="form-group">
								<label for="">Jenis Kelamin</label>
								<select name="jenis_kelamin" class="form-control">
									<option selected="selected" disabled="disabled">=== Pilih Jenis Kelamin ===</option>
									<option value="Laki - Laki">Laki - Laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Tanggal Lahir</label>
								<input type="date" name="tanggal_lahir" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Alamat Pasien</label>
								<textarea name="alamat_pasien" class="form-control" placeholder="Isi Alamat Pasien" cols="30" rows="10"></textarea>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-body">
						@if (session()->has('obat_habis'))
							@for ($i = 0; $i < count(session('obat_habis')); $i++)
								<div class="form-group" id="resep">
									<button type="button" class="btn btn-info pencet" style="margin-bottom:10px;" id="input-obat" data-id="{{$i+1}}">
										Input Obat
									</button>
									<button type="button" class="btn btn-danger button-hide destroy" style="margin-bottom:10px;" id="hapus-input-obat" data-id="{{$i+1}}">
										Batal Input Obat
									</button>
									<div class="coba" id="racik-obat" get-id="{{$i+1}}">
										<div class="col-md-3 padding-left-right-gone">
											<select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 sip" ajax-id="{{$i+1}}" required="required">
												<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
												@foreach ($jenis_obat as $element)
												<option value="{{$element->id_jenis_obat}}" @if(session('jenis_obat')[$i]['jenis_obat'] == $element->id_jenis_obat) selected="selected" @endif>{{$element->nama_jenis_obat}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<select name="obat[]" class="form-control select2" id="oke" get-ajax-id="{{$i+1}}" required="required">
												<option value="" selected="selected" disabled="disabled">=== Pilih Obat ===</option>
												@foreach ($obat->getByJenis(session('jenis_obat')[$i]['jenis_obat']) as $element)
												<option value="{{$element->id_obat}}" @if(session('obat_habis')[$i]['id_obat'] == $element->id_obat) selected="selected" @endif>{{$element->nomor_batch.' | '.$element->nama_obat.' | '.golongan_obat($element->golongan_obat)}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<input type="text" name="dosis_obat[]" class="form-control" placeholder="Isi Dosis Obat" value="{{session('dosis_obat')[$i]['dosis_obat']}}" required="required">
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<input type="number" name="banyak_obat[]" class="form-control" placeholder="Isi Banyak Obat" value="{{session('banyak_obat')[$i]['banyak_obat']}}" required="required">
										</div>
									</div>
									<div class="coba-juga form-hide" id="input-racik" get-id="{{$i+1}}">
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Nomor Batch</label>
												<input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch">
											</div>
											<div class="form-group">
												<label for="">Nama Obat</label>
												<input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat">
											</div>
											<div class="form-group">
												<label for="">Jenis Obat</label>
												<select name="jenis_input_obat[]" class="form-control select2">
													<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
													</option>
													@foreach ($jenis_obat as $element)
													<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="">Tanggal Expired</label>
												<input type="date" name="tanggal_expired[]" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Harga Obat</label>
												<input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat">
											</div>
											<div class="form-group">
												<label for="">Stok Obat</label>
												<input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat">
											</div>
											<div class="form-group">
												<label for="">Dosis Obat</label>
												<input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat">
											</div>
											<div class="form-group">
												<label for="">Banyak Beli Obat</label>
												<input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat">
											</div>
										</div>
									</div>
								</div>
							@endfor
						@else
							<div class="form-group" id="resep">
								<button type="button" class="btn btn-info pencet" style="margin-bottom:10px;" id="input-obat" data-id="1">
									Input Obat
								</button>
								<button type="button" class="btn btn-danger button-hide destroy" style="margin-bottom:10px;" id="hapus-input-obat" data-id="1">
									Batal Input Obat
								</button>
								<div class="coba" id="racik-obat" get-id="1">
									<div class="col-md-3 padding-left-right-gone">
										<select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 sip" ajax-id="1" required="required">
											<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
											@foreach ($jenis_obat as $element)
											<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<select name="obat[]" class="form-control select2" id="oke" get-ajax-id="1" disabled="disabled" required="required">
											<option selected="selected" disabled="disabled">=== Pilih Obat ===</option>
										</select>
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<input type="text" name="dosis_obat[]" class="form-control" placeholder="Isi Dosis Obat" required="required">
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<input type="number" name="banyak_obat[]" class="form-control" placeholder="Isi Banyak Obat" required="required">
									</div>
								</div>
								<div class="coba-juga form-hide" id="input-racik" get-id="1">
									<div class="col-md-6">
										<div class="form-group">
											<label for="">Nomor Batch</label>
											<input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch">
										</div>
										<div class="form-group">
											<label for="">Nama Obat</label>
											<input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat">
										</div>
										<div class="form-group">
											<label for="">Jenis Obat</label>
											<select name="jenis_input_obat[]" class="form-control select2">
												<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
												</option>
												@foreach ($jenis_obat as $element)
												<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="">Tanggal Expired</label>
											<input type="date" name="tanggal_expired[]" class="form-control">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="">Harga Obat</label>
											<input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat">
										</div>
										<div class="form-group">
											<label for="">Stok Obat</label>
											<input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat">
										</div>
										<div class="form-group">
											<label for="">Dosis Obat</label>
											<input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat">
										</div>
										<div class="form-group">
											<label for="">Banyak Beli Obat</label>
											<input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat">
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
					<div class="box-footer with-border">
						<button type="button" class="btn btn-primary" id="tambah-obat">
							Tambah Obat
						</button>
						<button type="button" class="btn btn-danger button-hide" id="hapus-obat">
							Hapus Obat
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@endsection

@section('js')
<script>
	$(function(){
		var select_ajax = 2;
		var get_ajax    = 2;
		var num_input   = 2;
		var num_cancel  = 2;
		var num_racik   = 2;
		var num_manual  = 2;

		$('.kategori-pasien').select2({disabled:true});

		$('select[name="pasien"]').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-kategori-pasien/'+val;
			$.ajax({
				url: getUrl
			})
			.done(function(done) {
				$('select[name="kategori_pasien"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$('select[name="kategori_pasien"]').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-pasien/'+val;
			$.ajax({
				url: getUrl
			})
			.done(function(done) {
				$('select[name="pasien"]').removeAttr('disabled');
				$('select[name="pasien"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$('#input-customer').click(function(){
			$('#form-customer').slideUp(function(){
				$('#form-customer').find('input,select').removeAttr('required');
			});
			$('#form-hide-customer').slideDown(function(){
				$('#form-hide-customer').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('#batal-customer').show();
		});

		$('#batal-customer').click(function(){
			$('#form-hide-customer').slideUp(function(){
				$('#form-hide-customer').find('input,select').removeAttr('required');
			});
			$('#form-customer').slideDown(function(){
				$('#form-customer').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('#input-customer').show();
		});

		$('#tambah-obat').click(function() {
			$('select[name="obat[]"]').select2('destroy');
			$('.sip').select2('destroy');
			$('#hapus-obat').show();
			$('#input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_input++).find('input').val('');
			$('#hapus-input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_cancel++);
			$('#racik-obat').clone().appendTo($('#resep')).attr('get-id',num_racik++);
			$('#input-racik').clone().appendTo($('#resep')).attr('get-id',num_manual++).find('input').val('');
			$('#pilih-jenis').attr('ajax-id',select_ajax++);
			$('#oke').attr('get-ajax-id',get_ajax++);
			$('.sip').select2();
			$('select[name="obat[]"]').select2();
		});

		$(document).on('change','.sip',function(){
			var getAttr = $(this).attr('ajax-id');
			var getVal  = $(this).val();
			var getUrl  = '..'+'/ajax/get-obat/'+getVal+'/klinik';
			$.ajax({
				url: getUrl,
			})
			.done(function(done) {
				$('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').removeAttr('disabled');
				$('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$(document).on('click','.pencet',function(){
			var attr = $(this).attr('data-id');
			$('.coba[get-id="'+attr+'"]').slideUp(function(){
				$('.coba[get-id="'+attr+'"]').find('input,select').removeAttr('required');
			});
			$('.coba-juga[get-id="'+attr+'"]').slideDown(function(){
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('.destroy[data-id="'+attr+'"]').show();
		});

		$(document).on('click','.destroy',function(){
			var attr = $(this).attr('data-id');
			$('.coba-juga[get-id="'+attr+'"]').slideUp(function(){
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('required');
			});
			$('.coba[get-id="'+attr+'"]').slideDown(function(){
				$('.coba[get-id="'+attr+'"]').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('.pencet[data-id="'+attr+'"]').show();
		});

		$('#hapus-obat').click(function(){
			$('#input-obat').remove().eq(1);
			$('#hapus-input-obat').remove().eq(1);
			$('#racik-obat').remove().eq(1);
			$('#input-racik').remove().eq(1);
			if ($('.coba').length == 1 && $('.coba-juga').length == 1) {
				$(this).hide();
			}
		});
	});
</script>
@endsection