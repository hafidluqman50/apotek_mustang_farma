@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Data Rujukan</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="col-md-5">
                        <label for="">Dari : </label>
                        <input type="text" name="from" class="form-control datepicker" placeholder="dd-mm-yyyy">
                    </div>
                    <div class="col-md-5">
                        <label for="">Sampai : </label>
                        <input type="text" name="to" class="form-control datepicker" placeholder="dd-mm-yyyy">
                    </div>
                    <div class="col-md-2">
                        <button id="cari" class="btn btn-primary" style="margin-top:15%;">Cari</button>
                    </div>
                </div>
                <div class="box-body">
                    @if (session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                    </div>
                    @endif
                    {{-- <div style="margin-bottom:5%;"> --}}
                    {{-- </div> --}}
                    <table class="table table-hover data-rujukan force-fullwidth">
                        <thead>
                            <th>No.</th>
                            <th>No BPJS</th>
                            <th>Nama Pasien</th>
                            <th>Nama Dokter</th>
                            <th>Tinggi Badan</th>
                            <th>Berat Badan</th>
                            <th>Suhu Badan</th>
                            <th>Tekanan Darah</th>
                            <th>Diagnosa</th>
                            <th>Rujukan</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var rujukan = $('.data-rujukan').DataTable({
            processing:true,
            serverSide:true,
            ajax:{
                url:"{{ url('/datatables/data-rujukan') }}",
                data:function(d) {
                    d.from = $('input[name="from"]').val();
                    d.to = $('input[name="to"]').val();
                }
            },
            columns:[
                {data:'id_resep',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'no_bpjs',name:'no_bpjs'},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'nama_dokter',name:'nama_dokter'},
                {data:'tinggi_badan',name:'tinggi_badan'},
                {data:'berat_badan',name:'berat_badan'},
                // {data:'umur_pasien',name:'umur_pasien'},
                {data:'suhu_badan',name:'suhu_badan'},
                {data:'tekanan_darah',name:'tekanan_darah'},
                {data:'diagnosa',name:'diagnosa'},
                {data:'rujukan',name:'rujukan'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        rujukan.on( 'order.dt search.dt', function () {
            rujukan.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        $('#cari').click(() => {
            rujukan.draw();
        })
    });
</script>
@endsection