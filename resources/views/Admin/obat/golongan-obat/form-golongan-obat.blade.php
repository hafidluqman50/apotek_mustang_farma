@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Golongan Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<form action="{{url('/admin/data-golongan-obat/save')}}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="box-body">
						@if (session()->has('message'))
						<div class="alert alert-success alert-dismissible">
							{{session('message')}} <button class="close" type="button" data-dismiss="alert">X</button>
						</div>
						@endif
						<div class="form-group">
							<label for="">Nama Golongan</label>
							<input type="text" name="nama_golongan" class="form-control" value="{{isset($row) ? $row->nama_golongan : ''}}" required="required" placeholder="Isi Nama Golongan Obat">
						</div>
						<div class="form-group">
							<label for="">Icon Golongan</label>
							<img id="uploadPreview" class="img-responsive" {!!isset($row) ? 'src="'.asset("assets/icon_golongan/".$row->foto_golongan).'"' : ''!!}>
							<input type="file" class="form-control" name="foto_golongan" id="image" {!!isset($row) ? '' : 'required="required"'!!}>
						</div>
					</div>
					<div class="box-footer">
						<button class="btn btn-primary">Simpan <span class="fa fa-save"></span></button>
						<input type="hidden" name="id_golongan_obat" value="{{isset($row)?$row->id_golongan_obat:''}}">
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')

@endsection