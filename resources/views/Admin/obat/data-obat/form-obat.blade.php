@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Obat</h1>
</section>

<section class="content">
    <div class="modal fade" id="data-supplier" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Data Supplier</h4>
          </div>
          <div class="modal-body">
            <table class="table table-hover data-supplier-obat nowrap force-fullwidth table-responsive">
                <thead>
                    <th>No.</th>
                    <th>Nama Supplier</th>
                    <th>Nomor Hp Supplier</th>
                    <th>Alamat Supplier</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="data-jenis-obat" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Data Jenis Obat</h4>
          </div>
          <div class="modal-body">
            <table class="table table-hover data-jenis-obat nowrap force-fullwidth table-responsive">
                <thead>
                    <th>No.</th>
                    <th>Nama Jenis Obat</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="data-golongan-obat" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Data Golongan Obat</h4>
          </div>
          <div class="modal-body">
            <table class="table table-hover force-fullwidth nowrap data-golongan-obat table-responsive">
                <thead>
                    <th>No.</th>
                    <th>Nama Golongan</th>
                    <th>Icon Golongan</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div>
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@elseif (session()->has('log'))
			<div class="alert alert-danger alert-dismissible">
				{{session('log')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-primary">
                <div class="box-header with-border">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#data-supplier">
                        Data Supplier
                    </button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#data-jenis-obat">
                        Data Jenis Obat
                    </button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#data-golongan-obat">
                        Data Golongan Obat
                    </button>
                </div>
				<form action="{{url('/admin/data-obat/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						{{-- <div class="form-group">
							<label for="">Nomor Batch</label>
							<input type="text" name="nomor_batch" class="form-control" placeholder="Isi Nomor Batch" value="{{isset($row)?$row->nomor_batch:''}}" required="required" {!!isset($row)?'disabled="disabled"':'autofocus="autofocus"'!!}>
							{!!isset($row)?'<input type="checkbox" id="check"> Ubah Nomor Batch':''!!}
						</div> --}}
						<div class="form-group">
							<label for="">Kode Obat / No Barcode</label>
							<input type="text" name="kode_obat" class="form-control" placeholder="Isi Kode Obat" value="{{isset($row)?$row->kode_obat:''}}" required="required" {!!isset($row)?'autofocus="autofocus"':''!!}>
						</div>
						<div class="form-group">
							<label for="">Nama Obat</label>
							<input type="text" name="nama_obat" class="form-control" placeholder="Isi Nama Obat" value="{{isset($row)?$row->nama_obat:''}}" required="required">
						</div>
						<div class="form-group">
							<label for="">Jenis Obat</label>
							<select name="jenis_obat" class="form-control select2" required="required">
								<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
								@foreach ($jenis_obat as $element)
								<option value="{{$element->id_jenis_obat}}" @if(isset($row)){!!$row->id_jenis_obat == $element->id_jenis_obat ? 'selected="selected"':''!!}@endif>{{$element->nama_jenis_obat}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Golongan Obat</label>
							<select name="golongan_obat" class="form-control select2" required="required">
								<option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===</option>
								@foreach ($golongan_obat as $element)
								<option value="{{$element->id_golongan_obat}}" @if (isset($row)){!!$row->id_golongan_obat == $element->id_golongan_obat ? 'selected="selected"' : ''!!}@endif>{{$element->nama_golongan}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="">Tanggal Expired</label>
							<input type="date" name="tanggal_expired" class="form-control" value="{{isset($row)?$row->tanggal_expired:''}}" required="required">
						</div>
						{{-- <div class="form-group">
							<label for="">Harga Obat</label>
							<input type="number" name="harga_obat" class="form-control" placeholder="Isi Harga Obat" value="{{isset($row)?$row->harga_obat:''}}" required="required">
						</div> --}}
                        <div class="form-group">
                            <label for="">Harga Modal</label>
                            <input type="number" name="harga_obat" class="form-control" placeholder="Isi Harga Modal" value="{{ isset($row) ? $row->harga_obat:'' }}" required="required">
                            <label for="" id="harga-modal-obat">Rp. 0,00</label>
                        </div>
                        <label for="">PPn</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="number" name="ppn" class="form-control" value="0" id="ppn" required placeholder="Isi PPn; Ex:11;">
                                <span class="input-group-addon">%</span>
                            </div>
                        </label>
                        <label for="">Diskon</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="number" name="disc" class="form-control" value="0" id="disc" required placeholder="Isi Disc;">
                                <span class="input-group-addon">%</span>
                            </div>
                        </label>
                        <div class="form-group">
                            <label for="">Harga Jual</label>
                            <input type="number" name="harga_jual" class="form-control" placeholder="Isi Harga Jual" value="{{ isset($row) ? $row->harga_jual:'' }}" required="required">
                            <label for="" id="harga-jual-obat">Rp. 0,00</label>
                        </div>
						<div class="form-group">
							<label for="">Stok Obat</label>
							<input type="number" name="stok_obat" class="form-control" placeholder="Isi Stok Obat" value="{{isset($row)?$row->stok_obat:''}}" required="required">
						</div>
						{{-- <div class="form-group">
							<label for="">Bobot Satuan</label>
							<input type="number" class="form-control" name="bobot_satuan" required="required" placeholder="Isi Bobot Satuan" value="{{isset($row)?$row->bobot_satuan:''}}">
						</div> --}}
					</div>
					<div class="box-footer">
						<input type="hidden" name="ket_data" value="inventory">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_obat:''}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
    $(() => {
        var supplier = $('.data-supplier-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-supplier-obat') }}",
            columns:[
                {data:'id_supplier',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_supplier',name:'nama_supplier'},
                {data:'nomor_telepon',name:'nomor_telepon'},
                {data:'alamat_supplier',name:'alamat_supplier'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        supplier.on( 'order.dt search.dt', function () {
            supplier.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var golongan_obat = $('.data-golongan-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-golongan-obat') }}",
            columns:[
                {data:'id_golongan_obat',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_golongan',name:'nama_golongan'},
                {data:'foto_golongan',name:'foto_golongan'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        golongan_obat.on( 'order.dt search.dt', function () {
            golongan_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var jenis_obat = $('.data-jenis-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-jenis-obat') }}",
            columns:[
                {data:'id_jenis_obat',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        jenis_obat.on( 'order.dt search.dt', function () {
            jenis_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        $('input[name="harga_obat"]').keyup(function() {
            let val       = parseInt($(this).val())
            let nilai_ppn = parseInt($('input[name="ppn"]').val())
            let disc      = parseFloat($('input[name="disc"]').val())
            $('#harga-modal-obat').html(rupiah_format(val))

            if (val != '') {
                let harga_modal_ppn  = parseFloat(val) + parseFloat(((val * nilai_ppn) / 100))
                let harga_modal_disc = harga_modal_ppn - ((harga_modal_ppn * disc) / 100)

                $('input[name="harga_jual"]').val(harga_modal_disc)
                $('#harga-jual-obat').html(rupiah_format(harga_modal_disc))
            }
        })

        $('input[name="ppn"]').keyup(function() {
            let val         = parseInt($(this).val())
            let harga_modal = parseInt($('input[name="harga_obat"]').val())
            let disc        = parseFloat($('input[name="disc"]').val())

            console.log(val,harga_modal,disc)

            if (val != '') {
                let harga_modal_ppn  = parseFloat(harga_modal) + parseFloat(((harga_modal * val) / 100))
                let harga_modal_disc = parseFloat(harga_modal_ppn) - parseFloat(((harga_modal_ppn * disc) / 100))

                $('input[name="harga_jual"]').val(harga_modal_disc)
                $('#harga-jual-obat').html(rupiah_format(harga_modal_disc))
            }
        })

        $('input[name="disc"]').keyup(function() {
            let val         = parseInt($(this).val())
            let harga_modal = parseInt($('input[name="harga_obat"]').val())
            let ppn        = parseFloat($('input[name="ppn"]').val())

            if (val != '') {
                let harga_modal_ppn  = parseFloat(harga_modal) + parseFloat(((harga_modal * ppn) / 100))
                let harga_modal_disc = parseFloat(harga_modal_ppn) - parseFloat(((harga_modal_ppn * val) / 100))

                $('input[name="harga_jual"]').val(harga_modal_disc)
                $('#harga-jual-obat').html(rupiah_format(harga_modal_disc))
            }
        })
    })
</script>
@endsection