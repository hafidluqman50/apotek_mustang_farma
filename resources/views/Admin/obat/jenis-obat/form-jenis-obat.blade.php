@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Jenis Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/data-jenis-obat/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Nama Jenis Obat</label>
							<input type="text" name="nama_jenis_obat" class="form-control" placeholder="Isi Nama Jenis Obat" value="{{isset($row)?$row->nama_jenis_obat:''}}" required="required" autofocus="autofocus">
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_jenis_obat:''}}">
						<button type="submit" class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection