@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Pengeluaran Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<form action="{{url('/admin/data-keluar-obat/rekap')}}">
						<div class="col-md-5">
							<div class="form-group">
								<label for="">Dari</label>
								<input type="date" name="from" class="form-control" required="required" {!!isset($_GET['from'])?'value="'.$_GET['from'].'"':''!!}>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label for="">Ke</label>
								<input type="date" name="to" class="form-control" required="required" {!!isset($_GET['to'])?'value="'.$_GET['to'].'"':''!!}>
							</div>
						</div>
						<div class="col-md-2">{{-- 
							<button class="btn btn-default" style="margin-top:8%;">
								Cari
							</button> --}}
							<button class="btn btn-success" style="margin-top:18%;">
								Export
							</button>
						</div>
					</form>
					<table class="table table-hover data-keluar-obat force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Tanggal Keluar</th>
							{{-- <th>Nomor Batch</th> --}}
							<th>Nama Obat</th>
							<th>Jenis Obat</th>
							<th>Stok Keluar</th>
							<th>Keterangan</th>
							<th>Input By</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var keluar_obat = $('.data-keluar-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-keluar-obat') }}",
            columns:[
                {data:'id_keluar',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tanggal_keluar',name:'tanggal_keluar'},
                // {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'stok_keluar',name:'stok_keluar'},
                {data:'keterangan',name:'keterangan'},
                {data:'name',name:'name'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        keluar_obat.on( 'order.dt search.dt', function () {
	        keluar_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection