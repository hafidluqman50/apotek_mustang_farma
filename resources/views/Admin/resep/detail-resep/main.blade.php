@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Resep</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<a href="{{url('/admin/laporan-resep')}}">
						<button class="btn btn-default">
							<span class="fa fa-long-arrow-left"></span> Kembali
						</button>
					</a>
				</div>
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<h5><b>Kode Pasien : {{$customer->kode_pasien}}</b></h5>
					<h5><b>Nama Pasien : {{$customer->nama_pasien}}</b></h5>
					<h5><b>Umur Pasien : {{hitung_umur($customer->tanggal_lahir)}}</b></h5>
					<h5><b>Jenis Kelamin : {{$customer->jenis_kelamin}}</b></h5>
					<table class="table table-hover data-detail-resep force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Nomor Batch</th>
							<th>Nama Obat</th>
							<th>Jenis Obat</th>
							<th>Dosis Obat</th>
							<th>Banyak Obat</th>
							<th>Sub Total</th>
							{{-- <th>#</th> --}}
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var detail_resep = $('.data-detail-resep').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-detail-resep/'.$id) }}",
            columns:[
                {data:'id_resep',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'dosis',name:'dosis'},
                {data:'banyak_obat',name:'banyak_obat'},
                {data:'sub_total',name:'sub_total'},
                // {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        detail_resep.on( 'order.dt search.dt', function () {
	        detail_resep.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection