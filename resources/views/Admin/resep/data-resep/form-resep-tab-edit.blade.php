@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Resep Edit</h1>
</section>

<section class="content">
    <form action="{{url('/admin/data-resep/update',$id)}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            @if (session()->has('berhasil'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">
                    {{session('berhasil')}} <button class="close" data-dismiss="alert">X</button>
                </div>
            </div>
            @elseif(session()->has('habis'))
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible">
                    <button class="close" data-dismiss="alert">X</button>
                    <ul>
                    @foreach (session('habis') as $key => $element)
                        <li>{!!$element['text']!!}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <div class="modal fade" id="tesModal">
              <div class="modal-lg modal-dialog">
                <div class="modal-content">
                <!-- header-->
                  <div class="modal-header">
                    <button class="close" data-dismiss="modal"><span>&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Rekam Medis Pasien</h4>
                  </div>
                  <!--body-->
                  <div class="modal-body">
                    
                    <h5 id="nama-pasien">Nama Pasien :</h5>
                    <h5 id="kategori-pasien">Kategori Pasien :</h5>
                    <h5 id="tanggal-lahir">Tanggal Lahir :</h5>
                    <h5 id="umur-pasien">Umur Pasien :</h5>
                    <h5 id="jenis-kelamin">Jenis Kelamin :</h5>
                    <table class="table table table-hover force-fullwidth data-rekam-medis">
                        <thead>
                            <th>No.</th>
                            <th>Tanggal Periksa</th>
                            <th>Tinggi Badan</th>
                            <th>Berat Badan</th>
                            <th>Tekanan Darah</th>
                            <th>Suhu Badan</th>
                            <th>Tindakan</th>
                            <th>Keluhan</th>
                            <th>Diagnosa</th>
                            <th>Anemnesis</th>
                            <th>Pemeriksaan Penunjang</th>
                            <th>Terapi</th>
                            <th>Rujukan</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                  </div>
                  <!--footer-->
                  <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <a href="{{ url('/admin/laporan-resep') }}">
                            <button class="btn btn-default" type="button">
                                <span class="fa fa-arrow-left"></span> Kembali
                            </button>
                        </a>
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="dokterTabs">
                                <li class="active"><a href="#catatan-medis" data-toggle="tab">Catatan Medis</a></li>
                                <li><a href="#obat-resep" data-toggle="tab">Obat Resep</a></li>
                                <li><a href="#data-obat" data-toggle="tab">Data Obat</a></li>
                                <li><a href="#data-tindakan" data-toggle="tab">Data Tindakan</a></li>
                                <li><a href="#data-penyakit-by-bpjs" data-toggle="tab">Data Penyakit by BPJS</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="catatan-medis">
                                    <div id="form-customer">
                                        <div class="form-group">
                                            <label for="">Pasien</label>
                                            <input type="text" class="form-control pasien-resep" readonly value="{{ human_date($row->getRow($id)->tgl_daftar).' | '.$row->getRow($id)->nama_pasien.' | '.hitung_umur($row->getRow($id)->tanggal_lahir).' | '.$row->getRow($id)->jenis_kelamin.' | '.$row->getRow($id)->nama_dokter }}" id-resep="{{ $row->getRow($id)->id_daftar }}">
                                            <hr>
                                            <button type="button" class="btn btn-primary" id="show-rekam-medis" data-toggle="modal" data-target="#tesModal">
                                                Lihat Rekam Medis Pasien
                                            </button>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Keluhan Utama</label>
                                            <input type="text" name="keluhan" class="form-control" value="{{session()->has('habis') ? old('keluhan') : $row->getRow($id)->keluhan_utama_resep}}" placeholder="Isi Keluhan Utama">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tinggi Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="tinggi_badan" class="form-control" value="{{ $row->getRow($id)->tinggi_badan_resep }}" id="tinggi-badan" placeholder="Isi Tinggi Badan">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Berat Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="berat_badan" class="form-control" value="{{ $row->getRow($id)->berat_badan_resep }}" id="berat-badan" placeholder="Isi Berat Badan">
                                                <span class="input-group-addon">kg</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tekanan Darah</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="tekanan_darah" class="form-control" value="{{ $row->getRow($id)->tekanan_darah_resep }}" id="tinggi-badan" placeholder="Isi Tekanan Darah">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Suhu Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="suhu_badan" class="form-control" value="{{ $row->getRow($id)->suhu_badan_resep }}" id="tinggi-badan" placeholder="Isi Suhu Badan">
                                                <span class="input-group-addon">&deg;C</span>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="">Cek Kadar Kolesterol</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="kolesterol" class="form-control" id="kolesterol" placeholder="Isi Hasil Kolesterol (Jika Ada)">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Cek Kadar Gula Darah</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="gula_darah" class="form-control" id="gula_darah" placeholder="Isi Hasil Gula Darah (Jika Ada)">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Cek Kadar Asam Urat</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="">Anemnesis</label>
                                            <input type="text" name="anemnesis" class="form-control" value="{{session()->has('habis') ? old('anemnesis') : $row->getRow($id)->anemnesis}}" placeholder="Isi Anemnesis">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Pemeriksaan Penunjang</label>
                                            {{-- <input type="text" name="pemeriksaan_penunjang" class="form-control" value="{{session()->has('habis') ? old('pemeriksaan_penunjang') : ''}}" placeholder="Isi Pemeriksaan Penunjang"> --}}
                                            <textarea name="pemeriksaan_penunjang" class="form-control summernote" id="" cols="30" rows="10">{!!$row->getRow($id)->pemeriksaan_penunjang!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Pemeriksaan Fisik</label>
                                            <textarea name="pemeriksaan_fisik" class="form-control summernote" id="pemeriksaan_fisik">{!!session()->has('habis') ? old('pemeriksaan_fisik') : $row->getRow($id)->pemeriksaan_fisik!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Diagnosa</label>
                                            <input type="text" name="diagnosa" class="form-control" value="{{session()->has('habis') ? old('diagnosa') : $row->getRow($id)->diagnosa}}" required="required" placeholder="Isi Diagnosa">
                                        </div>
                                        <div class="form-group">
                                            <label>Tindakan</label>
                                            {{-- <input type="text" name="tindakan_lab" class="form-control" value="{{ session()->has('habis') ? old('tindakan_lab') : '' }}" placeholder="Isi Tindakan Lab"> --}}
                                            <select name="tindakan_lab" class="form-control select2">
                                                <option value="" selected disabled>=== Pilih Tindakan ===</option>
                                                @foreach ($tindakan_lab as $value)
                                                <option value="{{ $value->id_tindakan_lab }}" {!!$row->getRow($id)->id_tindakan_lab == $value->id_tindakan_lab ? 'selected="selected"' : ''!!}>{{ $value->nama_tindakan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Terapi</label>
                                            <textarea name="terapi" class="form-control summernote" id="terapi">{!!session()->has('habis') ? old('terapi') : $row->getRow($id)->terapi!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Rujukan</label>
                                            <input type="text" name="rujukan" class="form-control" value="{{session()->has('habis') ? old('rujukan') : $row->getRow($id)->rujukan}}" placeholder="Isi Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tanggal Kontrol Selanjutnya</label>
                                            <input type="text" name="tgl_kontrol_selanjutnya" class="form-control datepicker" value="{{session()->has('habis') ? old('tgl_kontrol_selanjutnya') : reverse_date($row->getRow($id)->tgl_kontrol_selanjutnya)}}" placeholder="dd-mm-yyyy">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Keterangan</label>
                                            <input type="text" name="keterangan_resep" class="form-control" readonly value="{{session()->has('habis') ? old('keterangan_resep') : $row->getRow($id)->ket_resep}}" placeholder="Isi Keterangan">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="obat-resep">
                                    @if (session()->has('obat_habis'))
                                            <div class="form-group resep-layout" id="resep">
                                        @for ($i = 0; $i < count(session('obat_habis')); $i++)
                                                <button type="button" class="btn btn-info pencet input-obat" style="margin-bottom:10px;" id="input-obat" data-id="{{$i+1}}">
                                                    Input Obat
                                                </button>
                                                <button type="button" class="btn btn-danger button-hide destroy hapus-input-obat" style="margin-bottom:10px;" id="hapus-input-obat" data-id="{{$i+1}}">
                                                    Batal Input Obat
                                                </button>
                                                <div class="coba racik-obat" id="racik-obat" get-id="{{$i+1}}">
                                                    <div class="form-group">
                                                        <select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 pilih-jenis" ajax-id="{{$i+1}}">
                                                            <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
                                                            @foreach ($jenis_obat as $element)
                                                            <option value="{{$element->id_jenis_obat}}" @if(session('jenis_obat')[$i]['jenis_obat'] == $element->id_jenis_obat) selected="selected" @endif>{{$element->nama_jenis_obat}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <select name="obat[]" class="form-control select2 pilih-obat" id="oke" get-ajax-id="{{$i+1}}" required="required">
                                                            <option value="" selected="selected" disabled="disabled">=== Pilih Obat ===</option>
                                                            @foreach ($obat->getByJenis(session('jenis_obat')[$i]['jenis_obat']) as $element)
                                                            <option value="{{$element->id_obat}}" @if(session('obat_habis')[$i]['id_obat'] == $element->id_obat) selected="selected" @endif>{{$element->nomor_batch.' | '.$element->nama_obat.' | '.golongan_obat($element->golongan_obat)}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="dosis_obat[]" class="form-control" placeholder="Isi Dosis Obat" value="{{session('dosis_obat')[$i]['dosis_obat']}}" required="required">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="number" name="banyak_obat[]" class="form-control" placeholder="Isi Banyak Obat" value="{{session('banyak_obat')[$i]['banyak_obat']}}" required="required">
                                                    </div>
                                                </div>
                                                <div class="coba-juga form-hide input-racik" id="input-racik" get-id="{{$i+1}}">
                                                    {{-- <div class="col-md-6"> --}}
                                                        <div class="form-group">
                                                            <label for="">Nomor Batch</label>
                                                            <input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch" disabled>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Nama Obat</label>
                                                            <input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat" disabled>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Jenis Obat</label>
                                                            <select name="jenis_input_obat[]" class="form-control select2">
                                                                <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
                                                                </option>
                                                                @foreach ($jenis_obat as $element)
                                                                <option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Golongan Obat</label>
                                                            <select name="golongan_input_obat[]" class="form-control select2">
                                                                <option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===
                                                                </option>
                                                                @foreach ($golongan_obat as $element)
                                                                <option value="{{$element->id_golongan_obat}}">{{$element->nama_golongan_obat}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Tanggal Expired</label>
                                                            <input type="date" name="tanggal_expired[]" class="form-control" disabled>
                                                        </div>
                                                    {{-- </div> --}}
                                                    {{-- <div class="col-md-6"> --}}
                                                        <div class="form-group">
                                                            <label for="">Harga Obat</label>
                                                            <input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat" disabled>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Stok Obat</label>
                                                            <input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat" disabled>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Dosis Obat</label>
                                                            <input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat" disabled>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Banyak Beli Obat</label>
                                                            <input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat" disabled>
                                                        </div>
                                                    {{-- </div> --}}
                                                </div>
                                        @endfor
                                            </div>
                                    @else
                                        <div class="form-group resep-layout" id="resep">
                                        @foreach ($row_detail as $key => $value)
                                            @php $no = $key + 1 @endphp
                                            <button type="button" class="btn btn-info pencet input-obat" style="margin-bottom:10px;" id="input-obat" data-id="{{$no}}">
                                                Input Obat
                                            </button>
                                            <button type="button" class="btn btn-danger button-hide destroy hapus-input-obat" style="margin-bottom:10px;" id="hapus-input-obat" data-id="{{ $no }}">
                                                Batal Input Obat
                                            </button>
                                            <div class="coba racik-obat" id="racik-obat" get-id="{{ $no }}">
                                                <div class="form-group">
                                                    <select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 pilih-jenis" ajax-id="{{ $no }}">
                                                        <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
                                                        @foreach ($jenis_obat as $element)
                                                        <option value="{{$element->id_jenis_obat}}"@if($value->id_jenis_obat == $element->id_jenis_obat) selected="selected" @endif>{{$element->nama_jenis_obat}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select name="obat[]" class="form-control select2 pilih-obat" id="oke" get-ajax-id="{{ $no }}">
                                                        <option selected="selected" disabled="disabled">=== Pilih Obat ===</option>
                                                        @foreach ($obat->getByJenis($value->id_jenis_obat) as $element)
                                                        <option value="{{$element->id_obat}}" @if($value->id_obat == $element->id_obat) selected="selected" @endif>{{$element->nomor_batch.' | '.$element->nama_obat.' | '.golongan_obat($element->golongan_obat)}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="dosis_obat[]" class="form-control" value="{{ $value->dosis }}" placeholder="Isi Dosis Obat">
                                                </div>
                                                <div class="form-group">
                                                    <input type="number" name="banyak_obat[]" class="form-control" value="{{ $value->banyak_obat }}" placeholder="Isi Banyak Obat">
                                                </div>
                                            </div>
                                            <div class="coba-juga form-hide input-racik" id="input-racik" get-id="{{ $no }}">
                                                {{-- <div class="col-md-6"> --}}
                                                    <div class="form-group">
                                                        <label for="">Nomor Batch</label>
                                                        <input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch" disabled="disabled">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Nama Obat</label>
                                                        <input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat" disabled="disabled">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Golongan Obat</label>
                                                        <select name="golongan_input_obat[]" class="form-control select2">
                                                            <option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===
                                                            </option>
                                                            @foreach ($golongan_obat as $element)
                                                            <option value="{{$element->id_golongan_obat}}">{{$element->nama_golongan}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Jenis Obat</label>
                                                        <select name="jenis_input_obat[]" class="form-control select2" disabled="disabled">
                                                            <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
                                                            </option>
                                                            @foreach ($jenis_obat as $element)
                                                            <option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Tanggal Expired</label>
                                                        <input type="date" name="tanggal_expired[]" class="form-control" disabled="disabled">
                                                    </div>
                                                {{-- </div>
                                                <div class="col-md-6"> --}}
                                                    <div class="form-group">
                                                        <label for="">Harga Obat</label>
                                                        <input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat" disabled="disabled">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Stok Obat</label>
                                                        <input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat" disabled="disabled">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Dosis Obat</label>
                                                        <input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat" disabled="disabled">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Banyak Beli Obat</label>
                                                        <input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat" disabled="disabled">
                                                    </div>
                                                {{-- </div> --}}
                                            </div>
                                        @endforeach
                                        </div>
                                        <button type="button" class="btn btn-primary" id="tambah-obat">
                                            Tambah Obat
                                        </button>
                                        <button type="button" class="btn btn-danger" id="hapus-obat">
                                            Hapus Obat
                                        </button>
                                    @endif
                                </div>
                                <div class="tab-pane force-fullwidth" id="data-obat">
                                    <table class="table table-hover data-obat force-fullwidth">
                                        <thead>
                                            <th>No.</th>
                                            <th>Kode Obat</th>
                                            <th>Nomor Batch</th>
                                            <th>Nama Obat</th>
                                            <th>Nama Jenis Obat</th>
                                            <th>Golongan Obat</th>
                                            <th>Tanggal Expired</th>
                                            <th>Harga Obat</th>
                                            <th>Harga Jual</th>
                                            <th>Stok Obat</th>
                                            <th>Bobot Satuan</th>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane force-fullwidth" id="data-tindakan">
                                    <table class="table table-hover tindakan-lab force-fullwidth">
                                        <thead>
                                            <th>No.</th>
                                            <th>Nama Tindakan</th>
                                            <th>Biaya Jasa Lab</th>
                                            <th>Spesialis</th>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane force-fullwidth" id="data-penyakit-by-bpjs">
                                    @if ($diagnosa_bpjs['message']['code'] != '')
                                        <div class="alert alert-danger alert-dismissible">
                                            {{$diagnosa_bpjs['message']['message']}} <button class="close" data-dismiss="alert">X</button>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-2">
                                            <input type="text" name="search" class="form-control" id="input-cari-penyakit-bpjs" placeholder="Cari...">
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-primary" id="cari-penyakit-bpjs" type="button">Cari</button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                       <div class="col-md-6 col-md-offset-2">
                                            <table class="table table-hover table-bordered" id="table-penyakit-bpjs">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama Penyakit</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody-penyakit-bpjs">
                                                    @foreach ($diagnosa_bpjs['result'] as $key => $value)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td>{{ $value->nmDiag }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                       </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">
                            Simpan <span class="fa fa-save"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var base_url = "{{ url('/') }}"

        var obat = $('.data-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-obat/inventory') }}",
            columns:[
                {data:'id_obat',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'kode_obat',name:'kode_obat'},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'icon_golongan',name:'icon_golongan'},
                {data:'tanggal_expired',name:'tanggal_expired'},
                {data:'harga_obat',name:'harga_obat'},
                {data:'harga_jual',name:'harga_jual'},
                {data:'stok_obat',name:'stok_obat'},
                {data:'bobot_satuan',name:'bobot_satuan'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        obat.on( 'order.dt search.dt', function () {
            obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var tindakan_lab = $('.tindakan-lab').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{url('/datatables/data-tindakan-lab')}}",
            columns:[
                {data:'id_tindakan_lab',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'biaya_lab',name:'biaya_lab'},
                {data:'nama_spesialis_dokter',name:'nama_spesialis_dokter'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 1, 'desc' ]],
            responsive:true,
            fixedColumns: true,
        });
        tindakan_lab.on( 'order.dt search.dt', function () {
            tindakan_lab.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        function loadRekamMedis(id_dokter,id_pasien)
        {
            var pasien = $('.data-rekam-medis').DataTable({
                processing:true,
                serverSide:true,
                ajax:`${base_url}/datatables/pasien-rekam-medis/${id_dokter}/rekam/${id_pasien}`,
                columns:[
                    {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                        return meta.row + meta.settings._iDisplayStart+1;
                    }},
                    {data:'tgl_resep',name:'tgl_resep'},
                    {data:'tinggi_badan',name:'tinggi_badan'},
                    {data:'berat_badan',name:'berat_badan'},
                    {data:'tekanan_darah',name:'tekanan_darah'},
                    {data:'suhu_badan',name:'suhu_badan'},
                    {data:'nama_tindakan',name:'nama_tindakan'},
                    {data:'keluhan_utama_resep',name:'keluhan_utama_resep'},
                    {data:'diagnosa',name:'diagnosa'},
                    {data:'anemnesis',name:'anemnesis'},
                    {data:'pemeriksaan_penunjang',name:'pemeriksaan_penunjang'},
                    {data:'terapi',name:'terapi'},
                    {data:'rujukan',name:'rujukan'}
                ],
                scrollCollapse: true,
                columnDefs: [ {
                sortable: true,
                "class": "index",
                }],
                scrollX:true,
                order: [],
                responsive:true,
                fixedColumns: true
            });
            pasien.on( 'order.dt search.dt', function () {
                pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
        });

        var select_ajax    = parseInt($('.pilih-jenis:last').attr('ajax-id'))+1;
        var get_ajax       = parseInt($('.pilih-obat:last').attr('get-ajax-id'))+1;
        var num_input      = parseInt($('.input-obat:last').attr('data-id'))+1;
        var num_cancel     = parseInt($('.hapus-input-obat:last').attr('data-id'))+1;
        var num_racik      = parseInt($('.racik-obat:last').attr('get-id'))+1;
        var num_manual     = parseInt($('.input-racik:last').attr('get-id'))+1;
        var id_pendaftaran = $('.pasien-resep').attr('id-resep');

        $('#tambah-obat').click(function() {
            $('select[name="obat[]"]').select2('destroy');
            $('select[name="jenis_input_obat[]"]').select2('destroy');
            $('.pilih-jenis').select2('destroy');
            $('#hapus-obat').show();
            $('.input-obat:last').clone().appendTo($('.resep-layout:last')).css('margin-top','10px').attr('data-id',num_input++);
            $('.hapus-input-obat:last').clone().appendTo($('.resep-layout:last')).css('margin-top','10px').attr('data-id',num_cancel++);
            $('.racik-obat:last').clone().appendTo($('.resep-layout:last')).attr('get-id',num_racik++);
            reset_racik = num_racik - 1;
            $('.coba[get-id="'+reset_racik+'"]').find('select[name="obat[]"]').attr('disabled','disabled');
            $('.coba[get-id="'+reset_racik+'"]').find('input').val('');
            $('.input-racik:last').clone().appendTo($('.resep-layout:last')).attr('get-id',num_manual++);
            reset_manual = num_manual - 1;
            $('.coba-juga[get-id="'+reset_manual+'"]').find('input').val('');
            $('.pilih-jenis:last').attr('ajax-id',select_ajax++);
            $('.pilih-obat:last').attr('get-ajax-id',get_ajax++);
            $('.pilih-jenis').select2();
            $('select[name="obat[]"]').select2();
            $('select[name="jenis_input_obat[]"]').select2();
            // $('.coba[get-id="'+num_input+'"] > div > select[name="obat[]"]').attr('disabled','disabled');
        });

        function getDataResep(id) {
            $.ajax({
                url: `${base_url}/ajax/get-data-resep/${id}`
            })
            .done(function(done) {
                $('#form-customer > input[name="pasien"]').remove();
                $('#form-customer > input[name="dokter"]').remove();
                $('#form-customer').append(done.html);
                $('#nama-pasien').html(`<h5 id="nama-pasien">Nama Pasien : ${done.data_resep.pasien.nama_pasien}</h5>`)
                $('#kategori-pasien').html(`<h5 id="kategori-pasien">Kategori Pasien : ${done.data_resep.pasien.nama_kategori}</h5>`)
                $('#tanggal-lahir').html(`<h5 id="tanggal-lahir">Tanggal Lahir : ${done.data_resep.tanggal_lahir}</h5>`)
                $('#umur-pasien').html(`<h5 id="umur-pasien">Umur Pasien : ${done.data_resep.umur_pasien}</h5>`)
                $('#jenis-kelamin').html(`<h5 id="jenis-kelamin">Jenis Kelamin : ${done.data_resep.pasien.jenis_kelamin}</h5>`)
                loadRekamMedis(done.data_resep['id_dokter'],done.data_resep['id_pasien'])
            })
            .fail(function() {
                console.log("error");
            });
        }

        getDataResep(id_pendaftaran);

        $('.kategori-pasien').select2({disabled:true});

        $('select[name="pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-kategori-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="kategori_pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('select[name="kategori_pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="pasien"]').removeAttr('disabled');
                $('select[name="pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('#input-customer').click(function(){
            $('#form-customer').slideUp(function(){
                $('#form-customer').find('input,select').removeAttr('required');
            });
            $('#form-hide-customer').slideDown(function(){
                $('#form-hide-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#batal-customer').show();
        });

        $('#batal-customer').click(function(){
            $('#form-hide-customer').slideUp(function(){
                $('#form-hide-customer').find('input,select').removeAttr('required');
            });
            $('#form-customer').slideDown(function(){
                $('#form-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#input-customer').show();
        });

        $(document).on('change','.pilih-jenis',function(){
            var getAttr = $(this).attr('ajax-id');
            var getVal  = $(this).val();
            var getUrl  = base_url+'/ajax/get-obat/'+getVal;
            $.ajax({
                url: getUrl,
            })
            .done(function(done) {
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').removeAttr('disabled');
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $(document).on('click','.pencet',function(){
            var attr = $(this).attr('data-id');
            $('.coba[get-id="'+attr+'"]').slideUp(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba[get-id="${attr}"`).find('input').val('');
            });
            $('.coba-juga[get-id="'+attr+'"]').slideDown(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('disabled');
            });
            $(this).hide();
            $('.destroy[data-id="'+attr+'"]').show();
        });

        $(document).on('click','.destroy',function(){
            var attr = $(this).attr('data-id');
            $('.coba-juga[get-id="'+attr+'"]').slideUp(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba-juga[get-id="${attr}"`).find('input').val('');
            });
            $('.coba[get-id="'+attr+'"]').slideDown(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba[get-id="'+attr+'"]').find('input').removeAttr('disabled');
                $('.coba[get-id="'+attr+'"]').find('.pilih-jenis').removeAttr('disabled');
            });
            $(this).hide();
            $('.pencet[data-id="'+attr+'"]').show();
        });

        $('#hapus-obat').click(function(){
            $('.input-obat:last').remove();
            $('.hapus-input-obat:last').remove();
            $('.racik-obat:last').remove();
            $('.input-racik:last').remove();
            if ($('.coba').length == 1 && $('.coba-juga').length == 1) {
                $(this).hide();
            }
        });

        $('#cari-penyakit-bpjs').click(() => {
            let val = $('#input-cari-penyakit-bpjs').val();
            $('#tbody-penyakit-bpjs').html('<tr><td colspan="2" align="center">Loading...</td></tr>');
            $.ajax({
                url: "{{ url('/ajax/find-diagnosa-bpjs') }}",
                // dataType: 'application/json',
                data: {search: val},
            })
            .done(function(done) {
                $('#tbody-penyakit-bpjs').html(done)
            })
            .fail(function(error) {
                console.log(error);
            });
            
        })
    });
</script>
@endsection