@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Tindakan Lab</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/tindakan-lab/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label>Nama Tindakan</label>
							<input type="text" name="nama_tindakan" class="form-control" value="{{isset($row)?$row->nama_tindakan:''}}" placeholder="Isi Nama Tindakan" required="required" autofocus="autofocus">
						</div>
                        <div class="form-group">
                            <label for="">Spesialis</label>
                            <select name="spesialis_dokter" class="form-control select2">
                                <option selected="selected" disabled="disabled">=== Pilih Spesialis ===</option>
                                @foreach ($spesialis_dokter as $value)
                                <option value="{{ $value->id_spesialis_dokter }}" @if (isset($row)){!!$row->id_spesialis_dokter == $value->id_spesialis_dokter ? 'selected="selected"' : ''!!}@endif>{{ $value->nama_spesialis_dokter }}</option>
                                @endforeach
                            </select>
                        </div>
						<div class="form-group">
							<label>Biaya Lab</label>
							<input type="number" class="form-control" name="biaya_lab" value="{{isset($row)?$row->biaya_lab:''}}" placeholder="Isi Biaya Lab" required="required">
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id_tindakan_lab" value="{{isset($row)?$row->id_tindakan_lab:''}}">
						<button class="btn btn-primary">
							<span class="fa fa-save"></span> Simpan
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection