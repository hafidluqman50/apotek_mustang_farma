@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Tindakan Lab</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover tindakan-lab force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Nama Tindakan</th>
							<th>Biaya Jasa Lab</th>
                            <th>Spesialis</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var transaksi = $('.tindakan-lab').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{url('/datatables/data-tindakan-lab')}}",
            columns:[
                {data:'id_tindakan_lab',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'biaya_lab',name:'biaya_lab'},
                {data:'nama_spesialis_dokter',name:'nama_spesialis_dokter'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 1, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        transaksi.on( 'order.dt search.dt', function () {
	        transaksi.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection