<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cetak</title>
  <style>
    @page {
      size:landscape;
    }
    @media print {
      #hilang {
        display:none;
      }
    }
  </style>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/assets/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <div id="hilang">
      <a href="{{url('/admin/data-transaksi')}}">
      <button class="btn btn-default" id="sip">
        <span class="fa fa-long-arrow-left"></span> Kembali
      </button>
      </a>
    </div>
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="{{asset('profile-instansi/'.get_profile_instansi()->logo_instansi)}}" height="45" alt=""> {{ get_profile_instansi()->nama_instansi }}
          <br>
          <small class="pull-right">Tanggal: {{format_tanggal($data->tgl_byr)}}</small>
          <address style="font-size:15px; font-weight: 500; margin-bottom:0; margin-top:10px">
            {{ get_profile_instansi()->alamat_instansi }}
          </address>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      {{-- <div class=""> --}}
        <div class="col-sm-12">
          <div class="row">
              <div class="col-md-6 invoice-col">
                <address>
                  <table class="table">
                    <tr>
                      <th>Nama Dokter</th>
                      <th>:</th>
                      <td>{{$data->nama_dokter}}</td>
                    </tr>
                    <tr>
                      <th>Nama Pasien</th>
                      <th>:</th>
                      <td>{{$data->nama_pasien}}</td>
                    </tr>
                    <tr>
                      <th>Kategori Pasien</th>
                      <th>:</th>
                      <td>{{$data->nama_kategori}}</td>
                    </tr>
                    <tr>
                      <th>Tanggal Lahir</th>
                      <th>:</th>
                      <td>{{human_date($data->tanggal_lahir)}}</td>
                    </tr>
                  </table>
                </address>
              </div>
              <div class="col-md-6 invoice-col">
                <address>
                  <table class="table">
                    <tr>
                      <th>Umur Pasien</th>
                      <th>:</th>
                      <td>{{hitung_umur($data->tanggal_lahir)}}</td>
                    </tr>
                    <tr>
                      <th>Alamat Pasien</th>
                      <th>:</th>
                      <td>{{$data->alamat_pasien}}</td>
                    </tr>
                    <tr>
                      <th>Jenis Kelamin</th>
                      <th>:</th>
                      <td>{{$data->jenis_kelamin}}</td>
                    </tr>
                  </table>
                </address>
              </div>
          </div>
        </div>
      {{-- </div> --}}
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>No.</th>
            <th>Nomor Batch</th>
            <th>Nama Obat</th>
            <th>Jenis Obat</th>
            <th>Dosis Obat</th>
            <th>Banyak Obat</th>
            <th>Harga Obat</th>
          </tr>
          </thead>
          <tbody>
            @foreach ($obat as $key => $element)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$element->kode_obat}}</td>
                <td>{{$element->nama_obat}}</td>
                <td>{{$element->nama_jenis_obat}}</td>
                <td>{{$element->dosis}}</td>
                <td>{{$element->banyak_obat}}</td>
                <td>{{format_rupiah($element->sub_total)}}</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
    	<div class="col-md-6">
	        <div class="table-responsive">
	          <table class="table">
              <tr>
                <th style="width:50%;">Biaya Dokter</th>
                <th>:</th>
                <td>{{format_rupiah($data->harga_dokter)}}</td>
              </tr>
              <tr>
                <th style="width:50%;">Biaya Jasa Lab</th>
                <th>:</th>
                <td>{{format_rupiah($data->biaya_jasa_lab)}}</td>
              </tr>
              <tr>
                <th style="width:50%;">Biaya Resep</th>
                <th>:</th>
                <td>{{format_rupiah($data->biaya_resep)}}</td>
              </tr>
              <tr>
                <th style="width:50%;">Biaya Racik</th>
                <th>:</th>
                <td>{{format_rupiah($data->biaya_racik)}}</td>
              </tr>
              <tr>
                <th style="width:50%;">Biaya Tambahan</th>
                <th>:</th>
                <td>{{format_rupiah($data->biaya_tambahan)}}</td>
              </tr>
                <tr>
                  <th style="width:50%">Diskon</th>
                  <th>:</th>
                  <td>{{$data->diskon}} %</td>
                </tr>
	            <tr>
	              <th style="width:50%">Total Harga + PPn</th>
	              <th>:</th>
	              <td>{{format_rupiah($data->jumlah_byr)}}</td>
	            </tr>
	            <tr>
	              <th>Bayar</th>
	              <th>:</th>
	              <td>{{format_rupiah($data->bayar)}}</td>
	            </tr>
	            <tr>
	              <th>Kembali</th>
	              <th>:</th>
	              <td>{{format_rupiah($data->kembali)}}</td>
	            </tr>
	          </table>
	        </div>
    	</div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
