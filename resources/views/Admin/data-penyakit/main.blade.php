@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Data Resep</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    @if (session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                    </div>
                    @endif
                    <h5>Penyakit Terbanyak Berdasarkan Banyaknya Hasil Diagnosa</h5>
                    <table class="table table-hover data-penyakit force-fullwidth">
                        <thead>
                            <th>No.</th>
                            <th>Nama Penyakit</th>
                            <th>Hasil Diagnosa</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var data_penyakit = $('.data-penyakit').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-penyakit') }}",
            columns:[
                {data:'id_resep',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'diagnosa',name:'diagnosa'},
                {data:'count_diagnosa',name:'count_diagnosa'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 2, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        data_penyakit.on( 'order.dt search.dt', function () {
            data_penyakit.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();
    });
</script>
@endsection