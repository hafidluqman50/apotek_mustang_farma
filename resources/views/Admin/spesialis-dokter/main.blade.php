@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Data Dokter</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    @if (session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                    </div>
                    @endif
                    <table class="table table-hover force-fullwidth data-spesialis-dokter">
                        <thead>
                            <th>No.</th>
                            <th>Nama Spesialis</th>
                            <th>#</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>  
        </div>  
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var spesialis_dokter = $('.data-spesialis-dokter').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-spesialis-dokter') }}",
            columns:[
                {data:'id_spesialis_dokter',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_spesialis_dokter',name:'nama_spesialis_dokter'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        spesialis_dokter.on( 'order.dt search.dt', function () {
            spesialis_dokter.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();
    });
</script>
@endsection