@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Dokter</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
            <div class="box box-default">
                <form action="{{url('/admin/data-spesialis-dokter/save')}}" method="POST">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Nama Spesialis Dokter</label>
                            <input type="text" name="nama_spesialis_dokter" class="form-control" value="{{isset($row)?$row->nama_spesialis_dokter:''}}" placeholder="Isi Nama Spesialis Dokter" required="required" autofocus="autofocus">
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="id_spesialis_dokter" value="{{isset($row)?$row->id_spesialis_dokter:''}}">
                        <button class="btn btn-primary">
                            Simpan <span class="fa fa-save"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection