@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Dokter</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/data-dokter/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Nama Dokter</label>
							<input type="text" name="nama_dokter" class="form-control" value="{{isset($row)?$row->nama_dokter:''}}" placeholder="Isi Nama Dokter" required="required" autofocus="autofocus">
						</div>
                        <div class="form-group">
                            <label for="">Spesialis Dokter</label>
                            <select name="id_spesialis_dokter" class="form-control select2">
                                <option selected="selected" disabled="disabled">=== Pilih Spesialis Dokter ===</option>
                                @foreach ($spesialis_dokter as $value)
                                <option value="{{ $value->id_spesialis_dokter }}" @if (isset($row)){!!$row->id_spesialis_dokter == $value->id_spesialis_dokter ? 'selected="selected"' : ''!!}@endif>{{ $value->nama_spesialis_dokter }}</option>
                                @endforeach
                            </select>
                        </div>
						<div class="form-group">
							<label for="">Nomor Telepon Dokter</label>
							<input type="number" name="nomor_telepon_dokter" class="form-control" value="{{isset($row)?$row->nomor_telepon_dokter:''}}" placeholder="Isi Nomor Telepon Dokter">
						</div>
						<div class="form-group">
							<label>Biaya Konsul Dokter</label>
							<select name="biaya_dokter" class="form-control" required="required">
								<option value="" selected="selected" disabled="disabled">=== Pilih Biaya ===</option>
								<option value="50000" @if (isset($row)){!!$row->biaya_dokter=='50000'?'selected="selected"':''!!}@endif>Rp. 50.000,00</option>
                                <option value="55000" @if (isset($row)){!!$row->biaya_dokter=='55000'?'selected="selected"':''!!}@endif>Rp. 55.000,00</option>
								<option value="100000" @if (isset($row)){!!$row->biaya_dokter=='100000'?'selected="selected"':''!!}@endif>Rp. 100.000,00</option>
								<option value="150000" @if (isset($row)){!!$row->biaya_dokter=='150000'?'selected="selected"':''!!}@endif>Rp. 150.000,00</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Status Dokter</label>
							<select name="status_dokter" class="form-control">
								<option selected="selected" disabled="disabled">=== Pilih Status Dokter ===</option>
								<option value="1" @if (isset($row)){!!$row->status_dokter==1?'selected="selected"':''!!}@endif>Aktif</option>
								<option value="0" @if (isset($row)){!!$row->status_dokter==0?'selected="selected"':''!!}@endif>Non Aktif</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" name="username" class="form-control" value="{{isset($row) ? $row->username : ''}}" required="required" placeholder="Isi Username" {!!isset($row) ? 'disabled="disabled"' : ''!!}>
							{!!isset($row)?'<input type="checkbox" id="sip">Ubah Username':''!!}
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="password" name="password" class="form-control" {!!isset($row) ? '' : 'required="required"'!!}placeholder="Isi Password">
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id_dokter" value="{{isset($row)?$row->id_dokter:''}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection