@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Dokter</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover force-fullwidth data-dokter">
						<thead>
							<th>No.</th>
							<th>Nama Dokter</th>
                            <th>Nama Spesialis Dokter</th>
							<th>Nomor Telepon Dokter</th>
							{{-- <th>Spesialis Poli</th> --}}
							<th>Biaya Dokter</th>
							<th>Status Dokter</th>
							<th>Username</th>
							<th>Status Akun</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>	
		</div>	
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var dokter = $('.data-dokter').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-dokter') }}",
            columns:[
                {data:'id_dokter',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_dokter',name:'nama_dokter'},
                {data:'nama_spesialis_dokter',name:'nama_spesialis_dokter'},
                {data:'nomor_telepon_dokter',name:'nomor_telepon_dokter'},
                // {data:'spesialis_poli',name:'spesialis_poli'},
                {data:'biaya_dokter',name:'biaya_dokter'},
                {data:'status_dokter',name:'status_dokter'},
                {data:'username',name:'username'},
                {data:'status_akun',name:'status_akun'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        dokter.on( 'order.dt search.dt', function () {
	        dokter.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection