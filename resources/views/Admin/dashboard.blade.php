@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Dashboard</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-text-center">Rekam Medis Kontrol</h3>
				</div>	
				<div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" id="rmKontrolTabs">
                            <li class="active"><a href="#tab_1" data-toggle="tab" data-id="1">Semua Data</a></li>
                            <li><a href="#tab_2" data-toggle="tab" data-id="2">H-1 &amp; H</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table class="table table table-hover force-fullwidth data-rekam-medis-all">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal Periksa</th>
                                        <th>Tgl Kontrol Selanjutnya</th>
                                        <th>Nama Pasien</th>
                                        <th>Tinggi Badan</th>
                                        <th>Berat Badan</th>
                                        <th>Tekanan Darah</th>
                                        <th>Suhu Badan</th>
                                        <th>Tindakan</th>
                                        <th>Keluhan</th>
                                        <th>Diagnosa</th>
                                        <th>Anemnesis</th>
                                        <th>Pemeriksaan Penunjang</th>
                                        <th>Terapi</th>
                                        <th>Rujukan</th>
                                        <th>#</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <table class="table table table-hover force-fullwidth data-rekam-medis">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal Periksa</th>
                                        <th>Tgl Kontrol Selanjutnya</th>
                                        <th>Nama Pasien</th>
                                        <th>Tinggi Badan</th>
                                        <th>Berat Badan</th>
                                        <th>Tekanan Darah</th>
                                        <th>Suhu Badan</th>
                                        <th>Tindakan</th>
                                        <th>Keluhan</th>
                                        <th>Diagnosa</th>
                                        <th>Anemnesis</th>
                                        <th>Pemeriksaan Penunjang</th>
                                        <th>Terapi</th>
                                        <th>Rujukan</th>
                                        <th>#</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
					{{-- <h4 class="text-center">Selamat Datang Di Halaman Administrator!</h4> --}}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
    $(() => {
        var pasien = $('.data-rekam-medis').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/pasien-rekam-medis-admin') }}",
            columns:[
                {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tgl_resep',name:'tgl_resep'},
                {data:'tgl_kontrol_selanjutnya',name:'tgl_kontrol_selanjutnya'},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'tinggi_badan',name:'tinggi_badan'},
                {data:'berat_badan',name:'berat_badan'},
                {data:'tekanan_darah',name:'tekanan_darah'},
                {data:'suhu_badan',name:'suhu_badan'},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'keluhan_utama_resep',name:'keluhan_utama_resep'},
                {data:'diagnosa',name:'diagnosa'},
                {data:'anemnesis',name:'anemnesis'},
                {data:'pemeriksaan_penunjang',name:'pemeriksaan_penunjang'},
                {data:'terapi',name:'terapi'},
                {data:'rujukan',name:'rujukan'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [],
            responsive:true,
            fixedColumns: true
        });
        pasien.on( 'order.dt search.dt', function () {
            pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var pasienAll = $('.data-rekam-medis-all').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/pasien-rekam-medis-admin-all') }}",
            columns:[
                {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tgl_resep',name:'tgl_resep'},
                {data:'tgl_kontrol_selanjutnya',name:'tgl_kontrol_selanjutnya'},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'tinggi_badan',name:'tinggi_badan'},
                {data:'berat_badan',name:'berat_badan'},
                {data:'tekanan_darah',name:'tekanan_darah'},
                {data:'suhu_badan',name:'suhu_badan'},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'keluhan_utama_resep',name:'keluhan_utama_resep'},
                {data:'diagnosa',name:'diagnosa'},
                {data:'anemnesis',name:'anemnesis'},
                {data:'pemeriksaan_penunjang',name:'pemeriksaan_penunjang'},
                {data:'terapi',name:'terapi'},
                {data:'rujukan',name:'rujukan'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [],
            responsive:true,
            fixedColumns: true
        });
        pasienAll.on( 'order.dt search.dt', function () {
            pasienAll.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();
    })
</script>
@endsection