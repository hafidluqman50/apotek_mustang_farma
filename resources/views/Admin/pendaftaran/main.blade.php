@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Pendaftaran</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @elseif (session()->has('log'))
            <div class="alert alert-danger alert-dismissible">
                {{session('log')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
			<div class="box box-default">
				<div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" id="dokterTabs">
                            @foreach($dokter as $key => $dok)
                            <li @if ($key == 0) class="active" @endif><a href="#tab_1" data-toggle="tab" data-id="{{ $dok->id_dokter }}">{{ $dok->nama_dokter }}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table class="table table-hover force-fullwidth data-daftar">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal Daftar</th>
                                        <th>Nama Dokter</th>
                                        <th>Nama Pasien</th>
                                        <th>Kategori Pasien</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Umur Pasien</th>
                                        <th>Tinggi Badan</th>
                                        <th>Berat Badan</th>
                                        <th>Tekanan Darah</th>
                                        <th>Suhu Badan</th>
                                        {{-- <th>Hasil Cek</th> --}}
                                        <th>Ket. Daftar</th>
                                        <th>#</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        function loadData(id) {
            var pasien = $('.data-daftar').DataTable({
                processing:true,
                serverSide:true,
                ajax:"{{ url('/datatables/data-pendaftaran') }}" + '/' + id,
                columns:[
                    {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                        return meta.row + meta.settings._iDisplayStart+1;
                    }},
                    {data:'tgl_daftar',name:'tgl_daftar'},
                    {data:'nama_dokter',name:'nama_dokter'},
                    {data:'nama_pasien',name:'nama_pasien'},
                    {data:'nama_kategori',name:'nama_kategori'},
                    {data:'jenis_kelamin',name:'jenis_kelamin'},
                    {data:'umur_pasien',name:'umur_pasien'},
                    {data:'tinggi_badan',name:'tinggi_badan'},
                    {data:'berat_badan',name:'berat_badan'},
                    {data:'tekanan_darah',name:'tekanan_darah'},
                    {data:'suhu_badan',name:'suhu_badan'},
                    // {data:'hasil_cek',name:'hasil_cek'},
                    {data:'ket_daftar',name:'ket_daftar'},
                    {data:'action',name:'action',searchable:false,orderable:false}
                ],
                scrollCollapse: true,
                columnDefs: [ {
                sortable: true,
                "class": "index",
                }],
                scrollX:true,
                order: [],
                responsive:true,
                fixedColumns: true,
                destroy: true,
            });
            pasien.on( 'order.dt search.dt', function () {
                pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        }
        
        loadData(1)
        
        $('#dokterTabs a').click(function (e) {
            e.preventDefault()
            let id = $(this).data('id')
            loadData(id)
            $(this).tab('show')
            console.log(id)
        })
	});
</script>
@endsection