@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Pendaftaran (BPJS)</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @elseif (session()->has('log'))
            <div class="alert alert-danger alert-dismissible">
                {{session('log')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
            <div class="box box-default">
                <div class="box-body">
                    {{--<div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" id="dokterTabs">
                            @foreach($dokter as $key => $dok)
                            <li @if ($key == 0) class="active" @endif><a href="#tab_1" data-toggle="tab" data-id="{{ $dok->id_dokter }}">{{ $dok->nama_dokter }}</a></li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1"> --}}
                                <table class="table table-hover force-fullwidth data-daftar">
                                    <thead>
                                        <th>No.</th>
                                        <th>Tanggal Daftar</th>
                                        <th>No Kartu</th>
                                        <th>Nama Pasien</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Tinggi Badan</th>
                                        <th>Berat Badan</th>
                                        <th>Tekanan Darah</th>
                                        <th>Laju Respirasi</th>
                                        <th>Denyut Jantung</th>
                                        <th>#</th>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            {{-- </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        function loadData(id) {
            var pasien = $('.data-daftar').DataTable({
                processing:true,
                serverSide:true,
                ajax:"{{ url('/datatables/data-pendaftaran-bpjs') }}",
                columns:[
                    {data:'id',searchable:false,render:function(data,type,row,meta){
                        return meta.row + meta.settings._iDisplayStart+1;
                    }},
                    {data:'tglDaftar',name:'tglDaftar'},
                    {data:'noKartu',name:'noKartu'},
                    {data:'namaPasien',name:'namaPasien'},
                    {data:'jenisKelamin',name:'jenisKelamin'},
                    {data:'tinggiBadan',name:'tinggiBadan'},
                    {data:'beratBadan',name:'beratBadan'},
                    {data:'tekananDarah',name:'tekananDarah'},
                    {data:'respRate',name:'respRate'},
                    {data:'heartRate',name:'heartRate'},
                    {data:'action',name:'action',searchable:false,orderable:false}
                ],
                scrollCollapse: true,
                columnDefs: [ {
                sortable: true,
                "class": "index",
                }],
                scrollX:true,
                order: [],
                responsive:true,
                fixedColumns: true,
                destroy: true,
            });
            pasien.on( 'order.dt search.dt', function () {
                pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        }
        
        loadData(1)
        
        $('#dokterTabs a').click(function (e) {
            e.preventDefault()
            let id = $(this).data('id')
            loadData(id)
            $(this).tab('show')
            console.log(id)
        })
    });
</script>
@endsection