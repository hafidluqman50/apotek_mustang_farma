@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Pendaftaran Edit</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
            <div class="box box-default">
                <div class="box box-header with-border">
                    <a href="{{ url('/admin/laporan-pendaftaran') }}">
                        <button class="btn btn-default"><span class="fa fa-arrow-left"></span> Kembali</button>
                    </a>
                </div>
                <form action="{{url('/admin/pendaftaran/update',$id)}}" method="POST">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div id="pasien">
                            <div class="form-group">
                                <label for="">Kategori Pasien</label>
                                <select class="form-control select2" id="kategori_pasien" required="required" autofocus="autofocus">
                                    <option value="" selected="selected" disabled="disabled">=== Pilih Kategori Pasien ===</option>
                                    @foreach ($kategori_pasien as $element)
                                    <option value="{{$element->id_kategori_pasien}}" {!! $row->id_kategori_pasien == $element->id_kategori_pasien ? 'selected="selected"' : '' !!}>{{$element->nama_kategori}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Pasien</label>
                                <select name="pasien" class="form-control select2" id="pasien-input" required="required">
                                    <option value="" selected="selected" disabled="disabled">=== Pilih Pasien ===</option>
                                    @php
                                        $get_pasien = $pasien->where('id_kategori_pasien',$row->id_kategori_pasien)
                                                             ->get();
                                    @endphp
                                    @foreach ($get_pasien as $element)
                                    <option value="{{ $element->id_pasien }}" {!!$element->id_pasien == $row->id_pasien ? 'selected="selected"' : ''!!}>{{ $element->nama_pasien }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nomor BPJS</label>
                                <input type="text" name="no_bpjs" id="no-bpjs" class="form-control" value="{{ $row->no_bpjs }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Dokter</label>
                            <select name="dokter" class="form-control select2" required="required">
                                <option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
                                @foreach ($dokter as $element)
                                <option value="{{$element->id_dokter}}"{!!$element->id_dokter == $row->id_dokter ? 'selected="selected"' : ''!!}>{{$element->nama_dokter.' | '.$element->spesialis->nama_spesialis_dokter}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Keluhan</label>
                            <input type="text" name="keluhan" class="form-control" placeholder="Isi Keluhan" value="{{ $row->keluhan }}">
                        </div>
                        <div class="form-group">
                            <label for="">Tinggi Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="tinggi_badan" class="form-control" id="tinggi-badan" placeholder="Isi Tinggi Badan" value="{{ $row->tinggi_badan }}">
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Berat Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="berat_badan" class="form-control" id="berat-badan" placeholder="Isi Berat Badan" value="{{ $row->berat_badan }}">
                                <span class="input-group-addon">kg</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Tekanan Darah</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="tekanan_darah" class="form-control" id="tinggi-badan" placeholder="Isi Tekanan Darah" value="{{ $row->tekanan_darah }}">
                                <span class="input-group-addon">mmHg</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Suhu Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="suhu_badan" class="form-control" id="tinggi-badan" placeholder="Isi Suhu Badan" value="{{ $row->suhu_badan }}">
                                <span class="input-group-addon">&deg;C</span>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Cek Kadar Kolesterol</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="kolesterol" class="form-control" id="kolesterol" placeholder="Isi Hasil Kolesterol (Jika Ada)">
                                <span class="input-group-addon">mg/dL</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Cek Kadar Gula Darah</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="gula_darah" class="form-control" id="gula_darah" placeholder="Isi Hasil Gula Darah (Jika Ada)">
                                <span class="input-group-addon">mg/dL</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Cek Kadar Asam Urat</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)">
                                <span class="input-group-addon">mg/dL</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Cek Kadar Asam Urat</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)">
                                <span class="input-group-addon">mg/dL</span>
                            </div>
                        </div> --}}
                        <div class="form-group">
                            <label for="">Pemeriksaan Penunjang</label>
                            <input type="text" name="pemeriksaan_penunjang" class="form-control" placeholder="Isi Pemeriksaan Penunjang" value="{{ $row->pemeriksaan_penunjang }}">
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">Simpan <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){

        $('#show').click(function(){
            $(this).hide();
            $('#cancel').show();
            $('#input-pasien').slideDown();
            $('#input-pasien').find('input,select,textarea').attr('required','required');
            $('#pasien').slideUp();
            $('#pasien').find('input,select,textarea').removeAttr('required');
            $('.select2').select2({
                width:'resolve'
            })
        });

        $('#cancel').click(function(){
            $(this).hide();
            $('#show').show();
            $('#pasien').slideDown();
            $('#pasien').find('input,select,textarea').attr('required','required');
            $('#input-pasien').slideUp();
            $('#input-pasien').find('input,select,textarea').removeAttr('required');
        });

        $('#kategori_pasien').change(function(){
            var val = $(this).val();
            var getUrl = base_url+'/ajax/get-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="pasien"]').removeAttr('disabled');
                $('select[name="pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('#pasien-input').change(function(){
            let val    = $(this).val()
            let getUrl = `${base_url}/ajax/get-info-pasien/${val}`
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('#no-bpjs').val(done.no_bpjs)
            })
            .fail(function(fail) {
                console.log(fail);
            });
            
        })
    });
</script>
@endsection