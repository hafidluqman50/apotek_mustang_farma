@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Ubah Hasil Cek - Pendaftaran</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/pendaftaran/check/' . $row->id_daftar)}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div id="pasien">
							<div class="form-group">
								<label for="">Nama Pasien</label>
								<input type="text" name="nama_pasien" id="nama_pasien" class="form-control" value="{{ $row->patient->nama_pasien }}" readonly>
							</div>
							<div class="form-group">
								<label for="">Alamat</label>
								<input type="text" name="alamat_pasien" id="alamat_pasien" class="form-control" value="{{ $row->patient->alamat_pasien }}" readonly>
							</div>
						</div>
						<div class="form-group">
							<label for="">Cek Kadar Kolesterol</label>
							<div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
								<input type="text" name="kolesterol" class="form-control" id="kolesterol" placeholder="Isi Hasil Kolesterol (Jika Ada)" value="{{ isset($checkResult->kolesterol) ? $checkResult->kolesterol : '' }}">
								<span class="input-group-addon">mg/dL</span>
							</div>
						</div>
						<div class="form-group">
							<label for="">Cek Kadar Gula Darah</label>
							<div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
								<input type="text" name="gula_darah" class="form-control" id="gula_darah" placeholder="Isi Hasil Gula Darah (Jika Ada)" value="{{ isset($checkResult->kolesterol) ? $checkResult->kolesterol : '' }}">
								<span class="input-group-addon">mg/dL</span>
							</div>
						</div>
						<div class="form-group">
							<label for="">Cek Kadar Asam Urat</label>
							<div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
								<input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)" value="{{ isset($checkResult->kolesterol) ? $checkResult->kolesterol : '' }}">
								<span class="input-group-addon">mg/dL</span>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<button class="btn btn-primary" type="submit">Simpan <span class="fa fa-save"></span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection