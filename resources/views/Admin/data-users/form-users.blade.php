@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Users</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/data-users/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Nama</label>
							<input type="text" name="nama" class="form-control" value="{{isset($row)?$row->name:''}}" placeholder="Isi Nama" required="required" autofocus="autofocus">
						</div>
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" name="username" class="form-control" value="{{isset($row)?$row->username:''}}" placeholder="Isi Username" required="required" {!!isset($row)?'disabled="disabled"':''!!}>
							{!!isset($row)?'<input type="checkbox" id="sip">Ubah Username':''!!}
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="password" name="password" class="form-control" {!!isset($row)?'':'required="required"'!!} placeholder="Isi Password">
						</div>
						<div class="form-group">
							<label for="">Level User</label>
							<select name="level_user" class="form-control" required="required">
								<option selected="selected" disabled="disabled">=== Pilih Level User ===</option>
								<option value="4" @if (isset($row)){!!$row->level_user==4?'selected="selected"':''!!}@endif>Inventory</option>
								<option value="2" @if (isset($row)){!!$row->level_user==2?'selected="selected"':''!!}@endif>Operator</option>
								{{-- <option value="1" @if (isset($row)){!!$row->level_user==1?'selected="selected"':''!!}@endif>Resep</option> --}}
								<option value="0" @if (isset($row)){!!$row->level_user==0?'selected="selected"':''!!}@endif>Kasir</option>
							</select>
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_users:''}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>	
		</div>
	</div>
</section>
@endsection