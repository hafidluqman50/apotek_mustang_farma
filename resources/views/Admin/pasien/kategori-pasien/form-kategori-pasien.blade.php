@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Kategori Pasien</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/admin/data-kategori-pasien/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Nama Kategori Pasien</label>
							<input type="text" name="kategori_pasien" class="form-control" value="{{isset($row)?$row->nama_kategori:''}}" placeholder="Isi Nama Kategori Pasien" required="required" autofocus="autofocus">
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_kategori_pasien:''}}">
						<button type="submit" class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection