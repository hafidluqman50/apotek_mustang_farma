@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Kategori Pasien</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover data-kategori-pasien force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Nama Kategori</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var kategori_pasien = $('.data-kategori-pasien').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-kategori-pasien') }}",
            columns:[
                {data:'id_kategori_pasien',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_kategori',name:'nama_kategori'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        kategori_pasien.on( 'order.dt search.dt', function () {
	        kategori_pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection