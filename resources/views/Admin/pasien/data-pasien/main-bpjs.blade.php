@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Data Pasien</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-body">
                    @if (session()->has('message'))
                    <div class="alert alert-success alert-dismissible">
                        {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                    </div>
                    @endif
                    <table class="table table-hover data-pasien force-fullwidth">
                        <thead>
                            <th>No.</th>
                            <th>Kode Spesialis</th>
                            <th>Nama Spesialis</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        let data = @json($data);
        var pasien = $('.data-pasien').DataTable({
            processing:true,
            serverSide:true,
            // ajax:"{{ url('/datatables/data-pasien') }}",
            columns:[
                {data:'id',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'kdSpesialis',name:'kdSpesialis'},
                {data:'nmSpesialis',name:'nmSpesialis'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 1, 'asc' ]],
            responsive:true,
            fixedColumns: true
        });
        pasien.on( 'order.dt search.dt', function () {
            pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();
    });
</script>
@endsection