@extends('Admin.layout.layout-app')

@section('content')
    <section class="content-header">
        <h1>Laporan Data</h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4>Laporan Rekap</h4>
                    </div>
                    <div class="box-body">
                        <form action="{{ url('/admin/laporan-rekap/cetak') }}">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Dari</label>
                                    <input type="text" name="from" class="form-control datepicker" placeholder="dd-mm-yyyy" required="required" {!!isset($_GET['from'])?'value="'.$_GET['from'].'"':''!!}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Ke</label>
                                    <input type="text" name="to" class="form-control datepicker" placeholder="dd-mm-yyyy" required="required" {!!isset($_GET['to'])?'value="'.$_GET['to'].'"':''!!}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-pemasukan" style="margin-bottom:2%;">
                                        Laporan Pemasukan
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-pengeluaran" style="margin-bottom:2%;">
                                        Laporan Pengeluaran
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-distributor" style="margin-bottom:2%;">
                                        Laporan Distributor
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-total-penjualan" style="margin-bottom:2%;">
                                        Laporan Total Penjualan
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-transaksi" style="margin-bottom:2%;">
                                        Laporan Transaksi
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-pendapatan-tindakan" style="margin-bottom:2%;">
                                        Laporan Pendapatan Tindakan
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-pendapatan-penjualan-obat" style="margin-bottom:2%;">
                                        Laporan Pendapatan Penjualan Obat
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-jasa-dokter" style="margin-bottom:2%;">
                                        Laporan Jasa Dokter
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-keluhan-pasien" style="margin-bottom:2%;">
                                        Laporan Rujukan Pasien
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-rekam-medis" style="margin-bottom:2%;">
                                        Laporan Rekam Medis
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-pasien" style="margin-bottom:2%;">
                                        Laporan Pasien
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-kunjungan" style="margin-bottom:2%;">
                                        Laporan Kunjungan
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-statistik" style="margin-bottom:2%;">
                                        Laporan Statistik
                                    </button>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" name="btn_act" value="laporan-bulanan" style="margin-bottom:2%;">
                                        Laporan Bulanan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-6">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h4>Laporan Pembelian</h4>
                    </div>
                    <div class="box-body">
                        <form action="{{ url('/admin/laporan-data/pembelian') }}">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Dari</label>
                                    <input type="text" name="from" class="form-control datepicker" placeholder="dd-mm-yyyy" required="required" {!!isset($_GET['from'])?'value="'.$_GET['from'].'"':''!!}>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Ke</label>
                                    <input type="text" name="to" class="form-control datepicker" placeholder="dd-mm-yyyy" required="required" {!!isset($_GET['to'])?'value="'.$_GET['to'].'"':''!!}>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-faktur" style="margin-bottom:2%;">
                                    Laporan Faktur
                                </button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-beli-tunai" style="margin-bottom:2%;">
                                    Laporan Beli Tunai
                                </button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-jatuh-tempo" style="margin-bottom:2%;">
                                    Laporan Jatuh Tempo
                                </button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-retur-barang" style="margin-bottom:2%;">
                                    Laporan Retur Barang
                                </button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-konsinyasi" style="margin-bottom:2%;">
                                    Laporan Konsinyasi
                                </button>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-success" name="btn_act" value="laporan-konsinyasi-jatuh-tempo" style="margin-bottom:2%;">
                                    Laporan Konsinyasi Jatuh Tempo
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> --}}
        </div>
    </section>
@endsection