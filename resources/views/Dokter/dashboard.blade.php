@extends('Dokter.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Dashboard</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-text-center">Dashboard Dokter</h3>
				</div>	
				<div class="box-body">
					<div class="table-responsive">
						<table class="table table-hover force-fullwidth data-daftar">
							<thead>
								<th>No.</th>
								<th>Tanggal Daftar</th>
								<th>Nama Pasien</th>
								<th>Nama Kategori</th>
								<th>Jenis Kelamin</th>
								<th>Umur Pasien</th>
								<th>Tinggi Badan</th>
								<th>Berat Badan</th>
								<th>Tekanan Darah</th>
								<th>Suhu Badan</th>
								{{-- <th>Hasil Cek</th> --}}
								<th>Keluhan</th>
								<th>Ket. Daftar</th>
								<th>#</th>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('js')
<script>
	$(function(){
        function loadData(id) {
            var pasien = $('.data-daftar').DataTable({
                processing:true,
                serverSide:true,
                ajax:"{{ url('/datatables/data-pendaftaran') }}" + '/' + id,
                columns:[
                    {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                        return meta.row + meta.settings._iDisplayStart+1;
                    }},
                    {data:'tgl_daftar',name:'tgl_daftar'},
                    {data:'nama_pasien',name:'nama_pasien'},
                    {data:'nama_kategori',name:'nama_kategori'},
                    {data:'jenis_kelamin',name:'jenis_kelamin'},
                    {data:'umur_pasien',name:'umur_pasien'},
                    {data:'tinggi_badan',name:'tinggi_badan'},
                    {data:'berat_badan',name:'berat_badan'},
                    {data:'tekanan_darah',name:'tekanan_darah'},
                    {data:'suhu_badan',name:'suhu_badan'},
                    // {data:'hasil_cek',name:'hasil_cek'},
                    {data:'keluhan',name:'keluhan'},
                    {data:'ket_daftar',name:'ket_daftar'},
					{data:'link_rm',name:'link_rm'},
                ],
                scrollCollapse: true,
                columnDefs: [ {
                sortable: true,
                "class": "index",
                }],
                scrollX:true,
                order: [],
                responsive:true,
                fixedColumns: true,
                destroy: true,
            });
            pasien.on( 'order.dt search.dt', function () {
                pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        }
        
        loadData({{ $id_dokter }})
	});
</script>
@endsection