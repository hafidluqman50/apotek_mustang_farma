
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}}</strong> Jupiter
  </footer>

</div>
<!-- ./wrapper -->
</body>
</html>

<!-- jQuery 3 -->
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/dist/js/adminlte.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{{asset('assets/dist/js/pages/dashboard2.js')}}"></script> --}}
{{-- <script src="{{asset('assets/dist/js/demo.js')}}"></script> --}}
<script src="{{asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{ asset('assets/summernote-0.8.18-dist/summernote.min.js') }}"></script>
<script src="{{asset('assets/dist/js/custom.js')}}"></script>
<script>
  $(function(){
    $('.summernote').summernote({
        height:100
    })
  	$('.select2').select2({
  		width:'resolve'
  	});
    $('.datepicker').datepicker({
        format:'dd-mm-yyyy',
        autoclose: true,
        todayBtn: "linked",
        todayHighlight:true
        // showButtonPanel:true,
    })
  });
</script>