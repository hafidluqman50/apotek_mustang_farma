@extends('Dokter.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Rekam Medis</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-header with-border">
					<a href="{{url('/dokter/laporan-rekam-medis')}}">
						<button class="btn btn-default">
							<span class="fa fa-long-arrow-left"></span> Kembali
						</button>
					</a>
				</div>
				<div class="box-body">
                    @if (session()->has('message'))
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissible">
                            {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                        </div>
                    </div>
                    @endif
					<h5>Nama Pasien : {{$pasien->nama_pasien}}</h5>
					<h5>Kategori Pasien : {{$pasien->nama_kategori}}</h5>
					<h5>Tanggal Lahir : {{human_date($pasien->tanggal_lahir)}}</h5>
					<h5>Umur Pasien : {{hitung_umur($pasien->tanggal_lahir)}}</h5>
					<h5>Jenis Kelamin : {{$pasien->jenis_kelamin}}</h5>
					<table class="table table table-hover force-fullwidth data-rekam-medis">
						<thead>
							<th>No.</th>
							<th>Tanggal Periksa</th>
                            <th>Tanggal Kontrol Selanjutnya</th>
							<th>Tinggi Badan</th>
							<th>Berat Badan</th>
							<th>Tekanan Darah</th>
							<th>Suhu Badan</th>
                            {{-- <th>Hasil Cek Darah</th> --}}
                            <th>Tindakan</th>
							<th>Keluhan</th>
							<th>Diagnosa</th>
                            <th>Anemnesis</th>
                            <th>Pemeriksaan Fisik</th>
                            <th>Pemeriksaan Penunjang</th>
                            <th>Terapi</th>
                            <th>Rujukan</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var pasien = $('.data-rekam-medis').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/pasien-rekam-medis/'.$id.'/rekam/'.$id_pasien) }}",
            columns:[
                {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tgl_resep',name:'tgl_resep'},
                {data:'tgl_kontrol_selanjutnya',name:'tgl_kontrol_selanjutnya'},
                {data:'tinggi_badan_resep',name:'tinggi_badan_resep'},
                {data:'berat_badan_resep',name:'berat_badan_resep'},
                {data:'tekanan_darah_resep',name:'tekanan_darah_resep'},
                {data:'suhu_badan_resep',name:'suhu_badan_resep'},
                // {data:'hasil_cek_darah_resep',name:'hasil_cek_darah_resep'},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'keluhan_utama_resep',name:'keluhan_utama_resep'},
                {data:'diagnosa',name:'diagnosa'},
                {data:'pemeriksaan_fisik',name:'pemeriksaan_fisik'},
                {data:'anemnesis',name:'anemnesis'},
                {data:'pemeriksaan_penunjang',name:'pemeriksaan_penunjang'},
                {data:'terapi',name:'terapi'},
                {data:'rujukan',name:'rujukan'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [],
            responsive:true,
            fixedColumns: true
        });
        pasien.on( 'order.dt search.dt', function () {
	        pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection