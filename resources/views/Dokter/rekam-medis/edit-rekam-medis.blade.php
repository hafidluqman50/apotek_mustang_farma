@extends('Dokter.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Edit Rekam Medis</h1>
</section>

<section class="content">
    <form action="{{url('/dokter/laporan-rekam-medis/'.$id.'/rekam/'.$id_pasien.'/update/'.$id_resep)}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            @if (session()->has('berhasil'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">
                    {{session('berhasil')}} <button class="close" data-dismiss="alert">X</button>
                </div>
            </div>
            @elseif(session()->has('habis'))
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible">
                    <button class="close" data-dismiss="alert">X</button>
                    <ul>
                    @foreach (session('habis') as $key => $element)
                        <li>{!!$element['text']!!}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="{{ url('/dokter/laporan-rekam-medis/'.$id.'/rekam/'.$id_pasien) }}">
                            <button class="btn btn-default" type="button">
                                <span class="fa fa-long-arrow-left"></span> Kembali
                            </button>
                        </a>
                    </div>
                    <div class="box-body">
                        <div id="form-customer">
                            <div class="form-group">
                                <label for="">Pasien</label>
                                <input type="text" name="pasien" class="form-control" value="{{ human_date($row->tgl_daftar).' | '.$row->nama_pasien.' | '.hitung_umur($row->tanggal_lahir).' | '.$row->jenis_kelamin.' | '.$row->nama_dokter }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="">Keluhan Utama</label>
                                <input type="text" name="keluhan" class="form-control" value="{{ $row->keluhan_utama_resep }}"  placeholder="Isi Keluhan Utama" required>
                            </div>
                            <div class="form-group">
                                <label for="">Tinggi Badan</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="tinggi_badan" class="form-control" id="tinggi-badan" placeholder="Isi Tinggi Badan" value="{{ $row->tinggi_badan }}" readonly>
                                    <span class="input-group-addon">cm</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Berat Badan</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="berat_badan" class="form-control" id="berat-badan" placeholder="Isi Berat Badan" value="{{ $row->berat_badan }}" readonly>
                                    <span class="input-group-addon">kg</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Tekanan Darah</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="tekanan_darah" class="form-control" id="tinggi-badan" placeholder="Isi Tekanan Darah" value="{{ $row->tekanan_darah }}" readonly>
                                    <span class="input-group-addon">mmHg</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Suhu Badan</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="suhu_badan" class="form-control" id="tinggi-badan" placeholder="Isi Suhu Badan" value="{{ $row->suhu_badan }}" readonly>
                                    <span class="input-group-addon">&deg;C</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Cek Kadar Kolesterol</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="kolesterol" class="form-control" id="kolesterol" placeholder="Isi Hasil Kolesterol (Jika Ada)" value="{{ $kolesterol }}" readonly>
                                    <span class="input-group-addon">mg/dL</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Cek Kadar Gula Darah</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="gula_darah" class="form-control" id="gula_darah" placeholder="Isi Hasil Gula Darah (Jika Ada)" value="{{ $gula_darah }}" readonly>
                                    <span class="input-group-addon">mg/dL</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Cek Kadar Asam Urat</label>
                                <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                    <input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)" value="{{ $asam_urat }}" readonly>
                                    <span class="input-group-addon">mg/dL</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Anemnesis</label>
                                <input type="text" name="anemnesis" class="form-control" value="{{$row->anemnesis}}" placeholder="Isi Anemnesis">
                            </div>
                            <div class="form-group">
                                <label for="">Pemeriksaan Penunjang</label>
                                <input type="text" name="pemeriksaan_penunjang" class="form-control" value="{{$row->pemeriksaan_penunjang}}" placeholder="Isi Pemeriksaan Penunjang">
                            </div>
                            <div class="form-group">
                                <label>Tindakan</label>
                                <select name="tindakan_lab" class="form-control select2">
                                    <option value="" selected disabled>=== Pilih Tindakan Lab ===</option>
                                    @foreach ($tindakan_lab as $value)
                                    <option value="{{ $value->id_tindakan_lab }}" @if (isset($row)){!!$row->id_tindakan_lab == $value->id_tindakan_lab ? 'selected="selected"' : ''!!}@endif>{{ $value->nama_tindakan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Diagnosa</label>
                                <input type="text" name="diagnosa" class="form-control" value="{{$row->diagnosa}}" required="required" placeholder="Isi Diagnosa">
                            </div>
                            <div class="form-group">
                                <label for="">Terapi</label>
                                <input type="text" name="terapi" class="form-control" value="{{$row->terapi}}" placeholder="Isi Terapi">
                            </div>
                            <div class="form-group">
                                <label for="">Rujukan</label>
                                <input type="text" name="rujukan" class="form-control" value="{{$row->rujukan}}" placeholder="Isi Rujukan">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-warning">
                            Edit <span class="fa fa-save"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var select_ajax = 2;
        var get_ajax    = 2;
        var num_input   = 2;
        var num_cancel  = 2;
        var num_racik   = 2;
        var num_manual  = 2;

        $('.kategori-pasien').select2({disabled:true});

        $('select[name="pendaftaran"]').change(function(){
            var val = $(this).val();
            $.ajax({
                url: '..'+'/ajax/get-data-resep/'+val
            })
            .done(function(done) {
                $('#form-customer > input[name="pasien"]').remove();
                $('#form-customer > input[name="dokter"]').remove();
                $('#form-customer').append(done.html);
                $('input[name="keluhan"]').val(done.data_resep['keluhan'])
                $('input[name="pemeriksaan_penunjang"]').val(done.data_resep['pemeriksaan_penunjang'])
                $('input[name="tinggi_badan"]').val(done.data_resep['tinggi_badan'])
                $('input[name="berat_badan"]').val(done.data_resep['berat_badan'])
                $('input[name="tekanan_darah"]').val(done.data_resep['tekanan_darah'])
                $('input[name="suhu_badan"]').val(done.data_resep['suhu_badan'])
                $('input[name="kolesterol"]').val(done.data_resep['kolesterol'])
                $('input[name="asam_urat"]').val(done.data_resep['asam_urat'])
                $('input[name="gula_darah"]').val(done.data_resep['gula_darah'])
            })
            .fail(function() {
                console.log("error");
            });
        });

        $('select[name="pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-kategori-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="kategori_pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('select[name="kategori_pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="pasien"]').removeAttr('disabled');
                $('select[name="pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('#input-customer').click(function(){
            $('#form-customer').slideUp(function(){
                $('#form-customer').find('input,select').removeAttr('required');
            });
            $('#form-hide-customer').slideDown(function(){
                $('#form-hide-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#batal-customer').show();
        });

        $('#batal-customer').click(function(){
            $('#form-hide-customer').slideUp(function(){
                $('#form-hide-customer').find('input,select').removeAttr('required');
            });
            $('#form-customer').slideDown(function(){
                $('#form-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#input-customer').show();
        });

        $('#tambah-obat').click(function() {
            $('select[name="obat[]"]').select2('destroy');
            $('select[name="jenis_input_obat[]"]').select2('destroy');
            $('.sip').select2('destroy');
            $('#hapus-obat').show();
            $('#input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_input++);
            $('#hapus-input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_cancel++);
            $('#racik-obat').clone().appendTo($('#resep')).attr('get-id',num_racik++);
            reset_racik = num_racik - 1;
            $('.coba[get-id="'+reset_racik+'"]').find('select[name="obat[]"]').attr('disabled','disabled');
            $('.coba[get-id="'+reset_racik+'"]').find('input').val('');
            $('#input-racik').clone().appendTo($('#resep')).attr('get-id',num_manual++);
            reset_manual = num_manual - 1;
            $('.coba-juga[get-id="'+reset_manual+'"]').find('input').val('');
            $('#pilih-jenis').attr('ajax-id',select_ajax++);
            $('#oke').attr('get-ajax-id',get_ajax++);
            $('.sip').select2();
            $('select[name="obat[]"]').select2();
            $('select[name="jenis_input_obat[]"]').select2();
            // $('.coba[get-id="'+num_input+'"] > div > select[name="obat[]"]').attr('disabled','disabled');
        });

        $(document).on('change','.sip',function(){
            var getAttr = $(this).attr('ajax-id');
            var getVal  = $(this).val();
            var getUrl  = '..'+'/ajax/get-obat/'+getVal;
            $.ajax({
                url: getUrl,
            })
            .done(function(done) {
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').removeAttr('disabled');
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $(document).on('click','.pencet',function(){
            var attr = $(this).attr('data-id');
            $('.coba[get-id="'+attr+'"]').slideUp(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba[get-id="${attr}"`).find('input').val('');
            });
            $('.coba-juga[get-id="'+attr+'"]').slideDown(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('disabled');
            });
            $(this).hide();
            $('.destroy[data-id="'+attr+'"]').show();
        });

        $(document).on('click','.destroy',function(){
            var attr = $(this).attr('data-id');
            $('.coba-juga[get-id="'+attr+'"]').slideUp(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba-juga[get-id="${attr}"`).find('input').val('');
            });
            $('.coba[get-id="'+attr+'"]').slideDown(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba[get-id="'+attr+'"]').find('input').removeAttr('disabled');
                $('.coba[get-id="'+attr+'"]').find('.sip').removeAttr('disabled');
            });
            $(this).hide();
            $('.pencet[data-id="'+attr+'"]').show();
        });

        $('#hapus-obat').click(function(){
            $('#input-obat').remove().eq(1);
            $('#hapus-input-obat').remove().eq(1);
            $('#racik-obat').remove().eq(1);
            $('#input-racik').remove().eq(1);
            if ($('.coba').length == 1 && $('.coba-juga').length == 1) {
                $(this).hide();
            }
        });
    });
</script>
@endsection