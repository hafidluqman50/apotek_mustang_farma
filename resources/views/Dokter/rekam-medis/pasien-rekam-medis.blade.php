@extends('Dokter.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Pasien</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					<table class="table table table-hover force-fullwidth pasien-rekam-medis">
						<thead>
							<th>No.</th>
							<th>Nama Pasien</th>
							<th>Kategori Pasien</th>
							<th>Jenis Kelamin</th>
							<th>Umur</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var pasien_rekam_medis = $('.pasien-rekam-medis').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/pasien-rekam-medis',$dokter) }}",
            columns:[
                {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'nama_kategori',name:'nama_kategori'},
                {data:'jenis_kelamin',name:'jenis_kelamin'},
                {data:'umur_pasien',name:'umur_pasien'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        pasien_rekam_medis.on( 'order.dt search.dt', function () {
	        pasien_rekam_medis.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection