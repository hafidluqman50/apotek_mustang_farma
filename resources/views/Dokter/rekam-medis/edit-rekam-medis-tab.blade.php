@extends('Dokter.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Resep</h1>
</section>

<section class="content">
    <form action="{{url('/dokter/laporan-rekam-medis/'.$id.'/rekam/'.$id_pasien.'/update/'.$id_resep)}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            @if (session()->has('berhasil'))
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible">
                    {{session('berhasil')}} <button class="close" data-dismiss="alert">X</button>
                </div>
            </div>
            @elseif(session()->has('habis'))
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible">
                    <button class="close" data-dismiss="alert">X</button>
                    <ul>
                    @foreach (session('habis') as $key => $element)
                        <li>{!!$element['text']!!}</li>
                    @endforeach
                    </ul>
                </div>
            </div>
            @endif
            <div class="modal fade" id="tesModal">
              <div class="modal-lg modal-dialog">
                <div class="modal-content">
                <!-- header-->
                  <div class="modal-header">
                    <button class="close" data-dismiss="modal"><span>&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Rekam Medis Pasien</h4>
                  </div>
                  <!--body-->
                  <div class="modal-body">
                    
                    <h5 id="nama-pasien">Nama Pasien :</h5>
                    <h5 id="kategori-pasien">Kategori Pasien :</h5>
                    <h5 id="tanggal-lahir">Tanggal Lahir :</h5>
                    <h5 id="umur-pasien">Umur Pasien :</h5>
                    <h5 id="jenis-kelamin">Jenis Kelamin :</h5>
                    <table class="table table table-hover force-fullwidth data-rekam-medis">
                        <thead>
                            <th>No.</th>
                            <th>Tanggal Periksa</th>
                            <th>Tinggi Badan</th>
                            <th>Berat Badan</th>
                            <th>Tekanan Darah</th>
                            <th>Suhu Badan</th>
                            <th>Tindakan</th>
                            <th>Keluhan</th>
                            <th>Diagnosa</th>
                            <th>Anemnesis</th>
                            <th>Pemeriksaan Penunjang</th>
                            <th>Terapi</th>
                            <th>Rujukan</th>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                  </div>
                  <!--footer-->
                  <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <a href="{{ url('/dokter/laporan-rekam-medis/'.$id.'/rekam/'.$id_pasien) }}">
                            <button class="btn btn-default" type="button">
                                <span class="fa fa-long-arrow-left"></span> Kembali
                            </button>
                        </a>
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" id="dokterTabs">
                                <li class="active"><a href="#catatan-medis" data-toggle="tab">Catatan Medis</a></li>
                                <li><a href="#data-tindakan" data-toggle="tab">Data Tindakan</a></li>
                                <li><a href="#data-penyakit-by-bpjs" data-toggle="tab">Data Penyakit by BPJS</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="catatan-medis">
                                    <div id="form-customer">
                                        <div class="form-group">
                                            <label for="">Pasien</label>
                                            <input type="text" name="pasien" class="form-control" value="{{ human_date($row->tgl_daftar).' | '.$row->nama_pasien.' | '.hitung_umur($row->tanggal_lahir).' | '.$row->jenis_kelamin.' | '.$row->nama_dokter }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Keluhan Utama</label>
                                            <input type="text" name="keluhan" class="form-control" value="{{ $row->keluhan_utama_resep }}"  placeholder="Isi Keluhan Utama" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tinggi Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="tinggi_badan" class="form-control" id="tinggi-badan" placeholder="Isi Tinggi Badan" value="{{ $row->tinggi_badan }}">
                                                <span class="input-group-addon">cm</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Berat Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="berat_badan" class="form-control" id="berat-badan" placeholder="Isi Berat Badan" value="{{ $row->berat_badan }}">
                                                <span class="input-group-addon">kg</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tekanan Darah</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="tekanan_darah" class="form-control" id="tinggi-badan" placeholder="Isi Tekanan Darah" value="{{ $row->tekanan_darah }}">
                                                <span class="input-group-addon">mmHg</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Suhu Badan</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="suhu_badan" class="form-control" id="tinggi-badan" placeholder="Isi Suhu Badan" value="{{ $row->suhu_badan }}">
                                                <span class="input-group-addon">&deg;C</span>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="">Cek Kadar Kolesterol</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="kolesterol" class="form-control" id="kolesterol" placeholder="Isi Hasil Kolesterol (Jika Ada)" value="{{ $kolesterol }}">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Cek Kadar Gula Darah</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="gula_darah" class="form-control" id="gula_darah" placeholder="Isi Hasil Gula Darah (Jika Ada)" value="{{ $gula_darah }}">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Cek Kadar Asam Urat</label>
                                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                                <input type="text" name="asam_urat" class="form-control" id="asam_urat" placeholder="Isi Hasil Asam Urat (Jika Ada)" value="{{ $asam_urat }}">
                                                <span class="input-group-addon">mg/dL</span>
                                            </div>
                                        </div> --}}
                                        <div class="form-group">
                                            <label for="">Pemeriksaan Fisik</label>
                                            <textarea name="pemeriksaan_fisik" class="form-control summernote" id="pemeriksaan_fisik">{{$row->pemeriksaan_fisik}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Anemnesis</label>
                                            <input type="text" name="anemnesis" class="form-control" value="{{$row->anemnesis}}" placeholder="Isi Anemnesis">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Pemeriksaan Penunjang</label>
                                            {{-- <input type="text" name="pemeriksaan_penunjang" class="form-control" value="{{$row->pemeriksaan_penunjang}}" placeholder="Isi Pemeriksaan Penunjang"> --}}
                                            <textarea name="pemeriksaan_penunjang" class="form-control summernote" id="" cols="30" rows="10">{{$row->pemeriksaan_penunjang}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Tindakan</label>
                                            <select name="tindakan_lab" class="form-control select2">
                                                <option value="" selected disabled>=== Pilih Tindakan Lab ===</option>
                                                @foreach ($tindakan_lab as $value)
                                                <option value="{{ $value->id_tindakan_lab }}" @if (isset($row)){!!$row->id_tindakan_lab == $value->id_tindakan_lab ? 'selected="selected"' : ''!!}@endif>{{ $value->nama_tindakan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Diagnosa</label>
                                            <input type="text" name="diagnosa" class="form-control" value="{{$row->diagnosa}}" required="required" placeholder="Isi Diagnosa">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Terapi</label>
                                            {{-- <input type="text" name="terapi" class="form-control" value="{{$row->terapi}}" placeholder="Isi Terapi"> --}}
                                            <textarea name="terapi" class="form-control summernote" id="" cols="30" rows="10">{!!$row->terapi!!}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Rujukan</label>
                                            <input type="text" name="rujukan" class="form-control" value="{{$row->rujukan}}" placeholder="Isi Rujukan">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Tanggal Kontrol Selanjutnya</label>
                                            <input type="text" name="tgl_kontrol_selanjutnya" class="form-control datepicker" value="{{reverse_date($row->tgl_kontrol_selanjutnya)}}" placeholder="dd-mm-yyyy">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Keterangan</label>
                                            <input type="text" name="keterangan_resep" class="form-control" value="{{$row->keterangan_resep}}" placeholder="Isi Keterangan">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane force-fullwidth" id="data-tindakan">
                                    <table class="table table-hover tindakan-lab force-fullwidth">
                                        <thead>
                                            <th>No.</th>
                                            <th>Nama Tindakan</th>
                                            <th>Biaya Jasa Lab</th>
                                            <th>Spesialis</th>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane force-fullwidth" id="data-penyakit-by-bpjs">
                                    @if ($diagnosa_bpjs['message']['code'] != '')
                                        <div class="alert alert-danger alert-dismissible">
                                            {{$diagnosa_bpjs['message']['message']}} <button class="close" data-dismiss="alert">X</button>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-2">
                                            <input type="text" name="search" class="form-control" id="input-cari-penyakit-bpjs" placeholder="Cari...">
                                        </div>
                                        <div class="col-md-6">
                                            <button class="btn btn-primary" id="cari-penyakit-bpjs" type="button">Cari</button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                       <div class="col-md-6 col-md-offset-2">
                                            <table class="table table-hover table-bordered" id="table-penyakit-bpjs">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama Penyakit</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody-penyakit-bpjs">
                                                    @foreach ($diagnosa_bpjs['result'] as $key => $value)
                                                        <tr>
                                                            <td>{{ $key+1 }}</td>
                                                            <td>{{ $value->nmDiag }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                       </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-warning">
                            Edit <span class="fa fa-save"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection

@section('js')
<script>
    $(function(){
        var base_url = "{{ url('/') }}"

        var obat = $('.data-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-obat/inventory') }}",
            columns:[
                {data:'id_obat',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'kode_obat',name:'kode_obat'},
                // {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'icon_golongan',name:'icon_golongan'},
                {data:'tanggal_expired',name:'tanggal_expired'},
                {data:'harga_obat',name:'harga_obat'},
                {data:'harga_jual',name:'harga_jual'},
                {data:'stok_obat',name:'stok_obat'},
                {data:'bobot_satuan',name:'bobot_satuan'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        obat.on( 'order.dt search.dt', function () {
            obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        var tindakan_lab = $('.tindakan-lab').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{url('/datatables/data-tindakan-lab')}}",
            columns:[
                {data:'id_tindakan_lab',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'biaya_lab',name:'biaya_lab'},
                {data:'nama_spesialis_dokter',name:'nama_spesialis_dokter'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 1, 'desc' ]],
            responsive:true,
            fixedColumns: true,
        });
        tindakan_lab.on( 'order.dt search.dt', function () {
            tindakan_lab.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        }).draw();

        function loadRekamMedis(id_dokter,id_pasien)
        {
            var pasien = $('.data-rekam-medis').DataTable({
                processing:true,
                serverSide:true,
                ajax:`${base_url}/datatables/pasien-rekam-medis/${id_dokter}/rekam/${id_pasien}`,
                columns:[
                    {data:'id_pasien',searchable:false,render:function(data,type,row,meta){
                        return meta.row + meta.settings._iDisplayStart+1;
                    }},
                    {data:'tgl_resep',name:'tgl_resep'},
                    {data:'tinggi_badan',name:'tinggi_badan'},
                    {data:'berat_badan',name:'berat_badan'},
                    {data:'tekanan_darah',name:'tekanan_darah'},
                    {data:'suhu_badan',name:'suhu_badan'},
                    {data:'nama_tindakan',name:'nama_tindakan'},
                    {data:'keluhan_utama_resep',name:'keluhan_utama_resep'},
                    {data:'diagnosa',name:'diagnosa'},
                    {data:'anemnesis',name:'anemnesis'},
                    {data:'pemeriksaan_penunjang',name:'pemeriksaan_penunjang'},
                    {data:'terapi',name:'terapi'},
                    {data:'rujukan',name:'rujukan'}
                ],
                scrollCollapse: true,
                columnDefs: [ {
                sortable: true,
                "class": "index",
                }],
                scrollX:true,
                order: [],
                responsive:true,
                fixedColumns: true
            });
            pasien.on( 'order.dt search.dt', function () {
                pasien.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
            }).draw();
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
        });

        var select_ajax    = 2;
        var get_ajax       = 2;
        var num_input      = 2;
        var num_cancel     = 2;
        var num_racik      = 2;
        var num_manual     = 2;
        var id_pendaftaran = '';

        $('.kategori-pasien').select2({disabled:true});

        $('select[name="pendaftaran"]').change(function(){
            // var val = $(this).val();
            var id_pendaftaran = $(this).val()
            $.ajax({
                url: `${base_url}/ajax/get-data-resep/${id_pendaftaran}`
            })
            .done(function(done) {
                $('#form-customer > input[name="pasien"]').remove();
                $('#form-customer > input[name="dokter"]').remove();
                $('#form-customer').append(done.html);
                $('input[name="keluhan"]').val(done.data_resep['keluhan'])
                $('input[name="pemeriksaan_penunjang"]').val(done.data_resep['pemeriksaan_penunjang'])
                $('input[name="tinggi_badan"]').val(done.data_resep['tinggi_badan'])
                $('input[name="berat_badan"]').val(done.data_resep['berat_badan'])
                $('input[name="tekanan_darah"]').val(done.data_resep['tekanan_darah'])
                $('input[name="suhu_badan"]').val(done.data_resep['suhu_badan'])
                $('input[name="kolesterol"]').val(done.data_resep['kolesterol'])
                $('input[name="asam_urat"]').val(done.data_resep['asam_urat'])
                $('input[name="gula_darah"]').val(done.data_resep['gula_darah'])
                $('#nama-pasien').html(`<h5 id="nama-pasien">Nama Pasien : ${done.data_resep.pasien.nama_pasien}</h5>`)
                $('#kategori-pasien').html(`<h5 id="kategori-pasien">Kategori Pasien : ${done.data_resep.pasien.nama_kategori}</h5>`)
                $('#tanggal-lahir').html(`<h5 id="tanggal-lahir">Tanggal Lahir : ${done.data_resep.tanggal_lahir}</h5>`)
                $('#umur-pasien').html(`<h5 id="umur-pasien">Umur Pasien : ${done.data_resep.umur_pasien}</h5>`)
                $('#jenis-kelamin').html(`<h5 id="jenis-kelamin">Jenis Kelamin : ${done.data_resep.pasien.jenis_kelamin}</h5>`)
                loadRekamMedis(done.data_resep['id_dokter'],done.data_resep['id_pasien'])
            })
            .fail(function() {
                console.log("error");
            });
        });

        $('select[name="pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-kategori-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="kategori_pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('select[name="kategori_pasien"]').change(function(){
            var val = $(this).val();
            var getUrl = '..'+'/ajax/get-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="pasien"]').removeAttr('disabled');
                $('select[name="pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('#input-customer').click(function(){
            $('#form-customer').slideUp(function(){
                $('#form-customer').find('input,select').removeAttr('required');
            });
            $('#form-hide-customer').slideDown(function(){
                $('#form-hide-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#batal-customer').show();
        });

        $('#batal-customer').click(function(){
            $('#form-hide-customer').slideUp(function(){
                $('#form-hide-customer').find('input,select').removeAttr('required');
            });
            $('#form-customer').slideDown(function(){
                $('#form-customer').find('input,select').attr('required','required');
            });
            $(this).hide();
            $('#input-customer').show();
        });

        $('#tambah-obat').click(function() {
            $('select[name="obat[]"]').select2('destroy');
            $('select[name="jenis_input_obat[]"]').select2('destroy');
            $('.sip').select2('destroy');
            $('#hapus-obat').show();
            $('#input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_input++);
            $('#hapus-input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_cancel++);
            $('#racik-obat').clone().appendTo($('#resep')).attr('get-id',num_racik++);
            reset_racik = num_racik - 1;
            $('.coba[get-id="'+reset_racik+'"]').find('select[name="obat[]"]').attr('disabled','disabled');
            $('.coba[get-id="'+reset_racik+'"]').find('input').val('');
            $('#input-racik').clone().appendTo($('#resep')).attr('get-id',num_manual++);
            reset_manual = num_manual - 1;
            $('.coba-juga[get-id="'+reset_manual+'"]').find('input').val('');
            $('#pilih-jenis').attr('ajax-id',select_ajax++);
            $('#oke').attr('get-ajax-id',get_ajax++);
            $('.sip').select2();
            $('select[name="obat[]"]').select2();
            $('select[name="jenis_input_obat[]"]').select2();
            // $('.coba[get-id="'+num_input+'"] > div > select[name="obat[]"]').attr('disabled','disabled');
        });

        $(document).on('change','.sip',function(){
            var getAttr = $(this).attr('ajax-id');
            var getVal  = $(this).val();
            var getUrl  = '..'+'/ajax/get-obat/'+getVal;
            $.ajax({
                url: getUrl,
            })
            .done(function(done) {
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').removeAttr('disabled');
                $('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $(document).on('click','.pencet',function(){
            var attr = $(this).attr('data-id');
            $('.coba[get-id="'+attr+'"]').slideUp(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba[get-id="${attr}"`).find('input').val('');
            });
            $('.coba-juga[get-id="'+attr+'"]').slideDown(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('disabled');
            });
            $(this).hide();
            $('.destroy[data-id="'+attr+'"]').show();
        });

        $(document).on('click','.destroy',function(){
            var attr = $(this).attr('data-id');
            $('.coba-juga[get-id="'+attr+'"]').slideUp(function(){
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('required');
                $('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
                $(`.coba-juga[get-id="${attr}"`).find('input').val('');
            });
            $('.coba[get-id="'+attr+'"]').slideDown(function(){
                $('.coba[get-id="'+attr+'"]').find('input,select').attr('required','required');
                $('.coba[get-id="'+attr+'"]').find('input').removeAttr('disabled');
                $('.coba[get-id="'+attr+'"]').find('.sip').removeAttr('disabled');
            });
            $(this).hide();
            $('.pencet[data-id="'+attr+'"]').show();
        });

        $('#hapus-obat').click(function(){
            $('#input-obat').remove().eq(1);
            $('#hapus-input-obat').remove().eq(1);
            $('#racik-obat').remove().eq(1);
            $('#input-racik').remove().eq(1);
            if ($('.coba').length == 1 && $('.coba-juga').length == 1) {
                $(this).hide();
            }
        });

        

        $('#cari-penyakit-bpjs').click(() => {
            let val = $('#input-cari-penyakit-bpjs').val();
            $('#tbody-penyakit-bpjs').html('<tr><td colspan="2" align="center">Loading...</td></tr>');
            $.ajax({
                url: "{{ url('/ajax/find-diagnosa-bpjs') }}",
                // dataType: 'application/json',
                data: {search: val},
            })
            .done(function(done) {
                $('#tbody-penyakit-bpjs').html(done)
            })
            .fail(function(error) {
                console.log(error);
            });
            
        })
    });
</script>
@endsection