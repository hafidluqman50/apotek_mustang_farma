@extends('Operator.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Transaksi</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
            <div class="box box-default">
                <form action="{{url('/operator/data-transaksi/save')}}" method="POST">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Data Customer Resep</label>
                                <select name="customer" id="customer" class="form-control select2" required="required" autofocus="autofocus">
                                    <option value="" selected="selected" disabled="disabled">=== Pilih Data Customer Resep ===</option>
                                    @foreach ($resep as $element)
                                    <option value="{{$element->id_resep}}">{{$element->kode_resep.' | '.human_date($element->tgl_resep).' | '.$element->nama_dokter.' | '.$element->nama_pasien.' | '.$element->nama_kategori.' | '.$element->jenis_kelamin}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tindakan</label>
                                <input type="text" class="form-control" id="tindakan-lab" readonly>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Resep</label>
                                <input type="number" name="biaya_resep" class="form-control" placeholder="Biaya Resep" required="required">
                                <label for="" id="biaya_resep">Rp. 0,00</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Racik</label>
                                <input type="number" name="biaya_racik" class="form-control" placeholder="Biaya Racik" required="required">
                                <label for="" id="biaya_racik">Rp. 0,00</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Dokter</label>
                                <input type="number" name="biaya_dokter" class="form-control" placeholder="Isi Biaya Jasa Dokter" required="required">
                                <label for="" id="biaya_dokter">Rp. 0</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Klinik</label>
                                <input type="number" name="biaya_klinik" class="form-control" placeholder="Isi Biaya Klinik" required="required">
                                <label for="" id="biaya_klinik">Rp. 0</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Jasa Tindakan Lab</label>
                                <input type="number" name="biaya_jasa_lab" class="form-control" value="0" placeholder="Isi Biaya Jasa Lab" required="required">
                                <label for="" id="biaya_jasa_lab">Rp. 0</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Biaya Tambahan</label>
                                <input type="number" name="biaya_tambahan" class="form-control" value="0" placeholder="Isi Biaya Jasa Lab" required="required">
                                <label for="" id="biaya_tambahan">Rp. 0</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            {{-- <label for="">PPn</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="number" name="ppn" class="form-control" value="0" id="ppn" required placeholder="Isi PPn; Ex:11;">
                                <span class="input-group-addon">%</span>
                            </div>
                            <label for="">Diskon</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="number" name="diskon" class="form-control" id="diskon" placeholder="Isi Diskon; Ex:50;">
                                <span class="input-group-addon">%</span>
                            </div> --}}
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Total Harga</label>
                                <input type="number" name="total_harga" class="form-control" placeholder="Total Harga" readonly="readonly">
                                <label for="" id="total_harga">Rp. 0</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Bayar</label>
                                <input type="number" name="bayar" class="form-control" placeholder="Bayar" required="required">
                                <label for="" id="bayar">Rp. 0</label>
                            </div>
                            <div class="form-group" style="border-bottom:1px solid lightgrey;">
                                <label for="">Kembalian</label>
                                <input type="number" name="kembali" class="form-control" placeholder="Kembalian" readonly="readonly">
                                <label for="" id="kembalian">Rp. 0</label>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary">
                            Simpan <span class="fa fa-save"></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){
        function sumTotalHarga() {
            let biayaResep    = $('input[name="biaya_resep"]').val()
            let biayaDokter   = $('input[name="biaya_dokter"]').val()
            let biayaKlinik   = $('input[name="biaya_klinik"]').val()
            let biayaLab      = $('input[name="biaya_jasa_lab"]').val()
            let biayaRacik    = $('input[name="biaya_racik"]').val()
            let biayaTambahan = $('input[name="biaya_tambahan"]').val()
            // let diskon       = $('#diskon').val()
            // let ppn      = parseInt($('#ppn').val())

            let total = parseInt(biayaResep) + parseInt(biayaDokter) + parseInt(biayaKlinik) + parseInt(biayaLab) + parseInt(biayaRacik) + parseInt(biayaTambahan)

            // total = total + ((total * ppn) / 100);
            // if ($('#diskon').val() != '') {
            //  total = (total * parseInt(diskon)) / 100;
            // }

            $('input[name="total_harga"]').val(total);
            $('#total_harga').html(rupiah_format(total));
        }


        $('#customer').change(function(){
            var val = $(this).val();
            var getUrl = base_url+'/ajax/get-harga/'+val;
            $.ajax({
                url: getUrl,
            })
            .done(function(done) {
                var biaya_klinik        = Math.round(done.biaya_dokter * 10 / 100);
                var biaya_dokter        = done.biaya_dokter;
                // var biaya_total_obat = done.total_obat;
                var biaya_total_obat    = done.total_obat;
                var biaya_racik         = done.biaya_racik;
                var biaya_jasa_lab      = done.biaya_jasa_lab;
                var biaya_tambahan      = parseInt($('input[name="biaya_tambahan"]').val())
                var total               = biaya_klinik + biaya_dokter + biaya_total_obat + biaya_jasa_lab + biaya_racik + biaya_tambahan;
                // let ppn              = parseInt($('#ppn').val());
                // total = total + ((total * ppn) / 100);
                // if ($('#diskon').val() != '') {
                //  var diskon = parseInt($('#diskon').val());
                //  total = ((biaya_klinik + biaya_dokter + biaya_total_obat) * diskon) / 100;
                // }
                $('#tindakan-lab').val(done.nama_tindakan_lab)
                $('input[name="biaya_jasa_lab"]').val(biaya_jasa_lab);
                $('#biaya_jasa_lab').html(rupiah_format(biaya_jasa_lab));
                $('input[name="biaya_resep"]').val(biaya_total_obat);
                $('#biaya_resep').html(rupiah_format(biaya_total_obat));
                $('input[name="biaya_dokter"]').val(biaya_dokter);
                $('#biaya_dokter').html(rupiah_format(biaya_dokter));
                $('input[name="biaya_klinik"]').val(biaya_klinik);
                $('#biaya_klinik').html(rupiah_format(biaya_klinik));
                $('input[name="biaya_racik"]').val(biaya_racik);
                $('#biaya_racik').html(rupiah_format(biaya_racik));
                $('input[name="total_harga"]').val(total);
                $('#total_harga').html(rupiah_format(total));
            })
            .fail(function(fail) {
                console.log(fail);
            });
        });
        
        // $('select[name="tindakan_lab"]').change(function(){
        //  var val = $(this).val();
        //  var getUrl = base_url+'/ajax/get-biaya-lab/'+val;
        //  $.ajax({
        //      url: getUrl,
        //  })
        //  .done(function(done) {
        //      $('input[name="biaya_jasa_lab"]').val(done);
        //      $('#biaya_jasa_lab').html(rupiah_format(done));
        //      var get = $('input[name="total_harga"]').val();
        //      var total = parseInt(get) + parseInt(done);
        //      if ($('#diskon').val() != '') {
        //          var diskon = parseInt($('#diskon').val());
        //          total = ((parseInt(get) + parseInt(done)) * diskon) / 100;
        //      }
        //      $('input[name="total_harga"]').val(total);
        //      $('#total_harga').html(rupiah_format(total));
        //  })
        //  .fail(function(fail) {
        //      console.log(fail);
        //  });
        // });

        $('input[name="biaya_resep"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_dokter"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_klinik"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_resep"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_racik"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_jasa_lab"]').blur(function() {
            sumTotalHarga();
        })

        $('input[name="biaya_tambahan"]').blur(function() {
            sumTotalHarga();
        })

        $('#diskon').change(function(){
            var val_diskon = parseInt($(this).val());
            var val_total = parseInt($('input[name="total_harga"]').val());
            var totalDiskon = (val_total * val_diskon) / 100;
            var total = val_total - totalDiskon;
            $('input[name="total_harga"]').val(total);
            $('#total_harga').html(rupiah_format(total));
        });

        $('input[name="bayar"]').keyup(function(){
            var val_total = parseInt($('input[name="total_harga"]').val());
            var val_bayar = parseInt($(this).val());
            $('input[name="kembali"]').val(val_bayar - val_total);
            $('#bayar').html(rupiah_format(val_bayar));
            $('#kembalian').html(rupiah_format(val_bayar - val_total));
        });
    });
</script>
@endsection