@extends('Operator.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Total Penjualan</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					<table class="table table-hover data-total-penjualan force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Tanggal Transaksi</th>
							<th>Input By</th>
							<th>Total Penjualan</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var total_penjualan = $('.data-total-penjualan').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-total-penjualan') }}",
            columns:[
                {data:'id_transaksi',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tgl_byr',name:'tgl_byr'},
                {data:'name',name:'name'},
                {data:'total_penjualan',name:'total_penjualan'}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        total_penjualan.on( 'order.dt search.dt', function () {
	        total_penjualan.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection