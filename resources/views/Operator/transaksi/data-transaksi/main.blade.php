@extends('Operator.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Transaksi</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close">X</button>
					</div>
					@endif
					<form action="{{url('/operator/data-transaksi/export')}}">
						<div class="col-md-5">
							<div class="form-group">
								<label for="">Dari</label>
								<input type="date" name="from" class="form-control" required="required" {!!isset($_GET['from'])?'value="'.$_GET['from'].'"':''!!}>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label for="">Ke</label>
								<input type="date" name="to" class="form-control" required="required" {!!isset($_GET['to'])?'value="'.$_GET['to'].'"':''!!}>
							</div>
						</div>
						<div class="col-md-2">{{-- 
							<button class="btn btn-default" style="margin-top:8%;">
								Cari
							</button> --}}
							<button class="btn btn-success" style="margin-top:18%;">
								Export
							</button>
						</div>
					</form>
					<table class="table table-hover data-transaksi force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Tanggal Transaksi</th>
							<th>Nama Pasien</th>
							<th>Kategori Pasien</th>
							<th>Tindakan Lab</th>
							<th>Nama Dokter</th>
							<th>Biaya Klinik</th>
							<th>Biaya Dokter</th>
							<th>Biaya Resep</th>
                            <th>Biaya Racik</th>
							<th>Biaya Jasa Lab</th>
                            <th>Biaya Tambahan</th>
							<th>Total Harga</th>
							<th>Bayar</th>
							<th>Kembali</th>
							<th>Input By</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
{{-- @if (isset($_GET['from']) && isset($_GET['to'])) --}}{{-- 
<script>
	var getUrl = '..'+'/datatables/data-transaksi/'+window.location.search;
</script> --}}
{{-- @else --}}
<script>
	var getUrl = '..'+'/datatables/data-transaksi/';
</script>
{{-- @endif --}}
<script>
	$(function(){
		console.log(window.location);
        var transaksi = $('.data-transaksi').DataTable({
            processing:true,
            serverSide:true,
            ajax:getUrl,
            columns:[
                {data:'id_transaksi',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tgl_byr',name:'tgl_byr'},
                {data:'nama_pasien',name:'nama_pasien'},
                {data:'nama_kategori',name:'nama_kategori'},
                {data:'nama_tindakan',name:'nama_tindakan'},
                {data:'nama_dokter',name:'nama_dokter'},
                {data:'biaya_klinik',name:'biaya_klinik'},
                {data:'biaya_dokter',name:'biaya_dokter'},
                {data:'biaya_jasa_lab',name:'biaya_jasa_lab'},
                {data:'biaya_resep',name:'biaya_resep'},
                {data:'biaya_racik',name:'biaya_racik'},
                {data:'biaya_tambahan',name:'biaya_tambahan'},
                {data:'jumlah_byr',name:'jumlah_byr'},
                {data:'bayar',name:'bayar'},
                {data:'kembali',name:'kembali'},
                {data:'name',name:'name'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        transaksi.on( 'order.dt search.dt', function () {
	        transaksi.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection