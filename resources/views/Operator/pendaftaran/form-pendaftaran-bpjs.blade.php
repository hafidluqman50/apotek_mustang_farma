@extends('Operator.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Pendaftaran(BPJS)</h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                {{session('message')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @elseif (session()->has('log'))
            <div class="alert alert-danger alert-dismissible">
                {{session('log')}} <button class="close" data-dismiss="alert">X</button>
            </div>
            @endif
            <div class="box box-default">
                {{-- <div class="box-header with-border">
                    <button class="btn btn-primary" id="show">Input Pasien</button>
                    <button class="btn btn-danger button-hide" id="cancel">Batal Input</button>
                </div> --}}
                <form action="{{url('/operator/pendaftaran-bpjs/save')}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="box-body">
                        <div id="pasien">
                            <div class="form-group">
                                <label for="">Kategori Pasien</label>
                                <input type="text" name="kategori_pasien" class="form-control" placeholder="" disabled value="{{ $kategori_pasien->nama_kategori }}">
                            </div>
                            <div class="form-group">
                                <label for="">Pasien</label>
                                <select name="pasien" class="form-control select2" id="pasien-input" required="required">
                                    <option value="" selected="selected" disabled="disabled">=== Pilih Pasien ===</option>
                                    @foreach ($pasien as $element)
                                    <option value="{{ $element->id_pasien }}">{{ $element->nama_pasien }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nomor BPJS</label>
                                <input type="text" name="no_bpjs" id="no-bpjs" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Dokter</label>
                            <select name="dokter" class="form-control select2" required="required">
                                <option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
                                @foreach ($dokter as $element)
                                <option value="{{$element->id_dokter}}">{{$element->nama_dokter.' | '.$element->spesialis->nama_spesialis_dokter}}</option>
                                @endforeach
                            </select>
                        </div>
                        {{-- <div class="form-group">
                            <label for="">Kode Provider Peserta(BPJS)</label>
                            <input type="text" name="kode_provider_peserta" id="kode-provider-peserta" class="form-control" disabled>
                        </div> --}}
                        <div class="form-group">
                            <label for="">Kode Poli</label>
                            <select name="kode_poli" class="form-control select2" required>
                                <option value="" selected disabled>=== Pilih Kode Poli ===</option>
                                @foreach ($kode_poli as $value)
                                <option value="{{ $value->kdPoli }}">{{ $value->kdPoli.' | '.$value->nmPoli }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Keluhan</label>
                            <input type="text" name="keluhan" class="form-control" placeholder="Isi Keluhan" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tinggi Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="tinggi_badan" class="form-control" id="tinggi-badan" placeholder="Isi Tinggi Badan" required>
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Berat Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="berat_badan" class="form-control" id="berat-badan" placeholder="Isi Berat Badan" required>
                                <span class="input-group-addon">kg</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Tekanan Darah</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="tekanan_darah" class="form-control" id="tinggi-badan" placeholder="Isi Tekanan Darah" required>
                                <span class="input-group-addon">mmHg</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Suhu Badan</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="suhu_badan" class="form-control" id="suhu-badan" placeholder="Isi Suhu Badan" required>
                                <span class="input-group-addon">&deg;C</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Laju Respirasi (Respiration Rate)</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="laju_respirasi" class="form-control" id="laju-respirasi" required>
                                <span class="input-group-addon">Bpm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Lingkar Perut</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="lingkar_perut" class="form-control" id="lingkar-perut" required>
                                <span class="input-group-addon">cm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Denyut Jantung (Heart Rate)</label>
                            <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                                <input type="text" name="denyut_jantung" class="form-control" id="denyut-jantung" required>
                                <span class="input-group-addon">Bpm</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Pemeriksaan Penunjang</label>
                            <input type="text" name="pemeriksaan_penunjang" class="form-control" placeholder="Isi Pemeriksaan Penunjang" required>
                        </div>
                        <div class="form-group">
                            <label for="">Kunjungan Sakit</label>
                            <select name="kunjungan_sakit" name="kunjungan_sakit" id="kunjungan-sakit" class="form-control select2" required>
                                <option value="" selected disabled>=== Pilih Kunjungan Sakit ===</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Rujuk Balik</label>
                            <select name="rujuk_balik" name="rujuk_balik" id="rujuk-balik" class="form-control select2" required>
                                <option value="" selected disabled>=== Pilih Rujuk Balik ===</option>
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">Simpan <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(function(){

        $('#show').click(function(){
            $(this).hide();
            $('#cancel').show();
            $('#input-pasien').slideDown();
            $('#input-pasien').find('input,select,textarea').attr('required','required');
            $('#pasien').slideUp();
            $('#pasien').find('input,select,textarea').removeAttr('required');
            $('.select2').select2({
                width:'resolve'
            })
        });

        $('#cancel').click(function(){
            $(this).hide();
            $('#show').show();
            $('#pasien').slideDown();
            $('#pasien').find('input,select,textarea').attr('required','required');
            $('#input-pasien').slideUp();
            $('#input-pasien').find('input,select,textarea').removeAttr('required');
        });

        $('#kategori_pasien').change(function(){
            var val = $(this).val();
            var getUrl = base_url+'/ajax/get-pasien/'+val;
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('select[name="pasien"]').removeAttr('disabled');
                $('select[name="pasien"]').html(done);
            })
            .fail(function(error) {
                console.log(error);
            });
        });

        $('#pasien-input').change(function(){
            let val    = $(this).val()
            let getUrl = `${base_url}/ajax/get-info-pasien/${val}`
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $('#no-bpjs').val(done.no_bpjs)
                if(done.no_bpjs != '') {
                    $('#kode-provider-peserta').removeAttr('disabled')
                    $('#kode-provider-peserta').attr('required','required')
                    $('#laju-respirasi').removeAttr('disabled')
                    $('#laju-respirasi').attr('required','required')
                    $('#lingkar-perut').removeAttr('disabled')
                    $('#lingkar-perut').attr('required','required')
                    $('#denyut-jantung').removeAttr('disabled')
                    $('#denyut-jantung').attr('required','required')
                    $('#kunjungan-sakit').removeAttr('disabled')
                    $('#kunjungan-sakit').attr('required','required')
                    $('#rujuk-balik').removeAttr('disabled')
                    $('#rujuk-balik').attr('required','required')
                }
            })
            .fail(function(fail) {
                console.log(fail);
            });
        })
    });
</script>
@endsection