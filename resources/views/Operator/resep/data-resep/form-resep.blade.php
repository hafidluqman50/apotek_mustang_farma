@extends('Admin.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Resep</h1>
</section>

<section class="content">
	<form action="{{url('/admin/data-resep/save')}}" method="POST">
		{{csrf_field()}}
		<div class="row">
			@if (session()->has('berhasil'))
			<div class="col-md-12">
				<div class="alert alert-success alert-dismissible">
					{{session('berhasil')}} <button class="close" data-dismiss="alert">X</button>
				</div>
			</div>
			@elseif(session()->has('habis'))
			<div class="col-md-12">
				<div class="alert alert-danger alert-dismissible">
					<button class="close" data-dismiss="alert">X</button>
					<ul>
					@foreach (session('habis') as $key => $element)
						<li>{!!$element['text']!!}</li>
					@endforeach
					</ul>
				</div>
			</div>
			@endif
			<div class="col-md-4">
				<div class="box box-primary">{{-- 
					<div class="box-header with-border">
						<button type="button" class="btn btn-info" id="input-customer">
							Input Pasien
						</button>
						<button type="button" class="btn btn-danger button-hide" id="batal-customer">
							Batal Input
						</button>
					</div> --}}
					<div class="box-body">
						<div id="form-customer">
							{{-- <div class="form-group">
								<label for="">Dokter</label>
								<select name="dokter" class="form-control select2" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
									@foreach ($dokter as $element)
									<option value="{{$element->id_dokter}}" @if (old('dokter') !== null){!!$element->id_dokter == old('dokter')?'selected="selected"':''!!}@endif>{{$element->nama_dokter.' | '.$element->spesialis_poli}}</option>
									@endforeach
								</select>
							</div> --}}
							<div class="form-group">
								<label for="">Pasien</label>
								<select name="pendaftaran" class="form-control select2" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Pasien ===</option>
									@foreach ($pasien as $element)
									<option value="{{$element->id_daftar}}" @if (session()->has('habis')){!!old('pendaftaran') == $element->id_daftar ? 'selected="selected"' : ''!!}@endif>{{human_date($element->tgl_daftar).' | '.$element->nama_pasien.' | '.hitung_umur($element->tanggal_lahir).' | '.$element->jenis_kelamin.' | '.$element->nama_dokter}}</option>
									@endforeach
								</select>
							</div>
                            <div class="form-group">
                                <label>Keluhan</label>
                                <input type="text" name="tindakan_lab" class="form-control" value="{{ session()->has('habis') ? old('keluhan') : '' }}" placeholder="Isi Tindakan Lab" required>
                            </div>
                            <div class="form-group">
                                <label>Tindakan Lab</label>
                                <input type="text" name="tindakan_lab" class="form-control" value="{{ session()->has('habis') ? old('tindakan_lab') : '' }}" placeholder="Isi Tindakan Lab" required>
                            </div>
							<div class="form-group">
								<label for="">Anemnesis</label>
								<input type="text" name="anemnesis" class="form-control" value="{{session()->has('habis') ? old('anemnesis') : ''}}" required="required" placeholder="Isi Anemnesis">
							</div>
							<div class="form-group">
								<label for="">Pemeriksaan Penunjang</label>
								<input type="text" name="pemeriksaan_penunjang" class="form-control" value="{{session()->has('habis') ? old('pemeriksaan_penunjang') : ''}}" required="required" placeholder="Isi Pemeriksaan Penunjang">
							</div>
							<div class="form-group">
								<label for="">Diagnosa</label>
								<input type="text" name="diagnosa" class="form-control" value="{{session()->has('habis') ? old('diagnosa') : ''}}" required="required" placeholder="Isi Diagnosa">
							</div>
							<div class="form-group">
								<label for="">Terapi</label>
								<input type="text" name="terapi" class="form-control" value="{{session()->has('habis') ? old('terapi') : ''}}" required="required" placeholder="Isi Terapi">
							</div>
							<div class="form-group">
								<label for="">Rujukan</label>
								<input type="text" name="rujukan" class="form-control" value="{{session()->has('habis') ? old('rujukan') : ''}}" required="required" placeholder="Isi Rujukan">
							</div>
						</div>
						{{-- <div class="form-hide" id="form-hide-customer">
							<div class="form-group">
								<label for="">Dokter</label>
								<select name="dokter_input" class="form-control select2">
									<option value="" selected="selected" disabled="disabled">=== Pilih Dokter ===</option>
									@foreach ($dokter as $element)
									<option value="{{$element->id_dokter}}">{{$element->nama_dokter}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Kategori Pasien</label>
								<select name="kategori_pasien_input" class="form-control select2">
									<option value="" selected="selected" disabled="disabled">=== Pilih Kategori ===</option>
									@foreach ($kategori_pasien->all() as $element)
									<option value="{{$element->id_kategori_pasien}}">{{$element->nama_kategori}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Nama Pasien</label>
								<input type="text" name="nama_pasien" class="form-control" placeholder="Isi Nama Pasien">
							</div>
							<div class="form-group">
								<label for="">Jenis Kelamin</label>
								<select name="jenis_kelamin" class="form-control">
									<option selected="selected" disabled="disabled">=== Pilih Jenis Kelamin ===</option>
									<option value="Laki - Laki">Laki - Laki</option>
									<option value="Perempuan">Perempuan</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Tanggal Lahir</label>
								<input type="date" name="tanggal_lahir" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Alamat Pasien</label>
								<textarea name="alamat_pasien" class="form-control" placeholder="Isi Alamat Pasien" cols="30" rows="10"></textarea>
							</div>
						</div> --}}
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-body">
						@if (session()->has('obat_habis'))
							@for ($i = 0; $i < count(session('obat_habis')); $i++)
								<div class="form-group" id="resep">
									<button type="button" class="btn btn-info pencet" style="margin-bottom:10px;" id="input-obat" data-id="{{$i+1}}">
										Input Obat
									</button>
									<button type="button" class="btn btn-danger button-hide destroy" style="margin-bottom:10px;" id="hapus-input-obat" data-id="{{$i+1}}">
										Batal Input Obat
									</button>
									<div class="coba" id="racik-obat" get-id="{{$i+1}}">
										<div class="col-md-3 padding-left-right-gone">
											<select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 sip" ajax-id="{{$i+1}}">
												<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
												@foreach ($jenis_obat as $element)
												<option value="{{$element->id_jenis_obat}}" @if(session('jenis_obat')[$i]['jenis_obat'] == $element->id_jenis_obat) selected="selected" @endif>{{$element->nama_jenis_obat}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<select name="obat[]" class="form-control select2" id="oke" get-ajax-id="{{$i+1}}" required="required">
												<option value="" selected="selected" disabled="disabled">=== Pilih Obat ===</option>
												@foreach ($obat->getByJenis(session('jenis_obat')[$i]['jenis_obat']) as $element)
												<option value="{{$element->id_obat}}" @if(session('obat_habis')[$i]['id_obat'] == $element->id_obat) selected="selected" @endif>{{$element->nomor_batch.' | '.$element->nama_obat.' | '.golongan_obat($element->golongan_obat)}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<input type="text" name="dosis_obat[]" class="form-control" placeholder="Isi Dosis Obat" value="{{session('dosis_obat')[$i]['dosis_obat']}}" required="required">
										</div>
										<div class="col-md-3 padding-left-right-gone">
											<input type="number" name="banyak_obat[]" class="form-control" placeholder="Isi Banyak Obat" value="{{session('banyak_obat')[$i]['banyak_obat']}}" required="required">
										</div>
									</div>
									<div class="coba-juga form-hide" id="input-racik" get-id="{{$i+1}}">
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Nomor Batch</label>
												<input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch" disabled>
											</div>
											<div class="form-group">
												<label for="">Nama Obat</label>
												<input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat" disabled>
											</div>
											<div class="form-group">
												<label for="">Jenis Obat</label>
												<select name="jenis_input_obat[]" class="form-control select2">
													<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
													</option>
													@foreach ($jenis_obat as $element)
													<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="">Golongan Obat</label>
												<select name="golongan_input_obat[]" class="form-control select2">
													<option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===
													</option>
													@foreach ($golongan_obat as $element)
													<option value="{{$element->id_golongan_obat}}">{{$element->nama_golongan_obat}}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="">Tanggal Expired</label>
												<input type="date" name="tanggal_expired[]" class="form-control" disabled>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="">Harga Obat</label>
												<input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat" disabled>
											</div>
											<div class="form-group">
												<label for="">Stok Obat</label>
												<input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat" disabled>
											</div>
											<div class="form-group">
												<label for="">Dosis Obat</label>
												<input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat" disabled>
											</div>
											<div class="form-group">
												<label for="">Banyak Beli Obat</label>
												<input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat" disabled>
											</div>
										</div>
									</div>
								</div>
							@endfor
						@else
							<div class="form-group" id="resep">
								<button type="button" class="btn btn-info pencet" style="margin-bottom:10px;" id="input-obat" data-id="1">
									Input Obat
								</button>
								<button type="button" class="btn btn-danger button-hide destroy" style="margin-bottom:10px;" id="hapus-input-obat" data-id="1">
									Batal Input Obat
								</button>
								<div class="coba" id="racik-obat" get-id="1">
									<div class="col-md-3 padding-left-right-gone">
										<select id="pilih-jenis" name="jenis_obat[]" class="form-control select2 sip" ajax-id="1">
											<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
											@foreach ($jenis_obat as $element)
											<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<select name="obat[]" class="form-control select2" id="oke" get-ajax-id="1" disabled="disabled">
											<option selected="selected" disabled="disabled">=== Pilih Obat ===</option>
										</select>
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<input type="text" name="dosis_obat[]" class="form-control" placeholder="Isi Dosis Obat">
									</div>
									<div class="col-md-3 padding-left-right-gone">
										<input type="number" name="banyak_obat[]" class="form-control" placeholder="Isi Banyak Obat">
									</div>
								</div>
								<div class="coba-juga form-hide" id="input-racik" get-id="1">
									<div class="col-md-6">
										<div class="form-group">
											<label for="">Nomor Batch</label>
											<input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch" disabled="disabled">
										</div>
										<div class="form-group">
											<label for="">Nama Obat</label>
											<input type="text" name="nama_input_obat[]" class="form-control" placeholder="Isi Nama Obat" disabled="disabled">
										</div>
										<div class="form-group">
											<label for="">Golongan Obat</label>
											<select name="golongan_input_obat[]" class="form-control select2">
												<option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===
												</option>
												@foreach ($golongan_obat as $element)
												<option value="{{$element->id_golongan_obat}}">{{$element->nama_golongan}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="">Jenis Obat</label>
											<select name="jenis_input_obat[]" class="form-control select2" disabled="disabled">
												<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===
												</option>
												@foreach ($jenis_obat as $element)
												<option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
												@endforeach
											</select>
										</div>
										<div class="form-group">
											<label for="">Tanggal Expired</label>
											<input type="date" name="tanggal_expired[]" class="form-control" disabled="disabled">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="">Harga Obat</label>
											<input type="text" name="harga_input_obat[]" class="form-control" placeholder="Isi Harga Obat" disabled="disabled">
										</div>
										<div class="form-group">
											<label for="">Stok Obat</label>
											<input type="number" name="stok_input_obat[]" class="form-control" placeholder="Isi Stok Obat" disabled="disabled">
										</div>
										<div class="form-group">
											<label for="">Dosis Obat</label>
											<input type="text" name="dosis_input_obat[]" class="form-control" placeholder="Isi Dosis Obat" disabled="disabled">
										</div>
										<div class="form-group">
											<label for="">Banyak Beli Obat</label>
											<input type="number" name="banyak_input_beli[]" class="form-control" placeholder="Isi Banyak Beli Obat" disabled="disabled">
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
					<div class="box-footer with-border">
						<button type="button" class="btn btn-primary" id="tambah-obat">
							Tambah Obat
						</button>
						<button type="button" class="btn btn-danger button-hide" id="hapus-obat">
							Hapus Obat
						</button>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>
@endsection

@section('js')
<script>
	$(function(){
		var select_ajax = 2;
		var get_ajax    = 2;
		var num_input   = 2;
		var num_cancel  = 2;
		var num_racik   = 2;
		var num_manual  = 2;

		$('.kategori-pasien').select2({disabled:true});

		$('select[name="pendaftaran"]').change(function(){
			var val = $(this).val();
			$.ajax({
				url: '..'+'/ajax/get-data-resep/'+val
			})
			.done(function(done) {
				$('#form-customer > input[name="pasien"]').remove();
				$('#form-customer > input[name="dokter"]').remove();
				$('#form-customer').append(done);
			})
			.fail(function() {
				console.log("error");
			});
		});

		$('select[name="pasien"]').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-kategori-pasien/'+val;
			$.ajax({
				url: getUrl
			})
			.done(function(done) {
				$('select[name="kategori_pasien"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$('select[name="kategori_pasien"]').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-pasien/'+val;
			$.ajax({
				url: getUrl
			})
			.done(function(done) {
				$('select[name="pasien"]').removeAttr('disabled');
				$('select[name="pasien"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$('#input-customer').click(function(){
			$('#form-customer').slideUp(function(){
				$('#form-customer').find('input,select').removeAttr('required');
			});
			$('#form-hide-customer').slideDown(function(){
				$('#form-hide-customer').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('#batal-customer').show();
		});

		$('#batal-customer').click(function(){
			$('#form-hide-customer').slideUp(function(){
				$('#form-hide-customer').find('input,select').removeAttr('required');
			});
			$('#form-customer').slideDown(function(){
				$('#form-customer').find('input,select').attr('required','required');
			});
			$(this).hide();
			$('#input-customer').show();
		});

		$('#tambah-obat').click(function() {
			$('select[name="obat[]"]').select2('destroy');
			$('select[name="jenis_input_obat[]"]').select2('destroy');
			$('.sip').select2('destroy');
			$('#hapus-obat').show();
			$('#input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_input++);
			$('#hapus-input-obat').clone().appendTo($('#resep')).css('margin-top','10px').attr('data-id',num_cancel++);
			$('#racik-obat').clone().appendTo($('#resep')).attr('get-id',num_racik++);
			reset_racik = num_racik - 1;
			$('.coba[get-id="'+reset_racik+'"]').find('select[name="obat[]"]').attr('disabled','disabled');
			$('.coba[get-id="'+reset_racik+'"]').find('input').val('');
			$('#input-racik').clone().appendTo($('#resep')).attr('get-id',num_manual++);
			reset_manual = num_manual - 1;
			$('.coba-juga[get-id="'+reset_manual+'"]').find('input').val('');
			$('#pilih-jenis').attr('ajax-id',select_ajax++);
			$('#oke').attr('get-ajax-id',get_ajax++);
			$('.sip').select2();
			$('select[name="obat[]"]').select2();
			$('select[name="jenis_input_obat[]"]').select2();
			// $('.coba[get-id="'+num_input+'"] > div > select[name="obat[]"]').attr('disabled','disabled');
		});

		$(document).on('change','.sip',function(){
			var getAttr = $(this).attr('ajax-id');
			var getVal  = $(this).val();
			var getUrl  = '..'+'/ajax/get-obat/'+getVal;
			$.ajax({
				url: getUrl,
			})
			.done(function(done) {
				$('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').removeAttr('disabled');
				$('select[name="obat[]"][get-ajax-id="'+getAttr+'"]').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});

		$(document).on('click','.pencet',function(){
			var attr = $(this).attr('data-id');
			$('.coba[get-id="'+attr+'"]').slideUp(function(){
				$('.coba[get-id="'+attr+'"]').find('input,select').removeAttr('required');
				$('.coba[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
				$(`.coba[get-id="${attr}"`).find('input').val('');
			});
			$('.coba-juga[get-id="'+attr+'"]').slideDown(function(){
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('required','required');
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('disabled');
			});
			$(this).hide();
			$('.destroy[data-id="'+attr+'"]').show();
		});

		$(document).on('click','.destroy',function(){
			var attr = $(this).attr('data-id');
			$('.coba-juga[get-id="'+attr+'"]').slideUp(function(){
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').removeAttr('required');
				$('.coba-juga[get-id="'+attr+'"]').find('input,select').attr('disabled','disabled');
				$(`.coba-juga[get-id="${attr}"`).find('input').val('');
			});
			$('.coba[get-id="'+attr+'"]').slideDown(function(){
				$('.coba[get-id="'+attr+'"]').find('input,select').attr('required','required');
				$('.coba[get-id="'+attr+'"]').find('input').removeAttr('disabled');
				$('.coba[get-id="'+attr+'"]').find('.sip').removeAttr('disabled');
			});
			$(this).hide();
			$('.pencet[data-id="'+attr+'"]').show();
		});

		$('#hapus-obat').click(function(){
			$('#input-obat').remove().eq(1);
			$('#hapus-input-obat').remove().eq(1);
			$('#racik-obat').remove().eq(1);
			$('#input-racik').remove().eq(1);
			if ($('.coba').length == 1 && $('.coba-juga').length == 1) {
				$(this).hide();
			}
		});
	});
</script>
@endsection