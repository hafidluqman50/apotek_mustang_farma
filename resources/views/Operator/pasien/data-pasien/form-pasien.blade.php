@extends('Operator.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Pasien</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/operator/data-pasien/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Kategori Pasien</label>
							<select name="id_kategori_pasien" class="form-control select2" required="required" autofocus="autofocus">
								<option value="" selected="selected" disabled="disabled">=== Pilih Kategori Pasien ===</option>
								@foreach ($kategori_pasien as $element)
								<option value="{{$element->id_kategori_pasien}}" @if (isset($row)){!!$row->id_kategori_pasien == $element->id_kategori_pasien ? 'selected="selected"' : ''!!}@endif>{{$element->nama_kategori}}</option>
								@endforeach
							</select>
						</div>	
						<div class="form-group">
							<label for="">Kode Pasien</label>
							<input type="text" name="kode_pasien" class="form-control" value="{{isset($row)?$row->kode_pasien:''}}" autofocus="autofocus">
						</div>
						<div class="form-group">
							<label for="">Nama Pasien</label>
							<input type="text" name="nama_pasien" class="form-control" value="{{isset($row)?$row->nama_pasien:''}}" placeholder="Isi Nama Pasien" required="required">
						</div>
						<div class="form-group">
							<label for="">Nomor Telepon Pasien</label>
							<input type="number" name="nomor_telepon_pasien" class="form-control" value="{{isset($row)?$row->nomor_telepon_pasien:''}}" placeholder="Isi Nomor Telepon Pasien" required="required">
						</div>
						<div class="form-group">
							<label for="">Tanggal Lahir</label>
							<input type="date" class="form-control" name="tanggal_lahir" placeholder="Isi Tanggal Lahir" required="required" value="{{isset($row)?$row->tanggal_lahir:''}}">
						</div>
						<div class="form-group">
							<label for="">Jenis Kelamin</label>
							<select name="jenis_kelamin" class="form-control" required="required">
								<option selected="selected" disabled="disabled">=== Pilih Jenis Kelamin ===</option>
								<option value="Laki - Laki"@if(isset($row)){!!$row->jenis_kelamin == 'Laki - Laki'?'selected="selected"':''!!}@endif>Laki - Laki</option>
								<option value="Perempuan"@if(isset($row)){!!$row->jenis_kelamin == 'Perempuan' ? 'selected="selected"':''!!}@endif>Perempuan</option>
							</select>
						</div>
						<div class="form-group">
							<label for="">Alamat Pasien</label>
							<textarea name="alamat_pasien" class="form-control" placeholder="Isi Alamat Pasien" cols="30" rows="10" required="required">{{isset($row)?$row->alamat_pasien:''}}</textarea>
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_pasien:''}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')

@endsection