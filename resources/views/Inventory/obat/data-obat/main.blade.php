@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover data-obat force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Kode Obat</th>
							<th>Nomor Batch</th>
							<th>Nama Obat</th>
							<th>Nama Jenis Obat</th>
							<th>Golongan Obat</th>
							<th>Tanggal Expired</th>
							<th>Harga Obat</th>
							<th>Stok Obat</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var obat = $('.data-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-obat') }}",
            columns:[
                {data:'id_obat',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'kode_obat',name:'kode_obat'},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'icon_golongan',name:'icon_golongan'},
                {data:'tanggal_expired',name:'tanggal_expired'},
                {data:'harga_obat',name:'harga_obat'},
                {data:'stok_obat',name:'stok_obat'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        obat.on( 'order.dt search.dt', function () {
	        obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection