@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@elseif (session()->has('log'))
			<div class="alert alert-danger alert-dismissible">
				{{session('log')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@endif
			<div class="box box-default">
				<form action="{{url('/inventory/data-obat/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="form-group">
							<label for="">Nomor Batch</label>
							<input type="text" name="nomor_batch" class="form-control" placeholder="Isi Nomor Batch" value="{{isset($row)?$row->nomor_batch:''}}" required="required" {!!isset($row)?'disabled="disabled"':'autofocus="autofocus"'!!}>
							{!!isset($row)?'<input type="checkbox" id="check"> Ubah Nomor Batch':''!!}
						</div>
						<div class="form-group">
							<label for="">Nama Obat</label>
							<input type="text" name="nama_obat" class="form-control" placeholder="Isi Nama Obat" value="{{isset($row)?$row->nama_obat:''}}" required="required" {!!isset($row)?'autofocus="autofocus"':''!!}>
						</div>
						<div class="form-group">
							<label for="">Jenis Obat</label>
							<select name="jenis_obat" class="form-control select2" required="required">
								<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
								@foreach ($jenis_obat as $element)
								<option value="{{$element->id_jenis_obat}}" @if(isset($row)){!!$row->id_jenis_obat == $element->id_jenis_obat ? 'selected="selected"':''!!}@endif>{{$element->nama_jenis_obat}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label>Golongan Obat</label>
							<select name="golongan_obat" class="form-control select2" required="required">
								<option value="" selected="selected" disabled="disabled">=== Pilih Golongan Obat ===</option>
								@foreach ($golongan_obat as $element)
								<option value="{{$element->id_golongan_obat}}" @if (isset($row)){!!$row->id_golongan_obat == $element->id_golongan_obat ? 'selected="selected"' : ''!!}@endif>{{$element->nama_golongan}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="">Tanggal Expired</label>
							<input type="date" name="tanggal_expired" class="form-control" value="{{isset($row)?$row->tanggal_expired:''}}" required="required">
						</div>
						<div class="form-group">
							<label for="">Harga Obat</label>
							<input type="number" name="harga_obat" class="form-control" placeholder="Isi Harga Obat" value="{{isset($row)?$row->harga_obat:''}}" required="required">
						</div>
						<div class="form-group">
							<label for="">Stok Obat</label>
							<input type="number" name="stok_obat" class="form-control" placeholder="Isi Stok Obat" value="{{isset($row)?$row->stok_obat:''}}" required="required">
						</div>
						<div class="form-group">
							<label for="">Bobot Satuan</label>
							<input type="number" name="bobot_satuan" class="form-control" placeholder="Isi Bobot Satuan" value="{{isset($row)?$row->bobot_satuan:''}}" required="required">
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="ket_data" value="klinik">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_obat:''}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')

@endsection