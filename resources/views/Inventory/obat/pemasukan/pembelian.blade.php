@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Pembelian</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li role="presentation" {!!session()->has('titip_active') ? '' : 'class="active"'!!}><a href="#supplier" role="tab" data-toggle="tab">Beli Supplier</a></li>
				<li role="presentation" {!!session()->has('titip_active') ? 'class="active"' : ''!!}><a href="#titip" role="tab" data-toggle="tab">Titip Supplier</a></li>
			</ul>
			<div class="box box-default">
				<div class="box-header with-border">
					<a href="{{url('/inventory/laporan-pembelian')}}">
						<button class="btn btn-default">
							<span class="fa fa-long-arrow-left"></span> Kembali
						</button>
					</a>
				</div>
				<div class="box-body">
					<div class="tab-content">
						<div class="tab-pane fade {{session()->has('titip_active') ? '' : 'in active'}}" id="supplier">
							@if (session()->has('message_beli'))
							<div class="alert alert-success alert-dismissible">
								{{session('message_beli')}} <button class="close" data-dismiss="alert">X</button>
							</div>
							@elseif(session()->has('log_beli'))
							<div class="alert alert-danger alert-dismissible">
								{{session('log_beli')}} <button class="close" data-dismiss="alert">X</button>
							</div>
							@endif
							<a href="{{url('/inventory/data-pembelian/bayar-beli-semua',$id)}}">
								<button class="btn btn-success" onclick="return confirm('Yakin Bayar Semua ?');" style="margin-bottom:10px;">
									Bayar Semua
								</button>
							</a>
							<br>
							<h5><b>Total Bayar Beli : {{format_rupiah($sum_byr_beli)}}</b></h5>
							<table class="table table-hover force-fullwidth data-beli-obat">
								<thead>
									<th>No.</th>
									<th>Nama Supplier</th>
									<th>Tanggal Masuk Obat</th>
									<th>Tanggal Harus Bayar</th>
									<th>Nomor Batch</th>
									<th>Nama Obat</th>
									<th>Jenis Obat</th>
									<th>Stok Masuk</th>
									<th>Status Beli</th>
									<th>#</th>
								</thead>
								<tbody>
									
								</tbody>
							</table>	
						</div>
						<div class="tab-pane fade {{session()->has('titip_active') ? 'in active' : ''}}" id="titip">
							@if (session()->has('message_titip'))
							<div class="alert alert-success alert-dismissible">
								{{session('message_titip')}} <button class="close" data-dismiss="alert">X</button>
							</div>
							@elseif(session()->has('log_titip'))
							<div class="alert alert-danger alert-dismissible">
								{{session('log_titip')}} <button class="close" data-dismiss="alert">X</button>
							</div>
							@endif
							<a href="{{url('/inventory/data-pembelian/bayar-titip-semua',$id)}}">
								<button class="btn btn-success" onclick="return confirm('Yakin Bayar Semua ?');" style="margin-bottom:10px;">
									Bayar Semua
								</button>
							</a>
							<h5><b>Total Bayar Titip : {{format_rupiah($sum_byr_titip)}}</b></h5>
							<table class="table table-hover data-titip-obat force-fullwidth" cellspacing="0" width="100%">
								<thead>
									<th>No.</th>
									<th>Nama Supplier</th>
									<th>Tanggal Masuk Obat</th>
									<th>Tanggal Harus Bayar</th>
									<th>Nomor Batch</th>
									<th>Nama Obat</th>
									<th>Jenis Obat</th>
									<th>Jumlah Titip</th>
									<th>Banyak Pakai</th>
									<th>Harga Bayar</th>
									<th>Status Beli</th>
									<th>#</th>
								</thead>
								<tbody>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var beli_obat = $('.data-beli-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-pembelian/'.$id.'/supplier') }}",
            columns:[
                {data:'id_masuk',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_supplier',name:'nama_supplier'},
                {data:'tanggal_masuk',name:'tanggal_masuk'},
                {data:'tanggal_harus_bayar',name:'tanggal_harus_bayar'},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'stok_masuk',name:'stok_masuk'},
                {data:'status_beli',name:'status_beli'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        beli_obat.on( 'order.dt search.dt', function () {
	        beli_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();

        var titip_obat = $('.data-titip-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-pembelian/'.$id.'/titip') }}",
            columns:[
                {data:'id_masuk',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_supplier',name:'nama_supplier'},
                {data:'tanggal_masuk',name:'tanggal_masuk'},
                {data:'tanggal_harus_bayar',name:'tanggal_harus_bayar'},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                {data:'stok_masuk',name:'stok_masuk'},
                {data:'stok_pakai',name:'stok_pakai'},
                {data:'banyak_bayar',name:'banyak_bayar'},
                {data:'status_beli',name:'status_beli'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [{
	            width:'100%',
	            sortable: true,
	            "class": "index",
            }],
            scrollX:true,
            autoWidth:false,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        titip_obat.on( 'order.dt search.dt', function () {
	        titip_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();

	    $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
	        // var target = $(e.target).attr("href"); // activated tab
	        // alert (target);
	        $($.fn.dataTable.tables( true ) ).css('width', '100%');
	        $($.fn.dataTable.tables( true ) ).DataTable().columns.adjust().draw();
	    } ); 
	});
</script>
@endsection