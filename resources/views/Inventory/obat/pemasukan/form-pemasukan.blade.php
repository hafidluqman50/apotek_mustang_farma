@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
    <h1>Form Pemasukan</h1>
</section>

<section class="content">
    <form action="{{url('/inventory/data-masuk-obat/save')}}" method="POST">
        {{csrf_field()}}
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('message'))
                <div class="alert alert-success alert-dismissible">
                    {{session('message')}} <button class="close" data-dismiss="alert">X</button>
                </div>
                @endif
                <div class="box box-default">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="">Tanggal Masuk</label>
                            <input type="date" class="form-control" name="tanggal_masuk" value="{{isset($row)?$row->tanggal_masuk:''}}" required="required" autofocus="autofocus">
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Harus Bayar</label>
                            <input type="date" class="form-control" name="tanggal_harus_bayar" required="required">
                        </div>
                        <label for="">PPn</label>
                        <div class="input-group" style="border-bottom:1px solid lightgrey; margin-bottom:15px">
                            <input type="number" name="ppn" class="form-control" id="ppn" placeholder="Isi PPn; Ex:10;">
                            <span class="input-group-addon">%</span>
                        </div>
                        {{-- <button type="button" class="btn btn-primary" id="btn-supplier">
                            Input Supplier
                        </button>
                        <button type="button" class="btn btn-danger button-hide" id="cancel-supplier">
                            Batal Input
                        </button>
                        <div id="select-supplier">
                            <div class="form-group">
                                <label for="">Supplier</label>
                                <select name="supplier" class="form-control select2">
                                    <option selected="selected" disabled="disabled">=== Pilih Supplier ===</option>
                                    @foreach ($supplier as $element)
                                    <option value="{{$element->id_supplier}}" @if (isset($row)){!!$row->id_supplier == $element->id_supplier ? 'selected="selected"':''!!}@endif>{{$element->nama_supplier}}</option>
                                    @endforeach
                                </select>
                            </div> 
                        </div>--}}{{-- 
                        <div class="form-hide" id="input-supplier">
                            <div class="form-group">
                                <label for="">Nama Supplier</label>
                                <input type="text" name="nama_supplier" class="form-control" placeholder="Isi Nama Supplier">
                            </div>
                            <div class="form-group">
                                <label for="">Alamat Supplier</label>
                                <textarea name="alamat_supplier" id="" cols="30" rows="10" class="form-control" placeholder="Isi Alamat Supplier"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Nomor Telpon Supplier</label>
                                <input type="number" name="nomor_hp" class="form-control" placeholder="Isi Nomor Telpon Supplier">
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        @if (!isset($row))
                            <button type="button" class="btn btn-primary" id="tambah-obat">
                                Tambah Obat
                            </button>
                            <button type="button" class="btn btn-danger button-hide" id="kurang-obat">
                                Hapus Obat
                            </button>
                        @endif
                    </div>
                    <div class="box-body">
                        @if (!isset($row)){{-- 
                            <button type="button" class="btn btn-primary buka" id="btn-obat" data-id-obat="1">
                                Input Obat
                            </button>
                            <button type="button" class="btn btn-danger button-hide tutup" id="cancel-obat" data-id-obat="1">
                                Batal Input
                            </button> --}}
                        @endif
                        {{-- <div class="form-obat" id="select-obat" get-id-obat="1"> --}}
                            <table class="table table-hover force-fullwidth pemasukan">
                                <colgroup>
                                    <col span="1" style="width:20%;">
                                    <col span="1" style="width:20%;">
                                    <col span="1" style="width:20%;">
                                </colgroup>
                                <thead>
                                    <th>Supplier</th>
                                    <th>Jenis Obat</th>
                                    <th>Obat</th>
                                    <th>Stok Masuk</th>
                                    <th>Bentuk Satuan</th>
                                    <th>Harga Obat</th>
                                    <th>Diskon</th>
                                    <th>Total Harga Beli</th>
                                    <th>Jenis Masuk</th>
                                    <th>Keterangan</th>
                                    {{-- <th>Harga Beli + PPN</th> --}}
                                </thead>
                                <tbody>
                                    <tr class="ntap" id="sip">
                                        <td>
                                            <select name="supplier[]" class="form-control select2" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Supplier ===</option>
                                                @foreach ($supplier as $element)
                                                <option value="{{$element->id_supplier}}" @if (isset($row)){!!$row->id_supplier == $element->id_supplier ? 'selected="selected"':''!!}@endif>{{$element->nama_supplier}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select id="jenis-obat" class="form-control jenis-obat select2" select-ajax="1" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
                                                @foreach ($jenis_obat as $element)
                                                <option value="{{$element->id_jenis_obat}}" @if (isset($row)){!!$row->id_jenis_obat == $element->id_jenis_obat ? 'selected="selected"':''!!}@endif>{{$element->nama_jenis_obat}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select name="obat[]" id="obat-select" class="form-control select2" {!!isset($row)?'':'disabled="disabled"'!!} get-select-ajax="1" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Obat ===</option>
                                                @if (isset($row))
                                                    @foreach ($obat->getObat($row->id_jenis_obat,'inventory') as $element)
                                                        <option value="{{$element->id_obat}}" @if (isset($row)){!!$row->id_obat == $element->id_obat ? 'selected="selected"':''!!}@endif>{{$element->nomor_batch.' | '.$element->nama_obat}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" name="stok_masuk[]" class="form-control stok-masuk" id="stok-masuk" value="{{isset($row)?$row->stok_masuk:''}}" stok-masuk-id="1" placeholder="Isi Stok Masuk" required="required">
                                        </td>
                                        <td>
                                            <select name="bentuk_satuan[]" class="form-control" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Bentuk Satuan ===</option>
                                                <option value="pcs" @if(isset($row)){!!$row->bentuk_satuan == 'pcs' ? 'selected="selected"' : ''!!}@endif>Pcs</option>
                                                <option value="box" @if(isset($row)){!!$row->bentuk_satuan == 'box' ? 'selected="selected"' : ''!!}@endif>Box</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="number" class="form-control harga-beli" id="harga-beli" name="total_beli[]" value="{{isset($row)?$row->harga_beli:''}}" harga-beli-id="1" readonly>
                                            {{-- <input type="hidden" id="harga-beli-hidden" class="harga-beli-hidden" harga-beli-id="1"> --}}
                                        </td>
                                        <td>
                                            <input type="number" class="form-control diskon" id="diskon" name="diskon[]" value="{{isset($row)?$row->diskon:''}}" diskon-id="1" placeholder="Isi Diskon">
                                        </td>
                                        <td>
                                            <input type="number" class="form-control total-harga-beli" id="total-harga-beli" name="total_harga_beli[]" harga-beli-id="1" readonly>
                                            <input type="hidden" class="form-control total-harga-beli-hidden" id="total-harga-beli-hidden" harga-beli-id="1" readonly>
                                        </td>
                                        <td>
                                            <select name="jenis_masuk[]" class="form-control" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Jenis Masuk ===</option>
                                                <option value="-" @if (isset($row)){!!$row->jenis_masuk == '-' ? 'selected="selected"' : ''!!}@endif>-</option>
                                                <option value="supplier" @if (isset($row)){!!$row->jenis_masuk == 'supplier' ? 'selected="selected"' : ''!!}@endif>Beli Supplier</option>
                                                <option value="titip">Titip Supplier</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="keterangan[]" class="form-control" required="required">
                                                <option value="" selected="selected" disabled="disabled">=== Pilih Keterangan ===</option>
                                                <option value="Pemasukan Dari Supplier" @if (isset($row)){!!$row->keterangan == 'Pemasukan Dari Supplier'? 'selected="selected"' : ''!!}@endif>Pemasukan Dari Supplier</option>
                                                <option value="Pengembalian Obat Oleh Pasien" @if (isset($row)){!!$row->keterangan == 'Pengembalian Obat Oleh Pasien' ? 'selected="selected"' : ''!!}@endif>Pengembalian Obat Oleh Pasien</option>
                                            </select>
                                        </td>{{-- 
                                        <td>
                                            <input type="number" class="form-control" name="harga_beli[]" value="{{isset($row)?$row->harga_beli:''}}" placeholder="Isi Harga Beli" required="required">
                                        </td> --}}
                                    </tr>
                                </tbody>
                            </table>{{-- 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Jenis Obat</label>
                                    <select id="jenis-obat" class="form-control jenis-obat select2" select-ajax="1">
                                        <option selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
                                        @foreach ($jenis_obat as $element)
                                        <option value="{{$element->id_jenis_obat}}" @if (isset($row)){!!$row->id_jenis_obat == $element->id_jenis_obat ? 'selected="selected"':''!!}@endif>{{$element->nama_jenis_obat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Obat</label>
                                    <select name="obat[]" id="obat-select" class="form-control select2" {!!isset($row)?'':'disabled="disabled"'!!} get-select-ajax="1">
                                        <option selected="selected" disabled="disabled">=== Pilih Obat ===</option>
                                        @if (isset($row))
                                            @foreach ($obat->getObat($row->id_jenis_obat) as $element)
                                                <option value="{{$element->id_obat}}" @if (isset($row)){!!$row->id_obat == $element->id_obat ? 'selected="selected"':''!!}@endif>{{$element->nomor_batch.' | '.$element->nama_obat}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Stok Masuk</label>
                                    <input type="number" name="stok_masuk[]" class="form-control" value="{{isset($row)?$row->stok_masuk:''}}" placeholder="Isi Stok Masuk">
                                </div>
                                <div class="form-group">
                                    <label for="">Keterangan</label>
                                    <select name="keterangan[]" class="form-control">
                                        <option selected="selected" disabled="disabled">=== Pilih Keterangan ===</option>
                                        <option value="Pemasukan Dari Supplier" @if (isset($row)){!!$row->keterangan == 'Pemasukan Dari Supplier'? 'selected="selected"' : ''!!}@endif>Pemasukan Dari Supplier</option>
                                        <option value="Pengembalian Obat Oleh Pasien" @if (isset($row)){!!$row->keterangan == 'Pengembalian Obat Oleh Pasien' ? 'selected="selected"' : ''!!}@endif>Pengembalian Obat Oleh Pasien</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Harga Beli</label>
                                    <input type="number" class="form-control" name="harga_beli[]" value="{{isset($row)?$row->harga_beli:''}}" placeholder="Isi Harga Beli">
                                </div>
                            </div> --}}
                        {{-- </div> --}}{{-- 
                        <div class="form-input-obat form-hide" id="input-obat" get-id-obat="1">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Nomor Batch</label>
                                    <input type="text" name="nomor_batch[]" class="form-control" placeholder="Isi Nomor Batch">
                                </div>
                                <div class="form-group">
                                    <label for="">Nama Obat</label>
                                    <input type="text" name="nama_obat[]" class="form-control" placeholder="Isi Nama Obat">
                                </div>
                                <div class="form-group">
                                    <label for="">Jenis Obat</label>
                                    <select name="jenis_obat[]" class="form-control select2">
                                        <option selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
                                        @foreach ($jenis_obat as $element)
                                        <option value="{{$element->id_jenis_obat}}">{{$element->nama_jenis_obat}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Tanggal Expired</label>
                                    <input type="date" name="tanggal_expired[]" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Harga Beli</label>
                                    <input type="number" class="form-control" name="harga_beli_input[]" placeholder="Isi Harga Beli">
                                </div>
                                <div class="form-group">
                                    <label for="">Stok Masuk</label>
                                    <input type="number" name="stok_input_masuk[]" class="form-control" placeholder="Isi Stok Masuk">
                                </div>
                                <div class="form-group">
                                    <label for="">Harga Obat</label>
                                    <input type="number" name="harga_obat[]" class="form-control" placeholder="Isi Harga Obat">
                                </div>
                                <div class="form-group">
                                    <label for="">Keterangan Pemasukan</label>
                                    <select name="keterangan_input[]" class="form-control">
                                        <option selected="selected" disabled="disabled">=== Pilih Keterangan ===</option>
                                        <option value="Pemasukan Dari Supplier">Pemasukan Dari Supplier</option>
                                        <option value="Pengembalian Obat Oleh Pasien">Pengembalian Obat Oleh Pasien</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="id" value="{{isset($row)?$row->id_masuk:''}}">
                        <button type="submit" class="btn btn-primary">
                            Simpan <span class="fa fa-save"></span>
                        </button>
                    </div>
                </div>
            </div>
        </div> 
    </form>
</section>
@endsection

@section('js')
<script>
    $(function(){
        let base_url = "{{ url('/') }}";
        var btn_show_input    = 2;
        var btn_cancel_input  = 2;
        var input_obat        = 2;
        var cancel_input_obat = 2;
        var select_jenis_ajax = 2;
        var select_obat_ajax  = 2;
        var harga_beli_id     = 2;
        var diskon_id         = 2;

        // SUPPLIER ACT //
        $('#btn-supplier').click(function(){
            $('#select-supplier').slideUp();
            $('#input-supplier').slideDown();
            $(this).hide();
            $('#cancel-supplier').show();
        });
        $('#cancel-supplier').click(function(){
            $('#input-supplier').slideUp();
            $('#select-supplier').slideDown();
            $(this).hide();
            $('#btn-supplier').show();
        });
        // END SUPPLIER ACT //

        // AJAX GET OBAT BY JENIS OBAT //
        $(document).on('change','.jenis-obat',function(){
            // AMBIL VALUE SELECT JENIS OBAT //
            var val = $(this).val();
            // END AMBIL VALUE SELECT JENIS OBAT //

            // AMBIL VALUE ATTRIBUTE select-ajax //
            var attr = $(this).attr('select-ajax');
            // END AMBLI VALUE ATTRIBUTE select-ajax //

            // INISIALISASI VARIABLE getUrl DENGAN VALUE URL UNTUK DI AJAX //
            // var getUrl = '..'+'/ajax/get-obat/'+val;
            var getUrl = `${base_url}/ajax/get-obat/${val}`
            // END INISIALISASI VARIABLE getUrl DENGAN VALUE URL UNTUK DI AJAX //

            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                // alert($('select[name="obat[]"]').attr('get-select-ajax'));
                // HAPUS ATTRIBUTE DISABLED SESUAI VALUE ATTRIBUTE select-ajax //
                $('select[get-select-ajax="'+attr+'"]').removeAttr('disabled');
                // END HAPUS ATTRIBUTE DISABLED SESUAI VALUE ATTRIBUTE select-ajax //

                // MUNCULKAN HASIL AJAX SESUAI VALUE ATTRIBUTE select-ajax //
                $('select[get-select-ajax="'+attr+'"]').html(done);
                // END MUNCULKAN HASIL AJAX SESUAI VALUE ATTRIBUTE select-ajax //
            })
            .fail(function(error) {
                console.log(error);
            });
        });
        // END AJAX GET OBAT BY JENIS OBAT //

        // AJAX GET HARGA BELI BY OBAT //
        $('select[name="obat[]"]').change(function() {
            let val    = $(this).val()
            let attr   = $(this).attr('get-select-ajax')
            let getUrl = `${base_url}/ajax/get-harga-obat/${val}`
            $.ajax({
                url: getUrl
            })
            .done(function(done) {
                $(`.harga-beli[harga-beli-id="${attr}"]`).val(done)
                $(`.harga-beli-hidden[harga-beli-id="${attr}"]`).val(done)
            })
            .fail(function(error) {
                console.log(error)
            })
            .always(function() {
                console.log("complete");
            });
            
        })
        // END AJAX GET HARGA BELI BY OBAT //

        // OBAT ACT //
        $(document).on('click','.buka',function(){
            var attr = $(this).attr('data-id-obat');
            // alert(attr);
            $('.form-obat[get-id-obat="'+attr+'"]').slideUp();
            $('.form-input-obat[get-id-obat="'+attr+'"]').slideDown();
            $(this).hide();
            $('.tutup[data-id-obat="'+attr+'"]').show();
        });
        $(document).on('click','.tutup',function(){
            var attr = $(this).attr('data-id-obat');
            // alert(attr);
            // MUNCULKAN SELECT OBAT SESUAI VALUE DARI ATTRIBUTE data-id-obat //
            $('.form-input-obat[get-id-obat="'+attr+'"]').slideUp();
            $('.form-obat[get-id-obat="'+attr+'"]').slideDown();
            // MUNCULKAN SELECT OBAT SESUAI VALUE DARI ATTRIBUTE data-id-obat //

            // HILANGKAN TOMBOL TUTUP //
            $(this).hide();
            // END HILANGKAN TOMBOL TUTUP //

            // MUNCULKAN TOMBOL SESUAI VALUE ATTRIBUTE data-id-obat //
            $('.buka[data-id-obat="'+attr+'"]').show();
            // END MUNCULKAN TOMBOL SESUAI VALUE ATTRIBUTE data-id-obat //
        });
        // END OBAT ACT //

        // TAMBAH INPUT OBAT //
        // $('#tambah-obat').click(function(){

        //  // DESTROY SELECT2 //
        //  $('.jenis-obat').select2('destroy');
        //  $('select[name="obat[]"]').select2('destroy');
        //  $('select[name="jenis_obat[]"]').select2('destroy');
        //  // END DESTROY SELECT2 //

        //  // MUNCUL TOMBOL KURANG OBAT //
        //  $('#kurang-obat').show();
        //  // END MUNCUL KURANG TOMBOL OBAT //

        //  // CLONING ELEMEN KE CLASS col-md-8 box-body DAN ATTRIBUTE data-id-obat YANG DI INCREMENT //
        //  $('#btn-obat').clone().appendTo($('.col-md-8 .box-body')).attr('data-id-obat',btn_show_input++);
        //  $('#cancel-obat').clone().appendTo($('.col-md-8 .box-body')).attr('data-id-obat',btn_cancel_input++);
        //  $('#select-obat').clone().appendTo($('.col-md-8 .box-body')).attr('get-id-obat',cancel_input_obat++);
        //  $('#input-obat').clone().appendTo($('.col-md-8 .box-body')).attr('get-id-obat',input_obat++);
        //  // END CLONING ELEMEN KE CLASS col-md-8 box-body DAN ATTRIBUTE data-id-obat YANG DI INCREMENT //

        //  // INCREMENT ATTRIBUTE VALUE select-ajax UNTUK AJAX SESUAI SELECT //
        //  $('#jenis-obat').attr('select-ajax',select_jenis_ajax++);
        //  $('#obat-select').attr('get-select-ajax',select_obat_ajax++);
        //  // END INCREMENT ATTRIBUTE VALUE select-ajax UNTUK AJAX SESUAI SELECT //

        //  // INISIALISASI KEMBALI SELECT2 YANG DI DESTROY //
        //  $('.jenis-obat').select2();
        //  $('select[name="obat[]"]').select2();
        //  $('select[name="jenis_obat[]"]').select2();
        //  // INISIALISASI KEMBALI SELECT2 YANG DI DESTROY //
        // });
        // $('#kurang-obat').click(function(){
        //  // HAPUS ELEMENT SATU PER SATU
        //  $('#btn-obat').remove().eq(1);
        //  $('#cancel-obat').remove().eq(1);
        //  $('#select-obat').remove().eq(1);
        //  $('#input-obat').remove().eq(1);
        //  // END HAPUS ELEMENT SATU PER SATU //

        //  // CEK BANYAK ELEMEN form-input-obat DAN form-obat //
        //  if ($('.form-input-obat').length == 1 && $('.form-obat').length == 1) {
        //      $(this).hide();
        //  }
        //  // END CEK BANYAK ELEMEN form-input-obat DAN form-obat //
        // });
        // END TAMBAH INPUT OBAT //

        // SEMENTARA //
        $('#tambah-obat').click(function(){
            $('select[name="supplier[]"]').select2('destroy');
            $('.jenis-obat').select2('destroy');
            $('select[name="obat[]"]').select2('destroy');
            $('#sip').clone().prependTo('tbody').find('input').val('');

            $('#jenis-obat').attr('select-ajax',select_jenis_ajax++);
            $('#obat-select').attr('get-select-ajax',select_obat_ajax++);
            $('#diskon').attr('diskon-id',diskon_id++);
            $('#stok-masuk').attr('stok-masuk-id',harga_beli_id);

            // $('#harga-beli-hidden').attr('harga-beli-id',harga_beli_id);
            $('#harga-beli').attr('harga-beli-id',harga_beli_id);
            $('#total-harga-beli').attr('harga-beli-id',harga_beli_id);
            $('#total-harga-beli-hidden').attr('harga-beli-id',harga_beli_id);

            $('select[name="supplier[]"]').select2();
            $('.jenis-obat').select2();
            $('select[name="obat[]"]').select2();
            kurang_increment = select_obat_ajax - 1;
            $('select[get-select-ajax="'+kurang_increment+'"]').attr('disabled','disabled');
            $('#kurang-obat').show();
            harga_beli_id++;
        });

        $('#kurang-obat').click(function(){
            $('#sip').remove().eq(-1);
            if ($('.ntap').length == 1) {
                $(this).hide();
            }
        });

        $('.pemasukan').DataTable({
            scrollX:true,
            autoWidth:false
        });

        $('.stok-masuk').keyup(function() {
            let val               = parseInt($(this).val())
            let attr              = $(this).attr('stok-masuk-id')
            let harga_beli_hidden = parseInt($(`.harga-beli[harga-beli-id="${attr}"]`).val())
            let ppn               = $('#ppn').val()

            var kalkulasi = val * harga_beli_hidden
            kalkulasi     = kalkulasi + ((kalkulasi * ppn) / 100)

            $(`.total-harga-beli[harga-beli-id="${attr}"]`).val(kalkulasi)
            $(`.total-harga-beli-hidden[harga-beli-id="${attr}"]`).val(kalkulasi)
            // var kalkulasi = 
        })

        $('.diskon').keyup(function() {
            let val               = parseInt($(this).val())
            let attr              = $(this).attr('diskon-id')
            let total_beli_hidden = parseInt($(`.total-harga-beli-hidden[harga-beli-id="${attr}"]`).val())
            
            let kalkulasi = total_beli_hidden - ((total_beli_hidden * val) / 100)

            $(`.total-harga-beli[harga-beli-id="${attr}"]`).val(kalkulasi)
        })
        // $('.select2').select2({
        //     // dropdownAutoWidth : true,
        //     // width: 'auto'
        // })
        // END SEMENTARA //
    });
</script>
@endsection