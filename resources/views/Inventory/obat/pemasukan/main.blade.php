@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Pemasukan Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover data-masuk-obat force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Tanggal Masuk</th>
							<th>Nama Supplier</th>
							<th>Nomor Batch</th>
							<th>Nama Obat</th>
							<th>Jenis Obat</th>
							{{-- <th>Harga Beli</th> --}}
							<th>Stok Masuk</th>
							<th>Keterangan</th>
							<th>Input By</th>
							{{-- <th>#</th> --}}
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var masuk_obat = $('.data-masuk-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-masuk-obat') }}",
            columns:[
                {data:'id_masuk',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'tanggal_masuk',name:'tanggal_masuk'},
                {data:'nama_supplier',name:'nama_supplier'},
                {data:'nomor_batch',name:'nomor_batch'},
                {data:'nama_obat',name:'nama_obat'},
                {data:'nama_jenis_obat',name:'nama_jenis_obat'},
                // {data:'harga_beli',name:'harga_beli'},
                {data:'stok_masuk',name:'stok_masuk'},
                {data:'keterangan',name:'keterangan'},
                {data:'name',name:'name'}
                // {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        masuk_obat.on( 'order.dt search.dt', function () {
	        masuk_obat.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection