@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Form Pengeluaran</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			@if (session()->has('message'))
			<div class="alert alert-success alert-dismissible">
				{{session('message')}} <button class="close" data-dismiss="alert">X</button>
			</div>
			@elseif ($errors->any())
				@foreach ($errors->all() as $error)
					<div class="alert alert-danger alert-dismissible">
						{{$error}} <button class="close" data-dismiss="alert">X</button>
					</div>
				@endforeach
			@endif
			<div class="box box-default">
				<form action="{{url('/inventory/data-keluar-obat/save')}}" method="POST">
					{{csrf_field()}}
					<div class="box-body">
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Tanggal Keluar</label>
								<input type="date" name="tanggal_keluar" value="{{isset($row)?$row->tanggal_keluar:(count($errors) > 0 ? old('tanggal_keluar') : '')}}" class="form-control" autofocus="autofocus">
							</div>
							<div class="form-group">
								<label for="">Jenis Obat</label>
								<select class="form-control select2" name="jenis_obat" id="jenis" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Jenis Obat ===</option>
									@foreach ($jenis_obat as $element)
									<option value="{{$element->id_jenis_obat}}" @if (isset($row)){!!$row->id_jenis_obat == $element->id_jenis_obat ? 'selected="selected"' : ''!!}@elseif(count($errors) > 0){!!old('jenis_obat') == $element->id_jenis_obat ? 'selected="selected"' : ''!!}@endif>{{$element->nama_jenis_obat}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label for="">Obat</label>
								<select name="obat" class="form-control select2" id="obat" required="required" {!! isset($row)?'':(count($errors) > 0 ? '' : 'disabled="disabled"') !!}>
									<option selected="selected" disabled="disabled">=== Pilih Obat ===</option>
									@if (isset($row))
										@foreach ($obat->getObat($row->id_jenis_obat,'klinik') as $element)
											<option value="{{$element->nomor_batch}}" {!!$row->id_obat == $element->id_obat ? 'selected="selected"' : ''!!}>{{$element->nomor_batch.' | '.$element->nama_obat}}</option>
										@endforeach
									@endif
									@if(count($errors) > 0)
										@foreach ($obat->getObat(old('jenis_obat'),'klinik') as $element)
											<option value="{{$element->nomor_batch}}" {!! old('obat') == $element->nomor_batch ? 'selected="selected"' : '' !!}>{{$element->nomor_batch.' | '.$element->nama_obat}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Stok Keluar</label>
								<input type="number" name="stok_keluar" class="form-control" value="{{isset($row)?$row->stok_keluar:(count($errors) > 0 ? old('stok_keluar') : '')}}" placeholder="Isi Stok Keluar" required="required">
							</div>
							<div class="form-group">
								<label for="">Bentuk Satuan</label>
								<select name="bentuk_stok" class="form-control" required="required">
									<option value="" selected="selected" disabled="disabled">=== Pilih Bentuk Satuan ===</option>
									<option value="pcs" @if(count($errors) > 0){!!old('bentuk_stok') == 'pcs' ? 'selected="selected"' : ''!!}@endif>Pcs</option>
									<option value="box" @if(count($errors) > 0){!!old('bentuk_stok') == 'box' ? 'selected="selected"' : ''!!}@endif>Box</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Keterangan</label>
								<select name="keterangan" class="form-control">
									<option selected="selected" disabled="disabled">=== Pilih Keterangan ===</option>
									<option value="Pengembalian Ke Supplier" @if(isset($row)) {!!$row->keterangan == 'Pengembalian Ke Supplier' ? 'selected="selected"' : ''!!} @elseif(count($errors) > 0) {!!old('keterangan') == 'Pengembalian Ke Supplier' ? 'selected="selected"' : ''!!} @endif>Pengembalian Ke Supplier</option>
									<option value="Pengeluaran Klinik" @if(isset($row)) {!!$row->keterangan == 'Pengeluaran Klinik' ? 'selected="selected"' : ''!!} @elseif(count($errors) > 0) {!!old('keterangan') == 'Pengeluaran Klinik' ? 'selected="selected"' : ''!!} @endif>Tujuan Ke Klinik</option>
									<option value="Pengeluaran Kasir" @if(isset($row)) {!!$row->keterangan == 'Pengeluaran Kasir' ? 'selected="selected"' : ''!!} @elseif(count($errors) > 0) {!!old('keterangan') == 'Pengeluaran Kasir' ? 'selected="selected"' : ''!!} @endif>Tujuan Ke Kasir</option>
								</select>
							</div>
						</div>
					</div>
					<div class="box-footer">
						<input type="hidden" name="id" value="{{isset($row)?$row->id_keluar:(count($errors) > 0 ? old('id') : '')}}">
						<button class="btn btn-primary">
							Simpan <span class="fa fa-save"></span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
		$('#jenis').change(function(){
			var val = $(this).val();
			var getUrl = '..'+'/ajax/get-obat/'+val+'/inventory';
			$.ajax({
				url: getUrl,
			})
			.done(function(done) {
				$('#obat').removeAttr('disabled');
				$('#obat').html(done);
			})
			.fail(function(error) {
				console.log(error);
			});
		});
	});
</script>
@endsection