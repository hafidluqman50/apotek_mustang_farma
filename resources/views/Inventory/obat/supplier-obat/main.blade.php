@extends('Inventory.layout.layout-app')

@section('content')
<section class="content-header">
	<h1>Data Supplier Obat</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default">
				<div class="box-body">
					@if (session()->has('message'))
					<div class="alert alert-success alert-dismissible">
						{{session('message')}} <button class="close" data-dismiss="alert">X</button>
					</div>
					@endif
					<table class="table table-hover data-supplier-obat force-fullwidth">
						<thead>
							<th>No.</th>
							<th>Nama Supplier</th>
							<th>Nomor Hp Supplier</th>
							<th>Alamat Supplier</th>
							<th>#</th>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(function(){
        var supplier = $('.data-supplier-obat').DataTable({
            processing:true,
            serverSide:true,
            ajax:"{{ url('/datatables/data-supplier-obat') }}",
            columns:[
                {data:'id_supplier',searchable:false,render:function(data,type,row,meta){
                    return meta.row + meta.settings._iDisplayStart+1;
                }},
                {data:'nama_supplier',name:'nama_supplier'},
                {data:'nomor_telepon',name:'nomor_telepon'},
                {data:'alamat_supplier',name:'alamat_supplier'},
                {data:'action',name:'action',searchable:false,orderable:false}
            ],
            scrollCollapse: true,
            columnDefs: [ {
            sortable: true,
            "class": "index",
            }],
            scrollX:true,
            order: [[ 0, 'desc' ]],
            responsive:true,
            fixedColumns: true
        });
        supplier.on( 'order.dt search.dt', function () {
	        supplier.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        	cell.innerHTML = i+1;
	        });
        }).draw();
	});
</script>
@endsection