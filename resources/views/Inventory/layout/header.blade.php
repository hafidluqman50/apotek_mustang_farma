<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$title}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bower_components/select2/dist/css/select2.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('assets/dist/css/skins/_all-skins.min.css')}}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">{{-- 
    <style>
    .content-header > h1 {
      color:white;
    }
    </style> --}}
    <style>
      select.select2 {
        position:static !important;
        outline:none !important;
      }
    </style>
</head>
<body class="hold-transition skin-green sidebar-mini fixed">
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>88</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Klinik</b>88</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
          <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                  <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image">
                          <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                          <!-- User image -->
                          <li class="user-header">
                            <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                            <p>
                              {{Auth::user()->name}}
                            </p>
                          </li>
                          <li class="user-footer">
                            <div class="pull-left">
                              <a href="{{url('/inventory/ubah-profile')}}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                              <a href="{{url('/logout')}}" class="btn btn-default btn-flat">Logout</a>
                            </div>
                          </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li @if(isset($page)){!!$page=='dashboard'?'class="active"':''!!}@endif>
          <a href="{{url('/inventory/dashboard')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview @if(isset($link)){!!$link=='obat'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-medkit"></i> <span>Obat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if (isset($page)){!!$page=='laporan-order-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-order-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Order Obat</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview @if (isset($link)){!!$link=='supplier-obat'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-address-book-o"></i> <span>Supplier Obat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(isset($page)){!!$page=='data-supplier-obat'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/data-supplier-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Supplier Obat</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-supplier'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-supplier')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Supplier</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview @if (isset($link)){!!$link=='pemasukan-obat'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-book"></i> <span>Pemasukan Obat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(isset($page)){!!$page=='data-masuk'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/data-masuk-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Pemasukan Obat</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-pemasukan'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-pemasukan')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Pemasukan Obat</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-pembelian'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-pembelian')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Pembelian Obat</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview @if (isset($link)){!!$link=='pengeluaran-obat'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-book"></i> <span>Pengeluaran Obat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if(isset($page)){!!$page=='data-keluar'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/data-keluar-obat')}}">
                <i class="fa fa-circle-o"></i> <span>Data Pengeluaran Obat</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-pengeluaran'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-pengeluaran')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Pengeluaran Obat</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview @if (isset($link)){!!$link=='stok-opnem'?'active menu-open':''!!}@endif">
          <a href="#">
            <i class="fa fa-database"></i> <span>Stok Opnem</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if (isset($page)){!!$page=='stok-opnem'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/stok-opnem')}}">
                <i class="fa fa-circle-o"></i> <span>Data Stok Opnem</span>
              </a>
            </li>
            <li @if (isset($page)){!!$page=='laporan-stok-opnem'?'class="active"':''!!}@endif>
              <a href="{{url('/inventory/laporan-stok-opnem')}}">
                <i class="fa fa-circle-o"></i> <span>Laporan Stok Opnem</span>
              </a>
            </li>
          </ul>
        </li>
        {{-- <li class="treeview @if (isset($link)){{$link=='laporan'?'active menu-open':''}}@endif">
          <a href="#">
            <i class="fa fa-file"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          </ul>
        </li> --}}
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
