<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function index() {
    	return view('login');
    }

    public function login(Request $request) {
    	$username = $request->username;
    	$password = $request->password;
    	if (Auth::attempt(['username'=>$username,'password'=>$password,'status_delete'=>0],true)) {
            if (Auth::user()->level_user == 5) {
                return redirect('/admin/dashboard');
            }
            elseif (Auth::user()->level_user == 4) {
                return redirect('/inventory/dashboard');
            }
            elseif(Auth::user()->level_user == 3) {
                return redirect('/dokter/dashboard');
            }
            elseif(Auth::user()->level_user == 2) {
                return redirect('/operator/dashboard');
            }
            elseif (Auth::user()->level_user == 1) {
                return redirect('/resep/dashboard');
            }
            elseif (Auth::user()->level_user == 0) {
                Auth::logout();
                return redirect('/login')->with('log','Kredensial Salah');
            }
            elseif (Auth::user()->active == 0) {
                Auth::logout();
                return redirect('/login')->with('log','Maaf Akun Anda Non Aktif');
            }
    	}
        else {
            return redirect('/login')->with('log','Kredensial Salah');
        }
    }

    public function user() {
        User::insert([
                0=>[
                    'name'          => 'dr. James Andrews',
                    'username'      => 'james',
                    'password'      => bcrypt('james'),
                    'level_user'    => 3,
                    'active'        => 1,
                    'status_delete' => 0
                ],
                1=>[
                    'name'          => 'dr. OZ',
                    'username'      => 'oz',
                    'password'      => bcrypt('oz'),
                    'level_user'    => 3,
                    'active'        => 1,
                    'status_delete' => 0
                ]
            ]
        );
    }

    public function logout() {
    	Auth::logout();
    	return redirect('/login');
    }
}
