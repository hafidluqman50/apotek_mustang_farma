<?php

namespace App\Http\Controllers\Resep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TindakanLabModel as TindakanLab;

class TindakanLabController extends Controller
{
	public function index() {
		$title = 'Tindakan Lab | Resep';
		$page = 'laporan-tindakan-lab';
		$link = 'laporan';
		return view('Resep.transaksi.tindakan-lab.main',compact('title','page','link'));
	}

	public function tambah() {
		$title = 'Form Tindakan Lab | Resep';
		$page = 'tindakan-lab';
		$link = 'transaksi';
		return view('Resep.transaksi.tindakan-lab.form-tindakan-lab',compact('title','page','link'));
	}

	public function edit($id) {
		$title = 'Form Tindakan Lab | Resep';
		$page = 'tindakan-lab';
		$link = 'transaksi';
		$row = TindakanLab::where('id_tindakan_lab',$id)->firstOrFail();
		return view('Resep.transaksi.tindakan-lab.form-tindakan-lab',compact('title','page','link','row'));
	}

	public function delete($id) {
		TindakanLab::where('id_tindakan_lab',$id)->delete();
		return redirect('/admin/laporan-tindakan-lab')->with('message','Berhasil Hapus Tindakan Lab');
	}

	public function save(Request $request) {
		$tindakan_lab = $request->nama_tindakan;
		$biaya_lab	  = $request->biaya_lab;
		$id 		  = $request->id_tindakan_lab;
		$array = [
			'nama_tindakan' => $tindakan_lab,
			'biaya_lab'     => $biaya_lab
		];
		if ($id == '') {
			TindakanLab::create($array);
			$message = 'Berhasil Input Tindakan Lab';
		}
		else {
			TindakanLab::where('id_tindakan_lab',$id)->update($array);
			$message = 'Berhasil Update Tindakan Lab';
		}
		return redirect('/admin/tindakan-lab')->with('message',$message);
	}
}
