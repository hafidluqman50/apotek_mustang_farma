<?php

namespace App\Http\Controllers\Resep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function dashboard() {
    	$title = 'Dashboard';
    	$page = 'dashboard';
    	return view('Resep.dashboard',compact('title','page'));
    }

    public function ubahProfile() {
    	$title = 'Ubah Profile | Resep';
    	return view('Resep.ubah-profile',compact('title','page'));
    }

    public function save(Request $request) {
		$name     = $request->nama;
		$username = $request->username;
		if (User::where('username',$username)->count()>1) {
			return redirect()->back()->withErrors(['log'=>'Username Sudah Ada'])->withInput();
		}
		$password      = $request->password;
		$level_user    = $request->level_user;
		$status_delete = 0;
		$active        = 1;
		$array = [
			'name'          => $name,
			'username'      => $username,
			'password'      => bcrypt($password),
			'level_user'    => $level_user,
			'status_delete' => $status_delete,
			'active'        => $active
		];
		if ($username == '' && $password == '') {
			$array = array_slice($array,0,-5);
		}
		elseif($username == '') {
			$array = array_slice($array,0,-4);
			unset($array['username']);
		}
		elseif ($password == '') {
			$array = array_slice($array,0,-4);
		}
		else {
			$array = array_slice($array,0,-3);
		}
		User::where('id_users',Auth::id())->update($array);
		$message = 'Berhasil Update User';
		return redirect('/resep/ubah-profile')->with('message',$message);
    }
}
