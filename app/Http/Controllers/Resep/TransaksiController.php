<?php

namespace App\Http\Controllers\Resep;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\TransaksiModel as Transaksi;
use App\Models\TindakanLabModel as TindakanLab;
use App\Models\DiskonModel as Diskon;
use App\Models\PengeluaranModel as Pengeluaran;
use App\Models\PemasukanModel as Pemasukan;
use App\Models\ObatModel as Obat;
use Auth;

class TransaksiController extends Controller
{
    public function index() {
        $title = 'Data Transaksi | Resep';
        $page  = 'laporan-transaksi';
        $link  = 'laporan';
    	return view('Resep.transaksi.data-transaksi.main',compact('title','page','link'));
    }

    public function tambah() {
        $title        = 'Form Transaksi | Resep';
        $page         = 'transaksi';
        $link         = 'transaksi';
        $resep        = Resep::getData('belum-bayar');
        $tindakan_lab = TindakanLab::whereNotIn('id_tindakan_lab',[4])->get();
    	return view('Resep.transaksi.data-transaksi.form-transaksi',compact('title','page','resep','link','tindakan_lab'));
    }

    public function delete($id) {
        Transaksi::where('id_transaksi',$id)->delete();
        return redirect('/resep/laporan-transaksi')->with('message','Berhasil Hapus Transaksi');
    }

   public function save(Request $request) {
        $resep             = $request->customer;
        $tindakan_lab      = $request->tindakan_lab != null ? $request->tindakan_lab : 4;
        $biaya_klinik      = $request->biaya_klinik;
        $biaya_resep       = $request->biaya_resep;
        $biaya_jasa_lab    = $request->biaya_jasa_lab != null ? $request->biaya_jasa_lab : 0;
        $total_harga       = $request->total_harga;
        $bayar             = $request->bayar;
        $kembali           = $request->kembali;

        $data_transaksi = [
            'tgl_byr'           => now()->format('Y-m-d'),
            'id_resep'          => $resep,
            'id_tindakan_lab'   => $tindakan_lab,
            'id_users'          => Auth::id(),
            'biaya_klinik'      => $biaya_klinik,
            'biaya_resep'       => $biaya_resep,
            'biaya_jasa_lab'    => $biaya_jasa_lab,
            'jumlah_byr'        => $total_harga,
            'bayar'             => $bayar,
            'kembali'           => $kembali,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        ];
        $transaksi_id = Transaksi::insertGetId($data_transaksi);

        $get = ResepDetail::where('id_resep',$resep)->get();
        foreach ($get as $key => $value) {
            // $cek_batch   = Obat::where('nomor_batch',$value->nomor_batch)->->count();
            
            // if ($cek_batch > 0) {
                $harga_obat  = Obat::where('id_obat',$value->id_obat)->firstOrFail();
                $obat_inv    = Obat::where('nomor_batch',$harga_obat->nomor_batch)->where('ket_data','inventory')->firstOrFail();
                $cek         = Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->count();
                
                if ($cek > 0) {
                    $pakai       = Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->firstOrFail();
                    // if ($pakai->stok_pakai != $pakai->stok_masuk) {
                        $stok_pakai  = $value->banyak_obat+$pakai->stok_pakai;
                        $banyak_bayar = $harga_obat * $stok_pakai;
                        
                        Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->update(['stok_pakai'=>$stok_pakai,'banyak_bayar'=>$banyak_bayar]);
                    // }
                }
            // }

            $array[] = [
                'tanggal_keluar' => $data_transaksi['tgl_byr'],
                'id_obat'        => $value->id_obat,
                'stok_keluar'    => $value->banyak_obat,
                'bentuk_stok'    => 'pcs',
                'keterangan'     => 'Transaksi Klinik',
                'id_users'       => Auth::id(),
                'jenis_keluar'   => 'transaksi'
            ];
        }
        Pengeluaran::insert($array);
        return redirect('/resep/data-transaksi/cetak/'.$transaksi_id);
    }

    public function cetak($id) {
        $data = Transaksi::dataCetak($id);
        $obat = Transaksi::dataCetak($id,'obat');
        
        return view('Resep.transaksi.data-transaksi.cetak',compact('data','obat'));
    }
}
