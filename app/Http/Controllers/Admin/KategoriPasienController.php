<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KategoriPasienModel as KategoriPasien;

class KategoriPasienController extends Controller
{
    public function index() {
		$title = 'Data Kategori Pasien | Admin';
		$page  = 'laporan-kategori-pasien';
		$link  = 'pasien';
    	return view('Admin.pasien.kategori-pasien.main',compact('title','page','link'));
    }

    public function tambah() {
        $title = 'Form Kategori Pasien | Admin';
        $page  = 'data-kategori-pasien';
        $link  = 'pasien';
    	return view('Admin.pasien.kategori-pasien.form-kategori-pasien',compact('title','page','link'));
    }

    public function edit($id) {
        $title = 'Form Kategori Pasien | Admin';
        $page  = 'data-kategori-pasien';
        $link  = 'pasien';
        $row   = KategoriPasien::where('id_kategori_pasien',$id)->firstOrFail();
    	return view('Admin.pasien.kategori-pasien.form-kategori-pasien',compact('title','page','link','row'));
    }

    public function delete($id) {
    	KategoriPasien::where('id_kategori_pasien',$id)->update(['status_delete' => 1]);
    	return redirect('/admin/laporan-kategori-pasien')->with('message','Berhasil Hapus Kategori Pasien');
    }

    public function save(Request $request) {
		$kategori_pasien = $request->kategori_pasien;
		$id              = $request->id;
    	$data_kategori_pasien = [
    		'nama_kategori' => $kategori_pasien,
            'status_delete' => 0
    	];

    	if ($id == '') {
    		KategoriPasien::create($data_kategori_pasien);
    		$message = 'Berhasil Input Kategori Pasien';
    	}
    	else {
            unset($data_kategori_pasien['status_delete']);
    		KategoriPasien::where('id_kategori_pasien',$id)->update($data_kategori_pasien);
    		$message = 'Berhasil Update Kategori Pasien';
    	}
    	return redirect('/admin/data-kategori-pasien')->with('message',$message);
    }
}
