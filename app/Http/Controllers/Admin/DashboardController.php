<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProfileInstansiModel as ProfileInstansi;
use App\Models\PasienModel as Pasien;
use App\Models\ResepDetailModel as ResepDetail;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function dashboard() {
        $title  = 'Dashboard | Admin';
        $page   = 'dashboard';
        // $pasien = Pasien::ktgPasien($id_pasien);

    	return view('Admin.dashboard',compact('title','page'));
    }

    public function rekamMedisDetail($id)
    {
        $title  = 'Rekam Medis Detail | Admin';
        $page   = 'dashboard';
        $detail = ResepDetail::getPasien('',$id);

        return view('Admin.detail-rekam-medis',compact('title','page','detail','id'));
    }

    public function ubahProfile() {
        $title            = 'Ubah Profile | Admin';
        $page             = 'dashboard';
        $profile_instansi = ProfileInstansi::first();
    	return view('Admin.ubah-profile',compact('title','page','profile_instansi'));
    }

    public function save(Request $request) {
        $name                      = $request->nama;
        $username                  = $request->username;
        $password                  = $request->password;
        $nama_instansi             = $request->nama_instansi;
        $alamat_instansi           = $request->alamat_instansi;
        $nomor_telepon_instansi    = $request->nomor_telepon_instansi;
        $logo_instansi_input       = $request->logo_instansi_input;
        $background_instansi_input = $request->background_instansi_input;
        $id_profile_instansi       = $request->id_profile_instansi;

		if (User::where('username',$username)->count()>1) {
			return redirect()->back()->withErrors(['log'=>'Username Sudah Ada'])->withInput();
		}

		$data_user = [
			'name'          => $name,
			'username'      => $username,
			'password'      => bcrypt($password)
		];

        $data_profile_instansi = [
            'nama_instansi'          => $nama_instansi,
            'alamat_instansi'        => $alamat_instansi,
            'nomor_telepon_instansi' => $nomor_telepon_instansi
        ];

		if ($username == '' && $password == '') {
            unset($data_user['username']);
            unset($data_user['password']);
		}
		else if($username == '') {
			unset($data_user['username']);
		}
		else if ($password == '') {
            unset($data_user['password']);
		}

        if ($logo_instansi_input != '') {
            if ($id_profile_instansi != '') {
                $get_logo_old = ProfileInstansi::firstOrFail()->logo_instansi;
                unlink(public_path('/profile-instansi/').$get_logo_old);
            }
            $data_profile_instansi['logo_instansi'] = $logo_instansi_input->getClientOriginalName();
            $logo_instansi_input->move(public_path('/profile-instansi/'),$logo_instansi_input->getClientOriginalName());
        }

        if ($background_instansi_input != '') {
            if ($id_profile_instansi != '') {
                $get_background_old = ProfileInstansi::firstOrFail()->background_instansi;
                unlink(public_path('/profile-instansi/').$get_background_old);
            }
            $data_profile_instansi['background_instansi'] = $background_instansi_input->getClientOriginalName();
            $background_instansi_input->move(public_path('/profile-instansi/'),$background_instansi_input->getClientOriginalName());
        }

		User::where('id_users',Auth::id())->update($data_user);

        if (ProfileInstansi::count() == 0) {
            ProfileInstansi::create($data_profile_instansi);
        }
        else {
            ProfileInstansi::where('id_profile_instansi',$id_profile_instansi)
                            ->update($data_profile_instansi);
        }
		$message = 'Berhasil Update User';
		return redirect('/admin/ubah-profile')->with('message',$message);
    }
}
