<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObatModel as Obat;
use App\Models\SupplierModel as Supplier;
use App\Models\JenisObatModel as JenisObat;
use App\Models\PemasukanModel as Pemasukan;
// use App\Exports\PemasukanExport;
use Auth;
// use Excel;
// use PHPExcel_Style_Border;
// use PHPExcel_Style_Alignment;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PemasukanController extends Controller
{
    public function index() {
		$title = 'Data Pemasukan | Admin';
		$page  = 'laporan-pemasukan';
		$link  = 'pemasukan-obat';
    	return view('Admin.obat.pemasukan.main',compact('title','page','link'));
    }

    public function pembelian() {
        $title = 'Supplier | Admin';
        $page  = 'laporan-pembelian';
        $link  = 'pemasukan-obat';
    	return view('Admin.obat.pemasukan.supplier-beli',compact('title','page','link'));
    }

    public function beliDetail($id) {
		$title         = 'Data Pembelian | Admin';
		$page          = 'laporan-pembelian';
		$link          = 'pemasukan-obat';
		$sum_byr_titip = Pemasukan::where('id_supplier',$id)->where('jenis_masuk','titip')->where('status_beli',0)->sum('banyak_bayar');
		$sum_byr_beli  = Pemasukan::where('id_supplier',$id)->where('jenis_masuk','supplier')->where('status_beli',0)->sum('banyak_bayar');
		return view('Admin.obat.pemasukan.pembelian',compact('title','page','link','id','sum_byr_titip','sum_byr_beli'));
    }

    public function bayar($id_supplier,$id_masuk) {
    	$pemasukan = Pemasukan::where('id_supplier',$id_supplier)->where('id_masuk',$id_masuk);
    	if ($pemasukan->firstOrFail()->status_beli == 0) {
    		$pemasukan->update(['status_beli'=>1]);
			$status  = 'message';
			$message = 'Berhasil Ubah Status Beli';
    	}
    	else {
			$status  = 'log';
			$message = 'Obat sudah dibayar';
    	}
    	return redirect('/admin/laporan-pembelian/detail/'.$id_supplier)->with($status,$message);
    }

    public function bayarSemua($id) {
    	$pemasukan = Pemasukan::where('id_supplier',$id)->update(['status_beli'=>1]);
    	return redirect('/admin/laporan-pembelian/detail/'.$id)->with('message','Berhasil Bayar Semua');
    }

    public function tambah() {
		$title      = 'Form Pemasukan | Admin';
		$page       = 'data-masuk';
		$link       = 'pemasukan-obat';
		// $obat       = Obat::getData();
		$supplier   = Supplier::whereNotIn('id_supplier',[1])->get();
		$jenis_obat = JenisObat::all();
    	return view('Admin.obat.pemasukan.form-pemasukan',compact('title','page','link','supplier','jenis_obat'));
    }

    public function edit($id) {
		$title      = 'Form Pemasukan | Admin';
		$page       = 'data-masuk';
		$link       = 'pemasukan-obat';
		$obat       = new Obat;
		$supplier   = Supplier::whereNotIn('id_supplier',[1])->get();
		$jenis_obat = JenisObat::all();
		$row        = Pemasukan::getRow($id);
		return view('Admin.obat.pemasukan.form-pemasukan',compact('title','page','link','row','obat','supplier','jenis_obat'));
    }

    public function delete($id) {
    	Pemasukan::where('id_masuk',$id)->delete();
    	return redirect('/admin/laporan-pemasukan')->with('message','Berhasil Hapus Data Pemasukan');
    }

    public function bayarBeliSemua($id) {
    	$cek = Pemasukan::where('id_supplier',$id)->where('jenis_masuk','supplier');
    	if ($cek->count() == 0) {
    		$status = 'log_beli';
    		$message = 'Data Pemasukan Beli Supplier Tidak Ada';
    	}
    	else {
    		$status = 'message_beli';
    		$message = 'Berhasil Ubah Status Beli Obat Supplier';
    	}
    	$cek->update(['status_beli'=>1]);
    	return redirect('/admin/laporan-pembelian/detail/'.$id)->with($status,$message);
    }

    public function bayarTitipSemua($id) {
    	$cek = Pemasukan::where('id_supplier',$id)->where('jenis_masuk','titip');
    	if ($cek->count() == 0) {
			$status  = 'log_titip';
			$message = 'Data Pemasukan Titip Supplier Tidak Ada';
    	}
    	else {
			$status  = 'message_titip';
			$message = 'Berhasil Ubah Status Beli Titip Obat';
    	}
    	session()->flash($status,$message);
    	session()->flash('titip_active','active');
    	return redirect('/admin/laporan-pembelian/detail/'.$id);
    }

    public function save(Request $request) {
		$tanggal_masuk       = $request->tanggal_masuk;
		$tanggal_harus_bayar = $request->tanggal_harus_bayar;
        $ppn                 = $request->ppn;
		$id                  = $request->id;

		// INPUT SUPPLIER //
		// $nama_supplier   = $request->nama_supplier;
		// $nomor_hp        = $request->nomor_hp;
		// $alamat_supplier = $request->alamat_supplier;
		// END INPUT SUPPLIER //

		// INPUT OBAT //
		// $nomor_batch      = $request->nomor_batch;
		// $nama_obat        = $request->nama_obat;
		// $jenis_obat       = $request->jenis_obat;
		// $tanggal_expired  = $request->tanggal_expired;
		// $harga_obat       = $request->harga_obat;
		// $stok_input_masuk = $request->stok_input_masuk;
		// $keterangan_input = $request->keterangan_input;
		// $obat             = $request->obat;
		// $stok_masuk       = $request->stok_masuk;
		// $keterangan       = $request->keterangan;
		// END INPUT OBAT //

        $obat             = $request->obat;
        $supplier         = $request->supplier;
        $stok_masuk       = $request->stok_masuk;
        $diskon           = $request->diskon;
        $bentuk_stok      = $request->bentuk_satuan;
        $jenis_masuk      = $request->jenis_masuk;
        $keterangan       = $request->keterangan;
        $total_harga_beli = $request->total_harga_beli;
        $obat_id          = [];

		// PROSES INPUT SUPPLIER //
		// if ($nama_supplier != '' && $nomor_hp != '' && $alamat_supplier != '') {
		// 	$data_supplier = [
		// 		'nama_supplier'   => $nama_supplier,
		// 		'nomor_telepon'   => $nomor_hp,
		// 		'alamat_supplier' => $alamat_supplier
		// 	];
		// 	$supplier = Supplier::insertGetId($data_supplier);
		// }
		// // END PROSES SUPPLIER //

		// INPUT OBAT IF EXISTS INPUT //
		// if (count($nomor_batch) > 0 && count($nama_obat) > 0 && count($jenis_obat) > 0 && count($tanggal_expired) > 0 && count($harga_obat) > 0 && count($stok_input_masuk) > 0) {
		// 	for ($i=0; $i < count($nama_obat); $i++) {
		// 		$data_obat = [
		// 			'nomor_batch'     => $nomor_batch[$i],
		// 			'id_jenis_obat'	  => $jenis_obat[$i],
		// 			'nama_obat'       => $nama_obat[$i],
		// 			'tanggal_expired' => $tanggal_expired[$i],
		// 			'harga_obat'      => $harga_obat[$i],
		// 			'stok_obat'       => 0,
		// 			'ket_data'		  => 'inventory',
		// 		];
		// 		$obat_id[] = Obat::insertGetId($data_obat);
		// 		$ket[]     = $keterangan_input[$i];
		// 		$stok_in[] = $stok_input_masuk[$i];
		// 		$harga[]   = $harga_beli_input[$i];
		// 	}
		// }
		// END INPUT OBAT IF EXISTS //
		// else {
			foreach ($obat as $key => $value) {
				// $lastNumCode = Obat::lastNumCode();
				// $data        = Obat::where('id_obat',$value);

				// if ($data->count() == 0) {

				// 	$get_jenis_obat = JenisObat::where('id_jenis_obat',$data->id_jenis_obat)->firstOrFail();
				// 	$kode_obat      = makeAcronym($get_jenis_obat->nama_jenis_obat);

			 //        $array = [
				// 		'kode_obat'        => generateCode('OMF-'.$kode_obat,'-',$lastNumCode,11),
				// 		'nomor_batch'      => $data->nomor_batch,
				// 		'nama_obat'        => $data->nama_obat,
				// 		'id_jenis_obat'    => $data->id_jenis_obat,
				// 		'id_golongan_obat' => $data->id_golongan_obat,
				// 		'tanggal_expired'  => $data->tanggal_expired,
				// 		'harga_obat'       => $data->harga_obat,
				// 		'harga_jual'       => $data->harga_jual,
				// 		'stok_obat'        => $data->stok_obat,
				// 		'ket_data'         => 'inventory'
			 //        ];

			 //        $obat_id[] = Obat::insertGetId($array);
				// }
				// else {
					$obat_id[] = $value;
				// }
			}
			$ket     = $keterangan;
			$stok_in = $stok_masuk;
			// $harga   = $harga_beli;
		// }

		for ($i=0; $i < count($obat); $i++) {
			if ($jenis_masuk[$i] == 'supplier') {
				if ($bentuk_stok[$i] == 'box') {
					$first_obat = Obat::where('id_obat',$obat_id[$i])->firstOrFail();
					$banyak_bayar = $first_obat->harga_obat * ($first_obat->bobot_satuan * $stok_in[$i]);
				}
				else {
					$banyak_bayar = Obat::where('id_obat',$obat_id[$i])->firstOrFail()->harga_obat * $stok_in[$i];
				}
			}
			else {
				$banyak_bayar = 0;
			}
			$data_pemasukan[] = [
				'tanggal_masuk'       => $tanggal_masuk,
				'tanggal_harus_bayar' => $tanggal_harus_bayar,
				'id_supplier'         => $supplier[$i] == '' ? 1 : $supplier[$i],
				'id_obat'             => $obat_id[$i],
				'stok_masuk'          => $stok_in[$i],
				'bentuk_stok'		  => $bentuk_stok[$i],
				'stok_pakai'      	  => 0,
				'jenis_masuk'         => $jenis_masuk[$i],
				'keterangan'          => $ket[$i],
                'diskon'              => $diskon[$i],
                'ppn'                 => $ppn,
				'banyak_bayar'        => $total_harga_beli[$i],
				'status_beli'         => 0,
				'id_users'            => Auth::id(),
				'created_at'          => date('Y-m-d H:i:s'),
				'updated_at'          => date('Y-m-d H:i:s')
			];
		}

		if ($id == '') {
			Pemasukan::insert($data_pemasukan);
			$message = 'Berhasil Input Data Pemasukan';
		}
		else {
			// for ($i=0; $i < count($data_pemasukan); $i++) { 
			Pemasukan::where('id_masuk',$id)->update($data_pemasukan[0]);
			// }
			$message = 'Berhasil Update Data Pemasukan';
		}
		return redirect('/admin/data-masuk-obat')->with('message',$message);
    }

    public function export(Request $request) {
        ob_end_clean();
        $from        = $request->from;
        $to          = $request->to;
        $title       = 'Laporan Pemasukan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName    = $title.'.xlsx';
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1','Apotek Klinik 88');
        $spreadsheet->getActiveSheet()->setCellValue('A3','Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang');
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');

        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Kode Obat');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Stok Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Diskon');
        $spreadsheet->getActiveSheet()->setCellValue('H7','PPn');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Total Beli');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Jenis Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('K7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('L7','Input By');

        $get   = Pemasukan::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tanggal_masuk));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->kode_obat);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->stok_masuk.' '.ucwords($value->bentuk_stok));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->diskon.' %');
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->ppn.' %');
            $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,format_rupiah($value->banyak_bayar));
            $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,jenis_masuk($value->jenis_masuk));
            $spreadsheet->getActiveSheet()->setCellValue('K'.$cell,$value->keterangan);
            $spreadsheet->getActiveSheet()->setCellValue('L'.$cell,$value->name);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }
}
