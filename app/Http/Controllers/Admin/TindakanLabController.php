<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TindakanLabModel as TindakanLab;
use App\Models\SpesialisDokterModel as SpesialisDokter;

class TindakanLabController extends Controller
{
	public function index() {
		$title = 'Tindakan Lab | Admin';
		$page  = 'laporan-tindakan-lab';
		$link  = 'transaksi';
		return view('Admin.transaksi.tindakan-lab.main',compact('title','page','link'));
	}

	public function tambah() {
        $title            = 'Form Tindakan Lab | Admin';
        $page             = 'tindakan-lab';
        $link             = 'transaksi';
        $spesialis_dokter = SpesialisDokter::where('status_delete',0)->get();
		return view('Admin.transaksi.tindakan-lab.form-tindakan-lab',compact('title','page','link','spesialis_dokter'));
	}

	public function edit($id) {
        $title            = 'Form Tindakan Lab | Admin';
        $page             = 'tindakan-lab';
        $link             = 'transaksi';
        $spesialis_dokter = SpesialisDokter::where('status_delete',0)->get();
        $row              = TindakanLab::where('id_tindakan_lab',$id)->firstOrFail();
		return view('Admin.transaksi.tindakan-lab.form-tindakan-lab',compact('title','page','link','row','spesialis_dokter'));
	}

	public function delete($id) {
		TindakanLab::where('id_tindakan_lab',$id)->update(['status_delete' => 1]);
		return redirect('/admin/laporan-tindakan-lab')->with('message','Berhasil Hapus Tindakan Lab');
	}

	public function save(Request $request) {
        $tindakan_lab     = $request->nama_tindakan;
        $biaya_lab        = $request->biaya_lab;
        $spesialis_dokter = $request->spesialis_dokter;
        $id               = $request->id_tindakan_lab;

		$data_tindakan_lab = [
            'id_spesialis_dokter' => $spesialis_dokter,
            'nama_tindakan'       => $tindakan_lab,
            'biaya_lab'           => $biaya_lab,
            'status_delete'       => 0
		];
		if ($id == '') {
			TindakanLab::create($data_tindakan_lab);
			$message = 'Berhasil Input Tindakan Lab';
		}
		else {
            unset($data_tindakan_lab['status_delete']);
			TindakanLab::where('id_tindakan_lab',$id)->update($data_tindakan_lab);
			$message = 'Berhasil Update Tindakan Lab';
		}
		return redirect('/admin/tindakan-lab')->with('message',$message);
	}
}
