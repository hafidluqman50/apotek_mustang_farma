<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ObatModel as Obat;
use App\Models\TransaksiModel as Transaksi;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\PemasukanModel as Pemasukan;
use App\Models\PengeluaranModel as Pengeluaran;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\ResepModel as Resep;
use App\Models\ProfileInstansiModel as ProfileInstansi;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\TransaksiKasirModel as TransaksiKasir;
use App\Models\PasienModel as Pasien;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class LaporanController extends Controller
{
    public function index()
    {
        $title = 'Laporan Rekap | Admin';
        $page  = 'laporan-rekap';

        return view('Admin.laporan-rekap.main',compact('title','page'));
    }

    public function laporan(Request $request)
    {
        $btn_act = $request->btn_act;

        if ($btn_act == 'laporan-pemasukan') {
            $this->laporanPemasukan($request);
        }
        if ($btn_act == 'laporan-pengeluaran') {
            $this->laporanPengeluaran($request);
        }
        if ($btn_act == 'laporan-total-penjualan') {
            $this->laporanTotalPenjualan($request);
        }
        if ($btn_act == 'laporan-transaksi') {
            $this->laporanTransaksi($request);
        }
        if ($btn_act == 'laporan-obat') {
            $this->laporanObat();
        }
        if ($btn_act == 'laporan-keluhan-pasien') {
            $this->laporanRujukanPasien($request);
        }
        if ($btn_act == 'laporan-rekam-medis') {
            $this->laporanRekamMedis($request);
        }
        if ($btn_act == 'laporan-pasien') {
            $this->laporanPasien($request);
        }
        if ($btn_act == 'laporan-pendapatan-tindakan') {
            $this->laporanPendapatanTindakan($request);
        }
        if ($btn_act == 'laporan-pendapatan-penjualan-obat') {
            $this->laporanPendapatanPenjualanObat($request);
        }
        if ($btn_act == 'laporan-kunjungan') {
            $this->laporanKunjungan($request);
        }
        if ($btn_act == 'laporan-jasa-dokter') {
            $this->laporanJasaDokter($request);
        }
        if ($btn_act == 'laporan-statistik') {
            $this->laporanStatistik($request);
        }
        if ($btn_act == 'laporan-bulanan') {
            $this->laporanBulanan($request);
        }
    }

    public function laporanTransaksi(Request $request)
    {
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan-Transaksi Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:N1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:N5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Transaksi');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Kategori Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Tindakan Lab');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Biaya Klinik');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Biaya Jasa Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Biaya Resep');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Biaya Jasa Lab');
        $spreadsheet->getActiveSheet()->setCellValue('K7','Total Bayar');
        $spreadsheet->getActiveSheet()->setCellValue('L7','Bayar');
        $spreadsheet->getActiveSheet()->setCellValue('M7','Kembali');
        $spreadsheet->getActiveSheet()->setCellValue('N7','Input By');
        $get   = Transaksi::dataExport($from,$to);
        $no    = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$no,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$no,human_date($value->tgl_byr));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$no,$value->nama_pasien);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$no,$value->nama_kategori);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$no,$value->nama_tindakan ?? '-');
            $spreadsheet->getActiveSheet()->setCellValue('F'.$no,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('G'.$no,format_rupiah($value->biaya_klinik));
            $spreadsheet->getActiveSheet()->setCellValue('H'.$no,format_rupiah($value->biaya_dokter));
            $spreadsheet->getActiveSheet()->setCellValue('I'.$no,format_rupiah($value->biaya_resep));
            $spreadsheet->getActiveSheet()->setCellValue('J'.$no,format_rupiah($value->biaya_jasa_lab ?? 0));
            $spreadsheet->getActiveSheet()->setCellValue('K'.$no,format_rupiah($value->jumlah_byr));
            $spreadsheet->getActiveSheet()->setCellValue('L'.$no,format_rupiah($value->bayar));
            $spreadsheet->getActiveSheet()->setCellValue('M'.$no,format_rupiah($value->kembali));
            $spreadsheet->getActiveSheet()->setCellValue('N'.$no,$value->name);
            $count++;
            $no++;
        }
        $style = $no-1;
        $spreadsheet->getActiveSheet()->getStyle('A7:N'.$style)->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ],
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanTotalPenjualan(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Total Penjualan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';
        
        $sheet = [0 => 'Total Penjualan', 1 => 'Umum', 2 => 'BPJS'];

        $spreadsheet = new Spreadsheet();
        for ($i=0; $i < count($sheet); $i++) { 
            $spreadsheet->setActiveSheetIndex($i)->setTitle($sheet[$i]);

            
            $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
            $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
            $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
            $spreadsheet->getActiveSheet()->mergeCells('A1:D1');
            $spreadsheet->getActiveSheet()->mergeCells('A3:D3');
            $spreadsheet->getActiveSheet()->mergeCells('A5:D5');
            $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                'alignment'=>[
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);
            $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
            $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Transaksi');
            $spreadsheet->getActiveSheet()->setCellValue('C7','Input By');
            $spreadsheet->getActiveSheet()->setCellValue('D7','Total Penjualan');
            $get   = Transaksi::totalPenjualanExport($sheet[$i],$from,$to);
            $no    = 8;
            $count = 1;
            foreach ($get as $key => $value) {
                $get = ResepDetail::getTotalByResep($value->id_resep);
                $spreadsheet->getActiveSheet()->setCellValue('A'.$no,"$count");
                $spreadsheet->getActiveSheet()->setCellValue('B'.$no,human_date($value->tgl_byr));
                $spreadsheet->getActiveSheet()->setCellValue('C'.$no,$value->name);
                $spreadsheet->getActiveSheet()->setCellValue('D'.$no,format_rupiah($get));
                $count++;
                $no++;
            }
            $style = $no-1;
            $spreadsheet->getActiveSheet()->getStyle('A7:D'.$style)->applyFromArray([
                'borders'=>[
                    'allBorders'=>[
                        'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                    ]
                ],
                'alignment'=>[
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

            $spreadsheet->createSheet();
        }

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanPemasukan(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Pemasukan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');

        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nomor Batch');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Stok Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Diskon');
        $spreadsheet->getActiveSheet()->setCellValue('H7','PPn');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Total Beli');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Jenis Masuk');
        $spreadsheet->getActiveSheet()->setCellValue('K7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('L7','Input By');

        $get   = Pemasukan::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tanggal_masuk));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nomor_batch);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->stok_masuk.' '.ucwords($value->bentuk_stok));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->diskon.' %');
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->ppn.' %');
            $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,format_rupiah($value->banyak_bayar));
            $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,jenis_masuk($value->jenis_masuk));
            $spreadsheet->getActiveSheet()->setCellValue('K'.$cell,$value->keterangan);
            $spreadsheet->getActiveSheet()->setCellValue('L'.$cell,$value->name);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanPengeluaran(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Pengeluaran Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nomor Batch');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Stok Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Input By');
        $get   = Pengeluaran::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tanggal_keluar));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nomor_batch);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->stok_masuk.' '.ucwords($value->bentuk_stok));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->keterangan);
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->name);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanObat()
    {
        ob_end_clean();
        $title            = 'Laporan-Obat Tanggal '.human_date(now()->format('Y-m-d'));
        $profile_instansi = ProfileInstansi::first();
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Obat Tanggal '.human_date(now()->format('Y-m-d')));
        $spreadsheet->getActiveSheet()->mergeCells('A1:N1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:N5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Nomor Batch');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Nama Golongan Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Tanggal Expire');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Harga Obat');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Harga Jual');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Stok Obat');
        $get   = Obat::getData('inventory');
        $no    = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$no,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$no,$value->nomor_batch);
            $spreadsheet->getActiveSheet()->setCellValue('C'.$no,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$no,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$no,$value->nama_golongan);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$no,human_date($value->tanggal_expired));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$no,format_rupiah($value->harga_obat));
            $spreadsheet->getActiveSheet()->setCellValue('H'.$no,format_rupiah($value->harga_jual));
            $spreadsheet->getActiveSheet()->setCellValue('I'.$no,$value->stok_obat);
            $count++;
            $no++;
        }
        $style = $no-1;
        $spreadsheet->getActiveSheet()->getStyle('A7:N'.$style)->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ],
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');   
    }

    public function laporanRujukanPasien(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Rujukan Pasien Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Rujukan Pasien Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Resep');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Kategori Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Rujukan');
        $get   = Resep::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tgl_resep));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nama_pasien);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_kategori);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->rujukan);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanRekamMedis(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Rekam Medis Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Rujukan Pasien Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Periksa');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Tinggi Badan');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Berat Badan');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Tekanan Darah');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Suhu Badan');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Tindakan');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Keluhan');
        $spreadsheet->getActiveSheet()->setCellValue('K7','Diagnosa');
        $spreadsheet->getActiveSheet()->setCellValue('L7','Anemnesis');
        $spreadsheet->getActiveSheet()->setCellValue('M7','Pemeriksaan Penunjang');
        $spreadsheet->getActiveSheet()->setCellValue('N7','Terapi');
        $spreadsheet->getActiveSheet()->setCellValue('O7','Rujukan');

        $get   = Resep::exportRekamMedis($from,$to);
        $cell  = 8;
        $count = 1;

        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tgl_resep));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_pasien);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->tinggi_badan.' cm');
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->berat_badan.' kg');
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->tekanan_darah.' mmHg');
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->suhu_badan);
            $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,$value->nama_tindakan);
            $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,$value->keluhan_utama_resep);
            $spreadsheet->getActiveSheet()->setCellValue('K'.$cell,$value->diagnosa);
            $spreadsheet->getActiveSheet()->setCellValue('L'.$cell,$value->anemnesis);
            $spreadsheet->getActiveSheet()->setCellValue('M'.$cell,$value->pemeriksaan_penunjang);
            $spreadsheet->getActiveSheet()->setCellValue('N'.$cell,$value->terapi);
            $spreadsheet->getActiveSheet()->setCellValue('O'.$cell,$value->rujukan);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanPasien(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Pasien Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();
        $kategori_pasien = KategoriPasien::all();

        foreach ($kategori_pasien as $key => $value) {
            $spreadsheet->setActiveSheetIndex($key)->setTitle($value->nama_kategori);
            $spreadsheet->getActiveSheet()->setCellValue('F3','Laporan Pelayanan');
            $spreadsheet->getActiveSheet()->setCellValue('F4','Rawat Jalan Tingkat Pertama');
            $spreadsheet->getActiveSheet()->setCellValue('F5','Bulan ……………………..   Tahun………………');
            $spreadsheet->getActiveSheet()->getStyle('F3:F5')->applyFromArray([
                'alignment'=>[
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);
            $spreadsheet->getActiveSheet()->setCellValue('B7','Nama FKTP');
            $spreadsheet->getActiveSheet()->setCellValue('D7',':');
            $spreadsheet->getActiveSheet()->setCellValue('E7',$profile_instansi->nama_instansi);
            $spreadsheet->getActiveSheet()->mergeCells('B7:C7');
            $spreadsheet->getActiveSheet()->setCellValue('B8','Alamat');
            $spreadsheet->getActiveSheet()->setCellValue('D8',':');
            $spreadsheet->getActiveSheet()->setCellValue('E8',$profile_instansi->alamat_instansi);
            $spreadsheet->getActiveSheet()->mergeCells('B8:C8');

            $spreadsheet->getActiveSheet()->setCellValue('B10','No.');
            $spreadsheet->getActiveSheet()->mergeCells('B10:B11');
            $spreadsheet->getActiveSheet()->setCellValue('C10','Tanggal Pelayanan');
            $spreadsheet->getActiveSheet()->mergeCells('C10:C11');
            $spreadsheet->getActiveSheet()->setCellValue('D10','No. Kartu Peserta');
            $spreadsheet->getActiveSheet()->mergeCells('D10:D11');
            $spreadsheet->getActiveSheet()->setCellValue('E10','Nama Peserta');
            $spreadsheet->getActiveSheet()->mergeCells('E10:E11');
            $spreadsheet->getActiveSheet()->setCellValue('F10','No. Telp/Hp');
            $spreadsheet->getActiveSheet()->mergeCells('F10:F11');
            $spreadsheet->getActiveSheet()->setCellValue('G10','Diagnosa');
            $spreadsheet->getActiveSheet()->mergeCells('G10:G11');
            $spreadsheet->getActiveSheet()->setCellValue('H10','Dirujuk');
            $spreadsheet->getActiveSheet()->mergeCells('H10:H11');
            $spreadsheet->getActiveSheet()->getStyle('B10:H11')->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ]
            ]);

            $styleTable = ['borders'=>['allBorders'=>['borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN]]];
            $spreadsheet->getActiveSheet()->getStyle('B10:H11')->applyFromArray($styleTable);

            $resep = Resep::exportPasien($value->id_kategori_pasien,$from,$to);
            $cell  = 12;
            foreach ($resep as $i => $v) {
                $no = $i+1;

                $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$no);
                $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,human_date($v->tgl_resep));
                $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$v->no_bpjs);
                $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$v->nama_pasien);
                $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$v->nomor_telepon_pasien);
                $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$v->diagnosa);
                $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$v->rujukan);
                $styleTable = ['borders'=>['allBorders'=>['borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN]]];
                $spreadsheet->getActiveSheet()->getStyle('B10:H'.$cell)->applyFromArray($styleTable);

                $cell++;
            }

            $pasien_berkunjung      = Pendaftaran::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                                                    ->where('id_kategori_pasien',$value->id_kategori_pasien)
                                                    ->whereBetween('tgl_daftar',[$from,$to])
                                                    ->count();
            $pasien_berkunjung_cell = $cell+2;

            $pasien_tidak_terdaftar_cell = $cell+3;

            $pasien_dirujuk = Resep::join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                    ->where('id_kategori_pasien',$value->id_kategori_pasien)
                                    ->whereNotIn('rujukan',['','-'])
                                    ->whereBetween('tgl_resep',[$from,$to])
                                    ->count();

            $pasien_dirujuk_cell = $cell+4;

            $spreadsheet->getActiveSheet()->setCellValue('B'.$pasien_berkunjung_cell,'Total Peserta Yang Berkunjung');
            $spreadsheet->getActiveSheet()->mergeCells("B$pasien_berkunjung_cell:D$pasien_berkunjung_cell");
            $spreadsheet->getActiveSheet()->setCellValue('E'.$pasien_berkunjung_cell,$pasien_berkunjung);

            $spreadsheet->getActiveSheet()->setCellValue('B'.$pasien_tidak_terdaftar_cell,'Total Peserta Yang Tidak Terdaftar Tapi Berkunjung');
            $spreadsheet->getActiveSheet()->mergeCells("B$pasien_tidak_terdaftar_cell:D$pasien_tidak_terdaftar_cell");
            $spreadsheet->getActiveSheet()->setCellValue('E'.$pasien_tidak_terdaftar_cell,0);

            $spreadsheet->getActiveSheet()->setCellValue('B'.$pasien_dirujuk_cell,'Total Peserta Yang Dirujuk');
            $spreadsheet->getActiveSheet()->mergeCells("B$pasien_dirujuk_cell:D$pasien_dirujuk_cell");
            $spreadsheet->getActiveSheet()->setCellValue('E'.$pasien_dirujuk_cell,$pasien_dirujuk);

            $fasilitas_utama_kesehatan_cell = $pasien_dirujuk_cell+2;
            $ttd_cell                       = $fasilitas_utama_kesehatan_cell+5;

            $spreadsheet->getActiveSheet()->setCellValue('F'.$fasilitas_utama_kesehatan_cell,'Fasilitas Utama Kesehatan');
            $spreadsheet->getActiveSheet()->setCellValue('F'.$ttd_cell,'TTD');
            $spreadsheet->getActiveSheet()->getStyle('F'.$fasilitas_utama_kesehatan_cell.':F'.$ttd_cell)->applyFromArray([
                'alignment'=>[
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

            $spreadsheet->createSheet();

        }

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanPendapatanTindakan(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Pendapatan Tindakan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();

        $explode1 = explode('-', $from);
        $explode2 = explode('-', $to);

        if ($explode1[0] == $explode2[0]) {

            $sheet_index = 0;
            for ($i=$explode1[1]; $i <= $explode2[1]; $i++) { 
                $spreadsheet->setActiveSheetIndex($sheet_index)->setTitle(month(zero_front_number($i)).', '.$explode1[0]);

                $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
                $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
                $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Pendapatan Tindakan Bulan '.month(zero_front_number($i)).', '.$explode1[0]);
                $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
                $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
                $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
                $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                    'alignment'=>[
                        'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ]
                ]);
                $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
                $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Tindakan');
                $spreadsheet->getActiveSheet()->setCellValue('C7','Pendapatan');
                $styleTable = [
                    'borders'=>[
                        'allBorders'=>[
                            'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ]
                    ],
                    'alignment'=>[
                        'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ]
                ];
                $spreadsheet->getActiveSheet()->getStyle('A7:C7')->applyFromArray($styleTable);
                $get   = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                    ->join('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                                    ->whereMonth('tgl_byr',zero_front_number($i))
                                    ->whereYear('tgl_byr',$explode1[0])
                                    ->groupBy('resep.id_tindakan_lab')
                                    ->get();
                $cell  = 8;
                $count = 1;
                foreach ($get as $key => $value) {
                    $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
                    $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$value->nama_tindakan);
                    $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,Transaksi::getSumPerBulan(zero_front_number($i),$explode1[0],$value->id_tindakan_lab));
                    $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

                    $spreadsheet->getActiveSheet()->getStyle('A8:C'.$cell)->applyFromArray($styleTable);
                    $count++;
                    $cell++;
                }

                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

                $sheet_index++;
                $spreadsheet->createSheet();
            }

            $spreadsheet->setActiveSheetIndex(0);
        }
        elseif ($explode1[0] != $explode2[0]) {
            $arr_tahun = [$explode1[0],$explode2[0]];

            $sheet_index = 0;

            $bulan_range[0]['bulan_awal']  = $explode1[1];
            $bulan_range[0]['bulan_akhir'] = 12;
            $bulan_range[1]['bulan_awal']  = 1;
            $bulan_range[1]['bulan_akhir'] = $explode2[1];

            for ($i=0; $i < count($arr_tahun); $i++) { 
                for ($j=$bulan_range[$i]['bulan_awal']; $j <= $bulan_range[$i]['bulan_akhir']; $j++) {
                    $spreadsheet->setActiveSheetIndex($sheet_index)->setTitle(month(zero_front_number($j)).', '.$arr_tahun[$i]);

                    $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
                    $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
                    $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Pendapatan Tindakan Bulan '.month(zero_front_number($j)).', '.$arr_tahun[$i]);
                    $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
                    $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
                    $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
                    $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                        'alignment'=>[
                            'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                        ]
                    ]);
                    $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
                    $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Tindakan');
                    $spreadsheet->getActiveSheet()->setCellValue('C7','Pendapatan');
                    $styleTable = [
                        'borders'=>[
                            'allBorders'=>[
                                'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                            ]
                        ],
                        'alignment'=>[
                            'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                        ]
                    ];
                    $spreadsheet->getActiveSheet()->getStyle('A7:C7')->applyFromArray($styleTable);
                    $get   = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                                        ->whereMonth('tgl_byr',zero_front_number($j))
                                        ->whereYear('tgl_byr',$arr_tahun[$i])
                                        ->groupBy('resep.id_tindakan_lab')
                                        ->get();
                    $cell  = 8;
                    $count = 1;
                    foreach ($get as $key => $value) {
                        $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
                        $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$value->nama_tindakan);
                        $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,Transaksi::getSumPerBulan(zero_front_number($j),$arr_tahun[$i],$value->id_tindakan_lab));
                        $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

                        $spreadsheet->getActiveSheet()->getStyle('A8:C'.$cell)->applyFromArray($styleTable);
                        $count++;
                        $cell++;
                    }

                    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

                    $sheet_index++;
                    $spreadsheet->createSheet();
                }
            }

            $spreadsheet->setActiveSheetIndex(0);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanPendapatanPenjualanObat(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Pendapatan Penjualan Obat  Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();

        $explode1 = explode('-', $from);
        $explode2 = explode('-', $to);

        if ($explode1[0] == $explode2[0]) {

            $sheet_index = 0;
            for ($i=$explode1[1]; $i <= $explode2[1]; $i++) { 
                $spreadsheet->setActiveSheetIndex($sheet_index)->setTitle(month(zero_front_number($i)).', '.$explode1[0]);

                $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
                $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
                $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Pendapatan Penjualan Obat Bulan '.month(zero_front_number($i)).', '.$explode1[0]);
                $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
                $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
                $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
                $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                    'alignment'=>[
                        'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ]
                ]);
                $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
                $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Obat');
                $spreadsheet->getActiveSheet()->setCellValue('C7','Pendapatan');
                $styleTable = [
                    'borders'=>[
                        'allBorders'=>[
                            'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                        ]
                    ],
                    'alignment'=>[
                        'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                    ]
                ];
                $spreadsheet->getActiveSheet()->getStyle('A7:C7')->applyFromArray($styleTable);
                $get   = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                    ->join('detail_resep','resep.id_resep','=','detail_resep.id_resep')
                                    ->join('obat','detail_resep.id_obat','=','obat.id_obat')
                                    ->whereMonth('tgl_byr',zero_front_number($i))
                                    ->whereYear('tgl_byr',$explode1[0])
                                    ->groupBy('detail_resep.id_obat')
                                    ->get();
                $cell  = 8;
                $count = 1;
                foreach ($get as $key => $value) {
                    $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
                    $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$value->nama_obat);
                    $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,Transaksi::getObatSumPerBulan(zero_front_number($i),$explode1[0],$value->id_obat));
                    $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

                    $spreadsheet->getActiveSheet()->getStyle('A8:C'.$cell)->applyFromArray($styleTable);
                    $count++;
                    $cell++;
                }

                $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

                $sheet_index++;
                $spreadsheet->createSheet();
            }

            $spreadsheet->setActiveSheetIndex(0);
        }
        elseif ($explode1[0] != $explode2[0]) {
            $arr_tahun = [$explode1[0],$explode2[0]];

            $sheet_index = 0;

            $bulan_range[0]['bulan_awal']  = $explode1[1];
            $bulan_range[0]['bulan_akhir'] = 12;
            $bulan_range[1]['bulan_awal']  = 1;
            $bulan_range[1]['bulan_akhir'] = $explode2[1];

            for ($i=0; $i < count($arr_tahun); $i++) { 
                for ($j=$bulan_range[$i]['bulan_awal']; $j <= $bulan_range[$i]['bulan_akhir']; $j++) {
                    $spreadsheet->setActiveSheetIndex($sheet_index)->setTitle(month(zero_front_number($j)).', '.$arr_tahun[$i]);

                    $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
                    $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
                    $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Pendapatan Penjualan Obat Bulan '.month(zero_front_number($j)).', '.$arr_tahun[$i]);
                    $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
                    $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
                    $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
                    $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                        'alignment'=>[
                            'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                        ]
                    ]);
                    $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
                    $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Obat');
                    $spreadsheet->getActiveSheet()->setCellValue('C7','Pendapatan');
                    $styleTable = [
                        'borders'=>[
                            'allBorders'=>[
                                'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                            ]
                        ],
                        'alignment'=>[
                            'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                        ]
                    ];
                    $spreadsheet->getActiveSheet()->getStyle('A7:C7')->applyFromArray($styleTable);
                    $get   = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('detail_resep','resep.id_resep','=','detail_resep.id_resep')
                                        ->join('obat','detail_resep.id_obat','=','obat.id_obat')
                                        ->whereMonth('tgl_byr',zero_front_number($j))
                                        ->whereYear('tgl_byr',$arr_tahun[$i])
                                        ->groupBy('detail_resep.id_obat')
                                        ->get();
                    $cell  = 8;
                    $count = 1;
                    foreach ($get as $key => $value) {
                        $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
                        $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$value->nama_obat);
                        $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,Transaksi::getObatSumPerBulan(zero_front_number($j),$arr_tahun[$i],$value->id_obat));
                        $spreadsheet->getActiveSheet()->getStyle('C'.$cell)->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

                        $spreadsheet->getActiveSheet()->getStyle('A8:C'.$cell)->applyFromArray($styleTable);
                        $count++;
                        $cell++;
                    }

                    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

                    $sheet_index++;
                    $spreadsheet->createSheet();
                }
            }

            $spreadsheet->setActiveSheetIndex(0);
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanKunjungan(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Kunjungan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();

        $dokter_groupby_resep = Resep::join('dokter','resep.id_dokter','=','dokter.id_dokter')
                                    ->whereBetween('tgl_resep',[$from,$to])
                                    ->groupBy('resep.id_dokter')
                                    ->get();

        foreach ($dokter_groupby_resep as $key => $value) {
            $spreadsheet->setActiveSheetIndex($key)->setTitle(str_limit($value->nama_dokter,20));

            $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
            $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
            $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Kunjungan '.$value->nama_dokter.' Dari '.human_date($from).' Sampai '.human_date($to));
            $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
            $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
            $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
            $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
                'alignment'=>[
                    'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);

            $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
            $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Periksa');
            $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Pasien');
            $spreadsheet->getActiveSheet()->setCellValue('D7','Tinggi Badan');
            $spreadsheet->getActiveSheet()->setCellValue('E7','Berat Badan');
            $spreadsheet->getActiveSheet()->setCellValue('F7','Tekanan Darah');
            $spreadsheet->getActiveSheet()->setCellValue('G7','Suhu Badan');
            $spreadsheet->getActiveSheet()->setCellValue('H7','Tindakan');
            $spreadsheet->getActiveSheet()->setCellValue('I7','Keluhan');
            $spreadsheet->getActiveSheet()->setCellValue('J7','Diagnosa');
            $spreadsheet->getActiveSheet()->setCellValue('K7','Anemnesis');
            $spreadsheet->getActiveSheet()->setCellValue('L7','Pemeriksaan Penunjang');
            $spreadsheet->getActiveSheet()->setCellValue('M7','Terapi');
            $spreadsheet->getActiveSheet()->setCellValue('N7','Rujukan');

            $get   = Resep::exportRekamMedisByDokter($value->id_dokter,$from,$to);
            $cell  = 8;
            $count = 1;

            foreach ($get as $i => $v) {
                $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
                $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($v->tgl_resep));
                $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$v->nama_pasien.' | '.$v->nama_kategori);
                $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$v->tinggi_badan.' cm');
                $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$v->berat_badan.' kg');
                $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$v->tekanan_darah.' mmHg');
                $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$v->suhu_badan);
                $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$v->nama_tindakan);
                $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,$v->keluhan_utama_resep);
                $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,$v->diagnosa);
                $spreadsheet->getActiveSheet()->setCellValue('K'.$cell,$v->anemnesis);
                $spreadsheet->getActiveSheet()->setCellValue('L'.$cell,$v->pemeriksaan_penunjang);
                $spreadsheet->getActiveSheet()->setCellValue('M'.$cell,$v->terapi);
                $spreadsheet->getActiveSheet()->setCellValue('N'.$cell,$v->rujukan);
                $count++;
                $cell++;
            }

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

            $spreadsheet->createSheet();
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanJasaDokter(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Jasa Dokter Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Jasa Dokter Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Biaya Jasa');

        $get   = Transaksi::exportJasaDokter($from,$to);
        $cell  = 8;
        $count = 1;

        foreach ($get as $key => $value) {
            $sum_biaya_jasa = Transaksi::sumBiayaJasaDokter($value->id_dokter,$from,$to);
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$sum_biaya_jasa);
            $count++;
            $cell++;
        }
        $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,'Grand Total');
        $spreadsheet->getActiveSheet()->mergeCells("A$cell:B$cell");
        $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,"=SUM(C8:C$cell)");

        $spreadsheet->getActiveSheet()->getStyle("C8:C$cell")->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanStatistik(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Statistik Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';

        $spreadsheet     = new Spreadsheet();

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(0)->setTitle('Statistik');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Jasa Dokter Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Bulan');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Jumlah Rekam Medis');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Total Kunjungan Harian');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Total Rerata Kunjungan Harian');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Jumlah Pasien Baru');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Jumlah Pasien Berulang');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Jumlah Kunjungan By Shift');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Total Pelayanan Non Poliklinik');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Total Pelayanan Gigi');

        $rekam_medis_per_bulan = Resep::whereBetween('tgl_resep',[$from,$to])->selectRaw('*,MONTH(tgl_resep) as bulan_resep, YEAR(tgl_resep) as tahun_resep')->groupBy('bulan_resep','tahun_resep')->get();

        $cell = 8;

        foreach ($rekam_medis_per_bulan as $key => $value) {
            $jumlah_rekam_medis = Resep::whereMonth('tgl_resep',$value->bulan_resep)
                                        ->whereYear('tgl_resep',$value->tahun_resep)
                                        ->count();

            $jumlah_kunjungan_harian = Pendaftaran::whereMonth('tgl_daftar',$value->bulan_resep)
                                                    ->whereYear('tgl_daftar',$value->tahun_resep)
                                                    ->count();

            $get_jumlah_pasien = Pendaftaran::whereMonth('tgl_daftar',$value->bulan_resep)
                                            ->whereYear('tgl_daftar',$value->tahun_resep)
                                            ->groupBy('id_pasien')
                                            ->count();

            $jumlah_pasien_baru = Resep::whereMonth('tgl_resep',$value->bulan_resep)
                                        ->whereYear('tgl_resep',$value->tahun_resep)
                                        ->select(['*',\DB::raw('COUNT(id_pasien) as hitung')])
                                        ->groupBy('id_pasien')
                                        ->having('hitung','=',1)
                                        ->get();

            $jumlah_pasien_berulang = Resep::whereMonth('tgl_resep',$value->bulan_resep)
                                            ->whereYear('tgl_resep',$value->tahun_resep)
                                            ->select(['*',\DB::raw('COUNT(id_pasien) as hitung')])
                                            ->groupBy('id_pasien')
                                            ->having('hitung','>',1)
                                            ->get();

            $shift_pagi = Pendaftaran::select(['*',\DB::raw('TIME(created_at) as waktu')])
                                    ->whereMonth('tgl_daftar',$value->bulan_resep)
                                    ->whereYear('tgl_daftar',$value->tahun_resep)
                                    ->having('waktu','>=','09:00')
                                    ->having('waktu','<=','15:00')
                                    ->groupByRaw('waktu')
                                    ->get();

            $shift_malam = Pendaftaran::select(['*',\DB::raw('TIME(created_at) as waktu')])
                                    ->whereMonth('tgl_daftar',$value->bulan_resep)
                                    ->whereYear('tgl_daftar',$value->tahun_resep)
                                    ->having('waktu','>=','15:01')
                                    ->having('waktu','<=','21:00')
                                    ->groupByRaw('waktu')
                                    ->get();

            $pelayanan_non_klinik = \DB::table('transaksi_kasir')->whereMonth('tanggal_transaksi',$value->bulan_resep)
                                                ->whereYear('tanggal_transaksi',$value->tahun_resep)
                                                ->count();

            $total_pelayanan_gigi = Resep::join('dokter','resep.id_dokter','=','dokter.id_dokter')
                                            ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                            ->whereMonth('tgl_resep',$value->bulan_resep)
                                            ->whereYear('tgl_resep',$value->tahun_resep)
                                            ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                            ->groupBy('resep.id_dokter')
                                            ->count();

            if ($jumlah_kunjungan_harian == 0) { $jumlah_rerata_kunjungan_harian = 0 ;}
            else { $jumlah_rerata_kunjungan_harian = $jumlah_kunjungan_harian / $get_jumlah_pasien; }

            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,$key+1);
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,month(zero_front_number($value->bulan_resep)));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$jumlah_rekam_medis);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$jumlah_kunjungan_harian);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$jumlah_rerata_kunjungan_harian);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,count($jumlah_pasien_baru));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,count($jumlah_pasien_berulang));
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,'Pagi :'.count($shift_pagi).', Malam : '.count($shift_malam));
            $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,$pelayanan_non_klinik);
            $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,$total_pelayanan_gigi);
            $cell = $cell+1;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);

        $spreadsheet->setActiveSheetIndex(1)->setTitle('Top 10 Penyakit');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Jasa Dokter Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Nama Penyakit');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Jumlah Diagnosa');

        $data_penyakit = Resep::selectRaw('*,COUNT(diagnosa) as hitung')
                                ->orderBy('hitung','DESC')
                                ->limit(10)
                                ->groupBy('diagnosa')
                                ->get();

        $cell = 8;
        foreach ($data_penyakit as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,$key+1);
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,ucwords($value->diagnosa));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,Resep::countDiagnosa(strtolower($value->diagnosa)));
            $cell = $cell+1;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }

    public function laporanBulanan(Request $request)
    {
        ob_end_clean();
        $from             = reverse_date($request->from);
        $to               = reverse_date($request->to);
        $profile_instansi = ProfileInstansi::first();
        $title            = 'Laporan Bulanan Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName         = $title.'.xlsx';
        $bulan_tahun      = explode('-',$from);
        $date1            = Carbon::parse($from);
        $date2            = Carbon::parse($to);
        $date_excel       = date_format(date_create($from),"M-y");
        

        $spreadsheet     = new Spreadsheet();

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(0)->setTitle('ANALISIS');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Bulanan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('B7',month($bulan_tahun[1]).' ('.$bulan_tahun[0].')');

        $spreadsheet->getActiveSheet()->setCellValue('A8','Rekam Medis');
        $rekam_medis_count = Pasien::orderBy('id_pasien','DESC')->firstOrFail();
        $spreadsheet->getActiveSheet()->setCellValue('B8',$rekam_medis_count->id_pasien);

        $spreadsheet->getActiveSheet()->setCellValue('A9','Total Kunjungan');
        $total_kunjungan = Pendaftaran::whereMonth('tgl_daftar',$bulan_tahun[1])->count();
        $spreadsheet->getActiveSheet()->setCellValue('B9',$total_kunjungan);

        $spreadsheet->getActiveSheet()->setCellValue('A10','Rerata Kunjungan / Hari');
        if ($total_kunjungan == 0) {
            $rerata_kunjungan_harian = 0;
        }
        else {
            $rerata_kunjungan_harian = $date1->diffInDays($to) / $total_kunjungan;
        }

        $spreadsheet->getActiveSheet()->setCellValue('B10',$rerata_kunjungan_harian);

        $spreadsheet->getActiveSheet()->setCellValue('A11','Shift I (09.00 - 15.00)');
        $shift_pagi = Pendaftaran::select(['*',\DB::raw('TIME(created_at) as waktu')])
                                ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                ->whereYear('tgl_daftar',$bulan_tahun[0])
                                ->having('waktu','>=','09:00')
                                ->having('waktu','<=','15:00')
                                ->groupByRaw('waktu')
                                ->get();
        $spreadsheet->getActiveSheet()->setCellValue('B11',count($shift_pagi));

        $spreadsheet->getActiveSheet()->setCellValue('A12','Shift II (15.00 - 21.00');
        $shift_malam = Pendaftaran::select(['*',\DB::raw('TIME(created_at) as waktu')])
                                ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                ->whereYear('tgl_daftar',$bulan_tahun[0])
                                ->having('waktu','>=','15:01')
                                ->having('waktu','<=','21:00')
                                ->groupByRaw('waktu')
                                ->get();
        $spreadsheet->getActiveSheet()->setCellValue('B12',count($shift_malam));

        $spreadsheet->getActiveSheet()->setCellValue('A14','Total Kunjungan Berdasarkan Repetisi');
        $spreadsheet->getActiveSheet()->setCellValue('A15','Baru');
        $spreadsheet->getActiveSheet()->setCellValue('B15',0);
        $spreadsheet->getActiveSheet()->setCellValue('A16','Berulang');
        $spreadsheet->getActiveSheet()->setCellValue('B16',0);
        $spreadsheet->getActiveSheet()->setCellValue('A17','Total Pelayanan Poliklinik');
        $total_pelayanan_poliklinik = Transaksi::whereMonth('tgl_byr',$bulan_tahun[1])
                                            ->whereYear('tgl_byr',$bulan_tahun[0])
                                            ->count();
        $spreadsheet->getActiveSheet()->setCellValue('B17',$total_pelayanan_poliklinik);

        $spreadsheet->getActiveSheet()->setCellValue('A18','Total Pelayanan Gigi');
        $total_pelayanan_gigi = Resep::join('dokter','resep.id_dokter','=','dokter.id_dokter')
                                        ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                        ->whereMonth('tgl_resep',$bulan_tahun[1])
                                        ->whereYear('tgl_resep',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                        ->groupBy('resep.id_dokter')
                                        ->count();
        $spreadsheet->getActiveSheet()->setCellValue('B18',$total_pelayanan_gigi);

        $spreadsheet->getActiveSheet()->setCellValue('A19','Total Pelayanan Obat');
        $total_pelayanan_obat = \DB::table('transaksi_kasir')->whereMonth('tanggal_transaksi',$bulan_tahun[1])
                                            ->whereYear('tanggal_transaksi',$bulan_tahun[0])
                                            ->count();
        $spreadsheet->getActiveSheet()->setCellValue('B19',$total_pelayanan_obat);

        $spreadsheet->getActiveSheet()->setCellValue('A20','Rerata Kunjungan Pelayanan Gigi/Hari');
        if ($total_pelayanan_gigi == 0) {
            $rerata_kunjungan_harian_gigi = 0;
        }
        else {
            $rerata_kunjungan_harian_gigi = $date1->diffInDays($to) / $total_pelayanan_gigi;
        }

        $spreadsheet->getActiveSheet()->setCellValue('B20',$rerata_kunjungan_harian_gigi);

        $spreadsheet->getActiveSheet()->setCellValue('A21','Kunjungan pelayanan gigi (Umum)');
        $total_kunjungan_pelayanan_gigi_umum = Pendaftaran::join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                                        ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                        ->join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                        ->whereYear('tgl_daftar',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                        ->whereRaw("LOWER(nama_kategori) = 'umum'")
                                        ->groupBy('pendaftaran.id_pasien')
                                        ->count();
        $spreadsheet->getActiveSheet()->setCellValue('B21',$total_kunjungan_pelayanan_gigi_umum);

        $spreadsheet->getActiveSheet()->setCellValue('A22','Kunjungan pelayanan gigi (Asuransi)');
        $total_kunjungan_pelayanan_gigi_asuransi = Pendaftaran::join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                                        ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                        ->join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                        ->whereYear('tgl_daftar',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                        ->whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->groupBy('pendaftaran.id_pasien')
                                        ->count();
        $spreadsheet->getActiveSheet()->setCellValue('B22',$total_kunjungan_pelayanan_gigi_asuransi);

        $spreadsheet->getActiveSheet()->getStyle('A7:B22')->applyFromArray(['borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);

        $spreadsheet->setActiveSheetIndex(1)->setTitle('Poliklinik');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Bulanan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:J14')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Bulan');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Jumlah Pasien Umum (Poliklinik)');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Jumlah Pasien Asuransi (Poliklinik)');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Total Kunjungan Poliklinik');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Total Pembayaran Pasien Umum Poliklinik');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Total Pembayaran Pasien Asuransi Poliklinik');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Total Pemasukan Pasien Poliklinik');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Rerata Pembayaran / Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Rerata Pembayaran / Pasien Asuransi');
        $spreadsheet->getActiveSheet()->getStyle('A7:J7')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('A8','1');
        $spreadsheet->getActiveSheet()->setCellValue('B8',$date_excel);
        $spreadsheet->getActiveSheet()->setCellValue('C8','=E11');
        $spreadsheet->getActiveSheet()->setCellValue('D8','=J11');
        $spreadsheet->getActiveSheet()->setCellValue('E8','=SUM(C2+D2)');
        $spreadsheet->getActiveSheet()->setCellValue('F8','=E14');
        $spreadsheet->getActiveSheet()->setCellValue('G8','=J14');
        $spreadsheet->getActiveSheet()->setCellValue('H8','=F2+G2');
        $spreadsheet->getActiveSheet()->setCellValue('I8','=F2/C2');
        $spreadsheet->getActiveSheet()->setCellValue('J8','=G2/D2');

        $spreadsheet->getActiveSheet()->getStyle('A7:J8')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);
        $spreadsheet->getActiveSheet()->getStyle('F8:J8')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->setCellValue('B10','');
        $spreadsheet->getActiveSheet()->setCellValue('C10','Total Kunjungan Umum');
        $spreadsheet->getActiveSheet()->setCellValue('D10','Total Kunjungan Pasien Umum Gigi');
        $spreadsheet->getActiveSheet()->setCellValue('E10','Jumlah Pasien Umum (Poliklinik)');
        $spreadsheet->getActiveSheet()->getStyle('B10:E10')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('B11',$date_excel);
        $total_kunjungan_pasien_umum = Pendaftaran::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                        ->whereYear('tgl_daftar',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'umum'")
                                        ->groupBy('pendaftaran.id_pasien')
                                        ->count();
        $spreadsheet->getActiveSheet()->setCellValue('C11',$total_kunjungan_pasien_umum);
        $spreadsheet->getActiveSheet()->setCellValue('D11',$total_kunjungan_pelayanan_gigi_umum);
        $spreadsheet->getActiveSheet()->setCellValue('E11','=C11-D11');
        $spreadsheet->getActiveSheet()->getStyle('B10:E11')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('B13','');
        $spreadsheet->getActiveSheet()->setCellValue('C13','Total Pembayaran Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('D13','Total Pembayaran Pasien Umum Gigi');
        $spreadsheet->getActiveSheet()->setCellValue('E13','Total Pembayaran Pasien Umum Poliklinik');
        $spreadsheet->getActiveSheet()->getStyle('B13:E13')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('B14',$date_excel);
        $total_pembayaran_pasien_umum = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'umum'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('C14',$total_pembayaran_pasien_umum);
        $total_pembayaran_pasien_umum_gigi = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->join('dokter','resep.id_dokter','=','dokter.id_dokter')
                                        ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'umum'")
                                        ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('D14',$total_pembayaran_pasien_umum_gigi);
        $spreadsheet->getActiveSheet()->setCellValue('E14','=C14-D14');
        $spreadsheet->getActiveSheet()->getStyle('B13:E14')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);
        $spreadsheet->getActiveSheet()->getStyle('C14:E14')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->setCellValue('G10','');
        $spreadsheet->getActiveSheet()->setCellValue('H10','Total Kunjungan Pasien Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('I10','Total Kunjungan Pasien Asuransi Gigi');
        $spreadsheet->getActiveSheet()->setCellValue('J10','Jumlah Pasien Asuransi (Poliklinik)');
        $spreadsheet->getActiveSheet()->getStyle('G10:J10')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('G11',$date_excel);
        $total_kunjungan_pasien_asuransi = Pendaftaran::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_daftar',$bulan_tahun[1])
                                        ->whereYear('tgl_daftar',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->groupBy('pendaftaran.id_pasien')
                                        ->count();
        $spreadsheet->getActiveSheet()->setCellValue('H11',$total_kunjungan_pasien_asuransi);
        $spreadsheet->getActiveSheet()->setCellValue('I11',$total_kunjungan_pelayanan_gigi_asuransi);
        $spreadsheet->getActiveSheet()->setCellValue('J11','=H11-I11');
        $spreadsheet->getActiveSheet()->getStyle('G10:J11')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('G13','');
        $spreadsheet->getActiveSheet()->setCellValue('H13','Total Pembayaran Pasien Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('I13','Total Pembayaran Pasien Asuransi Gigi');
        $spreadsheet->getActiveSheet()->setCellValue('J13','Total Pembayaran Pasien Asuransi Poliklinik');
        $spreadsheet->getActiveSheet()->getStyle('H13:J13')->getAlignment()->setWrapText(true);

        $spreadsheet->getActiveSheet()->setCellValue('G14',$date_excel);
        $total_pembayaran_pasien_bpjs = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('H14',$total_pembayaran_pasien_bpjs);
        $total_pembayaran_pasien_bpjs_gigi = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->join('dokter','resep.id_dokter','=','dokter.id_dokter')
                                        ->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->whereRaw("LOWER(nama_spesialis_dokter) = 'gigi'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('I14',$total_pembayaran_pasien_bpjs_gigi);
        $spreadsheet->getActiveSheet()->setCellValue('J14','=H14-I14');
        $spreadsheet->getActiveSheet()->getStyle('G13:J14')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);
        $spreadsheet->getActiveSheet()->getStyle('H14:J14')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(15);

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(2)->setTitle('Pelayanan Gigi');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Bulanan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:J12')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Bulan');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Total Kunjungan Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Total Kunjungan Pasien Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Total Jumlah Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Total Pembayaran Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Total Pembayaran Pasien Gigi Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Total Pemasukan Pasien Gigi');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Rerata Pembayaran / Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Rerata Pembayaran / Pasien Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('A8','1');
        $spreadsheet->getActiveSheet()->setCellValue('B8',$date_excel);
        $spreadsheet->getActiveSheet()->setCellValue('C8','=Poliklinik!D11');
        $spreadsheet->getActiveSheet()->setCellValue('D8','=Poliklinik!I11');
        $spreadsheet->getActiveSheet()->setCellValue('E8','=C8+D8');
        $spreadsheet->getActiveSheet()->setCellValue('F8','=Poliklinik!D14');
        $spreadsheet->getActiveSheet()->setCellValue('G8','=Poliklinik!J14');
        $spreadsheet->getActiveSheet()->setCellValue('H8','=SUM(F2:G2)');
        $spreadsheet->getActiveSheet()->setCellValue('I8','=F2/C2');
        $spreadsheet->getActiveSheet()->setCellValue('J8','=G2/D2');
        $spreadsheet->getActiveSheet()->getStyle('A7:J7')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('F8:J8')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->getStyle('A7:J8')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('B10','BULAN');
        $spreadsheet->getActiveSheet()->setCellValue('C10','UMUM');
        $spreadsheet->getActiveSheet()->setCellValue('D10','ASURANSI');
        $spreadsheet->getActiveSheet()->setCellValue('E10','Total');

        $spreadsheet->getActiveSheet()->setCellValue('B11',$date_excel);
        $spreadsheet->getActiveSheet()->setCellValue('C11','=F2');
        $spreadsheet->getActiveSheet()->setCellValue('D11','=G2');
        $spreadsheet->getActiveSheet()->setCellValue('E11','=C11+D11');

        $spreadsheet->getActiveSheet()->getStyle('B10:E11')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');

        $spreadsheet->getActiveSheet()->getStyle('B10:E11')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(15);

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(3)->setTitle('Jumlah Kunjungan Asuransi');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Bulanan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:D8')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Total Kunjungan Pasien Asuransi');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Total Kunjungan Pasien Umum');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Total Kunjungan Pasien');

        $spreadsheet->getActiveSheet()->setCellValue('A8',$date_excel);
        $spreadsheet->getActiveSheet()->setCellValue('B8',$total_kunjungan_pasien_asuransi);
        $spreadsheet->getActiveSheet()->setCellValue('C8',$total_kunjungan_pasien_umum);
        $spreadsheet->getActiveSheet()->setCellValue('D8','=SUM(B8:C8)');
        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(4)->setTitle('Laporan Omzet');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Bulanan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:D8')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','Bulan');
        $spreadsheet->getActiveSheet()->setCellValue('A8',$date_excel);
        $spreadsheet->getActiveSheet()->setCellValue('B7','UMUM');
        $omzet_umum = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'umum'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('B8',$omzet_umum);
        $spreadsheet->getActiveSheet()->setCellValue('C7','ASURANSI');
        $omzet_asuransi = Transaksi::join('resep','transaksi.id_resep','=','resep.id_resep')
                                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                        ->whereMonth('tgl_byr',$bulan_tahun[1])
                                        ->whereYear('tgl_byr',$bulan_tahun[0])
                                        ->whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->sum('jumlah_byr');
        $spreadsheet->getActiveSheet()->setCellValue('C8',$omzet_asuransi);
        $spreadsheet->getActiveSheet()->setCellValue('D7','TOTAL OMZET');
        $spreadsheet->getActiveSheet()->setCellValue('D8','=SUM(B8:C8)');
        $spreadsheet->getActiveSheet()->getStyle('A8:D8')->getNumberFormat()->setFormatCode('"Rp "#,##0.00_-');
        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getStyle('A7:D8')->applyFromArray([
            'borders'=>[
                'allBorders'=>[
                    'borderStyle'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->createSheet();

        $spreadsheet->setActiveSheetIndex(5)->setTitle('PEMBELIAN');

        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nomor Batch');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Stok Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Input By');
        $get   = Pengeluaran::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tanggal_keluar));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nomor_batch);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->stok_masuk.' '.ucwords($value->bentuk_stok));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->keterangan);
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->name);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }
}