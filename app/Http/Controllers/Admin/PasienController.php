<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\ProfileInstansiModel as ProfileInstansi;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PasienController extends Controller
{
    public function index() {
        $title = 'Data Pasien';
        $page  = 'laporan-pasien';
        $link  = 'pasien';
        return view('Admin.pasien.data-pasien.main',compact('title','page','link'));
    }

    public function tambah() {
        $title           = 'Form Data Pasien';
        $page            = 'data-pasien';
        $link            = 'pasien';
        $kategori_pasien = KategoriPasien::all();
        return view('Admin.pasien.data-pasien.form-pasien',compact('title','page','link','kategori_pasien'));
    }

    public function edit($id) {
        $title           = 'Form Data Pasien';
        $page            = 'data-pasien';
        $link            = 'pasien';
        $row             = Pasien::where('id_pasien',$id)->firstOrFail();
        $kategori_pasien = KategoriPasien::all();
        return view('Admin.pasien.data-pasien.form-pasien',compact('title','page','row','link','kategori_pasien'));
    }

    public function delete($id) {
        Pasien::where('id_pasien',$id)->update(['status_delete' => 1]);
        return redirect('/admin/laporan-pasien')->with('message','Berhasil Hapus Pasien');
    }

    public function save(Request $request) {
        $no_bpjs              = $request->no_bpjs != '' ? $request->no_bpjs : '-';
        $nama_pasien          = $request->nama_pasien;
        $id_kategori_pasien   = $request->id_kategori_pasien;
        $nomor_telepon_pasien = $request->nomor_telepon_pasien;
        $tanggal_lahir        = $request->tanggal_lahir;
        $jenis_kelamin        = $request->jenis_kelamin;
        $alamat_pasien        = $request->alamat_pasien;
        $id                   = $request->id;
        // $kode_pasien       = generateCode('PSN-AMF','-',Pasien::lastNumCode(),11);
        $kode_pasien          = $request->kode_pasien;

        $data_pasien = [
            'kode_pasien'          => $kode_pasien,
            'no_bpjs'              => $no_bpjs,
            'nama_pasien'          => $nama_pasien,
            'id_kategori_pasien'   => $id_kategori_pasien,
            'nomor_telepon_pasien' => $nomor_telepon_pasien,
            'tanggal_lahir'        => $tanggal_lahir,
            'jenis_kelamin'        => $jenis_kelamin,
            'alamat_pasien'        => $alamat_pasien,
            'status_delete'        => 0
        ];

        if ($id == '') {
            Pasien::create($data_pasien);
            $message = 'Berhasil Input Data Pasien';            
        }
        else {
            unset($data_pasien['status_delete']);
            Pasien::where('id_pasien',$id)->update($data_pasien);
            $message = 'Berhasil Update Data Pasien';
        }
        return redirect('/admin/data-pasien')->with('message',$message);
    }

    public function pasienBpjs(ApiController $apiController)
    {
        $title = 'Pasien BPJS';
        $page  = 'laporan-pasien';
        $link  = 'pasien';
        $data  = $apiController->index();

        return view('Admin.pasien.data-pasien.main-bpjs',compact('title','page','link','data'));
    }

    public function cetakRekamMedis($id)
    {

        ob_end_clean();
        $profile_instansi = ProfileInstansi::first();
        $pasien_row       = Pasien::with(['kategori'])->where('id_pasien',$id)->firstOrFail();
        $title            = 'Laporan Rekam Medis Pasien '.$pasien_row->nama_pasien;
        $fileName         = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        
        $spreadsheet->getActiveSheet()->setCellValue('A1',$profile_instansi->nama_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A3',$profile_instansi->alamat_instansi);
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Rujukan Pasien '.$pasien_row->nama_pasien);
        $spreadsheet->getActiveSheet()->mergeCells('A1:K1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:K3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:K5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->setCellValue('A7','Nama Pasien :'.$pasien_row->nama_pasien);
        $spreadsheet->getActiveSheet()->setCellValue('A8','Kategori Pasien :'.$pasien_row->kategori->nama_kategori);
        $spreadsheet->getActiveSheet()->setCellValue('A9','Tanggal Lahir : '.human_date($pasien_row->tanggal_lahir));
        $spreadsheet->getActiveSheet()->setCellValue('A10','Umur Pasien : '.hitung_umur($pasien_row->tanggal_lahir));
        $spreadsheet->getActiveSheet()->setCellValue('A11','Jenis Kelamin : '.$pasien_row->jenis_kelamin);

        $spreadsheet->getActiveSheet()->setCellValue('A13','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B13','Tanggal Periksa');
        $spreadsheet->getActiveSheet()->setCellValue('C13','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('D13','Tinggi Badan');
        $spreadsheet->getActiveSheet()->setCellValue('E13','Berat Badan');
        $spreadsheet->getActiveSheet()->setCellValue('F13','Tekanan Darah');
        $spreadsheet->getActiveSheet()->setCellValue('G13','Suhu Badan');
        $spreadsheet->getActiveSheet()->setCellValue('H13','Tindakan');
        $spreadsheet->getActiveSheet()->setCellValue('I13','Keluhan');
        $spreadsheet->getActiveSheet()->setCellValue('J13','Diagnosa');
        $spreadsheet->getActiveSheet()->setCellValue('K13','Anemnesis');
        $spreadsheet->getActiveSheet()->setCellValue('L13','Pemeriksaan Penunjang');
        $spreadsheet->getActiveSheet()->setCellValue('M13','Terapi');
        $spreadsheet->getActiveSheet()->setCellValue('N13','Rujukan');

        $get   = Resep::exportRekamMedisByIdPasien($id);
        $cell  = 14;
        $count = 1;

        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tgl_resep));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->tinggi_badan.' cm');
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->berat_badan.' kg');
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->tekanan_darah.' mmHg');
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->suhu_badan);
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->nama_tindakan);
            $spreadsheet->getActiveSheet()->setCellValue('I'.$cell,$value->keluhan_utama_resep);
            $spreadsheet->getActiveSheet()->setCellValue('J'.$cell,$value->diagnosa);
            $spreadsheet->getActiveSheet()->setCellValue('K'.$cell,$value->anemnesis);
            $spreadsheet->getActiveSheet()->setCellValue('L'.$cell,$value->pemeriksaan_penunjang);
            $spreadsheet->getActiveSheet()->setCellValue('M'.$cell,$value->terapi);
            $spreadsheet->getActiveSheet()->setCellValue('N'.$cell,$value->rujukan);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }
}
