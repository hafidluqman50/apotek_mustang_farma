<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SpesialisDokterModel as SpesialisDokter;

class SpesialisDokterController extends Controller
{
    public function index()
    {
        $title = 'Laporan Spesialis Dokter | Admin';
        $page  = 'laporan-spesialis-dokter';
        $link  = 'dokter';

        return view('Admin.spesialis-dokter.main',compact('title','page','link'));
    }

    public function tambah() {
        $title = 'Form Spesialis Dokter | Admin';
        $page  = 'data-spesialis-dokter';
        $link  = 'dokter';
        return view('Admin.spesialis-dokter.form-spesialis-dokter',compact('title','page','link'));
    }

    public function edit($id) {
        $title = 'Form Spesialis Dokter | Admin';
        $page  = 'data-spesialis-dokter';
        $link  = 'dokter';
        $row   = SpesialisDokter::where('id_spesialis_dokter',$id)
                        ->firstOrFail();
        return view('Admin.spesialis-dokter.form-spesialis-dokter',compact('title','page','row','link'));
    }

    public function delete($id) {
        SpesialisDokter::where('id_spesialis_dokter',$id)->update(['status_delete' => 1]);
        return redirect('/admin/laporan-spesialis-dokter')->with('message','Berhasil Hapus Data Spesialis Dokter');
    }

    public function save(Request $request) {
        $nama_spesialis_dokter = $request->nama_spesialis_dokter;
        $id                    = $request->id_spesialis_dokter;

        $data_spesialis_dokter = [
            'nama_spesialis_dokter' => $nama_spesialis_dokter,
            'status_delete'         => 0
        ];
        if ($id == '') {
            SpesialisDokter::create($data_spesialis_dokter);
            $message = 'Berhasil Input Spesialis Dokter';
        }
        else {
            SpesialisDokter::where('id_spesialis_dokter',$id)->update($data_spesialis_dokter);
            $message = 'Berhasil Update Spesialis Dokter';
        }

        return redirect('/admin/data-spesialis-dokter')->with('message',$message);
    }
}
