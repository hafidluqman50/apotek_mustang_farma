<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DataPenyakitController extends Controller
{
    public function index()
    {
        $title = 'Data Penyakit | Admin';
        $page  = 'data-penyakit';
        $link  = 'penyakit';

        return view('Admin.data-penyakit.main',compact('title','page','link'));
    }

    public function penyakitBpjs()
    {
        $title    = 'Data Penyakit (BPJS) | Admin';
        $page     = 'data-penyakit-bpjs';
        $link     = 'penyakit';
        // $diagnosa = app('App\Http\Controllers\ApiController')->diagnosa();
        // dd($diagnosa);

        return view('Admin.data-penyakit.main-bpjs',compact('title','page','link'));
    }
}
