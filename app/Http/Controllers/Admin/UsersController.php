<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index() {
		$title = 'Data Users | Admin';
		$page  = 'laporan-users';
		$link  = 'users';
    	return view('Admin.data-users.main',compact('title','page','link'));
    }

    public function tambah() {
        $title = 'Form Users | Admin';
        $page  = 'user';
        $link  = 'users';
    	return view('Admin.data-users.form-users',compact('title','page','link'));
    }

    public function edit($id) {
        $title = 'Form Users | Admin';
        $page  = 'user';
        $link  = 'users';
        $row   = User::where('id_users',$id)->whereNotIn('level_user',[2])->firstOrFail();
    	return view('Admin.data-users.form-users',compact('title','page','row','link'));
    }

    public function statusUser($id) {
    	$active = User::where('id_users',$id)->firstOrFail()->active;
    	if ($active == 1) {
    		User::where('id_users',$id)->update(['active'=>0]);
    		$message = 'Berhasil Non Aktifkan';
    	}
    	else {
    		User::where('id_users',$id)->update(['active'=>1]);
    		$message = 'Berhasil Aktifkan';	
    	}
    	return redirect('/admin/laporan-users')->with('message',$message);
    }

    public function delete($id) {
    	User::where('id_users',$id)
    		->whereNotIn('level_user',[2])
    		->update(['status_delete'=>1]);
    	return redirect('/admin/laporan-users')->with('message','Berhasil Hapus User');
    }

    public function save(Request $request) {
		$name     = $request->nama;
		$username = $request->username;
		if (User::where('username',$username)->count()>1) {
			return redirect()->back()->withErrors(['log'=>'Username Sudah Ada'])->withInput();
		}
		$password      = $request->password;
		$level_user    = $request->level_user;
		$status_delete = 0;
		$active        = 1;
		$id            = $request->id;
		$array = [
			'name'          => $name,
			'username'      => $username,
			'password'      => bcrypt($password),
			'level_user'    => $level_user,
			'status_delete' => $status_delete,
			'active'        => $active,
		];
		if ($id == '') {
			User::create($array);
			$message = 'Berhasil Input User';
		}
		else {
			if ($username == '' && $password == '') {
				$array = array_slice($array,0,-5);
			}
			elseif($username == '') {
				$array = array_slice($array,0,-4);
				unset($array['username']);
			}
			elseif ($password == '') {
				$array = array_slice($array,0,-4);
			}
			else {
				$array = array_slice($array,0,-3);
			}
			User::where('id_users',$id)->update($array);
			$message = 'Berhasil Update User';
		}
		return redirect('/admin/data-users')->with('message',$message);
    }
}
