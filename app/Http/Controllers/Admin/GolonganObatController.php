<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GolonganObatModel as GolonganObat;
use Intervention\Image\ImageManagerStatic as Image;

class GolonganObatController extends Controller
{
	public function index() 
	{
		$title = 'Golongan Obat | Admin';
		$page  = 'laporan-golongan-obat';
		$link  = 'obat';
		return view('Admin.obat.golongan-obat.main',compact('title','page','link'));
	}

	public function tambah()
	{
		$title = 'Form Golongan Obat | Admin';
		$page  = 'data-golongan-obat';
		$link  = 'obat';
		return view('Admin.obat.golongan-obat.form-golongan-obat',compact('title','page','link'));
	}

	public function edit($id)
	{
		$title = 'Form Golongan Obat | Admin';
		$page  = 'data-golongan-obat';
		$link  = 'obat';
		$row   = GolonganObat::where('id_golongan_obat',$id)->firstOrFail();
		return view('Admin.obat.golongan-obat.form-golongan-obat',compact('title','page','link','row'));
	}

	public function save(Request $request)
	{
		$nama_golongan = $request->nama_golongan;
		$slug_golongan = str_slug($nama_golongan,'-');
		$foto_golongan = $request->foto_golongan;
		$fileName      = $foto_golongan != '' ?uniqid('_icon_golongan_').'_'.$slug_golongan.'.jpg':'-';
		$id 		   = $request->id_golongan_obat;
		
		$data_golongan_obat = [
			'nama_golongan' => $nama_golongan,
			'slug_golongan' => $slug_golongan,
			'foto_golongan' => '',
			'status_delete' => 0
		];

		if ($id == '') {
			if ($foto_golongan != '') {
				Image::make($foto_golongan)->resize(45.733,45.721)->save('assets/icon_golongan/'.$fileName);
			}
			// data_golongan_obat_push($data_golongan_obat,'foto_golongan',$fileName);
			$data_golongan_obat['foto_golongan'] = $fileName;
			GolonganObat::create($data_golongan_obat);
			$message = 'Berhasil Input Golongan Obat';
		}
		else {
			unset($data_golongan_obat['status_delete']);
			$get = GolonganObat::where('id_golongan_obat',$id);
			if ($foto_golongan != '') {
                $foto = $get->firstOrFail()->foto_golongan;
                if (file_exists(public_path('assets/icon_golongan/'.$foto))) {
                    unlink(public_path('assets/icon_golongan/'.$foto));
                }
                Image::make($foto_golongan)->resize(45.733,45.721)->save('assets/icon_golongan/'.$fileName);
				$data_golongan_obat['foto_golongan'] = $fileName;
			}
			else {
				unset($data_golongan_obat['foto_golongan']);
			}
			$get->update($data_golongan_obat);
			$message = 'Berhasil Update Golongan Obat';
		}
		return redirect('/admin/data-golongan-obat')->with('message',$message);
	}

	public function delete($id)
	{
		$get = GolonganObat::where('id_golongan_obat',$id);
		$file = 'assets/icon_golongan/'.$get->foto_golongan;
		if (file_exists(public_path($file))) {
			unlink(public_path($file));
		}
		$get->update(['status_delete' => 1]);
		return redirect('/admin/laporan-golongan-obat')->with('message','Berhasil Hapus Data');
	}
}
