<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RujukanController extends Controller
{
    public function index()
    {
        $title = 'Rujukan | Admin';
        $page  = 'laporan-rujukan';
        $link  = 'rujukan';

        return view('Admin.rujukan.main',compact('title','page','link'));
    }
}
