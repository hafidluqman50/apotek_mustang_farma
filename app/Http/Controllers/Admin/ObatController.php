<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObatModel as Obat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\GolonganObatModel as GolonganObat;
use App\Models\HargaObatModel as HargaObat;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ObatController extends Controller
{
    public function index() {
        $title = 'Data Obat | Admin';
        $page  = 'laporan-obat';
        $link  = 'obat';
        return view('Admin.obat.data-obat.main',compact('title','page','link'));
    }

    public function orderObat() {
        $title = 'Order Obat | Admin';
        $page  = 'laporan-order-obat';
        $link  = 'obat';
        return view('Admin.obat.order-obat.main',compact('title','page','link'));
    }

    public function tambah() {
        $title         = 'Form Data Obat | Admin';
        $page          = 'data-obat';
        $link          = 'obat';
        $jenis_obat    = JenisObat::all();
        $golongan_obat = GolonganObat::all();
        return view('Admin.obat.data-obat.form-obat',compact('title','page','link','jenis_obat','golongan_obat'));
    }

    public function edit($id) {
        $title         = 'Form Data Obat';
        $page          = 'data-obat';
        $link          = 'obat';
        $jenis_obat    = JenisObat::all();
        $golongan_obat = GolonganObat::all();
        $row           = Obat::where('id_obat',$id)->firstOrFail();
        return view('Admin.obat.data-obat.form-obat',compact('title','page','row','link','jenis_obat','golongan_obat'));
    }

    public function delete($id) {
        // $obat = Obat::where('id_obat',$id)->firstOrFail();
        Obat::where('id_obat',$id_obat)->update(['status_delete' => 1]);
        return redirect('/admin/laporan-obat')->with('message','Berhasil Hapus Obat');
    }

    public function save(Request $request) {
        // $nomor_batch     = $request->nomor_batch;
        $nama_obat       = $request->nama_obat;
        $jenis_obat      = $request->jenis_obat;
        $golongan_obat   = $request->golongan_obat;
        $tanggal_expired = $request->tanggal_expired;
        $harga_obat      = $request->harga_obat;
        $ppn             = $request->ppn;
        $disc            = $request->disc;
        $harga_jual      = $request->harga_jual;
        $stok_obat       = $request->stok_obat;
        $bobot_satuan    = $request->bobot_satuan;
        $ket_data        = $request->ket_data;
        $id              = $request->id;
        // $cek = Obat::where('nomor_batch',$nomor_batch)->count();
        // if ($cek > 0) {
        //     return redirect()->back()->with('log','Obat Dengan Nomor Batch '.$nomor_batch.' Sudah Ada');
        // }
        // $first = JenisObat::where('id_jenis_obat',$jenis_obat)->firstOrFail();
        // $kode_obat = makeAcronym($first->nama_jenis_obat);
        // $kode_obat = generateCode('OMF-'.$kode_obat,'-',Obat::lastNumCode(),11);
        $kode_obat = $request->kode_obat;
        $data_obat = [
            'kode_obat'        => $kode_obat,
            // 'nomor_batch'      => $nomor_batch,
            'nama_obat'        => $nama_obat,
            'id_jenis_obat'    => $jenis_obat,
            'id_golongan_obat' => $golongan_obat,
            'tanggal_expired'  => $tanggal_expired,
            'harga_obat'       => $harga_obat,
            'ppn'              => $ppn,
            'disc'             => $disc,
            'harga_jual'       => $harga_jual,
            'stok_obat'        => $stok_obat,
            'bobot_satuan'     => 0,
            'ket_data'         => 'inventory',
            'status_delete'    => 0
        ];

        if ($id == '') {
            Obat::create($data_obat);
            $message = 'Berhasil Input Data Obat';
        }
        else {
            unset($data_obat['status_delete']);
            unset($data_obat['kode_obat']);
            // if ($nomor_batch == '') {
            //     unset($data_obat['nomor_batch']);
            // }

            // $nomor_batch = Obat::where('id_obat',$id)->firstOrFail()->nomor_batch;
            Obat::where('id_obat',$id)->update($data_obat);
            // unset($data_obat['stok_obat'],$data_obat['ket_data']);
            // Obat::where('nomor_batch',$nomor_batch)->update($data_obat);

            $message = 'Berhasil Update Data Obat';
        }
        return redirect('/admin/data-obat')->with('message',$message);
    }

    public function cetak()
    {
        $title    = 'Laporan-Obat Tanggal '.human_date(now()->format('Y-m-d'));
        $fileName = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1','Apotek Klinik 88');
        $spreadsheet->getActiveSheet()->setCellValue('A3','Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang');
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Obat Tanggal '.human_date(now()->format('Y-m-d')));
        $spreadsheet->getActiveSheet()->mergeCells('A1:N1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:N5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Kode Obat');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Nama Golongan Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Tanggal Expire');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Harga Obat');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Harga Jual');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Stok Obat');
        $get   = Obat::getData('inventory');
        $no    = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$no,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$no,$value->kode_obat);
            $spreadsheet->getActiveSheet()->setCellValue('C'.$no,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$no,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$no,$value->nama_golongan);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$no,human_date($value->tanggal_expired));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$no,format_rupiah($value->harga_obat));
            $spreadsheet->getActiveSheet()->setCellValue('H'.$no,format_rupiah($value->harga_jual));
            $spreadsheet->getActiveSheet()->setCellValue('I'.$no,$value->stok_obat);
            $count++;
            $no++;
        }
        $style = $no-1;
        $spreadsheet->getActiveSheet()->getStyle('A7:N'.$style)->applyFromArray([
            'borders'=>[
                'allborders'=>[
                    'style'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ],
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }
}
