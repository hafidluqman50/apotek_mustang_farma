<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DokterModel as Dokter;
use App\Models\SpesialisDokterModel as SpesialisDokter;
use App\User;

class DokterController extends Controller
{
    public function index() {
        $title = 'Data Dokter | Admin';
        $page  = 'laporan-dokter';
        $link  = 'dokter';
    	return view('Admin.dokter.main',compact('title','page','link'));
    }

    public function tambah() {
        $title            = 'Form Dokter | Admin';
        $page             = 'data-dokter';
        $link             = 'dokter';
        $spesialis_dokter = SpesialisDokter::where('status_delete',0)->get();
    	return view('Admin.dokter.form-dokter',compact('title','page','link','spesialis_dokter'));
    }

    public function edit($id) {
        $title            = 'Form Dokter | Admin';
        $page             = 'data-dokter';
        $link             = 'dokter';
        $spesialis_dokter = SpesialisDokter::where('status_delete',0)->get();

        $row   = Dokter::join('users','dokter.id_users','=','users.id_users')
                        ->where('id_dokter',$id)
                        ->firstOrFail();
    	return view('Admin.dokter.form-dokter',compact('title','page','row','link','spesialis_dokter'));
    }

    public function statusAkun($id) {
        $id_users = Dokter::where('id_dokter',$id)->firstOrFail()->id_users;
        $active = User::where('id_users',$id_users)->firstOrFail()->active;
        if ($active == 1) {
            User::where('id_users',$id_users)->update(['active'=>0]);
            $message = 'Berhasil Non Aktifkan User';
        }
        else {
            User::where('id_users',$id_users)->update(['active'=>1]);
            $message = 'Berhasil Aktifkan User';   
        }

        return redirect('/admin/laporan-dokter')->with('message',$message);
    }

    public function delete($id) {
        $id_users = Dokter::where('id_dokter',$id)->firstOrFail()->id_users;
        User::where('id_users',$id_users)->update(['status_delete' => 1]);
        Dokter::where('id_dokter',$id)->update(['status_delete' => 1]);
    	return redirect('/admin/laporan-dokter')->with('message','Berhasil Hapus Data Dokter');
    }

    public function save(Request $request) {
        $nama_dokter          = $request->nama_dokter;
        $nomor_telepon_dokter = $request->nomor_telepon_dokter;
        $spesialis_dokter     = $request->id_spesialis_dokter;
        $spesialis_poli       = $request->spesialis_poli;
        $biaya_dokter         = $request->biaya_dokter;
        $status_dokter        = $request->status_dokter;
        $username             = $request->username;
        $password             = $request->password;
        $id                   = $request->id_dokter;
        $data_dokter = [
            'nama_dokter'          => $nama_dokter,
            'id_spesialis_dokter'  => $spesialis_dokter,
            'nomor_telepon_dokter' => $nomor_telepon_dokter,
            'spesialis_poli'       => $spesialis_poli,
            'biaya_dokter'         => $biaya_dokter,
            'status_dokter'        => $status_dokter,
            'status_delete'        => 0
        ];

        $data_user = [
            'name'          => $nama_dokter,
            'username'      => $username,
            'password'      => bcrypt($password),
            'active'        => 1,
            'level_user'    => 3,
            'status_delete' => 0
        ];
    	if ($id == '') {
            $id_users = User::insertGetId($data_user);
            $data_dokter['id_users'] = $id_users;
            Dokter::create($data_dokter);
            $message = 'Berhasil Input Dokter';
    	}
    	else {
            unset($data_user['level_user']);
            unset($data_user['active']);
            unset($data_user['status_delete']);

            unset($data_dokter['status_dokter']);
            unset($data_dokter['status_delete']);

            Dokter::where('id_dokter',$id)->update($data_dokter);

            $dokter = Dokter::where('id_dokter',$id)->firstOrFail();
            if ($username != '' && $password != '') {
                unset($data_user['username']);
                unset($data_user['password']);
                User::where('id_users',$dokter->id_users)->update($data_user);
            }
            elseif($username != '') {
                unset($data_user['password']);
                User::where('id_users',$dokter->id_users)->update($data_user);
            }
            elseif ($password != '') {
                unset($data_user['username']);
                User::where('id_users',$dokter->id_users)->update($data_user);
            }
            $message = 'Berhasil Update Dokter';
    	}

        return redirect('/admin/data-dokter')->with('message',$message);
    }
}
