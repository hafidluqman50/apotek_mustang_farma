<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObatModel as Obat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\HargaObatModel as HargaObat;
use App\Models\PengeluaranModel as Pengeluaran;
use Auth;
// use Excel;
// use PHPExcel_Style_Border;
// use PHPExcel_Style_Alignment;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PengeluaranController extends Controller
{
    public function index() {
		$title = 'Data Pengeluaran | Admin';
		$page  = 'laporan-pengeluaran';
		$link  = 'pengeluaran-obat';
    	return view('Admin.obat.pengeluaran.main',compact('title','page','link'));
    }

    public function tambah() {
		$title      = 'Form Pengeluaran | Admin';
		$page       = 'data-keluar';
		$link       = 'pengeluaran-obat';
		$obat 		= new Obat;
		$jenis_obat = JenisObat::all();
		return view('Admin.obat.pengeluaran.form-pengeluaran',compact('title','page','link','jenis_obat','obat'));
    }

    public function edit($id) {
		$title      = 'Form Pengeluaran | Admin';
		$page       = 'data-keluar';
		$link       = 'pengeluaran-obat';
		$obat       = new Obat;
		$jenis_obat = JenisObat::all();
		$row        = Pengeluaran::getRow($id);
		return view('Admin.obat.pengeluaran.form-pengeluaran',compact('title','page','link','jenis_obat','obat','row'));	
    }

    public function delete($id) {
    	Pengeluaran::where('id_keluar',$id)->delete();
    	return redirect('/admin/laporan-pengeluaran')->with('message','Berhasil Hapus Data Keluar');
    }

    public function save(Request $request) {
		$tanggal_keluar = $request->tanggal_keluar;
		$obat           = $request->obat;
		$stok_keluar    = $request->stok_keluar;
		$bentuk_stok	= $request->bentuk_stok;
		$keterangan     = $request->keterangan;
		$ket_data 		= $keterangan == 'Pengeluaran Klinik' ? 'klinik' : 'kasir';
		$jenis_keluar   = 'non-transaksi';
		$id 			= $request->id;
		$lastNumCode    = Obat::lastNumCode();
		$obat_gudang 	= Obat::getRowJoinJenis($obat,'inventory');
		$cek 			= Obat::where('kode_obat',$obat_gudang->kode_obat)->where('ket_data',$ket_data)->count();

		// CEK STOK OBAT DI GUDANG //
		if ($obat_gudang->stok_obat < $stok_keluar) {
			$log = ['log'=>'Stok Obat '.$obat.' | '.$obat_gudang->nama_obat.' Di Gudang Kurang Hanya Tersisa '.$obat_gudang->stok_obat];
			return redirect()->back()->withInput()->withErrors($log);
		}
		// END CEK STOK OBAT DI GUDANG //

		else {
			// CEK DATA OBAT SESUAI TUJUAN KELUAR //
			if ($cek > 0) {
				$obat_id = Obat::where('kode_obat',$obat_gudang->kode_obat)->where('ket_data',$ket_data)->firstOrFail()->id_obat;
			}
			else {
				if ($bentuk_stok == 'box') {
					$stok_obt = $stok_keluar * $obat_gudang->satuan_stok;
				}
				else {
					$stok_obt = $stok_keluar;
				}
        		$kode_obat = makeAcronym($obat_gudang->nama_jenis_obat);
		        $data = [
                    'kode_obat'        => generateCode('OMF-'.$kode_obat,'-',$lastNumCode,11),
                    'kode_obat'        => $obat_gudang->kode_obat,
                    'nama_obat'        => $obat_gudang->nama_obat,
                    'id_golongan_obat' => $obat_gudang->id_golongan_obat,
                    'id_jenis_obat'    => $obat_gudang->id_jenis_obat,
                    'tanggal_expired'  => $obat_gudang->tanggal_expired,
                    'harga_obat'       => $obat_gudang->harga_obat,
                    'ppn'              => $obat_gudang->ppn,
                    'disc'             => $obat_gudang->disc,
                    'harga_jual'       => round(($obat_gudang->harga_obat * 70) / 100),
                    'stok_obat'        => $stok_obt,
                    'bobot_satuan'     => $obat_gudang->bobot_satuan,
                    'ket_data'         => $ket_data
		        ];
		        $obat_id = Obat::insertGetId($data);
			}
			// END CEK DATA OBAT BERDASARKAN TUJUAN KELUAR //

			$array = [
				'tanggal_keluar' => $tanggal_keluar,
				'id_obat'        => $obat_id,
				'stok_keluar'    => $stok_keluar,
				'bentuk_stok'	 => $bentuk_stok,
				'keterangan'     => $keterangan,
				'id_users'		 => Auth::id(),
				'jenis_keluar'   => $jenis_keluar,
			];


			$obat_kasir  = Obat::where('nomor_batch',$obat_gudang->nomor_batch)->where('ket_data',$ket_data)->firstOrFail();
			$jenis_harga = ['alpha','beta','gama','partai','eceran','luar-kota','lain-lain'];
			$harga       = ['92','90','88','85','80','65','2'];

			if ($id == '') {
				Pengeluaran::create($array);
				if ($ket_data == 'kasir') {
					for ($i=0; $i < count($jenis_harga); $i++) {
						if ($jenis_harga[$i] == 'lain-lain') {
							$jumlah = $obat_kasir->harga_obat * $harga[$i];
						}
						else {
							$jumlah = $obat_kasir->harga_obat / $harga[$i] * 100;
						}
						$harga_obat[] = [
							'nama_harga'  => $jenis_harga[$i],
							'id_obat'     => $obat_kasir->id_obat,
							'harga_total' => $jumlah
						];
					}
					HargaObat::insert($harga_obat);
				}
				$message = 'Berhasil Input Data Keluar';
			}
			else {
				unset($array['jenis_keluar']);
				Pengeluaran::where('id_keluar',$id)->update($array);
				$message = 'Berhasil Update Data Keluar';
			}

			return redirect('/admin/data-keluar-obat')->with('message',$message);
		}
    }

    public function export(Request $request) {
        $from     = $request->from;
        $to       = $request->to;
        $title    = 'Laporan Pengeluaran Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1','Apotek Klinik 88');
        $spreadsheet->getActiveSheet()->setCellValue('A3','Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang');
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:I1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:I3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:I5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Kode Obat');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Nama Obat');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Jenis Obat');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Stok Keluar');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Keterangan');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Input By');
        $get   = Pengeluaran::export($from,$to);
        $cell  = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$cell,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$cell,human_date($value->tanggal_keluar));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$cell,$value->kode_obat);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$cell,$value->nama_obat);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$cell,$value->nama_jenis_obat);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$cell,$value->stok_masuk.' '.ucwords($value->bentuk_stok));
            $spreadsheet->getActiveSheet()->setCellValue('G'.$cell,$value->keterangan);
            $spreadsheet->getActiveSheet()->setCellValue('H'.$cell,$value->name);
            $count++;
            $cell++;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
    }
}