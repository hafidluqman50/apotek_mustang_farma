<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisObatModel as JenisObat;

class JenisObatController extends Controller
{
    public function index() {
		$title = 'Data Jenis Obat | Admin';
		$page  = 'laporan-jenis-obat';
		$link  = 'obat';
    	return view('Admin.obat.jenis-obat.main',compact('title','page','link'));
    }

    public function tambah() {
		$title = 'Form Jenis Obat | Admin';
		$page  = 'data-jenis-obat';
		$link  = 'obat';
		return view('Admin.obat.jenis-obat.form-jenis-obat',compact('title','page','link'));
    }

    public function edit($id) {
		$title = 'Form Jenis Obat | Admin';
		$page  = 'data-jenis-obat';
		$link  = 'obat';
		$row   = JenisObat::where('id_jenis_obat',$id)->firstOrFail();
    	return view('Admin.obat.jenis-obat.form-jenis-obat',compact('title','page','link','row'));
    }

    public function delete($id) {
    	JenisObat::where('id_jenis_obat',$id)->update(['status_delete' => 1]);
    	return redirect('/admin/laporan-jenis-obat')->with('message','Berhasil Hapus Jenis Obat');
    }

    public function save(Request $request) {
		$nama_jenis_obat = $request->nama_jenis_obat;
		$id              = $request->id;

    	$data_jenis_obat = [
			'nama_jenis_obat' => $nama_jenis_obat,
			'status_delete'   => 0
    	];
    	if ($id == '') {
    		JenisObat::create($data_jenis_obat);
    		$message = 'Berhasil Input Jenis Obat';
    	}
    	else {
    		unset($data_jenis_obat['status_delete']);
    		JenisObat::where('id_jenis_obat',$id)->update($data_jenis_obat);
    		$message = 'Berhasil Update Jenis Obat';
    	}

    	return redirect('/admin/data-jenis-obat')->with('message',$message);
    }
}
