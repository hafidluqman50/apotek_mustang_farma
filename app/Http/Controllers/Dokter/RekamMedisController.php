<?php

namespace App\Http\Controllers\Dokter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\DokterModel as Dokter;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\ObatModel as Obat;
use App\Models\HargaObatModel as HargaObat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\GolonganObatModel as GolonganObat;
use App\Models\TindakanLabModel as TindakanLab;
use Auth;

class RekamMedisController extends Controller
{
    protected $apiController;

    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function index() {
        $title  = 'Pasien Rekam Medis | Dokter';
        $page   = 'laporan-rekam-medis';
        $link   = 'laporan';
        $dokter = Dokter::where('id_users',Auth::id())->firstOrFail()->id_dokter;
    	return view('Dokter.rekam-medis.pasien-rekam-medis',compact('dokter','title','page','link'));
    }

    public function rekamMedis($id,$id_pasien) {
        $title  = 'Rekam Medis | Dokter';
        $page   = 'laporan-rekam-medis';
        $link   = 'laporan';
        $pasien = Pasien::ktgPasien($id_pasien);
        return view('Dokter.rekam-medis.main',compact('id','pasien','title','page','link','id_pasien'));   
    }

    public function editRekamMedis($id,$id_pasien,$id_resep) {
        $title              = 'Edit Rekam Medis | Dokter';
        $page               = 'laporan-rekam-medis';
        $link               = 'laporan';
        $dokter             = Dokter::where('status_delete',0)->get();

        $dokter_row    = Dokter::where('id_users',auth()->id())->firstOrFail();
        $pasien        = Pendaftaran::getData($dokter_row->id_dokter);
        $tindakan_lab  = TindakanLab::where('id_spesialis_dokter',$dokter_row->id_spesialis_dokter)->whereNotIn('id_tindakan_lab',[4])->where('status_delete',0)->get();
        $obat          = new Obat;
        $jenis_obat    = JenisObat::where('status_delete',0)->get();
        $golongan_obat = GolonganObat::where('status_delete',0)->get();
        $row           = Resep::getRekamMedisPasienByIdResep($id_resep);
        $diagnosa_bpjs = $this->apiController->diagnosa();

        $hasil = json_decode($row->hasil_cek_darah);
        $kolesterol = '-';
        $gula_darah = '-';
        $asam_urat  = '-';

        if (!empty($hasil)) {
            if (isset($hasil->kolesterol)) {
                $kolesterol = $hasil->kolesterol;
            }
            if (isset($hasil->gula_darah)) {
                $gula_darah = $hasil->gula_darah;
            }
            if (isset($hasil->asam_urat)) {
                $asam_urat = $hasil->asam_urat;
            }
        }

        $compact = compact('title','page','link','pasien','tindakan_lab','obat','jenis_obat','dokter','golongan_obat','row','kolesterol','gula_darah','asam_urat','id','id_pasien','id_resep','diagnosa_bpjs');

        return view('Dokter.rekam-medis.edit-rekam-medis-tab',$compact);
    }

    public function updateRekamMedis(Request $request,$id,$id_pasien,$id_resep) {
        $tgl_kontrol_selanjutnya = reverse_date($request->tgl_kontrol_selanjutnya);
        $tinggi_badan            = replace_comma_to_dot($request->tinggi_badan);
        $berat_badan             = replace_comma_to_dot($request->berat_badan);
        $tekanan_darah           = $request->tekanan_darah;
        $suhu_badan              = replace_comma_to_dot($request->suhu_badan);
        $kolesterol              = $request->kolestrol != '' ? replace_comma_to_dot($request->kolesterol) : '-';
        $gula_darah              = $request->gula_darah != '' ? replace_comma_to_dot($request->gula_darah) : '-';
        $asam_urat               = $request->asam_urat != '' ? replace_comma_to_dot($request->asam_urat) : '-';
        $tindakan_lab            = $request->tindakan_lab;
        $anemnesis               = $request->anemnesis;
        $pemeriksaan_fisik       = $request->pemeriksaan_fisik;
        $keluhan                 = $request->keluhan;
        $pemeriksaan_penunjang   = $request->pemeriksaan_penunjang;
        $diagnosa                = $request->diagnosa;
        $terapi                  = $request->terapi;
        $rujukan                 = $request->rujukan;
        $keterangan_resep        = $request->keterangan_resep;
        $keluhan                 = $request->keluhan;

        if (isset($kolesterol)) {
            $hasil_cek['kolesterol'] = $kolesterol;
        }
        if (isset($gula_darah)) {
            $hasil_cek['gula_darah'] = $gula_darah;
        }
        if (isset($asam_urat)) {
            $hasil_cek['asam_urat'] = $asam_urat;
        }

        $hasil_cek = json_encode($hasil_cek);

        $data_resep = [
            'tgl_kontrol_selanjutnya' => $tgl_kontrol_selanjutnya,
            'tinggi_badan_resep'      => $tinggi_badan,
            'berat_badan_resep'       => $berat_badan,
            'tekanan_darah_resep'     => $tekanan_darah,
            'suhu_badan_resep'        => $suhu_badan,
            'hasil_cek_darah_resep'   => $hasil_cek,
            'id_tindakan_lab'         => $tindakan_lab,
            'keluhan_utama_resep'     => $keluhan,
            'anemnesis'               => $anemnesis,
            'pemeriksaan_fisik'       => $pemeriksaan_fisik,
            'pemeriksaan_penunjang'   => $pemeriksaan_penunjang,
            'diagnosa'                => $diagnosa,
            'terapi'                  => $terapi,
            'rujukan'                 => $rujukan,
            'keterangan_resep'        => $keterangan_resep
        ];

        Resep::where('id_resep',$id_resep)->update($data_resep);

        return redirect('/dokter/laporan-rekam-medis/'.$id.'/rekam/'.$id_pasien)->with('message','Berhasil Update Rekam Medis');
    }

    public function detailRekamMedis($id,$id_pasien,$id_rekam) {
        $title  = 'Rekam Medis Detail | Dokter';
        $page   = 'laporan-rekam-medis';
        $link   = 'laporan';
        $detail = ResepDetail::getPasien($id_pasien,$id_rekam);
    	return view('Dokter.rekam-medis.main-detail',compact('title','page','link','id_pasien','detail','id_rekam','id'));
    }
}
