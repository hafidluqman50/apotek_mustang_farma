<?php

namespace App\Http\Controllers\Dokter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DokterModel as Dokter;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function dashboard() {
        $title     = 'Dashboard';
        $page      = 'dashboard';
        $id_dokter = Dokter::where('id_users',Auth::id())->firstOrFail()->id_dokter;

    	return view('Dokter.dashboard',compact('title', 'page', 'id_dokter'));
    }

    public function ubahProfile() {
        $title = 'Ubah Profile | Dokter';
        $page  = 'dasboard';
        
    	return view('Dokter.ubah-profile',compact('title','page'));
    }

    public function save(Request $request) {
		$name     = $request->nama;
		$username = $request->username;
		if (User::where('username',$username)->count()>1) {
			return redirect()->back()->withErrors(['log'=>'Username Sudah Ada'])->withInput();
		}
		$password      = $request->password;
		// $level_user    = $request->level_user;
		$status_delete = 0;
		$active        = 1;
		$array = [
			'name'          => $name,
			'username'      => $username,
			'password'      => bcrypt($password)
		];
		if ($username == '' && $password == '') {
            unset($array['username']);
            unset($array['password']);
		}
		elseif($username == '') {
			unset($array['username']);
		}
		elseif ($password == '') {
            unset($array['password']);
		}

		Dokter::where('id_users',Auth::id())->update(['nama_dokter' => $array['name']]);
		User::where('id_users',Auth::id())->update($array);
		$message = 'Berhasil Update User';
        
		return redirect('/dokter/ubah-profile')->with('message',$message);
    }
}
