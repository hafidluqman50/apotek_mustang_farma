<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class DashboardController extends Controller
{
    public function dashboard() {
        $title = 'Dashboard';
        $page  = 'dashboard';
        
    	return view('Operator.dashboard',compact('title','page'));
    }

    public function ubahProfile() {
        $title = 'Ubah Profile | Operator';
        $page  = 'dashboard';

    	return view('Operator.ubah-profile',compact('title','page'));
    }

    public function save(Request $request) {
		$name     = $request->nama;
		$username = $request->username;
		if (User::where('username',$username)->count()>1) {
			return redirect()->back()->withErrors(['log'=>'Username Sudah Ada'])->withInput();
		}
		$password      = $request->password;

		$array = [
			'name'          => $name,
			'username'      => $username,
			'password'      => bcrypt($password)
		];
		if ($username == '' && $password == '') {
            unset($array['username']);
            unset($array['password']);
		}
		elseif($username == '') {
			unset($array['username']);
		}
		elseif ($password == '') {
            unset($array['password']);
		}
        
		User::where('id_users',Auth::id())->update($array);
		$message = 'Berhasil Update User';
		return redirect('/operator/ubah-profile')->with('message',$message);
    }
}
