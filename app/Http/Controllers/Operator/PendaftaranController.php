<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\DokterModel as Dokter;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Http\Controllers\ApiController;

class PendaftaranController extends Controller
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function index() {
        $title  = 'Data Pendaftaran';
        $page   = 'laporan-pendaftaran';
        $link   = 'pendaftaran';
        $dokter = Dokter::where('status_delete',0)->get();
		return view('Operator.pendaftaran.main',compact('title','page','link','dokter'));
    }

    public function form() {
		$title           = 'Form Pendaftaran';
		$page            = 'pendaftaran';
        $link            = 'pendaftaran';
		$dokter          = Dokter::where('status_delete',0)->get();
		$kategori_pasien = KategoriPasien::whereRaw("LOWER(nama_kategori) != 'bpjs'")->where('status_delete',0)->get();
    	return view('Operator.pendaftaran.form-pendaftaran',compact('title','page','link','dokter','kategori_pasien'));
    }

    public function delete($id) {
    	Pendaftaran::where('id_pendaftaran',$id)->update(['status_delete' => 1]);
    	return redirect('/operator/laporan-pendaftaran')->with('message','Berhasil Hapus Data Pendaftaran');
    }

    public function save(Request $request) {
        $tanggal_daftar        = date('Y-m-d');
        $pasien                = $request->pasien;
        $dokter                = $request->dokter;
        $nama_pasien_input     = $request->nama_pasien_input;
        $kategori_pasien_input = $request->kategori_pasien_input;
        $nomor_telpon_input    = $request->nomor_telpon_input;
        $tgl_lahir_input       = $request->tgl_lahir_input;
        $jenis_kelamin_input   = $request->jenis_kelamin_input;
        $alamat_pasien_input   = $request->alamat_pasien_input;
        $keluhan               = $request->keluhan ?? '-';
        $tinggi_badan          = replace_comma_to_dot($request->tinggi_badan);
        $berat_badan           = replace_comma_to_dot($request->berat_badan);
        $tekanan_darah         = $request->tekanan_darah;
        $suhu_badan            = replace_comma_to_dot($request->suhu_badan);
        $kolesterol            = $request->kolestrol != '' ? replace_comma_to_dot($request->kolesterol) : '-';
        $gula_darah            = $request->gula_darah != '' ? replace_comma_to_dot($request->gula_darah) : '-';
        $asam_urat             = $request->asam_urat != '' ? replace_comma_to_dot($request->asam_urat) : '-';
        $pemeriksaan_penunjang = $request->pemeriksaan_penunjang ?? '-';
        $hasil_cek             = [];

        if (!empty($nama_pasien_input) && !empty($kategori_pasien_input) && !empty($nomor_telpon_input) && !empty($tgl_lahir_input) && !empty($jenis_kelamin_input) && !empty($alamat_pasien_input)) {
            $data_pasien = [
                'nama_pasien'          => $nama_pasien_input,
                'id_kategori_pasien'   => $kategori_pasien_input,
                'nomor_telepon_pasien' => $nomor_telepon_input,
                'tanggal_lahir'        => $tgl_lahir_input,
                'jenis_kelamin'        => $jenis_kelamin_input,
                'alamat_pasien'        => $alamat_pasien_input,
                'status_delete'        => 0
            ];

            $pasien_id = Pasien::insertGetId($data_pasien);
        }
        else {
            $pasien_id = $pasien;
        }

        if (isset($kolesterol)) {
            $hasil_cek['kolesterol'] = $kolesterol;
        }
        if (isset($gula_darah)) {
            $hasil_cek['gula_darah'] = $gula_darah;
        }
        if (isset($asam_urat)) {
            $hasil_cek['asam_urat'] = $asam_urat;
        }

        $hasil_cek = json_encode($hasil_cek);

        $data_daftar = [
            'tgl_daftar'            => $tanggal_daftar,
            'id_pasien'             => $pasien_id,
            'id_dokter'             => $dokter,
            'ket_daftar'            => 'belum-masuk',
            'keluhan'               => $keluhan,
            'tinggi_badan'          => $tinggi_badan,
            'berat_badan'           => $berat_badan,
            'tekanan_darah'         => $tekanan_darah,
            'suhu_badan'            => $suhu_badan,
            'hasil_cek_darah'       => $hasil_cek,
            'pemeriksaan_penunjang' => $pemeriksaan_penunjang,
            'status_delete'         => 0
        ];

        Pendaftaran::create($data_daftar);
		return redirect('/operator/pendaftaran')->with('message','Berhasil Input Pendaftaran');
    }

    public function check($id) {
        $title = 'Data Pendaftaran Hasil Cek';
        $page  = 'check-pendaftaran';
        $row = Pendaftaran::where('id_daftar', $id)->first();
        $checkResult = json_decode($row->hasil_cek_darah);
        return view('Admin.pendaftaran.form-check',compact('title', 'page', 'row', 'checkResult'));
    }

    public function saveCheck($id, Request $request) {
        $kolesterol            = replace_comma_to_dot($request->kolesterol);
        $gula_darah            = replace_comma_to_dot($request->gula_darah);
        $asam_urat             = replace_comma_to_dot($request->asam_urat);
        $hasil_cek             = [];

        if (isset($kolesterol)) {
            $hasil_cek['kolesterol'] = $kolesterol;
        }
        if (isset($gula_darah)) {
            $hasil_cek['gula_darah'] = $gula_darah;
        }
        if (isset($asam_urat)) {
            $hasil_cek['asam_urat'] = $asam_urat;
        }

        $hasil_cek = json_encode($hasil_cek);

        $updateData = Pendaftaran::find($id);
        $updateData['hasil_cek_darah'] = $hasil_cek;
        $updateData->update();
        return redirect('/operator/laporan-pendaftaran')->with('message','Berhasil Input Hasil Check');
    }

    public function indexBpjs() {
        $title = 'Data Pendaftaran';
        $page  = 'laporan-pendaftaran-bpjs';
        $link  = 'pendaftaran';
        // $dokter = Dokter::where('status_delete',0)->get();
        return view('Operator.pendaftaran.main-bpjs',compact('title', 'page', 'link'));
    }

    public function formBpjs() {
        $title           = 'Form Pendaftaran';
        $page            = 'pendaftaran-bpjs';
        $link            = 'pendaftaran';
        $dokter          = Dokter::with('spesialis')->where('status_delete',0)->get();
        $kategori_pasien = KategoriPasien::whereRaw("LOWER(nama_kategori) = 'bpjs'")
                                        ->where('status_delete',0)->firstOrFail();

        $pasien          = Pasien::where('id_kategori_pasien',$kategori_pasien->id_kategori_pasien)->get();
        $kode_poli       = $this->apiController->kodePoli();
        return view('Operator.pendaftaran.form-pendaftaran-bpjs',compact('title','page','link','dokter','kategori_pasien','kode_poli','pasien'));
    }

    public function saveBpjs(Request $request) {
        $no_bpjs               = $request->no_bpjs;
        $kode_provider_peserta = $this->apiController->getPesertaKdProvider($no_bpjs);
        $tanggal_daftar        = date('Y-m-d');
        $pasien                = $request->pasien;
        $dokter                = $request->dokter;
        $nama_pasien_input     = $request->nama_pasien_input;
        $kategori_pasien_input = $request->kategori_pasien_input;
        $nomor_telpon_input    = $request->nomor_telpon_input;
        $tgl_lahir_input       = $request->tgl_lahir_input;
        $jenis_kelamin_input   = $request->jenis_kelamin_input;
        $alamat_pasien_input   = $request->alamat_pasien_input;
        $kode_poli             = $request->kode_poli;
        $keluhan               = $request->keluhan ?? '-';
        $tinggi_badan          = replace_comma_to_dot($request->tinggi_badan);
        $berat_badan           = replace_comma_to_dot($request->berat_badan);
        $tekanan_darah         = $request->tekanan_darah;
        $suhu_badan            = replace_comma_to_dot($request->suhu_badan);
        $laju_respirasi        = $request->laju_respirasi;
        $denyut_jantung        = $request->denyut_jantung;
        $lingkar_perut         = $request->lingkar_perut;
        $kolesterol            = $request->kolestrol != '' ? replace_comma_to_dot($request->kolesterol) : '-';
        $gula_darah            = $request->gula_darah != '' ? replace_comma_to_dot($request->gula_darah) : '-';
        $asam_urat             = $request->asam_urat != '' ? replace_comma_to_dot($request->asam_urat) : '-';
        $kunjungan_sakit       = (boolean) $request->kunjungan_sakit == '1' ? true : false;
        $rujuk_balik           = (int) $request->rujuk_balik;
        $pemeriksaan_penunjang = $request->pemeriksaan_penunjang ?? '-';
        $hasil_cek             = [];

        if (!empty($nama_pasien_input) && !empty($kategori_pasien_input) && !empty($nomor_telpon_input) && !empty($tgl_lahir_input) && !empty($jenis_kelamin_input) && !empty($alamat_pasien_input)) {
            $data_pasien = [
                'nama_pasien'          => $nama_pasien_input,
                'id_kategori_pasien'   => $kategori_pasien_input,
                'nomor_telepon_pasien' => $nomor_telepon_input,
                'tanggal_lahir'        => $tgl_lahir_input,
                'jenis_kelamin'        => $jenis_kelamin_input,
                'alamat_pasien'        => $alamat_pasien_input,
                'status_delete'        => 0
            ];

            $pasien_id = Pasien::insertGetId($data_pasien);
        }
        else {
            $pasien_id = $pasien;
        }

        if (isset($kolesterol)) {
            $hasil_cek['kolesterol'] = $kolesterol;
        }
        if (isset($gula_darah)) {
            $hasil_cek['gula_darah'] = $gula_darah;
        }
        if (isset($asam_urat)) {
            $hasil_cek['asam_urat'] = $asam_urat;
        }

        $hasil_cek = json_encode($hasil_cek);

        $data_daftar = [
            'kode_provider_peserta' => $kode_provider_peserta,
            'no_bpjs'               => $no_bpjs,
            'tgl_daftar'            => $tanggal_daftar,
            'id_pasien'             => $pasien_id,
            'id_dokter'             => $dokter,
            'ket_daftar'            => 'belum-masuk',
            'keluhan'               => $keluhan,
            'tinggi_badan'          => $tinggi_badan,
            'berat_badan'           => $berat_badan,
            'tekanan_darah'         => $tekanan_darah,
            'suhu_badan'            => $suhu_badan,
            'hasil_cek_darah'       => $hasil_cek,
            'laju_respirasi'        => $laju_respirasi,
            'lingkar_perut'         => $lingkar_perut,
            'denyut_jantung'        => $denyut_jantung,
            'kunjungan_sakit'       => $kunjungan_sakit,
            'rujuk_balik'           => $rujuk_balik,
            'kode_poli_bpjs'        => $kode_poli,
            'pemeriksaan_penunjang' => $pemeriksaan_penunjang,
            'status_delete'         => 0
        ];

        // if (KategoriPasien::checkKategori($kategori_pasien_input) == 'bpjs') {
            $daftar_bpjs = $this->apiController->pendaftaran($data_daftar);
            if ($daftar_bpjs['code'] == 412) {
                $message = [
                    'type'    => 'log',
                    'message' => $daftar_bpjs['message']
                ];
            }
            else {
                unset($data_daftar['kode_provider_peserta']);
                unset($data_daftar['no_bpjs']);
                
                Pendaftaran::create($data_daftar);

                $message = [
                    'type'    => 'message',
                    'message' => 'Berhasil Input Pendaftaran BPJS'
                ];
            }
        // }

        return redirect('/operator/pendaftaran-bpjs')->with($message['type'],$message['message']);
    }

    public function deleteBpjs($no_kartu,$tgl_daftar,$no_urut,$kode_poli)
    {
        $data = [
            'noKartu'   => $no_kartu,
            'tglDaftar' => $tgl_daftar,
            'noUrut'    => $no_urut,
            'kdPoli'    => $kode_poli
        ];

        $delete = $this->apiController->deletePendaftaran($data);

        if ($delete['code'] == 200) {
            $message = [
                'type'    => 'message',
                'message' => $delete['message']
            ];
        }
        else {
            $message = [
                'type'    => 'log',
                'message' => $delete['message']
            ];
        }

        return redirect('/operator/laporan-pendaftaran-bpjs')->with($message['type'],$message['message']);
    }
}
