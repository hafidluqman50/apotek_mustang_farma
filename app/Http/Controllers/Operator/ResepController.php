<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DokterModel as Dokter;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\ObatModel as Obat;
use App\Models\HargaObatModel as HargaObat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\GolonganObatModel as GolonganObat;
use App\Models\TindakanLabModel as TindakanLab;
use Auth;

class ResepController extends Controller
{
    public function resep() {
		$title = 'Data Resep';
        $page = 'laporan-resep';
        $link = 'resep';
    	return view('Operator.resep.data-resep.main',compact('title','page','link'));
    }

    public function tambahResep() {
        $title              = 'Tambah Resep | Resep';
        $page               = 'resep';
        $dokter             = Dokter::all();
        // $pasien          = new Pasien;
        // $kategori_pasien = KategoriPasien::all();
        $pasien             = Pendaftaran::getData();
        $obat               = new Obat;
        $jenis_obat         = JenisObat::all();
    	return view('Operator.resep.data-resep.form-resep-tab',compact('title','page','pasien','obat','kategori_pasien','jenis_obat','dokter'));
    }

    public function deleteResep($id) {
        Resep::where('id_resep',$id)->delete();
        return redirect('/operator/laporan-resep')->with('message','Berhasil Hapus Resep');
    }

    public function detailResep($id) {
        $customer = Resep::getPasien($id);
        $title    = 'Detail Resep';
        $page     = 'resep';
        return view('Operator.resep.detail-resep.main',compact('title','page','customer','id'));
    }

    public function deleteDetailResep($id,$id_detail) {
        ResepDetail::where('id_resep',$id)->where('id_detail_resep',$id_detail)->delete();
        return redirect('/operator/laporan-resep/detail/'.$id)->with('message','Berhasil Hapus Obat Resep');
    }

    public function save(Request $request) {
        $message = [];
        // VARIABLE INPUTAN CUSTOMER //
        $nama_dokter_input     = $request->nama_dokter_input;
        $nama_pasien           = $request->nama_pasien;
        $kategori_pasien_input = $request->kategori_pasien_input;
        $tanggal_lahir         = $request->tanggal_lahir;
        $jenis_kelamin         = $request->jenis_kelamin;
        $alamat_pasien         = $request->alamat_pasien;
        // END VARIABLE INPUTAN CUSTOMER //

        // VARIABLE INPUTAN OBAT //
        $nomor_batch_obat  = $request->nomor_batch_obat;
        $nama_input_obat   = $request->nama_input_obat;
        $jenis_input_obat  = $request->jenis_input_obat;
        $tanggal_expired   = $request->tanggal_expired;
        $harga_input_obat  = $request->harga_input_obat;
        $stok_input_obat   = $request->stok_input_obat;
        $dosis_input_obat  = $request->dosis_input_obat;
        $banyak_input_beli = $request->banyak_input_beli;
        // END VARIABLE INPUTAN OBAT //

        // INPUTAN RESEP //
        $daftar            = $request->pendaftaran;
        $pasien            = $request->pasien;
        $dokter            = $request->dokter;
        $diagnosa          = $request->diagnosa;
        $keluhan           = $request->keluhan;
        $kategori_pasien   = $request->kategori_pasien;
        $obat              = $request->obat;
        $jenis_obat        = $request->jenis_obat;
        $dosis_obat        = $request->dosis_obat;
        $banyak_obat       = $request->banyak_obat;
        $data_detail_resep = [];
        // END INPUTAN RESEP //
        
        $jenis_harga = ['alpha','beta','gama','partai','eceran','luar-kota','lain-lain'];
        $harga       = ['92','90','88','85','80','65','2'];

        // if ($kategori_pasien_input != '' && $nama_pasien != '' && $tanggal_lahir != '' && $jenis_kelamin != '' && $alamat_pasien != '') {
     //        $data_pasien = [
     //            'nama_pasien'          => $nama_pasien,
     //            'id_kategori_pasien'   => $kategori_pasien_input,
     //            'nomor_telepon_pasien' => $nomor_telepon_pasien,
     //            'tanggal_lahir'        => $tanggal_lahir,
     //            'jenis_kelamin'        => $jenis_kelamin,
     //            'alamat_pasien'        => $alamat_pasien
     //        ];
     //        Pasien::firstOrNew($data_pasien);
     //        $pasien_id   = Pasien::where('nama_pasien',$nama_pasien)->firstOrFail()->id_pasien;
     //    }
        // else {
            $pasien_id   = $pasien;
        // }
        // END CEK CUSTOMER //

        $data_resep = [
            'kode_resep'         => generateCode('RSP','-',Resep::lastNumCode(),11),
            'tgl_resep'          => now()->format('Y-m-d'),
            'id_daftar'          => $daftar,
            'id_dokter'          => $dokter,
            'id_pasien'          => $pasien_id,
            'keluhan'            => $keluhan,
            'diagnosa'           => $diagnosa,
            'id_users'           => Auth::id(),
            'ket_resep'          => 'belum-bayar',
            'created_at'         => date('Y-m-d H:i:s'),
            'updated_at'         => date('Y-m-d H:i:s')
        ];
        if (!empty($nama_input_obat) && !empty($harga_input_obat) && !empty($stok_input_obat) && !empty($dosis_input_obat) && !empty($banyak_input_beli)) {
            // $resep_id = Resep::insertGetId($data_resep);
            
            for ($i=0; $i < count($nama_input_obat); $i++) { 
                $data_obat = [
                    'nomor_batch'     => $nomor_batch_obat[$i],
                    'nama_obat'       => $nama_input_obat[$i],
                    'id_jenis_obat'   => $jenis_obat[$i],
                    'tanggal_expired' => $tanggal_expired[$i],
                    'harga_obat'      => $harga_input_obat[$i],
                    'harga_jual'      => round(($harga_obat[$i] * 70) / 100),
                    'stok_obat'       => $stok_obat[$i]
                ];
                Obat::firstOrNew($data_obat);

                if ($jenis_harga[$i] == 'lain-lain') {
                    $jumlah = $get_obat->harga_jual * $harga[$i];
                }
                else {
                    $jumlah = $get_obat->harga_jual / $harga[$i] * 100;
                }
                $harga_total[] = [
                    'nama_harga'  => $jenis_harga[$i],
                    'id_obat'     => $get_obat->id_obat,
                    'harga_total' => $jumlah
                ];

                $obat_first = Obat::where('nomor_batch',$data_obat['nomor_batch'])->firstOrFail();
                $sub_total = $banyak_input_beli[$i] * $obat_first->harga_jual;
                $data_detail_resep[] = [
                    // 'id_resep'    => $resep_id,
                    'id_obat'     => $obat_first->id_obat,
                    'dosis'       => $dosis_obat[$i],
                    'banyak_obat' => $banyak_beli[$i],
                    'sub_total'   => $sub_total,
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s')
                ];
            }
            HargaObat::insert($harga_total);
            $message['berhasil'] = 'Berhasil Input Resep';
        }
        else {
            for ($i=0; $i < count($obat); $i++) {
                $get_obat   = Obat::where('id_obat',$obat[$i])->firstOrFail();
                if ($get_obat->stok_obat < $banyak_obat[$i]) {
                    $message['habis'][] = [
                        'text'=>'Stok Obat <b>'.$get_obat->nomor_batch.'|'.$get_obat->nama_obat.'</b> Hanya Tersisa '.$get_obat->stok_obat.' pcs'
                    ];
                }
                elseif($get_obat->stok_obat == 0) {
                    $message['habis'][] = [
                        'text'=>'Stok Obat <b>'.$get_obat->nomor_batch.'|'.$get_obat->nama_obat.'</b> Stok Obat Habis'
                    ];
                }
                else {
                    $harga_jual = $get_obat->harga_jual;
                    $sub_total  = $banyak_obat[$i] * $harga_jual;
                    $data_detail_resep[] = [
                        // 'id_resep'    => $resep_id,
                        'id_obat'     => $obat[$i],
                        'dosis'       => $dosis_obat[$i],
                        'banyak_obat' => $banyak_obat[$i],
                        'sub_total'   => $sub_total,
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s')
                    ];
                    $message['berhasil'] = 'Berhasil Input Resep';
                }
            }
        }
        if (array_key_exists('habis',$message) === false) {
            // dd(empty($nama_input_obat));
            $hitung = empty($nama_input_obat) ? 0 : count($nama_input_obat);
            for ($i=0; $i < count($obat) + $hitung; $i++) { 
                $cek = Resep::where('kode_resep',$data_resep['kode_resep']);
                if ($cek->count() > 0) {
                    $resep_id = $cek->firstOrFail()->id_resep;
                }
                else {
                    $resep_id = Resep::insertGetId($data_resep);
                }
                $data_detail_resep[$i]['id_resep'] = $resep_id;
            }
            // dd($data_detail_resep);
            ResepDetail::insert($data_detail_resep);
            return redirect('/operator/laporan-resep')->with('berhasil',$message['berhasil']);
        }
        else {
            $data_obat        = [];
            $data_jenis       = [];
            $data_dosis       = [];
            $data_banyak_obat = [];

            // AMBIL INPUT OBAT KEMUDIAN MASUK SESSION //
            for ($i=0; $i < count($obat); $i++) { 
                if ($obat[$i] != null) {
                    $data_obat[] = [
                        'id_obat' => $obat[$i]
                    ];
                }
                if ($nama_input_obat[$i] != null) {
                    $data_obat[] = [
                        'id_obat' => Obat::getRowId($nama_input_obat[$i],$nomor_batch_obat[$i])
                    ];
                }
            }
            // END AMBIL INPUT OBAT KEMUDIAN MASUK SESSION //

            // AMBIL OBAT KATEGORI //
            for ($j=0; $j < count($jenis_obat); $j++) { 
                if ($jenis_obat[$j] != null) {
                    $data_jenis[] = [
                        'jenis_obat' => $jenis_obat[$j]
                    ];
                }
                if ($jenis_input_obat[$j] != null) {
                    $data_jenis[] = [
                        'jenis_obat' => JenisObat::getRowId($jenis_input_obat[$j])
                    ];
                }
            }
            // END AMBIL OBAT KATEGORI //

            // AMBIL INPUT DOSIS OBAT //
            for ($k=0; $k < count($dosis_obat); $k++) { 
                if ($dosis_obat[$k] != null) {
                    $data_dosis[] = [
                        'dosis_obat' => $dosis_obat[$k]
                    ];
                }
                if ($dosis_input_obat[$k] != null) {
                    $data_dosis[] = [
                        'dosis_obat' => $dosis_input_obat[$k]
                    ];
                }
            }
            // END AMBIL INPUT DOSIS OBAT //

            // AMBIL INPUT BANYAK OBAT //
            for ($n=0; $n < count($banyak_obat); $n++) { 
                if ($banyak_obat[$n] != null) {
                    $data_banyak_obat[] = [
                        'banyak_obat' => $banyak_obat[$n]
                    ];
                }
                if ($banyak_input_beli[$n] !=  null) {
                    $data_banyak_obat[] = [
                        'banyak_obat' => $banyak_input_beli[$n]
                    ];
                }
            }
            // END AMBIL INPUT BANYAK OBAT //

            session()->flash('habis',$message['habis']);
            session()->flash('obat_habis',$data_obat);
            session()->flash('jenis_obat',$data_jenis);
            session()->flash('dosis_obat',$data_dosis);
            session()->flash('banyak_obat',$data_banyak_obat);
            return redirect()->back()->withInput();
        }
    }

    public function inputObat($id) 
    {
        $title              = 'Tambah Resep | Operator';
        $page               = 'data-resep';
        $link               = 'resep';
        $resep_row = Resep::join('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                            ->join('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                            ->join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                            ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                            ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                            ->where('id_resep',$id)
                            ->firstOrFail();
        // $tindakan_lab       = TindakanLab::whereNotIn('id_tindakan_lab',[4])->where('status_delete',0)->get();
        $obat               = new Obat;
        $jenis_obat         = JenisObat::where('status_delete',0)->get();
        $golongan_obat      = GolonganObat::where('status_delete',0)->get();
        return view('Operator.resep.data-resep.form-resep-tab',compact('title','page','link','resep_row','obat','jenis_obat','golongan_obat','id'));
    }

    public function inputObatSave(Request $request, $id)
    {
        $message = [];
        $obat                  = $request->obat;
        $jenis_obat            = $request->jenis_obat;
        $dosis_obat            = $request->dosis_obat;
        $banyak_obat           = $request->banyak_obat;
        $data_detail_resep     = [];

        // VARIABLE INPUTAN OBAT //
        $nomor_batch_obat    = $request->nomor_batch;
        $nama_input_obat     = $request->nama_input_obat;
        $jenis_input_obat    = $request->jenis_input_obat;
        $golongan_input_obat = $request->golongan_input_obat;
        $tanggal_expired     = $request->tanggal_expired;
        $harga_input_obat    = $request->harga_input_obat;
        $stok_input_obat     = $request->stok_input_obat;
        $bobot_satuan        = $request->bobot_satuan;
        $dosis_input_obat    = $request->dosis_input_obat;
        $banyak_input_beli   = $request->banyak_input_beli;
        // END VARIABLE INPUTAN OBAT //

        // CEK JIKA NAMA INPUT OBAT DAN LAIN LAIN TERISI MAKA DIINSERT //

        if (!empty($nama_input_obat) && !empty($harga_input_obat) && !empty($stok_input_obat) && !empty($dosis_input_obat) && !empty($banyak_input_beli)) {

            $data_resep['status_resep'] = 1;
            // $resep_id = Resep::insertGetId($data_resep);
            for ($i=0; $i < count($nama_input_obat); $i++) { 

                $obat_row  = JenisObat::where('id_jenis_obat',$jenis_input_obat[$i])->first();
                $kode_obat = makeAcronym($obat_row->nama_jenis_obat);
                $data_obat = [
                    'kode_obat'        => generateCode('OMF-'.$kode_obat,'-',Obat::lastNumCode(),11),
                    'nomor_batch'      => $nomor_batch_obat[$i],
                    'nama_obat'        => $nama_input_obat[$i],
                    'id_jenis_obat'    => $jenis_input_obat[$i],
                    'id_golongan_obat' => $golongan_input_obat[$i],
                    'tanggal_expired'  => $tanggal_expired[$i],
                    'harga_obat'       => $harga_input_obat[$i],
                    'harga_jual'       => round(($harga_input_obat[$i] * 70) / 100),
                    'stok_obat'        => $stok_input_obat[$i],
                    'bobot_satuan'     => 10,
                    'ket_data'         => 'inventory',
                    'status_delete'    => 0
                ];
                Obat::firstOrCreate($data_obat);

            // PROSES INSERT HARGA JENIS OBAT BERDASARKAN VARIABEL JENIS HARGA //
            //     $obat_first = Obat::where('nomor_batch',$data_obat['nomor_batch'])->firstOrFail();
            //     if ($jenis_harga[$i] == 'lain-lain') {
            //         $jumlah = $obat_first->harga_jual * $harga[$i];
            //     }
            //     else {
            //         $jumlah = $obat_first->harga_jual / $harga[$i] * 100;
            //     }
            //     $harga_total[] = [
            //         'nama_harga'  => $jenis_harga[$i],
            //         'id_obat'     => $obat_first->id_obat,
            //         'harga_total' => $jumlah
            //     ];

            //     $sub_total = $banyak_input_beli[$i] * $obat_first->harga_jual;
            //     $data_detail_resep[] = [
            //         // 'id_resep'    => $resep_id,
            //         'id_obat'     => $obat_first->id_obat,
            //         'dosis'       => $dosis_input_obat[$i],
            //         'banyak_obat' => $banyak_input_beli[$i],
            //         'sub_total'   => $sub_total,
            //         'created_at'  => date('Y-m-d H:i:s'),
            //         'updated_at'  => date('Y-m-d H:i:s')
            //     ];
            }
            // HargaObat::insert($harga_total);
            // END PROSES INSERT HARGA JENIS OBAT BERDASARKAN VARIABEL JENIS HARGA //

            $message['berhasil'] = 'Berhasil Input Obat Resep';
        }
        else if(!empty($obat)) {
            for ($i=0; $i < count($obat); $i++) {
                $get_obat   = Obat::where('id_obat',$obat[$i])->firstOrFail();
                if ($get_obat->stok_obat < $banyak_obat[$i]) {
                    $message['habis'][] = [
                        'text'=>'Stok Obat <b>'.$get_obat->nomor_batch.'|'.$get_obat->nama_obat.'</b> Hanya Tersisa '.$get_obat->stok_obat.' pcs'
                    ];
                }
                elseif($get_obat->stok_obat == 0) {
                    $message['habis'][] = [
                        'text'=>'Stok Obat <b>'.$get_obat->nomor_batch.'|'.$get_obat->nama_obat.'</b> Stok Obat Habis'
                    ];
                }
                else {
                    $harga_jual = $get_obat->harga_jual;
                    $sub_total  = $banyak_obat[$i] * $harga_jual;
                    $data_detail_resep[] = [
                        // 'id_resep'    => $resep_id,
                        'id_obat'     => $obat[$i],
                        'dosis'       => $dosis_obat[$i],
                        'banyak_obat' => $banyak_obat[$i],
                        'sub_total'   => $sub_total,
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s')
                    ];
                    $message['berhasil'] = 'Berhasil Input Obat Resep';
                }
            }
        }

        // CEK KONDISI ARRAY HABIS PADA VARIABEL MESSAGE //
        if (array_key_exists('habis',$message) === false) {
            // dd(empty($nama_input_obat));
            $hitung     = empty($nama_input_obat) ? 0 : count($nama_input_obat);
            $count_obat = empty($obat) ? 0 : count($obat);

            // PROSES SET ID RESEP DETAIL UNTUK DIINSERT //
            for ($i=0; $i < $count_obat + $hitung; $i++) { 
                $data_detail_resep[$i]['id_resep'] = $id;
            }
            // PROSES SET ID RESEP DETAIL UNTUK DIINSERT //

            ResepDetail::insert($data_detail_resep);
            Resep::where('id_resep',$id)->update(['status_resep'=>1]);
            return redirect('/operator/laporan-resep')->with('berhasil',$message['berhasil']);
        }
        else {
            $data_obat        = [];
            $data_jenis       = [];
            $data_dosis       = [];
            $data_banyak_obat = [];

            // AMBIL INPUT OBAT KEMUDIAN MASUK SESSION //
            for ($i=0; $i < count($obat); $i++) { 
                if ($obat != null) {
                    $data_obat[] = [
                        'id_obat' => $obat[$i]
                    ];
                }
                if ($nama_input_obat != null) {
                    $data_obat[] = [
                        'id_obat' => Obat::getRowId($nama_input_obat[$i],$nomor_batch_obat[$i])
                    ];
                }
            }
            // END AMBIL INPUT OBAT KEMUDIAN MASUK SESSION //

            // AMBIL OBAT KATEGORI //
            for ($j=0; $j < count($jenis_obat); $j++) { 
                if ($jenis_obat != null) {
                    $data_jenis[] = [
                        'jenis_obat' => $jenis_obat[$j]
                    ];
                }
                if ($jenis_input_obat != null) {
                    $data_jenis[] = [
                        'jenis_obat' => JenisObat::getRowId($jenis_input_obat[$j])
                    ];
                }
            }
            // END AMBIL OBAT KATEGORI //

            // AMBIL OBAT KATEGORI //
            for ($j=0; $j < count($golongan_obat); $j++) { 
                if ($golongan_obat != null) {
                    $data_golongan[] = [
                        'golongan_obat' => $golongan_obat[$j]
                    ];
                }
                if ($golongan_input_obat != null) {
                    $data_golongan[] = [
                        'golongan_obat' => GolonganObat::getRowId($golongan_input_obat[$j])
                    ];
                }
            }
            // END AMBIL OBAT KATEGORI //

            // AMBIL INPUT DOSIS OBAT //
            for ($k=0; $k < count($dosis_obat); $k++) { 
                if ($dosis_obat != null) {
                    $data_dosis[] = [
                        'dosis_obat' => $dosis_obat[$k]
                    ];
                }
                if ($dosis_input_obat != null) {
                    $data_dosis[] = [
                        'dosis_obat' => $dosis_input_obat[$k]
                    ];
                }
            }
            // END AMBIL INPUT DOSIS OBAT //

            // AMBIL INPUT BANYAK OBAT //
            for ($n=0; $n < count($banyak_obat); $n++) { 
                if ($banyak_obat != null) {
                    $data_banyak_obat[] = [
                        'banyak_obat' => $banyak_obat[$n]
                    ];
                }
                if ($banyak_input_beli !=  null) {
                    $data_banyak_obat[] = [
                        'banyak_obat' => $banyak_input_beli[$n]
                    ];
                }
            }
            // END AMBIL INPUT BANYAK OBAT //

            session()->flash('habis',$message['habis']);
            session()->flash('obat_habis',$data_obat);
            session()->flash('jenis_obat',$data_jenis);
            session()->flash('golongan_obat',$data_golongan);
            session()->flash('dosis_obat',$data_dosis);
            session()->flash('banyak_obat',$data_banyak_obat);
            return redirect()->back()->withInput();
        }
        // END CEK KONDISI ARRAY HABIS PADA VARIABEL MESSAGE //
    }
}
