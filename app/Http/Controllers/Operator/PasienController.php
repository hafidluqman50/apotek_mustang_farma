<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;

class PasienController extends Controller
{
    public function index() {
        $title = 'Data Pasien';
        $page  = 'laporan-pasien';
        $link  = 'laporan';
        return view('Operator.pasien.data-pasien.main',compact('title','page','link'));
    }

    public function tambah() {
        $title = 'Form Data Pasien';
        $page  = 'data-pasien';
        $link  = 'pasien';
        $kategori_pasien = KategoriPasien::all();
        return view('Operator.pasien.data-pasien.form-pasien',compact('title','page','link','kategori_pasien'));
    }

    public function edit($id) {
        $title = 'Form Data Pasien';
        $page  = 'data-pasien';
        $link  = 'pasien';
        $row   = Pasien::where('id_pasien',$id)->firstOrFail();
        $kategori_pasien = KategoriPasien::all();
        return view('Operator.pasien.data-pasien.form-pasien',compact('title','page','row','link','kategori_pasien'));
    }

    public function delete($id) {
        Pasien::where('id_pasien',$id)->update(['status_delete' => 1]);
        return redirect('/operator/laporan-pasien')->with('message','Berhasil Hapus Pasien');
    }

    public function save(Request $request) {
        $nama_pasien          = $request->nama_pasien;
        $id_kategori_pasien   = $request->id_kategori_pasien;
        $nomor_telepon_pasien = $request->nomor_telepon_pasien;
        $tanggal_lahir        = $request->tanggal_lahir;
        $jenis_kelamin        = $request->jenis_kelamin;
        $alamat_pasien        = $request->alamat_pasien;
        $id                   = $request->id;
        // $kode_pasien       = generateCode('PSN-AMF','-',Pasien::lastNumCode(),11);
        $kode_pasien          = $request->kode_pasien;

        $data_pasien = [
            'kode_pasien'          => $kode_pasien,
            'nama_pasien'          => $nama_pasien,
            'id_kategori_pasien'   => $id_kategori_pasien,
            'nomor_telepon_pasien' => $nomor_telepon_pasien,
            'tanggal_lahir'        => $tanggal_lahir,
            'jenis_kelamin'        => $jenis_kelamin,
            'alamat_pasien'        => $alamat_pasien,
            'status_delete'        => 0
        ];

        if ($id == '') {
            Pasien::create($data_pasien);
            $message = 'Berhasil Input Data Pasien';            
        }
        else {
            unset($data_pasien['status_delete']);
            Pasien::where('id_pasien',$id)->update($data_pasien);
            $message = 'Berhasil Update Data Pasien';
        }
        return redirect('/operator/data-pasien')->with('message',$message);
    }
}
