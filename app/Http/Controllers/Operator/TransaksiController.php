<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\TransaksiModel as Transaksi;
use App\Models\TindakanLabModel as TindakanLab;
use App\Models\DiskonModel as Diskon;
use App\Models\PengeluaranModel as Pengeluaran;
use App\Models\PemasukanModel as Pemasukan;
use App\Models\ObatModel as Obat;
use Auth;
// use Excel;
// use \PhpOffice\PhpSpreadsheet\Style\Border;
// use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class TransaksiController extends Controller
{
    public function index() {
        $title = 'Data Transaksi | Operator';
        $page  = 'laporan-transaksi';
        $link  = 'transaksi';
    	return view('Operator.transaksi.data-transaksi.main',compact('title','page','link'));
    }

    public function tambah() {
        $title        = 'Form Transaksi | Operator';
        $page         = 'transaksi';
        $link         = 'transaksi';
        $resep        = Resep::getData('belum-bayar');
        $tindakan_lab = TindakanLab::whereNotIn('id_tindakan_lab',[4])->get();
    	return view('Operator.transaksi.data-transaksi.form-transaksi',compact('title','page','resep','link','tindakan_lab'));
    }

    public function delete($id) {
        Transaksi::where('id_transaksi',$id)->delete();
        return redirect('/operator/laporan-transaksi')->with('message','Berhasil Hapus Transaksi');
    }

    public function save(Request $request) {
        $resep             = $request->customer;
        // $tindakan_lab      = $request->tindakan_lab != null ? $request->tindakan_lab : 4;
        $biaya_klinik   = $request->biaya_klinik;
        $harga_dokter   = $request->biaya_dokter;
        $biaya_resep    = $request->biaya_resep;
        $biaya_racik    = $request->biaya_racik;
        $biaya_jasa_lab = $request->biaya_jasa_lab != null ? $request->biaya_jasa_lab : 0;
        $biaya_tambahan = $request->biaya_tambahan;
        $total_harga    = $request->total_harga;
        $ppn            = $request->ppn ?? 0;
        $diskon         = $request->diskon != null ? $request->diskon : 0;
        $bayar          = $request->bayar;
        $kembali        = $request->kembali;

        $data_transaksi = [
            'tgl_byr'           => now()->format('Y-m-d'),
            'id_resep'          => $resep,
            // 'id_tindakan_lab'   => $tindakan_lab,
            'id_users'          => Auth::id(),
            'biaya_klinik'      => $biaya_klinik,
            'harga_dokter'      => $harga_dokter,
            'biaya_resep'       => $biaya_resep,
            'biaya_racik'       => $biaya_racik,
            'biaya_jasa_lab'    => $biaya_jasa_lab,
            'biaya_tambahan'    => $biaya_tambahan,
            'jumlah_byr'        => $total_harga,
            'ppn'               => $ppn,
            'diskon'            => $diskon,
            'bayar'             => $bayar,
            'kembali'           => $kembali,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        ];
        $transaksi_id = Transaksi::insertGetId($data_transaksi);

        $get = ResepDetail::where('id_resep',$resep)->get();
        foreach ($get as $key => $value) {
            // $cek_batch   = Obat::where('nomor_batch',$value->nomor_batch)->->count();
            
            // if ($cek_batch > 0) {
                $harga_obat  = Obat::where('id_obat',$value->id_obat)->firstOrFail();
                $obat_inv    = Obat::where('nomor_batch',$harga_obat->nomor_batch)->where('ket_data','inventory')->firstOrFail();
                $cek         = Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->count();
                
                if ($cek > 0) {
                    $pakai       = Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->firstOrFail();
                    // if ($pakai->stok_pakai != $pakai->stok_masuk) {
                        $stok_pakai   = $value->banyak_obat+$pakai->stok_pakai;
                        $banyak_bayar = $harga_obat->harga_obat * $stok_pakai;
                        
                        Pemasukan::where('id_obat',$obat_inv->id_obat)->where('jenis_masuk','titip')->where('status_beli',0)->update(['stok_pakai'=>$stok_pakai,'banyak_bayar'=>$banyak_bayar]);
                    // }
                }
            // }

            $array[] = [
                'tanggal_keluar' => $data_transaksi['tgl_byr'],
                'id_obat'        => $value->id_obat,
                'stok_keluar'    => $value->banyak_obat,
                'bentuk_stok'    => 'pcs',
                'keterangan'     => 'Transaksi Klinik',
                'id_users'       => Auth::id(),
                'jenis_keluar'   => 'transaksi'
            ];
        }
        Pengeluaran::insert($array);
    	return redirect('/operator/data-transaksi/cetak/'.$transaksi_id);
    }

    public function totalPenjualan() {
        $title = 'Laporan Total Penjualan | Operator';
        $page  = 'laporan-total-penjualan';
        $link  = 'laporan';
        return view('Operator.transaksi.data-transaksi.total-penjualan',compact('title','page','link'));
    }

    public function cetak($id) {
        $data = Transaksi::dataCetak($id);
        $obat = Transaksi::dataCetak($id,'obat');
        
        return view('Operator.transaksi.data-transaksi.cetak',compact('data','obat'));
    }

    public function export(Request $request) {
        $from     = $request->from;
        $to       = $request->to;
        $title    = 'Laporan-Transaksi Dari Tanggal '.human_date($from).' Sampai Tanggal '.human_date($to);
        $fileName = $title.'.xlsx';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->getActiveSheet()->setCellValue('A1','Apotek Klinik 88');
        $spreadsheet->getActiveSheet()->setCellValue('A3','Jln. KH. Harun Nafsi No. 02, Loa Janan Ilir, Samarinda Seberang');
        $spreadsheet->getActiveSheet()->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        $spreadsheet->getActiveSheet()->mergeCells('A1:N1');
        $spreadsheet->getActiveSheet()->mergeCells('A3:N3');
        $spreadsheet->getActiveSheet()->mergeCells('A5:N5');
        $spreadsheet->getActiveSheet()->getStyle('A1:A5')->applyFromArray([
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);
        $spreadsheet->getActiveSheet()->setCellValue('A7','No.');
        $spreadsheet->getActiveSheet()->setCellValue('B7','Tanggal Transaksi');
        $spreadsheet->getActiveSheet()->setCellValue('C7','Nama Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('D7','Kategori Pasien');
        $spreadsheet->getActiveSheet()->setCellValue('E7','Tindakan Lab');
        $spreadsheet->getActiveSheet()->setCellValue('F7','Nama Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('G7','Biaya Klinik');
        $spreadsheet->getActiveSheet()->setCellValue('H7','Biaya Jasa Dokter');
        $spreadsheet->getActiveSheet()->setCellValue('I7','Biaya Resep');
        $spreadsheet->getActiveSheet()->setCellValue('J7','Biaya Jasa Lab');
        $spreadsheet->getActiveSheet()->setCellValue('K7','Total Bayar');
        $spreadsheet->getActiveSheet()->setCellValue('L7','Bayar');
        $spreadsheet->getActiveSheet()->setCellValue('M7','Kembali');
        $spreadsheet->getActiveSheet()->setCellValue('N7','Input By');
        $get   = Transaksi::dataExport($from,$to,auth()->id());
        $no    = 8;
        $count = 1;
        foreach ($get as $key => $value) {
            $spreadsheet->getActiveSheet()->setCellValue('A'.$no,"$count");
            $spreadsheet->getActiveSheet()->setCellValue('B'.$no,human_date($value->tgl_byr));
            $spreadsheet->getActiveSheet()->setCellValue('C'.$no,$value->nama_pasien);
            $spreadsheet->getActiveSheet()->setCellValue('D'.$no,$value->nama_kategori);
            $spreadsheet->getActiveSheet()->setCellValue('E'.$no,$value->nama_tindakan);
            $spreadsheet->getActiveSheet()->setCellValue('F'.$no,$value->nama_dokter);
            $spreadsheet->getActiveSheet()->setCellValue('G'.$no,format_rupiah($value->biaya_klinik));
            $spreadsheet->getActiveSheet()->setCellValue('H'.$no,format_rupiah($value->harga_dokter));
            $spreadsheet->getActiveSheet()->setCellValue('I'.$no,format_rupiah($value->biaya_resep));
            $spreadsheet->getActiveSheet()->setCellValue('J'.$no,format_rupiah($value->biaya_jasa_lab));
            $spreadsheet->getActiveSheet()->setCellValue('K'.$no,format_rupiah($value->jumlah_byr));
            $spreadsheet->getActiveSheet()->setCellValue('L'.$no,format_rupiah($value->bayar));
            $spreadsheet->getActiveSheet()->setCellValue('M'.$no,format_rupiah($value->kembali));
            $spreadsheet->getActiveSheet()->setCellValue('N'.$no,$value->name);
            $count++;
            $no++;
        }
        $style = $no-1;
        $spreadsheet->getActiveSheet()->getStyle('A7:N'.$style)->applyFromArray([
            'borders'=>[
                'allborders'=>[
                    'style'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
                ]
            ],
            'alignment'=>[
                'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ]
        ]);

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'"');
        $writer->save('php://output');
        // Excel::create($title,function($excel)use($from,$to){
        //     $excel->sheet('Laporan',function($sheet)use($from,$to){
        //         $sheet->setCellValue('A1','Apotek Mustang Farma');
        //         $sheet->setCellValue('A3','Jln. Pulau Kalimantan No. 63');
        //         $sheet->setCellValue('A5','Laporan Dari '.human_date($from).' Sampai '.human_date($to));
        //         $sheet->mergeCells('A1:N1');
        //         $sheet->mergeCells('A3:N3');
        //         $sheet->mergeCells('A5:N5');
        //         $sheet->getStyle('A1:A5')->applyFromArray([
        //             'alignment'=>[
        //                 'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
        //             ]
        //         ]);
        //         $sheet->setCellValue('A7','No.');
        //         $sheet->setCellValue('B7','Tanggal Transaksi');
        //         $sheet->setCellValue('C7','Nama Pasien');
        //         $sheet->setCellValue('D7','Kategori Pasien');
        //         $sheet->setCellValue('E7','Tindakan Lab');
        //         $sheet->setCellValue('F7','Nama Dokter');
        //         $sheet->setCellValue('G7','Biaya Klinik');
        //         $sheet->setCellValue('H7','Biaya Jasa Dokter');
        //         $sheet->setCellValue('I7','Biaya Resep');
        //         $sheet->setCellValue('J7','Biaya Jasa Lab');
        //         $sheet->setCellValue('K7','Total Bayar');
        //         $sheet->setCellValue('L7','Bayar');
        //         $sheet->setCellValue('M7','Kembali');
        //         $sheet->setCellValue('N7','Input By');
        //         $get   = Transaksi::dataExport($from,$to);
        //         $no    = 8;
        //         $count = 1;
        //         foreach ($get as $key => $value) {
        //             $sheet->setCellValue('A'.$no,"$count");
        //             $sheet->setCellValue('B'.$no,human_date($value->tgl_byr));
        //             $sheet->setCellValue('C'.$no,$value->nama_pasien);
        //             $sheet->setCellValue('D'.$no,$value->nama_kategori);
        //             $sheet->setCellValue('E'.$no,$value->nama_tindakan);
        //             $sheet->setCellValue('F'.$no,$value->nama_dokter);
        //             $sheet->setCellValue('G'.$no,format_rupiah($value->biaya_klinik));
        //             $sheet->setCellValue('H'.$no,format_rupiah($value->harga_dokter));
        //             $sheet->setCellValue('I'.$no,format_rupiah($value->biaya_resep));
        //             $sheet->setCellValue('J'.$no,format_rupiah($value->biaya_jasa_lab));
        //             $sheet->setCellValue('K'.$no,format_rupiah($value->jumlah_byr));
        //             $sheet->setCellValue('L'.$no,format_rupiah($value->bayar));
        //             $sheet->setCellValue('M'.$no,format_rupiah($value->kembali));
        //             $sheet->setCellValue('N'.$no,$value->name);
        //             $count++;
        //             $no++;
        //         }
        //         $style = $no-1;
        //         $sheet->getStyle('A7:N'.$style)->applyFromArray([
        //             'borders'=>[
        //                 'allborders'=>[
        //                     'style'=>\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN
        //                 ]
        //             ],
        //             'alignment'=>[
        //                 'horizontal'=>\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
        //             ]
        //         ]);
        //     });
        // })->export('xlsx');
    }
}
