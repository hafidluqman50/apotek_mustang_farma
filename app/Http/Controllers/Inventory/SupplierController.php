<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SupplierModel as Supplier;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class SupplierController extends Controller
{
    public function index() {
        $title = 'Data Supplier | Inventory';
        $page  = 'laporan-supplier';
        $link  = 'supplier-obat';
        return view('Inventory.obat.supplier-obat.main',compact('title','page','link'));
    }

    public function tambah() {
        $title = 'Form Supplier | Inventory';
        $page  = 'data-supplier-obat';
        $link  = 'supplier-obat';
        return view('Inventory.obat.supplier-obat.form-supplier',compact('title','page','link'));
    }

    public function edit($id) {
        $title = 'Form Supplier | Inventory';
        $page  = 'data-supplier-obat';
        $link  = 'supplier-obat';
        $row   = Supplier::where('id_supplier',$id)->firstOrFail();
        return view('Inventory.obat.supplier-obat.form-supplier',compact('title','page','link','row'));
    }

    public function delete($id) {
        Supplier::where('id_supplier',$id)->update(['status_delete' => 1]);
        return redirect('/inventory/laporan-supplier')->with('message','Berhasil Hapus Supplier');
    }

    public function save(Request $request) {
        $nama_supplier   = $request->nama_supplier;
        $nomor_hp        = $request->nomor_hp;
        $alamat_supplier = $request->alamat_supplier;
        $id              = $request->id;

        $data_supplier = [
            'nama_supplier'   => $nama_supplier,
            'nomor_telepon'   => $nomor_hp,
            'alamat_supplier' => $alamat_supplier,
            'status_delete'   => 0
        ];

        if ($id == '') {
            Supplier::create($data_supplier);
            $message = 'Berhasil Input Supplier';
        }
        else {
            unset($data_supplier['status_delete']);
            Supplier::where('id_supplier',$id)->update($data_supplier);
            $message = 'Berhasil Update Supplier';
        }

        return redirect('/inventory/data-supplier-obat')->with('message',$message);
    }
}
