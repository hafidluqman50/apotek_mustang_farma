<?php

namespace App\Http\Controllers\Inventory;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ObatModel as Obat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\GolonganObatModel as GolonganObat;
use App\Models\HargaObatModel as HargaObat;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ObatController extends Controller
{
    public function orderObat() {
        $title = 'Order Obat | Inventory';
        $page  = 'laporan-order-obat';
        $link  = 'obat';
        return view('Inventory.obat.order-obat.main',compact('title','page','link'));
    }
}
