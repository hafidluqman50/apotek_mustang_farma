<?php

namespace App\Http\Controllers\Inventory;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StokOpnemModel as StokOpnem;
use App\Models\StokOpnemDetailModel as StokOpnemDetail;
use App\Models\ObatModel as Obat;

class StokOpnemController extends Controller
{
    public function index()
    {
        $title = 'Stok Opnem | Inventory';
        $page  = 'laporan-stok-opnem';
        $link  = 'stok-opnem';

        return view('Inventory.stok-opnem.main',compact('title','page','link'));
    }

    public function tambah()
    {
        $title       = 'Form Stok Opnem | Inventory';
        $page        = 'stok-opnem';
        $link        = 'stok-opnem';
        $paging_obat = new Obat;
        $count_obat  = Obat::count();

        return view('Inventory.stok-opnem.form-stok-opnem',compact('title','page','paging_obat','count_obat','link'));
    }

    public function delete($id) 
    {
        StokOpnem::where('id_stok_opnem',$id)->delete();
        return redirect('/inventory/stok-opnem')->with('message','Berhasil Hapus Stok Opnem');
    }

    public function save(Request $request) 
    {
        $tanggal_stok_opnem = reverse_date($request->tanggal_stok_opnem);
        // $keterangan         = $request->keterangan;
        // $btn_act            = $request->btn_act;
        $id                 = $request->id;
        // $uuid               = $request->uuid;
        // dd($tanggal_stok_opnem);
        // $obat          = $request->id_obat;
        // $stok_komputer = $request->stok_komputer;
        // $stok_fisik    = $request->stok_fisik;
        // $stok_selisih  = $request->stok_selisih;
        // $sub_nilai     = $request->sub_nilai;

        // $total_nilai = 0;

        // if ($btn_act == 'input-obat') {
        //     $data_stok_opnem = [
        //         'tanggal_stok_opnem' => $tanggal_stok_opnem,
        //         'keterangan'         => $keterangan,
        //         'status_input'       => 0,
        //         'created_at'         => date('Y-m-d H:i:s'),
        //         'updated_at'         => date('Y-m-d H:i:s')
        //     ];

        //     if ($id == '') {
        //         $id_stok_opnem = StokOpnem::insertGetId($data_stok_opnem);
        //         $id = $id_stok_opnem;
        //     }

        //     foreach ($obat as $key => $value) {
        //         if (isset($stok_fisik[$key])) {
        //             $data_detail_stok_opnem[] = [
        //                 'id_stok_opnem' => $id,
        //                 'id_obat'       => $obat[$key],
        //                 'stok_komputer' => $stok_komputer[$key],
        //                 'stok_fisik'    => $stok_fisik[$key],
        //                 'stok_selisih'  => $stok_selisih[$key],
        //                 'sub_nilai'     => $sub_nilai[$key]
        //             ];   
        //             // $total_nilai = $total_nilai+$sub_nilai[$key];
        //         }
        //     }

        //     StokOpnemDetail::insert($data_detail_stok_opnem);

        //     $get_detail_stok = StokOpnemDetail::join('obat','stok_opnem_detail.id_obat','=','obat.id_obat')
        //                                     ->where('id_stok_opnem',$id)->get();

        //     // $total_nilai = StokOpnemDetail::where('id_stok_opnem',$id)->sum('sub_nilai');

        //     session()->put($id,$uuid);

        //     $message = 'Berhasil Input Stok Opnem';
            
        //     return redirect('/inventory/stok-opnem/lanjut-input/'.$id)->with('message',$message);
        // }
        // else {
        //     $data_stok_opnem = [
        //         'tanggal_stok_opnem' => $tanggal_stok_opnem,
        //         'keterangan'         => $keterangan,
        //         'status_input'       => 1,
        //         'created_at'         => date('Y-m-d H:i:s'),
        //         'updated_at'         => date('Y-m-d H:i:s')
        //     ];

        //     if ($id == '') {
        //         $id_stok_opnem = StokOpnem::insertGetId($data_stok_opnem);
        //         $id = $id_stok_opnem;
        //     }
        //     else {
        //         StokOpnem::where('id_stok_opnem',$id)->update($data_stok_opnem);
        //     }

        //     foreach ($obat as $key => $value) {
        //         if (isset($stok_fisik[$key])) {
        //             $data_detail_stok_opnem[] = [
        //                 'id_stok_opnem' => $id,
        //                 'id_obat'       => $obat[$key],
        //                 'stok_komputer' => $stok_komputer[$key],
        //                 'stok_fisik'    => $stok_fisik[$key],
        //                 'stok_selisih'  => $stok_selisih[$key],
        //                 'sub_nilai'     => $sub_nilai[$key]
        //             ];
        //         }
        //     }

        //     StokOpnemDetail::insert($data_detail_stok_opnem);

        //     $get_detail_stok = StokOpnemDetail::join('obat','stok_opnem_detail.id_obat','=','obat.id_obat')
        //                                     ->where('id_stok_opnem',$id)->get();

        //     $total_nilai = StokOpnemDetail::where('id_stok_opnem',$id)->sum('sub_nilai');
            
        //     $message = 'Berhasil Input Stok Opnem';

        //     return view('Inventory.stok-opnem.stok-opnem-print',compact('tanggal_stok_opnem','total_nilai','get_detail_stok'));
        // }
        // StokOpnem::where('id_stok_opnem',$id)->update(['status_input' => 1]);
        $get_detail_stok = StokOpnemDetail::join('obat','stok_opnem_detail.id_obat','=','obat.id_obat')
                                        ->where('id_stok_opnem',$id)->get();

        $total_nilai = StokOpnemDetail::where('id_stok_opnem',$id)->sum('sub_nilai');
        
        // $message = 'Berhasil Input Stok Opnem';

        return view('Inventory.stok-opnem.stok-opnem-print',compact('tanggal_stok_opnem','total_nilai','get_detail_stok'));
    }

    public function inputSebagian(Request $request)
    {
        $obat               = $request->id_obat;
        $stok_fisik         = $request->stok_fisik;
        $stok_komputer      = $request->stok_komputer;
        $stok_selisih       = $request->stok_selisih;
        $sub_nilai          = $request->sub_nilai;
        $tanggal_stok_opnem = reverse_date($request->tanggal_stok_opnem);
        // dd($request->tanggal_stok_opnem);
        $keterangan         = $request->keterangan;
        $id_stok_opnem      = $request->id_stok_opnem;

        if ($id_stok_opnem == '' || $id_stok_opnem == null) {
            $data_stok_opnem = [
                'tanggal_stok_opnem' => $tanggal_stok_opnem,
                'keterangan'         => $keterangan,
                'status_input'       => 1,
                'created_at'         => date('Y-m-d H:i:s'),
                'updated_at'         => date('Y-m-d H:i:s')
            ];
            $id = StokOpnem::insertGetId($data_stok_opnem);
            foreach ($obat as $key => $value) {
                // if (isset($stok_fisik[$key]) || $stok_fisik[$key] != '' || $stok_fisik[$key] != null) {

                if (isset($stok_fisik[$key]) || $stok_fisik[$key] != '' || $stok_fisik[$key] != null) {
                    $stok_fisik__   = $stok_fisik[$key];
                    $stok_selisih__ = $stok_selisih[$key];
                    $sub_nilai__    = $sub_nilai[$key];
                }
                else {
                    $stok_fisik__   = 0;
                    $stok_selisih__ = $stok_komputer[$key] - $stok_fisik__;
                    $sub_nilai__    = 0;
                }
                // }

                $cek_obat = StokOpnemDetail::where('id_stok_opnem',$id)
                                            ->where('id_obat',$obat[$key])
                                            ->count();

                if ($cek_obat == 0) {
                    $data_stok_opnem_detail = [
                        'id_stok_opnem' => $id,
                        'id_obat'       => $obat[$key],
                        'stok_komputer' => $stok_komputer[$key],
                        'stok_fisik'    => $stok_fisik__,
                        'stok_selisih'  => $stok_selisih__,
                        'sub_nilai'     => $sub_nilai__
                    ];

                    StokOpnemDetail::create($data_stok_opnem_detail);
                }
            }

            // StokOpnemDetail::insert($data_stok_opnem_detail);

            $id_stok_opnem = $id;
        }
        else {
            foreach ($obat as $key => $value) {
                if (isset($stok_fisik[$key]) || $stok_fisik[$key] != '' || $stok_fisik[$key] != null) {
                    $stok_fisik__   = $stok_fisik[$key];
                    $stok_selisih__ = $stok_selisih[$key];
                    $sub_nilai__    = $sub_nilai[$key];
                }
                else {
                    $stok_fisik__   = 0;
                    $stok_selisih__ = $stok_komputer[$key] - $stok_fisik__;
                    $sub_nilai__    = 0;
                }
                    // $data_stok_opnem_detail = [
                    //     'id_stok_opnem' => $id,
                    //     'id_obat'       => $obat[$key],
                    //     'stok_komputer' => $stok_komputer[$key],
                    //     'stok_fisik'    => $stok_fisik__,
                    //     'stok_selisih'  => $stok_selisih__,
                    //     'sub_nilai'     => $sub_nilai__
                    // ];


                $cek_obat = StokOpnemDetail::where('id_stok_opnem',$id_stok_opnem)
                                            ->where('id_obat',$obat[$key])
                                            ->count();

                if ($cek_obat == 0) {
                    $data_stok_opnem_detail = [
                        'id_stok_opnem' => $id_stok_opnem,
                        'id_obat'       => $obat[$key],
                        'stok_komputer' => $stok_komputer[$key],
                        'stok_fisik'    => $stok_fisik__,
                        'stok_selisih'  => $stok_selisih__,
                        'sub_nilai'     => $sub_nilai__
                    ];

                    StokOpnemDetail::create($data_stok_opnem_detail);
                }
            }

            // StokOpnemDetail::insert($data_stok_opnem_detail);
        }

        return response()->json(['message' => 'Berhasil Input Stok Opnem','id_stok_opnem' => $id_stok_opnem]);
    }
}
