<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PasienModel as Pasien;
use App\Models\KategoriPasienModel as KategoriPasien;
use App\Models\SpesialisDokterModel as SpesialisDokter;
use App\Models\DokterModel as Dokter;
use App\Models\GolonganObatModel as GolonganObat;
use App\Models\ObatModel as Obat;
use App\Models\JenisObatModel as JenisObat;
use App\Models\SupplierModel as Supplier;
use App\Models\PemasukanModel as Pemasukan;
use App\Models\PengeluaranModel as Pengeluaran;
use App\Models\ResepModel as Resep;
use App\Models\ResepDetailModel as ResepDetail;
use App\Models\TransaksiModel as Transaksi;
use App\Models\TindakanLabModel as TindakanLab;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\StokOpnemModel as StokOpnem;
use App\Models\StokOpnemDetailModel as StokOpnemDetail;
use App\User;
use Yajra\Datatables\Datatables;
use Auth;
use App\Http\Controllers\ApiController;

class AjaxController extends Controller
{
	private $level;

    protected $apiController;

	function __construct(ApiController $apiController)
	{
		$this->middleware(function($request,$next){
			$this->level = Auth::user()->level_user == 5 ? 'admin' : (Auth::user()->level_user == 4 ? 'inventory' : (Auth::user()->level_user == 3 ? 'dokter' : (Auth::user()->level_user == 2 ? 'operator' : (Auth::user()->level_user == 1 ? 'resep' : ''))));
			return $next($request);
		});

        $this->apiController = $apiController;
	}

	public function dataPasien() {
		$pasien = Pasien::getData();
		$datatables = Datatables::of($pasien)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-pasien/edit/$action->id_pasien").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-pasien/delete/$action->id_pasien").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
            if ($this->level == 'admin') {
                $column .= '<a href="'.url("/$this->level/data-pasien/cetak-rekam-medis/$action->id_pasien").'">
                              <button class="btn btn-success"> Cetak Rekam Medis </button>
                           </a>';
            }
    		return $column;
		})->editColumn('tanggal_lahir',function($edit){
			return human_date($edit->tanggal_lahir);
		})->editColumn('alamat_pasien',function($edit){
			return str_limit($edit->alamat_pasien,20);
		})->addColumn('umur_pasien',function($add){
			return hitung_umur($add->tanggal_lahir);
		})->make(true);
		return $datatables;
	}

	public function dataKategoriPasien() {
		$kategori_pasien = KategoriPasien::where('status_delete',0)->get();
		$datatables = Datatables::of($kategori_pasien)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-kategori-pasien/edit/$action->id_kategori_pasien").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-kategori-pasien/delete/$action->id_kategori_pasien").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->make(true);
		return $datatables;
	}

    public function dataSpesialisDokter() {
        $dokter = SpesialisDokter::where('status_delete',0)->get();
        $datatables = Datatables::of($dokter)->addColumn('action',function($action){
            $column = '<a href="'.url("/$this->level/data-spesialis-dokter/edit/$action->id_spesialis_dokter").'">
                          <button class="btn btn-warning"> Edit </button>
                       </a>
                       <a href="'.url("/$this->level/data-spesialis-dokter/delete/$action->id_spesialis_dokter").'">
                           <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
                       </a>
                    ';
            return $column;
        })->make(true);
        return $datatables;
    }

	public function dataDokter() {
		$dokter = Dokter::showData();
		$datatables = Datatables::of($dokter)->addColumn('action',function($action){
			$status = [
						0=>['class'=>'btn-success','text'=>'Aktifkan Akun'],
						1=>['class'=>'btn-danger','text'=>'Non Aktifkan Akun'],
					  ];
    		$column = '<a href="'.url("/$this->level/data-dokter/status-akun/$action->id_dokter").'">
    					  <button class="btn '.$status[$action->active]['class'].'"> '.$status[$action->active]['text'].' </button>
					   </a>
    				   <a href="'.url("/$this->level/data-dokter/edit/$action->id_dokter").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-dokter/delete/$action->id_dokter").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->addColumn('status_akun',function($add){
			$status = [
						0=>['class'=>'label-danger','text'=>'Non Aktif'],
						1=>['class'=>'label-success','text'=>'Aktif'],
					  ];
			return '<span class="label '.$status[$add->active]['class'].'">'.$status[$add->active]['text'].'</span>';
		})->editColumn('status_dokter',function($edit){
			$status = [
						0=>['class'=>'label-danger','text'=>'Non Aktif'],
						1=>['class'=>'label-success','text'=>'Aktif'],
					  ];
			return '<span class="label '.$status[$edit->status_dokter]['class'].'">'.$status[$edit->status_dokter]['text'].'</span>';
		})->editColumn('biaya_dokter',function($edit){
			return format_rupiah($edit->biaya_dokter);
		})->rawColumns(['action','status_dokter','status_akun'])->make(true);
		return $datatables;
	}

	public function dataPendaftaran($id) {
		$pendaftaran = Pendaftaran::showData($id);
		$datatables = Datatables::of($pendaftaran)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/pendaftaran/edit/$action->id_daftar").'">
                           <button class="btn btn-warning"> Edit </button>
                       </a>
                        <a href="'.url("/$this->level/pendaftaran/delete/$action->id_daftar").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('tgl_daftar',function($edit){
			return human_date($edit->tgl_daftar);
		})->addColumn('umur_pasien',function($add){
			return hitung_umur($add->tanggal_lahir);
		})->editColumn('ket_daftar',function($edit){
			$status = [
						'belum-masuk'=>['class'=>'label-warning','text'=>'Belum Proses Resep'],
						'masuk-resep'=>['class'=>'label-success','text'=>'Sudah Proses Resep'],
					  ];
			return '<span class="label '.$status[$edit->ket_daftar]['class'].'">'.$status[$edit->ket_daftar]['text'].'</span>';
		})->editColumn('tinggi_badan',function($edit){
			return replace_dot_to_comma($edit->tinggi_badan).' cm';
		})->editColumn('berat_badan',function($edit){
			return replace_dot_to_comma($edit->berat_badan).' kg';
		})->editColumn('tekanan_darah',function($edit){
			return $edit->tekanan_darah.' mmHg';
		})->editColumn('suhu_badan',function($edit){
			return replace_dot_to_comma($edit->suhu_badan).' &deg;C';
		})->editColumn('hasil_cek',function($edit){
			$hasil = json_decode($edit->hasil_cek_darah);
			$kolesterol = '-';
			$gula_darah = '-';
			$asam_urat  = '-';
			if (! empty($hasil)) {
				if (isset($hasil->kolesterol)) {
					$kolesterol = $hasil->kolesterol;
				}
				if (isset($hasil->gula_darah)) {
					$gula_darah = $hasil->gula_darah;
				}
				if (isset($hasil->asam_urat)) {
					$asam_urat = $hasil->asam_urat;
				}
				$response = '<ul>';
				$response .= '<li>Kolesterol: <strong>' . $kolesterol . 'mg/dL</strong></li>';
				$response .= '<li>Gula Darah: <strong>' . $gula_darah . 'mg/dL</strong></li>';
				$response .= '<li>Asam Urat: <strong>' . replace_dot_to_comma($asam_urat) . 'mg/dL</strong></li>';
				$response .= '</ul>';
			} else {
				if ($this->level == 'dokter') {
					$response = '<ul>';
					$response .= '<li>Kolesterol: <strong>' . $kolesterol . 'mg/dL</strong></li>';
					$response .= '<li>Gula Darah: <strong>' . $gula_darah . 'mg/dL</strong></li>';
					$response .= '<li>Asam Urat: <strong>' . $asam_urat . 'mg/dL</strong></li>';
					$response .= '</ul>';
				} else {
					$response = '<a href="'.url("/$this->level/pendaftaran/check/$edit->id_daftar").'">
									   	   <button class="btn btn-primary"> Ubah Hasil Cek </button>
									   </a>';	
				}
			}
			
			return $response;
		})->editColumn('link_rm',function($action){
    		$column = '<a href="'.url("/$this->level/laporan-rekam-medis/$action->id_dokter/rekam/$action->id_pasien").'">
					   	   <button class="btn btn-primary"> Rekam Medis </button>
					   </a>';
    		return $column;
		})->rawColumns(['nama_kategori', 'action', 'ket_daftar', 'suhu_badan', 'hasil_cek', 'link_rm'])->make(true);
		return $datatables;
	}

	public function dataGolonganObat() {
		$golongan_obat = GolonganObat::where('status_delete',0)->get();
		$datatables = Datatables::of($golongan_obat)->addColumn('action',function($action){
			$column = '<a href="'.url("/$this->level/data-golongan-obat/edit/$action->id_golongan_obat").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-golongan-obat/delete/$action->id_golongan_obat").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>';
			return $column;
		})->editColumn('foto_golongan',function($edit){
			return '<img src="'.asset("/assets/icon_golongan/$edit->foto_golongan").'">';
		})->rawColumns(['action','foto_golongan'])->make(true);
		return $datatables;
	}

	public function dataObat($ket_data) {
		// $ket = $this->level == 'inventory' ? 'inventory' : 'klinik';
		$obat = Obat::getData($ket_data);
		$datatables = Datatables::of($obat)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-obat/edit/$action->id_obat").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-obat/delete/$action->id_obat").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('stok_obat',function($edit){
			if ($edit->stok_obat == 0) {
				$label = '<span class="label label-danger"> 0 </span>';
			}
			else {
				$label = '<span class="label label-success"> '.$edit->stok_obat.' </span>';
			}
			return $label;
		})->addColumn('icon_golongan',function($edit){
			return '<img src="'.asset("/assets/icon_golongan/$edit->foto_golongan").'">';
		})->editColumn('tanggal_expired',function($edit){
			return human_date($edit->tanggal_expired);
		})->editColumn('harga_obat',function($edit){
			return format_rupiah($edit->harga_obat);
		})->editColumn('harga_jual',function($edit){
			return format_rupiah($edit->harga_jual);
		})->rawColumns(['action','icon_golongan','stok_obat'])->make(true);
		return $datatables;
	}

	public function dataOrderObat() {
		$obat = Obat::showOrderObat();
		$datatables = Datatables::of($obat)->editColumn('tanggal_expired',function($edit){
			return human_date($edit->tanggal_expired);
		})->editColumn('harga_obat',function($edit){
			return format_rupiah($edit->harga_obat);
		})->editColumn('ket_data',function($edit){
			return ucwords($edit->ket_data);
		})->make(true);
		return $datatables;
	}

	public function dataJenisObat() {
		$jenis_obat = JenisObat::where('status_delete',0)->get();
		$datatables = Datatables::of($jenis_obat)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-jenis-obat/edit/$action->id_jenis_obat").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-jenis-obat/delete/$action->id_jenis_obat").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->make(true);
		return $datatables;
	}

	public function dataSupplier() {
		$supplier = Supplier::whereNotIn('id_supplier',[1])->where('status_delete',0)->get();
		$datatables = Datatables::of($supplier)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-supplier-obat/edit/$action->id_supplier").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-supplier-obat/delete/$action->id_supplier").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->make(true);
		return $datatables;
	}

	public function dataPemasukan() {
		$pemasukan  = Pemasukan::getData();
		$datatables = Datatables::of($pemasukan)->addColumn('action',function($action){
			if ($this->level == 'admin') {
	    		$column = '<a href="'.url("/admin/data-masuk-obat/edit/$action->id_masuk").'">
						   	   <button class="btn btn-warning"> Edit </button>
						   </a>
						   <a href="'.url("/admin/data-masuk-obat/delete/$action->id_masuk").'">
						   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
						   </a>';
			}
			elseif($this->level == 'inventory') {
	    		$column = '<a href="'.url("/inventory/data-masuk-obat/delete/$action->id_masuk").'">
						   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
						   </a>';	
			}
    		return $column;
		})->editColumn('stok_masuk',function($edit){
			return $edit->stok_masuk.' '.ucwords($edit->bentuk_stok);
		})->editColumn('jenis_masuk',function($edit){
			if ($edit->jenis_masuk == 'supplier') {
				return '<span class="label label-info"> Beli Supplier </span>';
			}
			elseif($edit->jenis_masuk == 'titip') {
				return '<span class="label label-warning"> Titip Supplier </span>';
			}
			else {
				return '<span class="label label-danger"> - </span>';
			}
		})->editColumn('banyak_bayar',function($edit){
			return format_rupiah($edit->banyak_bayar);
		})->editColumn('diskon',function($edit){
            return $edit->diskon.' %';
        })->editColumn('ppn',function($edit){
            return $edit->ppn.' %';
        })->editColumn('tanggal_masuk',function($edit){
			return human_date($edit->tanggal_masuk);
		})->editColumn('tanggal_harus_bayar',function($edit){
			return human_date($edit->tanggal_harus_bayar);
		})->editColumn('tanggal_expired',function($edit){
			return human_date($edit->tanggal_expired);
		})->rawColumns(['jenis_masuk','action'])->make(true);
		return $datatables;
	}

	public function dataSupplierBeli() {
		$supplier_beli = Supplier::whereNotIn('id_supplier',[1])->get();
		$datatables = Datatables::of($supplier_beli)->addColumn('action',function($action){
			$attr = [0=>'',1=>'disabled="disabled"'];
    		$column = '<a href="'.url("/$this->level/laporan-pembelian/detail/$action->id_supplier").'">
    						<button class="btn btn-info"> Detail </button>
						</a>';
    		return $column;
		})->make(true);
		return $datatables;
	}

	// public function dataTitipObat() {
	// 	$titip_obat = Pemasukan::getTitip();
		
	// }

	public function dataPembelian($id,$jenis) {
		$pembelian  = Pemasukan::beliObat($id,$jenis);
		$datatables = Datatables::of($pembelian)->addColumn('action',function($action){
			$attr   = [0=>'',1=>'disabled="disabled"'];
			$column = '<a href="'.url("/$this->level/data-pembelian/supplier/$action->id_supplier/bayar/$action->id_masuk").'">
						   	   <button class="btn btn-info" onclick="return confirm(\'Yakin Obat Sudah Dibayar ?\');" '.$attr[$action->status_beli].'> Bayar </button>
						</a>';
    		return $column;
		})->editColumn('stok_pakai',function($edit){
			return $edit->stok_pakai.' Pcs ('.stokBox($edit->stok_pakai,$edit->bobot_satuan).')';
		})->editColumn('stok_masuk',function($edit){
			return $edit->stok_masuk.' '.$edit->bentuk_stok;
		})->editColumn('tanggal_masuk',function($edit){
			return human_date($edit->tanggal_masuk);
		})->editColumn('tanggal_harus_bayar',function($edit){
			return human_date($edit->tanggal_harus_bayar);
		})->editColumn('harga_bayar',function($edit){
			return format_rupiah($edit->harga_bayar);
		})->editColumn('status_beli',function($edit){
			if ($edit->status_beli == 0) {
				$label = '<span class="label label-danger"> Belum Bayar </span>';
			}
			else {
				$label = '<span class="label label-success"> Sudah Bayar </span>';
			}
			return $label;
		})->rawColumns(['action','status_beli'])->make(true);
		return $datatables;
	}

	public function dataPengeluaran() {
		$pengeluaran = Pengeluaran::getData();
		$datatables  = Datatables::of($pengeluaran)->addColumn('action',function($action){
			if ($this->level == 'admin') {
	    		$column = '<a href="'.url("/admin/data-keluar-obat/edit/$action->id_keluar").'">
						   	   <button class="btn btn-warning"> Edit </button>
						   </a>
						   <a href="'.url("/admin/data-keluar-obat/delete/$action->id_keluar").'">
						   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
						   </a>';
			}
			elseif($this->level == 'inventory') {
	    		$column = '<a href="'.url("/inventory/data-keluar-obat/delete/$action->id_keluar").'">
						   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
						   </a>';	
			}
    		return $column;
		})->editColumn('stok_keluar',function($edit){
			return $edit->stok_keluar.' '.ucwords($edit->bentuk_stok);
		})->editColumn('tanggal_keluar',function($edit){
			return human_date($edit->tanggal_keluar);
		})->make(true);
		return $datatables;
	}

    public function dataRujukan(Request $request) {
        $from = reverse_date($request->from);
        $to   = reverse_date($request->to);

        $resep = Resep::getDataRujukan($from,$to);

        $datatables = Datatables::of($resep)->editColumn('tgl_resep',function($edit){
            return human_date($edit->tgl_resep);
        })->editColumn('tinggi_badan',function($edit){
            return replace_dot_to_comma($edit->tinggi_badan).' cm';
        })->editColumn('berat_badan',function($edit){
            return replace_dot_to_comma($edit->berat_badan).' kg';
        })->editColumn('tekanan_darah',function($edit){
            return $edit->tekanan_darah.' mmHg';
        })->editColumn('suhu_badan',function($edit){
            return replace_dot_to_comma($edit->suhu_badan).' &deg;C';
        })->rawColumns(['suhu_badan'])->make(true);

        return $datatables;
    }

	public function dataResep() {
		$resep = Resep::getData();
		$datatables = Datatables::of($resep)->addColumn('action',function($action){
            if ($action->status_resep == 0) {
                $column = '<a href="'.url("/$this->level/laporan-resep/detail/$action->id_resep").'">
                              <button class="btn btn-info"> Detail </button>
                           </a>
                           <a href="'.url("/$this->level/data-resep/edit/$action->id_resep").'">
                              <button class="btn btn-warning"> Edit </button>
                           </a>
                           <a href="'.url("/$this->level/data-resep/input-obat/$action->id_resep").'">
                              <button class="btn btn-primary"> Input Obat </button>
                           </a>
                           <a href="'.url("/$this->level/data-resep/delete/$action->id_resep").'">
                               <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
                           </a>
                        ';
            }
            else {
                $column = '<a href="'.url("/$this->level/laporan-resep/detail/$action->id_resep").'">
                              <button class="btn btn-info"> Detail </button>
                           </a>
                           <a href="'.url("/$this->level/data-resep/edit/$action->id_resep").'">
                              <button class="btn btn-warning"> Edit </button>
                           </a>
                           <a href="'.url("/$this->level/data-resep/delete/$action->id_resep").'">
                               <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
                           </a>
                        ';
            }
    		return $column;
		})->editColumn('tgl_resep',function($edit){
			return human_date($edit->tgl_resep);
		})->editColumn('status_resep',function($edit){
            $array = [
                        0=>['class'=>'label label-danger','text'=>'Belum Input Obat'],
                        1=>['class'=>'label label-success','text'=>'Sudah Input Obat']
                     ];
            return '<span class="'.$array[$edit->status_resep]['class'].'">'.$array[$edit->status_resep]['text'].'</span>';
        })->rawColumns(['action','status_resep'])->make(true);
		return $datatables;
	}

	public function dataDetailResep($id) {
		$class  = new ResepDetail;
		$detail = $class->getData($id);
		$datatables = Datatables::of($detail)->addColumn('action',function($action)use($class){
    		$column = '<a href="'.url("/$this->level/laporan-resep/detail/$action->id_resep/delete/$action->id_detail_resep").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('sub_total',function($edit){
			return format_rupiah($edit->sub_total);
		})->make(true);
		return $datatables;
	}

	public function dataPasienRekamMedis($id) {
		$pasien_rekam_medis = Resep::pasienRekamMedis($id);
		$datatables         = Datatables::of($pasien_rekam_medis)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/laporan-rekam-medis/$action->id_dokter/rekam/$action->id_pasien").'">
    					  <button class="btn btn-info"> Detail </button>
					   </a>
    				';
    		return $column;
		})->addColumn('umur_pasien',function($add){
			return hitung_umur($add->tanggal_lahir);
		})->rawColumns(['action','terapi'])->make(true);
		return $datatables;
	}

	public function dataRekamMedis($id,$id_pasien) {
		$rekam_medis = Resep::rekamMedisPasienPerDokter($id,$id_pasien);
		$datatables  = Datatables::of($rekam_medis)->addColumn('action',function($action){
			$column = '<a href="'.url("/$this->level/laporan-rekam-medis/$action->id_dokter/rekam/$action->id_pasien/detail/$action->id_resep").'">
    					  <button class="btn btn-info"> Detail </button>
					   </a>
                       <a href="'.url("/$this->level/laporan-rekam-medis/$action->id_dokter/rekam/$action->id_pasien/edit/$action->id_resep").'">
                          <button class="btn btn-warning"> Edit </button>
                       </a>
    				';
    		return $column;
		})->editColumn('tgl_resep',function($edit){
			return human_date($edit->tgl_resep);
		})->editColumn('tgl_kontrol_selanjutnya',function($edit){
            return human_date($edit->tgl_kontrol_selanjutnya);
        })->editColumn('tinggi_badan_resep',function($edit){
			return replace_dot_to_comma($edit->tinggi_badan_resep == 0 ? $edit->tinggi_badan : $edit->tinggi_badan_resep).' cm';
		})->editColumn('berat_badan_resep',function($edit){
			return replace_dot_to_comma($edit->berat_badan_resep == 0 ? $edit->berat_badan : $edit->berat_badan_resep).' kg';
		})->editColumn('tekanan_darah_resep',function($edit){
			return $edit->tekanan_darah_resep == '' ? $edit->tekanan_darah.' mmHg' : $edit->tekanan_darah_resep.' mmHg';
		})->editColumn('suhu_badan_resep',function($edit){
			return replace_dot_to_comma($edit->suhu_badan_resep == 0 ? $edit->suhu_badan : $edit->suhu_badan_resep).' &deg;C';
		})->editColumn('hasil_cek_darah_resep',function($edit){
            $hasil = $edit->hasil_cek_darah_resep == '' ? json_decode($edit->hasil_cek_darah) : json_decode($edit->hasil_cek_darah_resep);
            $kolesterol = '-';
            $gula_darah = '-';
            $asam_urat  = '-';
            if (! empty($hasil)) {
                if (isset($hasil->kolesterol)) {
                    $kolesterol = $hasil->kolesterol;
                }
                if (isset($hasil->gula_darah)) {
                    $gula_darah = $hasil->gula_darah;
                }
                if (isset($hasil->asam_urat)) {
                    $asam_urat = $hasil->asam_urat;
                }
                $response = '<ul>';
                $response .= '<li>Kolesterol: <strong>' . $kolesterol . 'mg/dL</strong></li>';
                $response .= '<li>Gula Darah: <strong>' . $gula_darah . 'mg/dL</strong></li>';
                $response .= '<li>Asam Urat: <strong>' . replace_dot_to_comma($asam_urat) . 'mg/dL</strong></li>';
                $response .= '</ul>';
            } else {
                if ($this->level == 'dokter') {
                    $response = '<ul>';
                    $response .= '<li>Kolesterol: <strong>' . $kolesterol . 'mg/dL</strong></li>';
                    $response .= '<li>Gula Darah: <strong>' . $gula_darah . 'mg/dL</strong></li>';
                    $response .= '<li>Asam Urat: <strong>' . $asam_urat . 'mg/dL</strong></li>';
                    $response .= '</ul>';
                } else {
                    $response = '<a href="'.url("/$this->level/pendaftaran/check/$edit->id_daftar").'">
                                        <button class="btn btn-primary"> Ubah Hasil Cek </button>
                                 </a>';   
                }
            }
            
            return $response;
        })->rawColumns(['action','suhu_badan_resep','terapi','hasil_cek_darah_resep','pemeriksaan_fisik'])->make(true);
		return $datatables;
	}

    public function dataRekamMedisAdmin() {
        $rekam_medis = Resep::rekamMedisPasienAdmin();
        $datatables  = Datatables::of($rekam_medis)->addColumn('action',function($action){
            $column = '<a href="'.url("/$this->level/dashboard/rekam-medis-detail/$action->id_resep").'">
                          <button class="btn btn-info"> Detail </button>
                       </a>
                    ';
            return $column;
        })->editColumn('tgl_resep',function($edit){
            return human_date($edit->tgl_resep);
        })->editColumn('tgl_kontrol_selanjutnya',function($edit){
            return human_date($edit->tgl_kontrol_selanjutnya);
        })->editColumn('tinggi_badan',function($edit){
            return replace_dot_to_comma($edit->tinggi_badan).' cm';
        })->editColumn('berat_badan',function($edit){
            return replace_dot_to_comma($edit->berat_badan).' kg';
        })->editColumn('tekanan_darah',function($edit){
            return $edit->tekanan_darah.' mmHg';
        })->editColumn('suhu_badan',function($edit){
            return replace_dot_to_comma($edit->suhu_badan).' &deg;C';
        })->rawColumns(['action','suhu_badan','terapi'])->make(true);
        return $datatables;
    }

    public function dataRekamMedisAdminAll() {
        $rekam_medis = Resep::rekamMedisPasienAdminAll();
        $datatables  = Datatables::of($rekam_medis)->addColumn('action',function($action){
            $column = '<a href="'.url("/$this->level/dashboard/rekam-medis-detail/$action->id_resep").'">
                          <button class="btn btn-info"> Detail </button>
                       </a>
                    ';
            return $column;
        })->editColumn('tgl_resep',function($edit){
            return human_date($edit->tgl_resep);
        })->editColumn('tgl_kontrol_selanjutnya',function($edit){
            return human_date($edit->tgl_kontrol_selanjutnya);
        })->editColumn('tinggi_badan',function($edit){
            return replace_dot_to_comma($edit->tinggi_badan).' cm';
        })->editColumn('berat_badan',function($edit){
            return replace_dot_to_comma($edit->berat_badan).' kg';
        })->editColumn('tekanan_darah',function($edit){
            return $edit->tekanan_darah.' mmHg';
        })->editColumn('suhu_badan',function($edit){
            return replace_dot_to_comma($edit->suhu_badan).' &deg;C';
        })->rawColumns(['action','suhu_badan','terapi'])->make(true);
        return $datatables;
    }

	public function dataRekamMedisDetail($id,$id_pasien,$id_resep) {
		$detail     = ResepDetail::getData($id,$id_pasien,$id_resep);
		$datatables = Datatables::of($detail)->editColumn('sub_total',function($edit){
			return format_rupiah($edit->sub_total);
		})->make(true);
		return $datatables;
	}

	public function dataTransaksi(Request $request) {
		$from = $request->from;
		$to   = $request->to;
		if ($this->level == 'admin') {
			$transaksi  = Transaksi::getData($from,$to);
		}
		else {
			$transaksi  = Transaksi::getData($from,$to,auth()->id());
		}
		$datatables = Datatables::of($transaksi)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/data-transaksi/cetak/$action->id_transaksi").'">
    					  <button class="btn btn-success"> Cetak </button>
					   </a>
					   <a href="'.url("/$this->level/data-transaksi/delete/$action->id_transaksi").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('tgl_byr',function($edit){
			return human_date($edit->tgl_byr);
		})->editColumn('biaya_klinik',function($edit){
			return format_rupiah($edit->biaya_klinik);
		})->editColumn('biaya_dokter',function($edit){
			return format_rupiah($edit->biaya_dokter);
		})->editColumn('biaya_jasa_lab',function($edit){
			return format_rupiah($edit->biaya_jasa_lab);
		})->editColumn('biaya_resep',function($edit){
			return format_rupiah($edit->biaya_resep);
		})->editColumn('biaya_racik',function($edit){
            return format_rupiah($edit->biaya_racik);
        })->editColumn('biaya_tambahan',function($edit){
            return format_rupiah($edit->biaya_tambahan);
        })->editColumn('jumlah_byr',function($edit){
			return format_rupiah($edit->jumlah_byr);
		})->editColumn('bayar',function($edit){
			return format_rupiah($edit->bayar);
		})->editColumn('kembali',function($edit){
			return format_rupiah($edit->kembali);
		})->make(true);
		return $datatables;
	}

	public function dataTotalPenjualan() {
		$total_penjualan = Transaksi::totalPenjualan();
		$datatables = Datatables::of($total_penjualan)->editColumn('tgl_byr',function($edit){
			return human_date($edit->tgl_byr);
		})->addColumn('total_penjualan',function($add){
			$get = ResepDetail::getTotalByResep($add->id_resep);
			return format_rupiah($get);
		})->make(true);
		return $datatables;

	}

	public function dataTindakanLab() {
		$tindakan_lab = TindakanLab::with('spesialisDokter')->where('tindakan_lab.status_delete',0)->get();

		$datatables = Datatables::of($tindakan_lab)->addColumn('action',function($action){
    		$column = '<a href="'.url("/$this->level/tindakan-lab/edit/$action->id_tindakan_lab").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/tindakan-lab/delete/$action->id_tindakan_lab").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('nama_spesialis_dokter',function($edit){
            return $edit->spesialisDokter->nama_spesialis_dokter;
        })->editColumn('biaya_lab',function($edit){
			return format_rupiah($edit->biaya_lab);
		})->make(true);
		return $datatables;
	}

	public function dataUsers() {
		$users = User::whereNotIn('level_user',[5,3,1])->where('status_delete',0)->get();
		$datatables = Datatables::of($users)->addColumn('action',function($action){
			$array = [
						0=>['class'=>'btn-success','text'=>'Aktifkan'],
						1=>['class'=>'btn-danger','text'=>'Non Aktifkan']
					 ];
    		$column = '<a href="'.url("/$this->level/laporan-users/status-user/$action->id_users").'">
    					  <button class="btn '.$array[$action->active]['class'].'"> '.$array[$action->active]['text'].' </button>
					   </a>
					   <a href="'.url("/$this->level/data-users/edit/$action->id_users").'">
    					  <button class="btn btn-warning"> Edit </button>
					   </a>
					   <a href="'.url("/$this->level/data-users/delete/$action->id_users").'">
					   	   <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
					   </a>
    				';
    		return $column;
		})->editColumn('active',function($edit){
			$array = [
						0=>['class'=>'label label-danger','text'=>'Non Aktif'],
						1=>['class'=>'label label-success','text'=>'Aktif']
					 ];
			return '<span class="'.$array[$edit->active]['class'].'">'.$array[$edit->active]['text'].'</span>';
		})->editColumn('level_user',function($edit){
			// $level = $edit->level_user == 4 ? 'inventory' : ($edit->level_user == 1 ? 'resep' : ($edit->level_user== 0 ?'kasir':''));
            if ($edit->level_user == 4) {
                $level = 'Inventory';
            }
            else if ($edit->level_user == 2) {
                $level = 'Operator';
            }
            else if ($edit->level_user == 0) {
                $level = 'Kasir';
            }
			return $level;
		})->rawColumns(['active','action'])->make(true);
		return $datatables;
	}

	public function dataStokOpnem()
	{
		$transaksi = StokOpnem::all();

		$datatables = Datatables::of($transaksi)->addColumn('action',function($action){
			// if ($action->status_input == 0) {
	  //   		$column = '<a href="'.url("/$this->level/stok-opnem/lanjut-input/$action->id_stok_opnem").'">
	  //   					  <button class="btn btn-info"> Lanjut Input </button>
			// 			   </a>
	  //   					<a href="'.url("/$this->level/stok-opnem/cetak/$action->id_stok_opnem").'">
	  //   					  <button class="btn btn-info"> Cetak </button>
			// 			   </a>';
				// $column.='
				// 		   <a href="'.url("/$this->level/stok-opnem/detail/$action->id_stok_opnem").'">
	   //  					  <button class="btn btn-info"> Detail </button>
				// 		   </a>';
			// }
			// else {
	    		$column = '<a href="'.url("/$this->level/stok-opnem/cetak/$action->id_stok_opnem").'">
	    					  <button class="btn btn-success"> Cetak </button>
						   </a>
						   <a href="'.url("/$this->level/stok-opnem/detail/$action->id_stok_opnem").'">
	    					  <button class="btn btn-info"> Detail </button>
						   </a>
	    					<a href="'.url("/$this->level/stok-opnem/export/$action->id_stok_opnem").'">
	    					  <button class="btn btn-success"> Export <span class="fa fa-file-excel-o"></span> </button>
						   </a>';
			// }
			// if ($this->level == 'admin') {
				// $column .= '<a href="'.url("/admin/stok-opnem/delete/$action->id_stok_opnem").'">
	   //  					  <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
				// 		   </a>';
			// }
    		return $column;
		})->editColumn('tanggal_stok_opnem',function($edit){
			return human_date($edit->tanggal_stok_opnem);
		})->addColumn('total_nilai',function($add){
			$total = StokOpnemDetail::where('id_stok_opnem',$add->id_stok_opnem)->sum('sub_nilai');

			return format_rupiah($total);
		})->make(true);
		return $datatables;
	}

	public function dataStokOpnemDetail($id)
	{
		$stok_opnem_detail = StokOpnemDetail::join('obat','stok_opnem_detail.id_obat','=','obat.id_obat')
											->where('id_stok_opnem',$id)
											->get();

		$datatables = Datatables::of($stok_opnem_detail)->editColumn('tanggal_expired',function($edit){
			return human_date($edit->tanggal_expired);
		})->editColumn('harga_modal',function($edit){
			return format_rupiah($edit->harga_modal);
		})->editColumn('sub_nilai',function($edit){
			return format_rupiah($edit->sub_nilai);
		})->make(true);
		return $datatables;
	}

    public function dataPenyakit() {
        $data_penyakit = Resep::groupBy('diagnosa')->whereNotIn('diagnosa',['-'])->get();
        $datatables = Datatables::of($data_penyakit)->editColumn('diagnosa',function($edit){
            return ucwords($edit->diagnosa);
        })->addColumn('count_diagnosa',function($add){
            return Resep::countDiagnosa(strtolower($add->diagnosa));
        })->make(true);
        return $datatables;
    }

    public function dataPenyakitBpjs(Request $request)
    {
        $data_penyakit_bpjs = $this->apiController->diagnosa($request->search['value']);
        $datatables = Datatables::of($data_penyakit_bpjs['result'])->make(true);
        return $datatables;
    }

    public function dataPendaftaranBpjs() {
        $pendaftaran_bpjs = $this->apiController->listPendaftaran();
        $datatables = Datatables::of($pendaftaran_bpjs)->addColumn('action',function($action){
            $noKartu   = $action->peserta->noKartu;
            $tglDaftar = $action->tglDaftar;
            $noUrut    = $action->noUrut;
            $kdPoli    = $action->poli->kdPoli;

            $column = '<a href="'.url("/$this->level/pendaftaran-bpjs/delete/$noKartu/$tglDaftar/$noUrut/$kdPoli").'">
                           <button class="btn btn-danger" onclick="return confirm(\'Yakin Hapus ?\');"> Hapus </button>
                       </a>
                    ';
            return $column;
        })->editColumn('tglDaftar',function($edit){
            return human_date(reverse_date($edit->tglDaftar));
        })->editColumn('tinggiBadan',function($edit){
            return replace_dot_to_comma($edit->tinggiBadan).' cm';
        })->editColumn('beratBadan',function($edit){
            return replace_dot_to_comma($edit->beratBadan).' kg';
        })->addColumn('tekananDarah',function($add){
            return $add->sistole.'/'.$add->diastole.' mmHg';
        })->addColumn('poli',function($add){
            $kdPoli = $add->poli->kdPoli;
            $nmPoli = $add->poli->nmPoli;

            return $kdPoli.' | '.$nmPoli;
        })->addColumn('noKartu',function($add){
            return $add->peserta->noKartu;
        })->addColumn('namaPasien',function($add){
            return $add->peserta->nama;
        })->addColumn('jenisKelamin',function($add){
            if ($add->peserta->sex == 'L') {
                $jk = 'Laki - Laki';
            }
            else {
                $jk = 'Perempuan';
            }
            return $jk;
        })->editColumn('respRate',function($edit){
            return $edit->respRate.' Bpm';
        })->editColumn('heartRate',function($edit){
            return $edit->heartRate.' Bpm';
        })->make(true);
        return $datatables;
    }

    // public function dataPendaftaranBpjs()
    // {
    //     $data_penyakit_bpjs = $this->apiController->listPendaftaran();
    //     $datatables = Datatables::of($data_penyakit_bpjs)->make(true);
    //     return $datatables;
    // }

	// AJAX //

	public function getObat($id) {
		$obat = Obat::where('id_jenis_obat',$id)->where('ket_data','inventory')->get();
		$array[0] = '<option value="" selected="selected" disabled="disabled">=== Pilih Obat ===</option>';
		foreach ($obat as $key => $value) {
			// if ($param == 'inventory') {
			// 	$array[] = '<option value="'.$value->nomor_batch.'">'.$value->nomor_batch.' | '.$value->nama_obat.' | '.golongan_obat($value->golongan_obat).'</option>';	
			// }
			// else {
				$array[] = '<option value="'.$value->id_obat.'">'.$value->nomor_batch.' | '.$value->nama_obat.' | '.golongan_obat($value->golongan_obat).'</option>';
			// }
		}
		return $array;
	}

    public function getHargaObat($id)
    {
        $get_obat = Obat::where('id_obat',$id)->firstOrFail();

        return $get_obat->harga_jual;
    }

	public function getPasien($id) {
		$pasien = Pasien::where('id_kategori_pasien',$id)->get();
		$array[0] = '<option value="" selected="selected" disabled="disabled">=== Pilih Pasien ===</option>';
		foreach ($pasien as $key => $value) {
			$array[] = '<option value="'.$value->id_pasien.'">'.$value->kode_pasien.' | '.$value->nama_pasien.' | '.hitung_umur($value->tanggal_lahir).' | '.$value->jenis_kelamin;
		}
		return $array;
	}

	public function getInfoPasien($id) {
		$pasien = Pasien::where('id_pasien',$id)->firstOrFail();
		return $pasien;
	}

	public function getKategoriPasien($id) {
		$kategori_pasien = Pasien::join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')->where('id_pasien',$id)->get();
		$array = [];
		foreach ($kategori_pasien as $key => $value) {
			$array[] = '<option value="'.$value->id_kategori_pasien.'" selected="selected">'.$value->nama_kategori.'</option>';
		}
		return $array;
	}

	// public function getHarga($id) {
	// 	$harga = ResepDetail::where('id_resep',$id)->sum('sub_total');
	// 	return $harga;
	// }

	public function getDataResep($id) {
		$get = Pendaftaran::getById($id);
        $pasien = Pasien::ktgPasien($get->id_pasien);
		$html = '<input type="hidden" name="pasien" value="'.$get->id_pasien.'">
				<input type="hidden" name="dokter" value="'.$get->id_dokter.'">';

        $hasil = json_decode($get->hasil_cek_darah);
        $kolesterol = '-';
        $gula_darah = '-';
        $asam_urat  = '-';
        if (!empty($hasil)) {
            if (isset($hasil->kolesterol)) {
                $kolesterol = $hasil->kolesterol;
            }
            if (isset($hasil->gula_darah)) {
                $gula_darah = $hasil->gula_darah;
            }
            if (isset($hasil->asam_urat)) {
                $asam_urat = $hasil->asam_urat;
            }
        }

        $data_resep = ['keluhan' => $get->keluhan,'pemeriksaan_penunjang' => $get->pemeriksaan_penunjang, 'tinggi_badan' => $get->tinggi_badan, 'berat_badan'=>$get->berat_badan, 'tekanan_darah'=>$get->tekanan_darah, 'suhu_badan'=>$get->suhu_badan, 'kolesterol'=>$kolesterol, 'gula_darah' => $gula_darah, 'asam_urat' => $asam_urat, 'pasien' => $pasien, 'id_pasien' => $get->id_pasien, 'id_dokter' => $get->id_dokter, 'umur_pasien' => hitung_umur($pasien->tanggal_lahir), 'tanggal_lahir' => human_date($get->tanggal_lahir)];

		$data = ['data_resep'=>$data_resep,'html' => $html];
		return response()->json($data);
	}

	public function getObatName($kode_obat) {
		$obat = Obat::where('id_obat',$kode_obat)->firstOrFail();
		$array = [
			'nomor_batch' => $obat->nomor_batch,
			'nama_obat'   => $obat->nama_obat,
			'harga_obat'  => $obat->harga_jual
		];
		return response()->json($array,200);
	}

	public function getBiayaLab($id) {
		$get = TindakanLab::where('id_tindakan_lab',$id)->firstOrfail();
		return response()->json($get->biaya_lab,201);
	}

	public function getHarga($id) {
        $resep_row = Resep::leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                        ->where('id_resep',$id)
                        ->firstOrFail();

        $harga['nama_tindakan_lab'] = $resep_row->nama_tindakan ?? '-';
        $get_pasien_by_resep = Resep::join('pasien','resep.id_pasien','=','pasien.id_pasien')
                                    ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                                    ->where('id_resep',$id)
                                    ->firstOrFail();
                                    
        // if (strtolower($get_pasien_by_resep->nama_kategori) == 'bpjs') {
            // $harga['biaya_jasa_lab'] = 0;
            // $harga['total_obat']     = 0;

            // $harga['biaya_dokter']   = 0;
            // $harga['total_obat']     = 0;
            // $harga['biaya_racik']    = 0;
        // }
        // else {

            $harga['biaya_jasa_lab']    = $resep_row->biaya_lab ?? 0;
            // $harga['total_obat']        = (int)ResepDetail::where('id_resep',$id)->sum('sub_total');
            $harga['total_obat']  = 10000;
            $harga['biaya_racik'] = 15000;

            $harga['biaya_dokter'] = Resep::join('dokter','resep.id_dokter','=','dokter.id_dokter')
                        ->where('id_resep',$id)
                        ->firstOrFail()
                        ->biaya_dokter;
        // }
                    
		return response()->json($harga,200);
	}

    public function findDiagnosaBpjs(Request $request)
    {
        $search = $request->search;
        $get = $this->apiController->diagnosa($search);

        $data[] = [];
        foreach ($get as $key => $value) {
            $no = $key+1;
            $data[$key] = '<tr>
                                <td>'.$no.'</td>
                                <td>'.$value->kdDiag.' | '.$value->nmDiag.'</td>
                            </tr>';
        }

        return response()->json($data,200);
    }

	// END AJAX //
}
