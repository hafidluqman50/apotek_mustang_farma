<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\PendaftaranModel as Pendaftaran;
use App\Models\PasienModel as Pasien;
use App\Models\TokenApiModel as TokenApi;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    protected $headers;
    
    protected $base_url;

    protected $key_decrypt;

    public function __construct()
    {
        $user            = 'klinik88';
        $pass            = 'R@ndom123';
        $x_cons_id       = '5475';
        $user_key        = '003ab16c3b72b335a4e43abdbe394e4d';

        date_default_timezone_set('UTC');
        $x_timestamps    = strval(time()-strtotime('1970-01-01 00:00:00'));

        $data_signature  = "$x_cons_id&$x_timestamps";
        $secret_key      = '4qVC527FB2';
        $signature       = hash_hmac('sha256', $data_signature, $secret_key, true);
        $x_signature     = base64_encode($signature);

        $x_authorization = 'Basic '.base64_encode("$user:$pass:095");

        $this->key_decrypt = $x_cons_id.$secret_key.$x_timestamps;

        $this->headers = ['headers' => 
                            [
                                'Content-Type'    => 'application/json',
                                'X-cons-id'       => $x_cons_id, 
                                'X-Timestamp'     => $x_timestamps, 
                                'X-Signature'     => $x_signature, 
                                'X-Authorization' => $x_authorization,
                                'user_key'        => $user_key
                            ]
                        ];

        $this->base_url = 'https://apijkn-dev.bpjs-kesehatan.go.id/pcare-rest-dev';
    }

    public function diagnosa($request = '')
    {
        if ($request != '') {
            $url_diagnosa = $this->base_url.'/diagnosa/'.urlencode($request).'/0/30';
        }
        else {
            $url_diagnosa = $this->base_url.'/diagnosa/all/0/30';
        }

        // $this->headers['http_errors'] = false;

        $client     = new Client(['verify' => false]);
        $request    = $client->request('GET',$url_diagnosa,$this->headers);
        if ($request->getStatusCode() == 200) {
            $response   = $request->getBody()->getContents();

            $get_data               = json_decode($response)->response;
            $metaData               = json_decode($response)->metaData;
            $decompress             = decompress(stringDecrypt($this->key_decrypt,$get_data));
            $decode_json_decompress = json_decode($decompress);

            if ($metaData->code == 200) {
                $result = $decode_json_decompress->list;
            }
            else {
                $result = [];
            }
            $message = ['code' => 200, 'message' => 'Success'];
        }
        else {
            $response = $request->getBody()->getContents();
            $get_data = json_decode($response)->metaData;
            $message = ['code' => $request->getStatusCode(), 'message' => $get_data->message];
        }

        return ['message' => $message, 'result' => $result];
    }

    public function kodePoli()
    {
        $client     = new Client(['verify' => true]);
        $request    = $client->request('GET',$this->base_url.'/poli/fktp/0/20',$this->headers);
        $response   = $request->getBody()->getContents();

        $get_data               = json_decode($response)->response;
        $decompress             = decompress(stringDecrypt($this->key_decrypt,$get_data));
        $decode_json_decompress = json_decode($decompress);
        $result                 = $decode_json_decompress->list;

        return $result;
    }

    public function pendaftaran($data)
    {
        $explode_tekanan_darah = explode('/',$data['tekanan_darah']);
        $data_pendaftaran = [
            'kdProviderPeserta' => $data['kode_provider_peserta'],
            'tglDaftar'         => reverse_date($data['tgl_daftar']),
            'noKartu'           => $data['no_bpjs'],
            'kdPoli'            => $data['kode_poli_bpjs'],
            'keluhan'           => $data['keluhan'],
            'kunjSakit'         => $data['kunjungan_sakit'],
            'sistole'           => (int)$explode_tekanan_darah[0],
            'diastole'          => (int)$explode_tekanan_darah[1],
            'beratBadan'        => (int)$data['berat_badan'],
            'tinggiBadan'       => (int)$data['tinggi_badan'],
            'respRate'          => (int)$data['laju_respirasi'],
            'lingkarPerut'      => (int)$data['lingkar_perut'],
            'heartRate'         => (int)$data['denyut_jantung'],
            'rujukBalik'        => $data['rujuk_balik'],
            'kdTkp'             => "10"
        ];

        $this->headers['headers']['Content-Type'] = 'text/plain';
        $this->headers['http_errors']             = false;
        $this->headers['body']                    = json_encode($data_pendaftaran);

        try {
            $client   = new Client(['verify' => false]);
            $request  = $client->request('POST',$this->base_url.'/pendaftaran',$this->headers);
            if ($request->getStatusCode() == 412) {
                $response = $request->getBody()->getContents();
                $get_data = json_decode($response)->metaData;
                $result   = ['code' => 412, 'message' => $get_data->message, 'noUrut' => ''];
            }
            else if ($request->getStatusCode() == 201) {
                $response     = $request->getBody()->getContents();
                $get_response = json_decode($response)->response;
                $decompress   = decompress(stringDecrypt($this->key_decrypt,$get_response));
                $get_no_urut  = json_decode($decompress)->message;
                $get_data     = json_decode($response)->metaData;
                $result       = ['code' => 201, 'message' => $get_data->message, 'noUrut' => $get_no_urut];
            }
        } catch (Exception $e) {
            $result = $e->getMessage();
        }
        
        return $result;
    }

    public function listPendaftaran()
    {
        $this->headers['http_errors'] = false;

        $now        = date('d-m-Y');
        $client     = new Client(['verify' => true]);
        $request    = $client->request('GET',$this->base_url.'/pendaftaran/tglDaftar/'.$now.'/0/15',$this->headers);
        $getStatusCode = $request->getStatusCode();
        if ($getStatusCode == 200) {
            $response   = $request->getBody()->getContents();

            $get_data               = json_decode($response)->response;
            $decompress             = decompress(stringDecrypt($this->key_decrypt,$get_data));
            if ($decompress != null) {
                $decode_json_decompress = json_decode($decompress);
                $result                 = $decode_json_decompress->list;
            }
            else {
                // $result = json_decode(json_encode([],JSON_FORCE_OBJECT));
                $result = [];
            }
        }
        else {
            // $result = json_decode(json_encode([],JSON_FORCE_OBJECT));
            $result = [];
        }

        return $result;
    }

    public function deletePendaftaran($data)
    {
        $noKartu   = $data['noKartu'];
        $tglDaftar = $data['tglDaftar'];
        $noUrut    = $data['noUrut'];
        $kdPoli    = $data['kdPoli'];

        $client     = new Client(['verify' => true]);
        $request    = $client->request('DELETE',"$this->base_url/pendaftaran/peserta/$noKartu/tglDaftar/$tglDaftar/noUrut/$noUrut/kdPoli/$kdPoli",$this->headers);
        $response   = $request->getStatusCode();

        if ($response == 200) {
            $result = ['code' => 200, 'message' => 'Berhasil Delete Pendaftaran BPJS'];
        }
        else {
            $result = ['code' => $response, 'message' => 'Gagal'];   
        }

        return $result;
    }

    public function getPesertaKdProvider($no_kartu)
    {
        $this->headers['http_errors'] = false;

        $client     = new Client(['verify' => true]);
        $request    = $client->request('GET',$this->base_url.'/peserta/'.$no_kartu,$this->headers);
        if ($request->getStatusCode() == 200) {
            $response   = $request->getBody()->getContents();

            $get_data               = json_decode($response)->response;
            $decompress             = decompress(stringDecrypt($this->key_decrypt,$get_data));
            $decode_json_decompress = json_decode($decompress);
            $result                 = $decode_json_decompress;
            $code                   = 200;
            $message                = 'SUCCESS';
        }
        else {
            $result   = [];
            $code     = $request->getStatusCode();
            $response = $request->getBody()->getContents();
            $get_data = json_decode($response)->response;
            $message  = $get_data->message;
        }
        return ['code' => $code, 'message' => $message, 'result' => $result];
    }

    public function getPesertaByJenisKartu($jenis,$no_kartu)
    {
        $this->headers['http_errors'] = false;

        $client     = new Client(['verify' => true]);
        $request    = $client->request('GET',$this->base_url.'/peserta/'.$jenis.'/'.$no_kartu,$this->headers);
        if ($request->getStatusCode() == 200) {
            $response   = $request->getBody()->getContents();

            $get_data               = json_decode($response)->response;
            $decompress             = decompress(stringDecrypt($this->key_decrypt,$get_data));
            $decode_json_decompress = json_decode($decompress);
            $result                 = $decode_json_decompress;
            $code                   = 200;
            $message                = 'SUCCESS';
        }
        else {
            $result   = [];
            $code     = $request->getStatusCode();
            $response = $request->getBody()->getContents();
            $get_data = json_decode($response)->response;
            $message  = $get_data->message;
        }
        return ['code' => $code, 'message' => $message, 'result' => $result];
    }

    public function getToken()
    {
        $token = TokenApi::orderBy('valid_token','DESC')->first();

        if ($token) {
            if (check_token($token->token)) {
                $data_token = ['token' => $token->token, 'valid_token' => $token->valid_token];
                return response()->json(['status' => true, 'message' => 'Berhasil Mendapatkan Token!', 'data' => $data_token],200);
            }
            else {
                $create_token = [
                    'token'       => getRandomStringBin2hex(128),
                    'valid_token' => date('Y-m-d H:i:s')
                ];
                $created_token = TokenApi::create($create_token);

                return response()->json(['status' => true, 'message' => 'Berhasil Mendapatkan Token!', 'data' => $create_token],200);
            }
        }
        else {
            $create_token = [
                'token'       => getRandomStringBin2hex(128),
                'valid_token' => date('Y-m-d H:i:s')
            ];
            $created_token = TokenApi::create($create_token);
            
            return response()->json(['status' => true, 'message' => 'Berhasil Mendapatkan Token!', 'data' => $create_token],200);
        }
    }

    public function daftarPasien(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'no_bpjs'              => 'required|numeric',
            'nama_pasien'          => 'required|string',
            'nomor_telepon_pasien' => 'required|numeric',
            'alamat_pasien'        => 'required|string',
            'jenis_kelamin'        => 'required|string',
            'tanggal_lahir'        => 'required|date_format:Y-m-d'
        ]);

        if (!$validator->validated()) {
            return response()->json(['status' => false, 'message' => $validator->errors()],422);
        }

        $no_bpjs              = $request->no_bpjs;
        $nama_pasien          = $request->nama_pasien;
        $nomor_telepon_pasien = $request->nomor_telepon_pasien;
        $alamat_pasien        = $request->alamat_pasien;
        $jenis_kelamin        = $request->jenis_kelamin;
        $tanggal_lahir        = $request->tanggal_lahir;

        $data_pasien = [
            'no_bpjs'              => $no_bpjs,
            'nama_pasien'          => $nama_pasien,
            'id_kategori_pasien'   => 2,
            'nomor_telepon_pasien' => $nomor_telepon_pasien,
            'alamat_pasien'        => $alamat_pasien,
            'jenis_kelamin'        => $jenis_kelamin,
            'tanggal_lahir'        => $tanggal_lahir,
            'status_delete'        => 0
        ];

        try {
            $pasien = Pasien::create($data_pasien);

            unset($data_pasien['status_delete']);
            unset($data_pasien['id_kategori_pasien']);

            return response()->json(['status' => true, 'message' => 'Berhasil Daftar Pasien', 'data' => $data_pasien],200);   
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()],400);
        }
    }

    public function ambilAntrean(Request $request)
    {
        $no_bpjs               = $request->no_bpjs;
        $kode_provider_peserta = $this->getPesertaKdProvider($no_bpjs)['result']->kdProviderPst->kdProvider;
        $tanggal_daftar        = date('Y-m-d');
        $pasien_id             = Pasien::getIdByNoBpjs($no_bpjs);
        $dokter                = $request->dokter;
        $no_antrian            = Pendaftaran::noAntrian($dokter);
        $kode_poli             = $request->kode_poli;
        $keluhan               = $request->keluhan ?? '-';
        $tinggi_badan          = replace_comma_to_dot($request->tinggi_badan);
        $berat_badan           = replace_comma_to_dot($request->berat_badan);
        $tekanan_darah         = $request->tekanan_darah;
        $suhu_badan            = replace_comma_to_dot($request->suhu_badan);
        $laju_respirasi        = $request->laju_respirasi;
        $denyut_jantung        = $request->denyut_jantung;
        $lingkar_perut         = $request->lingkar_perut;
        $kunjungan_sakit       = (boolean) $request->kunjungan_sakit;
        $rujuk_balik           = (int) $request->rujuk_balik;
        $pemeriksaan_penunjang = $request->pemeriksaan_penunjang ?? '-';

        $data_daftar = [
            'kode_provider_peserta' => $kode_provider_peserta,
            'no_bpjs'               => $no_bpjs,
            'tgl_daftar'            => $tanggal_daftar,
            'id_pasien'             => $pasien_id,
            'id_dokter'             => $dokter,
            'no_antrian'            => $no_antrian,
            'ket_daftar'            => 'belum-masuk',
            'keluhan'               => $keluhan,
            'tinggi_badan'          => $tinggi_badan,
            'berat_badan'           => $berat_badan,
            'tekanan_darah'         => $tekanan_darah,
            'suhu_badan'            => $suhu_badan,
            'laju_respirasi'        => $laju_respirasi,
            'lingkar_perut'         => $lingkar_perut,
            'denyut_jantung'        => $denyut_jantung,
            'kunjungan_sakit'       => $kunjungan_sakit,
            'rujuk_balik'           => $rujuk_balik,
            'kode_poli_bpjs'        => $kode_poli,
            'pemeriksaan_penunjang' => $pemeriksaan_penunjang,
            'status_delete'         => 0
        ];

        // if (KategoriPasien::checkKategori($kategori_pasien_input) == 'bpjs') {
            $daftar_bpjs = $this->pendaftaran($data_daftar);
            if ($daftar_bpjs['code'] == 412) {
                $result = [
                    'status'  => 'false',
                    'message' => $daftar_bpjs['message'],
                    'data'    => []
                ];
                $code = 412;
            }
            elseif ($daftar_bpjs['code'] == 201) {
                unset($data_daftar['kode_provider_peserta']);
                unset($data_daftar['no_bpjs']);
                
                $pendaftaran_create = Pendaftaran::create($data_daftar);

                $message = [
                    'status'  => 'true',
                    'message' => 'Berhasil Ambil Nomor Antrian',
                    'data'    => ['no_antrian' => $no_antrian, 'tgl_daftar' => $tanggal_daftar, 'no_kartu' => $no_bpjs, 'kode_poli'=>$kode_poli, 'no_urut_bpjs' => $daftar_bpjs['noUrut']]
                ];
                $code = 200;
            }
            else {
                $message = [
                    'status'  => 'false',
                    'message' => 'Gagal!',
                    'data'    => ['no_antrian' => '', 'tgl_daftar' => '', 'no_kartu' => '', 'kode_poli' => '','no_urut_bpjs' => '']
                ];
                $code = 400;
            }
        // }

            return response()->json($message,$code);
        // return redirect('/admin/pendaftaran-bpjs')->with($message['type'],$message['message']);
    }

    public function statusAntrean(Request $request)
    {
        $no_kartu  = $request->no_kartu;
        $id_dokter = $request->id_dokter;

        $get = Pendaftaran::with(['patient'])
                    ->whereHas('patient',function($query)use($no_kartu){
                        $query->where('no_bpjs',$no_kartu);
                    })
                    ->where('tgl_daftar',date('2023-09-16'))
                    ->where('id_dokter',$id_dokter)
                    ->orderBy('created_at','DESC')
                    ->first();
                    
        if ($get) {
            if ($get->status_delete == 0 && $get->ket_daftar == 'belum-masuk') {
                $result = [
                    'status'  => 'true',
                    'message' => 'Berhasil Lihat Status Antrean',
                    'data'    => ['no_antrian' => $get->no_antrian, 'ket' => 'sedang-mengantri']
                ];
            }
            elseif ($get->status_delete == 0 && $get->ket_daftar == 'masuk-resep') {
                $result = [
                    'status'  => 'true',
                    'message' => 'Berhasil Lihat Status Antrean',
                    'data'    => ['no_antrian' => $get->no_antrian, 'ket' => 'sudah-diruang-dokter']
                ];
            }
            else {
                $result = [
                    'status'  => 'true',
                    'message' => 'Berhasil Lihat Status Antrean',
                    'data'    => ['no_antrian' => $get->no_antrian, 'ket' => 'batal-antri']
                ];
            }
        }
        else {
                $result = [
                    'status'  => 'true',
                    'message' => 'Berhasil Lihat Status Antrean',
                    'data'    => ['no_antrian' => '', 'ket' => 'belum-mendaftar']
                ];
        }

        return response()->json($result,200);
    }

    public function batalAntre(Request $request)
    {
        $no_kartu     = $request->no_kartu;
        $id_dokter    = $request->id_dokter;
        $tgl_daftar   = $request->tgl_daftar;
        $no_urut_bpjs = $request->no_urut_bpjs;
        $kode_poli    = $request->kode_poli;

        $data = [
            'noKartu'   => $no_kartu,
            'tglDaftar' => reverse_date($tgl_daftar),
            'noUrut'    => $no_urut_bpjs,
            'kdPoli'    => $kode_poli
        ];

        try {
            $delete = Pendaftaran::with(['patient'])
                        ->whereHas('patient',function($query)use($no_kartu){
                            $query->where('no_bpjs',$no_kartu);
                        })
                        ->where('tgl_daftar',date('Y-m-d'))
                        ->where('id_dokter',$id_dokter)
                        ->update(['status_delete' => 1]);

            if ($delete) {
                $deleteBpjs = $this->deletePendaftaran($data);
                
                if ($deleteBpjs['code'] != 200) {
                    
                    Pendaftaran::with(['patient'])
                        ->whereHas('patient',function($query)use($no_kartu){
                            $query->where('no_bpjs',$no_kartu);
                        })
                        ->where('tgl_daftar',date('Y-m-d'))
                        ->where('id_dokter',$id_dokter)
                        ->update(['status_delete' => 0]);

                    return response()->json(['status' => false, 'message'=>'Gagal Hapus'],$deleteBpjs['code']);
                }

                $get = Pendaftaran::with(['patient'])
                            ->whereHas('patient',function($query)use($no_kartu){
                                $query->where('no_bpjs',$no_kartu);
                            })
                            ->where('tgl_daftar',date('Y-m-d'))
                            ->where('id_dokter',$id_dokter)
                            ->first();

                $result = [
                    'status'  => 'true',
                    'message' => 'Berhasil Batalkan Antrean',
                    'data'    => ['no_antrian' => $get->no_antrian, 'ket' => 'batal-antri']
                ];

                return response()->json($result,200);
            }
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message'=>$e->getMessage()],500);
        }
    }
}
