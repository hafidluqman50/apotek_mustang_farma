<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class isLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->level_user == 5) {
                return redirect('/admin/dashboard');
            }
            elseif (Auth::user()->level_user == 4) {
                return redirect('/inventory/dashboard');
            }
            elseif (Auth::user()->level_user == 3) {
                return redirect('/dokter/dashboard');
            }
            elseif(Auth::user()->level_user == 2) {
                return redirect('/operator/dashboard');
            }
            elseif (Auth::user()->level_user == 1) {
                return redirect('/resep/dashboard');
            }
            // elseif (Auth::user()->level_user == 0) {
            //     return redirect('/kasir/dashboard');
            // }
        }
        return $next($request);
    }
}
