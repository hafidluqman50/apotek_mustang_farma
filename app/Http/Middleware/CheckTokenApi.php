<?php

namespace App\Http\Middleware;

use Closure;

class CheckTokenApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $get_authorization = $request->header('Authorization');
        if (str_contains($get_authorization,'Bearer')) {
            $get_token = get_token_bearer($get_authorization);

            if (!check_token($get_token)) {
                return response()->json(['status' => false, 'message' => 'Unauthorized'],400);
            }
        }
        else {
            return response()->json(['status' => false, 'message' => 'Unauthorized'],400);
        }
        return $next($request);
    }
}
