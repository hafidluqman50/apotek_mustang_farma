<?php
function get_profile_instansi() {
    // if ($param == 'logo') {
        $return = App\Models\ProfileInstansiModel::firstOrFail();
    // }
    // else if ($param == 'background') {
    //     $return = App\Models\ProfileInstansiModel::firstOrFail()->background_instansi;
    // }

    return $return;
}

function zero_front_number($num) {
    $num_padded = sprintf("%02d", $num);
    return $num_padded;
}

function get_profile_name($param) {
    $get = App\Models\ProfileInstansiModel::firstOrFail()->nama_instansi;
    if ($param == 'fullname') {
        $explode = explode(' ',$get);
        $return  = $explode;
    }
    else {
        $explode    = explode(' ',$get);
        $explode[0] = substr($explode[0],0,1);
        $return     = $explode;
    }

    return $return;
}

function format_rupiah($money) {
	$hasil_rupiah = 'Rp. ' . number_format($money,2,',','.');
	return $hasil_rupiah;
}

function replace_comma_to_dot($num) {
	$searchFor = ',';

	if (strpos($num, $searchFor) !== false) {
		$value = str_replace($searchFor,'.',$num);
	}
	else {
		$value = $num;
	}

	return $value;
}

function replace_dot_to_comma($num) {
	$searchFor = '.';

	if (strpos($num, $searchFor) !== false) {
		$value = str_replace($searchFor,',',$num);
	}
	else {
		$value = $num;
	}

	return $value;
}

function date_excel($date) {
	$new_date = date('d/m/Y',strtotime($date));

	return $new_date;
}

function reverse_date($date) {
	if ($date == '') {
		$val = '';
	}
	else {
		$explode = explode('-',$date);
		$val = $explode[2].'-'.$explode[1].'-'.$explode[0];	
	}

	return $val;
}

function format_tanggal($date) {
	$explode = explode('-',$date);
	return $explode[2].'/'.$explode[1].'/'.$explode[0];
}

function stokBox($stok,$bobot) {
	$sisa_stok = $stok%$bobot;
	$box = ceil($stok/$bobot);
	if ($sisa_stok == 0) {
		$hasil = $box.' Box';
	}
	else {
		$hasil = $box.' Box '.$sisa_stok.' Pcs';
	}
	
	return $hasil;
}

function golongan_obat($golongan) {
	if (strpos($golongan,'-') !== false) {
		$gol = ucwords(str_replace('-', ' ', $golongan));
	}
	else {
		$gol = ucwords($golongan);
	}
	return $gol;
}

function hitung_umur($date) {
    if ($date != '' && $date != '0000-00-00') {
    	$diff1 = new DateTime($date);
    	$diff2 = new DateTime(date('Y-m-d'));
    	$age = $diff1->diff($diff2);
    	if ($age->y == 0) {
    		$final = $age->m.' Bulan';
    	}
    	else {
    		$final = $age->y.' Tahun';
    	}
    }
    else {
        $final = '-';
    }

	return $final;
}

function generateCode($key,$separator,$num,$length) {
	$zero = '';
	for ($i=0; $i < 11; $i++) { 
		$zero .= 0;
	}
	$new_code = str_pad($num,$length,$zero,STR_PAD_LEFT);
	$generate = $key.$separator.$new_code;
	return $generate;
}

function makeAcronym($str) {
	$get = strtoupper($str);
	if (strlen($str) > 3) {
		$word[0] = substr($get,0,1);
		$word[1] = substr($get,2,1);
		$word[2] = substr($get,-1);
	}
	else {
		$word[0] = $get;
	}
	return implode('',$word);
}

function human_date($date) {
    if ($date != '' && $date != '0000-00-00') {
    	$explode = explode('-',$date);
    	$result = $explode[2].' '.month($explode[1]).' '.$explode[0];
    }
    else {
        $result = '-';
    }

    return $result;
}

function month($month) {
	$array = [
		'01' => 'Januari',
		'02' => 'Februari',
		'03' => 'Maret',
		'04' => 'April',
		'05' => 'Mei',
		'06' => 'Juni',
		'07' => 'Juli',
		'08' => 'Agustus',
		'09' => 'September',
		'10' => 'Oktober',
		'11' => 'November',
		'12' => 'Desember'
	];
	return $array[$month];
}

function jenis_masuk($str) {
	if ($str == 'titip') {
		$txt = 'Titip Supplier';
	}
	elseif ($str == 'supplier') {
		$txt = 'Beli Supplier';
	}
	else {
		$txt = '-';
	}
	return $txt;
}

function stringDecrypt($key, $string){
    $encrypt_method = 'AES-256-CBC';

    // hash
    $key_hash = hex2bin(hash('sha256', $key));

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);

    return $output;
}

// function lzstring decompress 
// download libraries lzstring : https://github.com/nullpunkt/lz-string-php
function decompress($string){
    return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
}

function getRandomStringBin2hex($length = 16)
{
    if (function_exists('random_bytes')) {
        $bytes = random_bytes($length / 2);
    } else {
        $bytes = openssl_random_pseudo_bytes($length / 2);
    }
    $randomString = bin2hex($bytes);
    return $randomString;
}

function explode_timestamp($desc,$timestamp) {
    $explode = explode(' ',$timestamp);
    if ($desc == 'date') {
        return $explode[0];
    }
    elseif ($desc == 'time') {
        return $explode[1];
    }
}

function get_token_bearer($token) {
    $explode = explode(' ',$token);

    return $explode[1];
}

function check_token($token) {
    date_default_timezone_set('Asia/Singapore');
    $token = \App\Models\TokenApiModel::where('token',$token)->first();
    if ($token) {
        $start = strtotime($token->valid_token);
        $end   = strtotime(date('Y-m-d H:i:s'));
        $minutes = floor(($end - $start) / 60);
        if ($minutes <= 1440) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}