<?php 
namespace App\Exports;

use App\Models\PemasukanModel as Pemasukan;
use Maatwebsite\Excel\Concerns\FromCollection;

class PemasukanExport implements FromCollection
{
    public $from;
    public $to;

    public function __construct($from,$to)
    {
        $this->from = $from;
        $this->to   = $to;
    }

    public function collection()
    {
        return Pemasukan::export($this->from,$this->to);
    }
}