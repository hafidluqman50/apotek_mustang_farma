<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpesialisDokterModel extends Model
{
    protected $table      = 'spesialis_dokter';
    protected $primaryKey = 'id_spesialis_dokter';
    protected $guarded    = [];
    public $timestamps    = false;
}
