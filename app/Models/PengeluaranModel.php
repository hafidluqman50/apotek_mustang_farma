<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengeluaranModel extends Model
{
	protected $table      = 'pengeluaran_obat';
	protected $primaryKey = 'id_keluar';
	protected $guarded    = [];

	public static function getData() {
		$db = self::join('users','pengeluaran_obat.id_users','=','users.id_users')
					->join('obat','pengeluaran_obat.id_obat','=','obat.id_obat')
					->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->get();
		return $db;
	}

	public static function getRow($id) {
		$db = self::join('obat','pengeluaran_obat.id_obat','=','obat.id_obat')
					->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->where('id_keluar',$id)
					->where('ket_data','klinik')
					->firstOrFail();
		return $db;
	}

    public static function export($from,$to) {
        $db = self::join('obat','pengeluaran_obat.id_obat','=','obat.id_obat')
                    ->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
                    ->join('users','pengeluaran_obat.id_users','=','users.id_users')
                    ->whereBetween('tanggal_keluar',[$from,$to])
                    ->orderBy('tanggal_keluar','desc')
                    ->get();
        return $db;
    }
}
