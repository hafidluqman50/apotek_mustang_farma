<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasienModel extends Model
{
	protected $table      = 'pasien';
	protected $primaryKey = 'id_pasien';
	protected $guarded    = [];
	public $timestamps    = false;

	public static function lastNumCode() {
		$db = self::query();
		if ($db->count() == 0) {
			$num = 1;
		}
		else {
			$explode = explode('-',$db->orderBy('kode_pasien','desc')->firstOrFail()->kode_pasien);
			$num = (int)$explode[1]+1;
		}
		return $num;
	}

	public static function getData() {
		$db = self::join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					->where('pasien.status_delete',0)
					->get();
					
		return $db;
	}

	public static function idPasien($nama,$tgl_lhr) {
		$db = self::where('nama_pasien',$name)->where('tanggal_lahir',$tgl_lhr)->firstOrFail()->id_pasien;
		return $db;
	}

	public static function ktgPasien($id_pasien) {
		$db = self::join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')->where('id_pasien',$id_pasien)->firstOrFail();
		return $db;
	}

	public function kategori()
	{
	    return $this->belongsTo(KategoriPasienModel::class, 'id_kategori_pasien', 'id_kategori_pasien');
	}

    public function getByKtg($id_kategori)
    {
        $db = self::where('id_kategori_pasien',$id)->get();
    }

    public static function getIdByNoBpjs($no_bpjs)
    {
        $get = self::where('no_bpjs',$no_bpjs)->firstOrFail()->id_pasien;

        return $get;
    }
}
