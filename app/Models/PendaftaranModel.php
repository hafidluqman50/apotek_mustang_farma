<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PendaftaranModel extends Model
{
    protected $table = 'pendaftaran';
    protected $primaryKey = 'id_daftar';
    protected $guarded = [];

    public static function showData($id = null) {
    	$db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
    				->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                    ->where('pendaftaran.status_delete',0)
                    ->where('dokter.id_dokter', $id)
                    ->where('tgl_daftar', date('Y-m-d'))
                    ->orderBy('created_at', 'asc')
                    ->get();
    	return $db;
    }

    public static function export($from,$to)
    {
        $db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                    ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                    ->where('pendaftaran.status_delete',0)
                    ->whereNotIn('keluhan',['-',''])
                    ->whereBetween('tgl_daftar',[$from,$to])
                    ->orderBy('created_at','DESC')
                    ->get();
        // dd($from,$to);

        return $db;
    }

    // public static function getByIdDokter($id)
    // {
        // $db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
        //             ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
        //             ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
        //             ->where('ket_daftar','belum-masuk')
        //             ->where('pendaftaran.id_dokter',$id_dokter)
        //             ->orderBy('id_daftar','desc')
        //             ->get();
    // }

    public static function getData($id_dokter = '') {
        if ($id_dokter != '') {
            $db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                        ->where('ket_daftar','belum-masuk')
                        ->where('tgl_daftar',date('Y-m-d'))
                        ->where('pendaftaran.id_dokter',$id_dokter)
                        ->orderBy('id_daftar','desc')
                        ->get();
        }
        else {
            $db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
                        ->where('ket_daftar','belum-masuk')
                        ->where('tgl_daftar',date('Y-m-d'))
                        ->orderBy('id_daftar','desc')
                        ->get();
        }
    	return $db;
    }

    public static function getById($id) {
    	$db = self::join('pasien','pendaftaran.id_pasien','=','pasien.id_pasien')
    				->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
    				->join('dokter','pendaftaran.id_dokter','=','dokter.id_dokter')
    				->where('id_daftar',$id)
    				->firstOrFail();
    	return $db;
    }

    public function patient() {
        return $this->belongsTo(PasienModel::class, 'id_pasien', 'id_pasien');
    }

    public static function noAntrian($id_dokter)
    {
        $db = self::where('tgl_daftar',date('Y-m-d'))
                   ->where('id_dokter',$id_dokter)
                   ->first();

        if ($db) {
            $result = (int)$db->no_antrian + 1;
        }
        else {
            $result = 1;
        }

        return $result;
    }
}
