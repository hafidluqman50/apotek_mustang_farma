<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DokterModel extends Model
{
	protected $table      = 'dokter';
	protected $primaryKey = 'id_dokter';
	protected $guarded    = [];
	public $timestamps    = false;

	public static function showData() {
		$db = self::join('users','dokter.id_users','=','users.id_users')
					->join('spesialis_dokter','dokter.id_spesialis_dokter','=','spesialis_dokter.id_spesialis_dokter')
                    ->where('dokter.status_delete',0)->get();
		return $db;
	}

	/**
	 * data spesialis
	 */
	public function spesialis(): BelongsTo
	{
	    return $this->belongsTo(SpesialisDokterModel::class, 'id_spesialis_dokter', 'id_spesialis_dokter');
	}
}
