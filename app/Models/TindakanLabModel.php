<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SpesialisDokterModel as SpesialisDokter;

class TindakanLabModel extends Model
{
	protected $table      = 'tindakan_lab';
	protected $primaryKey = 'id_tindakan_lab';
	protected $guarded    = [];
	public $timestamps    = false;

    public function spesialisDokter()
    {
        return $this->belongsTo(SpesialisDokter::class, 'id_spesialis_dokter','id_spesialis_dokter');
    }
}
