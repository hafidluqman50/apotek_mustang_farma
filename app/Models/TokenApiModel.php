<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenApiModel extends Model
{
    protected $table      = 'token_api';
    protected $primaryKey = 'id';
    protected $guarded    = [];
    public $timestamps    = false;
}
