<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HargaObatModel extends Model
{
	protected $table      = 'harga_obat';
	protected $primaryKey = 'id_harga_obat';
	protected $guarded    = [];
}
