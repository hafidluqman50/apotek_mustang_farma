<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransaksiModel extends Model
{
	protected $table      = 'transaksi';
	protected $primaryKey = 'id_transaksi';
	protected $guarded    = [];

	public static function getData($from = '',$to = '',$id = '') {
		$db = self::leftJoin('users','transaksi.id_users','=','users.id_users')
					->leftJoin('resep','transaksi.id_resep','=','resep.id_resep')
					->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
					->leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
					->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
					->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien');
		if ($id != '') {
			if ($from == '' && $to == '') {
				$result = $db->where('transaksi.id_users',$id)->get();
			}
			else {
				$result = $db->where('transaksi.id_users',$id)->whereBetween('tgl_byr',[$from,$to])->get();
			}
		}
		else {
			if ($from == '' && $to == '') {
				$result = $db->get();
			}
			else {
				$result = $db->whereBetween('tgl_byr',[$from,$to])->get();
			}
		}
		return $result;
	}

	public static function dataCetak($id,$cetak = '') {
		$db = self::join('resep','transaksi.id_resep','=','resep.id_resep')
					->join('dokter','resep.id_dokter','=','dokter.id_dokter')
					->join('pasien','resep.id_pasien','=','pasien.id_pasien')
					->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					->join('detail_resep','resep.id_resep','=','detail_resep.id_resep')
					->join('obat','detail_resep.id_obat','=','obat.id_obat')
					->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->where('id_transaksi',$id);
		if ($cetak == '') {
			$result = $db->firstOrFail();
		}
		else {
			$result = $db->get();
		}
		return $result;
	}

	public static function dataExport($from,$to,$id='') {
        if ($id == '') {
            $db = self::leftJoin('users','transaksi.id_users','=','users.id_users')
                        ->leftJoin('resep','transaksi.id_resep','=','resep.id_resep')
                        ->leftJoin('tindakan_lab','resep.id_tindakan_lab','tindakan_lab.id_tindakan_lab')
                        ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                        ->leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->whereBetween('tgl_byr',[$from,$to])
                        ->orderBy('tgl_byr','desc')
                        ->get();
        }
        else {
            $db = self::leftJoin('users','transaksi.id_users','=','users.id_users')
                        ->leftJoin('resep','transaksi.id_resep','=','resep.id_resep')
                        ->leftJoin('tindakan_lab','resep.id_tindakan_lab','tindakan_lab.id_tindakan_lab')
                        ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                        ->leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->where('id_users',$id)
                        ->whereBetween('tgl_byr',[$from,$to])
                        ->orderBy('tgl_byr','desc')
                        ->get();
        }
		return $db;
	}

	public static function totalPenjualan() {
        $db = self::join('users','transaksi.id_users','=','users.id_users')
                    ->groupBy('transaksi.id_resep')
                    ->get();
		return $db;
	}

    public static function totalPenjualanExport($ket,$from,$to) {
        if ($ket == 'Total Penjualan') {
            $db = self::join('users','transaksi.id_users','=','users.id_users')
                        ->whereBetween('tgl_byr',[$from,$to])
                        ->groupBy('id_resep')
                        ->get();
        }
        else if ($ket == 'Umum' || $ket == 'BPJS') {
            $db = self::join('users','transaksi.id_users','=','users.id_users')
                        ->join('resep','transaksi.id_resep','=','resep.id_resep')
                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->where('kategori_pasien.nama_kategori','like','%'.$ket.'%')
                        ->whereBetween('tgl_byr',[$from,$to])
                        ->groupBy('transaksi.id_resep')
                        ->get();
        }
        return $db;
    }

    public static function getSumPerBulan($bulan,$tahun,$id_tindakan_lab)
    {
        $db = self::join('resep','transaksi.id_resep','=','resep.id_resep')
                ->join('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')   
                ->whereMonth('tgl_byr',$bulan)
                ->whereYear('tgl_byr',$tahun)
                ->where('resep.id_tindakan_lab',$id_tindakan_lab)
                ->sum('biaya_jasa_lab');

        return $db;
    }

    public static function getObatSumPerBulan($bulan,$tahun,$id_obat)
    {
        $db = self::join('resep','transaksi.id_resep','=','resep.id_resep')
                ->join('detail_resep','resep.id_resep','=','detail_resep.id_resep')
                ->whereMonth('tgl_byr',$bulan)
                ->whereYear('tgl_byr',$tahun)
                ->where('detail_resep.id_obat',$id_obat)
                ->sum('sub_total');

        return $db;
    }

    public static function exportJasaDokter($from,$to)
    {
        $db = self::leftJoin('resep','transaksi.id_resep','=','resep.id_resep')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->whereBetween('tgl_byr',[$from,$to])
                    ->groupBy('resep.id_dokter')
                    ->get();

        return $db;
    }

    public static function sumBiayaJasaDokter($id_dokter,$from,$to)
    {
        $db = self::leftJoin('resep','transaksi.id_resep','=','resep.id_resep')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->whereBetween('tgl_byr',[$from,$to])
                    ->where('resep.id_dokter',$id_dokter)
                    ->sum('harga_dokter');

        return $db;
    }
}
