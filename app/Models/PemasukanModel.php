<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemasukanModel extends Model
{
    protected $table      = 'pemasukan_obat';
    protected $primaryKey = 'id_pemasukan';
    protected $guarded    = [];

    public static function getData() {
        $db = self::join('users','pemasukan_obat.id_users','=','users.id_users')
                    ->join('obat','pemasukan_obat.id_obat','=','obat.id_obat')
                    ->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
                    ->join('supplier_obat','pemasukan_obat.id_supplier','=','supplier_obat.id_supplier')
                    ->where('ket_data','inventory')
                    ->get();
        return $db;
    }

    public static function beliObat($id,$jenis) {
    	$db = self::join('users','pemasukan_obat.id_users','=','users.id_users')
    				->join('obat','pemasukan_obat.id_obat','=','obat.id_obat')
    				->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
    				->join('supplier_obat','pemasukan_obat.id_supplier','=','supplier_obat.id_supplier')
                    ->where('pemasukan_obat.id_supplier',$id)
                    ->where('jenis_masuk',$jenis)
                    ->get();
    	return $db;
    }

    public static function getRow($id) {
        $db = self::join('supplier_obat','pemasukan_obat.id_supplier','=','supplier_obat.id_supplier')
                    ->join('obat','pemasukan_obat.id_obat','=','obat.id_obat')
                    ->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
                    ->where('id_masuk',$id)
                    ->firstOrFail();
        return $db;
    }

    public static function export($from,$to) {
        $db = self::join('obat','pemasukan_obat.id_obat','=','obat.id_obat')
                    ->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
                    ->join('supplier_obat','pemasukan_obat.id_supplier','=','supplier_obat.id_supplier')
                    ->join('users','pemasukan_obat.id_users','=','users.id_users')
                    ->whereBetween('tanggal_masuk',[$from,$to])
                    ->orderBy('tanggal_masuk','desc')
                    ->get();
        return $db;
    }
}
