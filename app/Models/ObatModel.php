<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ObatModel extends Model
{
	protected $table      = 'obat';
	protected $primaryKey = 'id_obat';
	protected $guarded    = [];
	public $timestamps    = false;

    public static function obatPaging($offset,$data)
    {
        $get = self::offset($offset)->limit($data)->get();

        return $get;
    }

	public static function getData($ket) {
		$db = self::join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->join('golongan_obat','obat.id_golongan_obat','=','golongan_obat.id_golongan_obat')
					->where('ket_data',$ket)
					->where('obat.status_delete',0)
					->get();
		return $db;
	}

	public static function getRowJoinJenis($obat,$where) {
		$db = self::join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
								->where('id_obat',$obat)
								->where('ket_data',$where)
								->firstOrFail();
		return $db;
	}

	public static function lastNumCode() {
		$db = self::query();
		if ($db->count() == 0) {
			$num = 1;
		}
		else {
			$explode = explode('-',$db->orderBy('kode_obat','desc')->firstOrFail()->kode_obat);
			$num = (int)$explode[2]+1;
		}
		return $num;
	}

	public static function showOrderObat() {
		$db = self::join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->where('stok_obat','<',5)
					->get();
		return $db;
	}

	public function getObat($id,$ket) {
		$db = self::where('id_jenis_obat',$id)
					->where('ket_data',$ket)
					->get();
		return $db;
	}

	public static function getRowId($nama_obat,$nomor_batch) {
		$db = self::where('nomor_batch',$nomor_batch)
					->where('nama_obat',$nama_obat)
					->firstOrFail()
					->id_obat;
		return $db;
	}

	public static function getByJenis($id_jenis) {
		$db = self::where('id_jenis_obat',$id_jenis)->where('ket_data','inventory')->get();
		return $db;
	}
}
