<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriPasienModel extends Model
{
	protected $table      = 'kategori_pasien';
	protected $primaryKey = 'id_kategori_pasien';
	protected $guarded    = [];
	public $timestamps    = false;

    public static function checkKategori($id_kategori_pasien)
    {
        // dd($id_kategori_pasien);
        $get = self::where('id_kategori_pasien',$id_kategori_pasien)
                    ->firstOrFail()->nama_kategori;

        return strtolower($get);
    }
}
