<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResepModel extends Model
{
	protected $table      = 'resep';
	protected $primaryKey = 'id_resep';
	protected $guarded    = [];

	public static function getData($ket = '') {
		$db = self::leftJoin('users','resep.id_users','=','users.id_users')
					->leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
					->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab');
		if ($ket == '') {
				$result = $db->where('resep.status_delete',0)->get();
		}
		else {
			$result = $db->where('ket_resep',$ket)->where('resep.status_delete',0)->orderBy('id_resep','desc')->get();
		}
		return $result;
	}

    public static function getDataRujukan($from = '', $to = '')
    {
        if ($from != '' && $to != '') {
            $db = self::join('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->join('dokter','resep.id_dokter','=','dokter.id_dokter')
                        ->join('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                        ->whereBetween('tgl_resep',[$from,$to])
                        ->get();
        }
        else {
            $db = self::join('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->join('dokter','resep.id_dokter','=','dokter.id_dokter')
                        ->join('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                        ->get();
        }

        return $db;
    }

	public static function rekamMedis($id) {
		$db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
					->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
					->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
					->orderBy('tgl_resep','desc')
					// ->where('resep.id_dokter',$id)
					->get();
		return $db;
	}

	public static function rekamMedisPasienPerDokter($id_dokter, $id_pasien) {
		$db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
					->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
					->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
					->orderBy('tgl_resep','desc')
					// ->where('resep.id_dokter',$id_dokter)
                    // ->where('tgl_resep',date('Y-m-d'))
					->where('pasien.id_pasien',$id_pasien)
					->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan_resep','tinggi_badan','berat_badan_resep','berat_badan','tekanan_darah_resep','tekanan_darah','suhu_badan_resep','suhu_badan','hasil_cek_darah_resep','hasil_cek_darah','resep.pemeriksaan_penunjang','anemnesis','pemeriksaan_fisik','diagnosa','tindakan_lab.*','terapi','rujukan','tgl_kontrol_selanjutnya']);
		return $db;
	}

    public static function rekamMedisPasienAll() {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->orderBy('tgl_resep','desc')
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan','tgl_kontrol_selanjutnya']);
        return $db;
    }

    public static function rekamMedisPasienAdmin() {
        $h_min_1 = date('Y-m-d', strtotime('-1 day', strtotime(date('Y-m-d'))));
        $hari_h  = date('Y-m-d');
        
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->whereBetween('tgl_kontrol_selanjutnya',[$h_min_1,$hari_h])
                    ->orderBy('tgl_resep','desc')
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan','tgl_kontrol_selanjutnya']);
        return $db;
    }

    public static function rekamMedisPasienAdminAll() {
        $hari_h  = date('Y-m-d');
        
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->where('tgl_kontrol_selanjutnya', '>=', $hari_h)
                    ->orderBy('tgl_resep','desc')
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan','tgl_kontrol_selanjutnya']);
        return $db;
    }

    public static function getRekamMedisPasienByIdResep($id_resep) 
    {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->orderBy('tgl_resep','desc')
                    ->where('resep.id_resep',$id_resep)
                    ->get(['id_resep','tgl_daftar','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan','tgl_kontrol_selanjutnya'])[0];
        return $db;
    }

    public static function exportRekamMedis($from,$to) 
    {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->orderBy('tgl_resep','desc')
                    ->whereBetween('tgl_resep',[$from,$to])
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan']);
                    
        return $db;
    }

    public static function exportRekamMedisByIdPasien($id) 
    {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->orderBy('tgl_resep','desc')
                    ->where('resep.id_pasien',$id)
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan']);
                    
        return $db;
    }

    public static function exportRekamMedisByDokter($id_dokter,$from,$to) 
    {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->where('resep.id_dokter',$id_dokter)
                    ->whereBetween('tgl_resep',[$from,$to])
                    ->orderBy('tgl_resep','desc')
                    ->get(['id_resep','tgl_resep','pasien.*','dokter.*','keluhan_utama_resep','tinggi_badan','berat_badan','tekanan_darah','suhu_badan','hasil_cek_darah','resep.pemeriksaan_penunjang','pemeriksaan_fisik','anemnesis','diagnosa','tindakan_lab.*','terapi','rujukan','nama_kategori']);
                    
        return $db;
    }

	public static function getPasien($id) {
		$db = self::join('pasien','resep.id_pasien','=','pasien.id_pasien')->where('resep.id_resep',$id)->firstOrFail();
		return $db;	
	}

	public static function lastNumCode() {
		$db = self::query();
        // dd($db->orderBy('kode_resep','desc')->firstOrFail()->kode_resep);
		if ($db->count() == 0) {
			$num = 1;
		}
		else {
			$explode = explode('-',$db->orderBy('kode_resep','desc')->firstOrFail()->kode_resep);
			$num = (int)$explode[1]+1;
		}
		return $num;
	}

	public static function pasienRekamMedis($id) {
		$db = self::join('pasien','resep.id_pasien','=','pasien.id_pasien')
					->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
					// ->distinct()
					// ->where('id_dokter',$id)
                    ->groupBy('resep.id_pasien')
					->get(['resep.id_pasien','nama_pasien','tanggal_lahir','nama_kategori','jenis_kelamin','id_dokter']);
		return $db;
	}

    public static function export($from,$to)
    {
        $db = self::join('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->join('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->whereNotIn('rujukan',['-',''])
                    ->whereBetween('tgl_resep',[$from,$to])
                    ->get();

        return $db;
    }

    public static function exportPasien($id_kategori_pasien,$from,$to)
    {
        $db = self::leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->where('id_kategori_pasien',$id_kategori_pasien)
                    ->whereBetween('tgl_resep',[$from,$to])
                    ->get();

        return $db;
    }

    public static function countDiagnosa($diagnosa)
    {
        $db = self::whereRaw("LOWER(diagnosa) = '$diagnosa'")
                    ->count();

        return $db;
    }

    public static function getRow($id)
    {
        $db = self::leftJoin('pendaftaran','resep.id_daftar','=','pendaftaran.id_daftar')
                    ->leftJoin('pasien','resep.id_pasien','=','pasien.id_pasien')
                    ->leftJoin('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                    ->leftJoin('dokter','resep.id_dokter','=','dokter.id_dokter')
                    ->leftJoin('tindakan_lab','resep.id_tindakan_lab','=','tindakan_lab.id_tindakan_lab')
                    ->where('id_resep',$id)
                    ->firstOrFail();

        return $db;
    }
}
