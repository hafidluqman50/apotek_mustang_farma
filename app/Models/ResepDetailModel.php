<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TransaksiModel as Transaksi;

class ResepDetailModel extends Model
{
	protected $table      = 'detail_resep';
	protected $primaryKey = 'id_detail_resep';
	protected $guarded    = [];

	public static function getData($id) {
		$db = self::join('obat','detail_resep.id_obat','=','obat.id_obat')
					->join('jenis_obat','obat.id_jenis_obat','=','jenis_obat.id_jenis_obat')
					->where('id_resep',$id)
					->get();
		return $db;
	}

	public static function getPasien($id = '',$id_rekam) {
        if ($id != '') {
            $db = self::join('resep','detail_resep.id_resep','=','resep.id_resep')
                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->where('detail_resep.id_resep',$id_rekam)
                        ->where('resep.id_pasien',$id)
                        ->firstOrFail();
        }
        else {
            $db = self::join('resep','detail_resep.id_resep','=','resep.id_resep')
                        ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                        ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                        ->where('detail_resep.id_resep',$id_rekam)
                        ->firstOrFail();
        }
		return $db;
	}

	public function cekData($id) {
		$db = Transaksi::where('id_resep',$id)->count();
		if ($db > 0) {
			return true;
		}
		else {
			return false;
		}
	}

	public static function getTotalByResep($id) {
		$db = self::where('id_resep',$id)->sum('sub_total');
		return $db;
	}

    public static function checkPasien($id_resep)
    {
        $db = self::join('resep','detail_resep.id_resep','=','resep.id_resep')
                   ->join('pasien','resep.id_pasien','=','pasien.id_pasien')
                   ->join('kategori_pasien','pasien.id_kategori_pasien','=','kategori_pasien.id_kategori_pasien')
                   ->where('detail_resep.id_resep',$id_resep)
                   ->firstOrFail();

        return strtolower($db->nama_kategori);
    }
}
